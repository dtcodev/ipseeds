# Ipseeds

## Requirements

* Centos 7.2
* Erlang 19+
* Elixir 1.4+
* PostgreSQL 9.4+
* Node.js >=5.x (required for Phoenix)
* Git
* ImageMagick

## Vagrant for dev

Due to issues with [`listen`](https://github.com/guard/listen), fsnotify will poll for Vagrant versions `1.8.1 < v <= 1.8.4`. Please use Vagrant version 1.8.1 or >=1.8.5 (or install from source) if you wish to use fsnotify.

Initial install:

```
cp Vagrantfile.sample Vagrantfile
vagrant up
```

Subsequent runs:

```
vagrant box update && vagrant up
```

Optionally, using the `fsnotify` plugin to forward fs events:

```
vagrant plugin install vagrant-fsnotify
vagrant fsnotify
```

## Development

Make sure that your development db user `postgres` matches the settings in `config/config.exs` and has admin permissions for at least `ipseeds_dev` and `ipseeds_test`.

Edit dev_config.yml to input sensitive information used in dev env:

```
cp dev_config.yml.sample dev_config.yml
vi dev_config.yml
cp test_config.yml.sample test_config.yml
vi test_config.yml
```

Start the Phoenix server:

```
mix deps.get && npm install
mix ipseeds.create && mix ipseeds.migrate
mix phoenix.server
```

After the first time, please run the following commands to launch the server after `git pull`:

```
mix deps.get && npm install && mix ecto.migrate
mix phoenix.server
```

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

### Sending emails in dev

When using Amazon SES in dev environment, the verified domain is **ipseeds.net**, so make sure to use a **@ipseeds.net** email as FROM Address.

### Admin user account

Run the task `mix ipseeds.ensure_data` will seed up some build-in admin users into database if they are not exist. The accounts generate depends on the environment you're in, check the task file for more detail.

You can also use `Mix.Tasks.Ipseeds.EnsureData.add_admin(email, password, is_superadmin)` function in _iex_ to create admin user manually in :dev or :test environment.

### Generate Login's AuthCode

In _IEX_, run `Ipseeds.AuthHelpers.generate_auth_code(username, company, branch, password)` will return `{auth_code, utc_time, sign_hash}` to use in requests to `/account/login`:

```
iex(1)> Ipseeds.AuthHelpers.generate_auth_code("cogini_percy_test_1", "Cogini", "Taiwan", "CoginiPercyTest1")

{"1b7f22bfa1b861cc4038319696e9d090409ba443ce8ee91fc13d83d8cbaadeea", 1477037440,
 "777281294878c6d27bfc67d0b87f298163f7fb2755087e17462a4aa25b853e50"}
```

## Git push to deploy for staging and prod

 Add the `staging ` and `prod` remotes:

 ```
 git remote add staging ssh://deploy@staging.ipseeds.net:1022/opt/ipseeds/repo.git

 git remote add prod ssh://deploy@www.ipseeds.net:1022/opt/ipseeds/repo.git
 ```

 The `staging` and `prod` remotes will automatically deploy code on the branches of the same name. To deploy:

 ```
 # push staging branch to staging server to deploy
 git push staging staging

 # push prod branch to prod server to deploy
 git push prod prod
 ```

 It is *highly recommended* to use a Git GUI to track the state of all the remotes compared to your local.
 
## Provisioning AWS, app and monitoring servers

Please see `ansible/README.md`.
