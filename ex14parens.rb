#!/usr/bin/env ruby
###
# This utility adds missing parentheses to single word function calls
# that are now treated as warnings on Elixir 1.4.
#
# Download this file on your project repo and execute
#   ruby ex14parens.rb --help
####

require('fileutils')

replacements = []

WARNING_RE  = /^warning: variable "([^"]+)" does not exist and is being expanded to "([^"]+)", please use parentheses to remove the ambiguity or change the variable name/
LOCATION_RE = /^  ([^:]+):(\d+)/

dry     = "[dry] "
dry     = nil if ARGV.include?("-p") || ARGV.include?("--perform")
help    = ARGV.include?("-h") || ARGV.include?("--help")

app     = "ipseeds"

if File.exist?("mix.exs") && match = /app: :(.*?),/.match(File.read("mix.exs"))
  app = match[1]
end

CLEANUP = <<-EOF

After you have run with --perform, be sure to review the changes
and try to compile your app again, Elixir should not warn
about functions without parens.

git diff # review the changes to your code.
mix do clean, compile #{app} # recompile your code.

If everything is well, you can remove the .bak backup files.

find . -type f -name '*.bak' | xargs rm -v

EOF

if help
  puts <<-EOF
#{$0} - Add parens to function calls on Elixir 1.4 to avoid warnings.

USAGE:

  mix do clean, compile #{app} 2>&1 | ruby #{$0} [OPTIONS]


OPTIONS:

-h  --help      Show this help
-d  --dry       Dont actually perform any change (default)
-p  --perform   Perform changes (backup files with .bak extension)
                Be sure to have a clean working copy of your repo.
                Stash your current work and work on a new branch.

CLEANUP:

#{CLEANUP}
  EOF
  exit 0
end

if dry
  puts "=== Wont actually change any file. Run with --perform to do so."
  puts
else
  puts "=== Performing file replacements, backups with .bak extension"
  puts
end

while line = STDIN.readline rescue nil
  if match = WARNING_RE.match(line)
    from, to = match[1..2]
    line = STDIN.readline
    file, line = LOCATION_RE.match(line)[1..2]
    if File.exists?(file)
      replacements << [file, line, from, to]
    end
  end
end

grouped_by_file = replacements.group_by { |rep| rep[0] }

grouped_by_file.each do |file_group, file_replacements|
  lines = File.readlines(file_group) unless dry
  file_replacements.each do |replacement|
    _, line, from, to = replacement
    line = line.to_i
    idx = line - 1
    lines[idx] = lines[idx].gsub(/(?<![:\"\'])#{from}(?![:\"\'\(])/, to) unless dry
    puts "#{to}\n#{dry}\t#{file_group}:#{line}"
    puts
  end
  unless dry
    FileUtils.cp file_group, "#{file_group}.bak"
    File.open(file_group, 'w') { |f| f.puts(*lines) }
  end
end

unless dry
  puts
  puts "=== CLEANUP"
  puts CLEANUP
end
