# IPSeeds Provisioning

## Requirements

* Ansible `brew install ansible`
* awscli `pip install awscli`
* (If you an issue about `six`, try to use `sudo -H pip install awscli --upgrade --ignore-installed six`)
* AWS user with permissions, put the access keys in `~/.aws/credentials`

```[dtco]
aws_access_key_id = AKIAJEQKAZLA7XMKFYZA
aws_secret_access_key = ...
```

## Prepare Ansible

Make sure you have an `ansible.cfg` and correct `set_env.sh` before running any commands.

```bash
cp ansible.cfg.sample ansible.cfg
cp set_env.sh.sample set_env.sh && chmod +x set_env.sh
```

Set environment variables

```bash
export ENV=dev
source ./set_env.sh
```

## Disable requiretty to enable Ansible pipelining (for speed)

```bash
ansible-playbook disable-requiretty.yml --extra-vars "env=$ENV ansible_user=$ANSIBLE_USER ansible_port=$ANSIBLE_PORT"
```

## Provision AWS

Assumes that users ({{ env }}-ipseeds-app) have been manually created already in IAM (so that their credentials can be saved). Also assumes that cloudfront distributions will be created manually (since it's important that the URLs don't change).

* Attach correct policies to users (for app servers to access S3, etc.)
* Create S3 buckets
* Attach required S3 bucket policies

```bash
ansible-playbook provision-aws.yml --extra-vars "env=$ENV ansible_port=$ANSIBLE_PORT"
```

## Provision app servers

Use this for initial provisioning of the app server as well as when updates to packages are required. *Note that this playbook will result in port 22 being opened by default. Run the `close-firewall-port-22.yml` to close this port once you're sure 1022 is working as expected (see below).

```bash
ansible-playbook provision-app-servers.yml --extra-vars "env=$ENV ansible_user=$ANSIBLE_USER ansible_port=$ANSIBLE_PORT"
```

### Updating packages

* *Node.js*: Set the extra var `nodejs_package_state=latest` of the `ansible-playbook` command
* *nginx*: Set the extra var `nginx_package_state=latest` of the `ansible-playbook` command
* *Elixir*: Set the variable inside `provision-app-servers.yml`

## Update the app role only
 
```bash
ansible-playbook update-app.yml --extra-vars "env=$ENV ansible_user=$ANSIBLE_USER ansible_port=$ANSIBLE_PORT"
```

## Update the app configuration (and restart the app)

```bash
ansible-playbook update-app.yml --tags "install-app-config" --extra-vars "env=$ENV ansible_user=$ANSIBLE_USER ansible_port=$ANSIBLE_PORT"
```

## Install crontab for app servers

```bash
ansible-playbook install-crontab.yml --extra-vars "env=$ENV ansible_user=$ANSIBLE_USER ansible_port=$ANSIBLE_PORT"
```

## Close port 22 on app servers

For security purposes, it is best to run SSHD listening on port 22 and an alternate port, then close port 22 after the alternate port is confirmed to be working. By default, the provisioning script will leave port 22 open to reduce the risk of being locked out. After the alternate port is confirmed to work, use this playbook to close port 22 in the firewall.

```bash
ansible-playbook close-firewall-port-22.yml --extra-vars "env=$ENV ansible_user=$ANSIBLE_USER ansible_port=$ANSIBLE_PORT"
```

## Provision the monitoring server(s)

```bash
ansible-playbook provision-monitoring-server.yml --extra-vars "ansible_user=$ANSIBLE_USER ansible_port=$ANSIBLE_PORT"
```

## Update just the monitoring server config

```bash
ansible-playbook update-monitoring.yml --extra-vars "ansible_user=$ANSIBLE_USER ansible_port=$ANSIBLE_PORT"
ansible-playbook update-monitoring.yml --extra-vars "ansible_user=$ANSIBLE_USER ansible_port=$ANSIBLE_PORT" --tags "prometheus-config"
```