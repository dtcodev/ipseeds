defmodule Ipseeds.ValidationHelpers do
  import Ecto.Changeset

  alias Ipseeds.{User}

  require Ipseeds.Gettext

  def email_max_length, do: 100
  def email_format, do: ~S/^(?:[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-zA-Z0-9-]*[a-zA-Z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/

  def password_min_length, do: 8
  def password_max_length, do: 100
  def password_format, do: ~s/^[a-zA-Z0-9!@#_~\$%\^&\*-\+]{#{password_min_length()},}$/
  def password_format_message, do: Ipseeds.Gettext.gettext("must be at least 8 characters long and " <>
    "must contain at least 1 upper case letter, 1 lower case letter and 1 number, " <>
    "special characters allowed: ! @ # _ ~ $ % ^ & * - +")

  def username_min_length, do: 6
  def username_max_length, do: 100
  def username_format, do: ~s/^[a-zA-Z0-9_]{#{username_min_length()},}$/

  def company_branch_format, do: ~S/^[a-zA-Z0-9 ]+$/
  def company_branch_min_length, do: 3
  def company_branch_max_length, do: 100

  def validate_normal_text_field(changeset, field_atom) do
    changeset
    |> validate_format(field_atom, ~r/^(?![<>\\\/;]+).*$/i)
  end

  def validate_password(changeset, field_atom \\ :password) do
    changeset
    |> validate_length(field_atom, min: password_min_length(), max: password_max_length())
    |> validate_format(field_atom, ~r/[A-Z]+/, message: "must contains at least one uppercase character")
    |> validate_format(field_atom, ~r/[a-z]+/, message: "must contains at least one downcase character")
    |> validate_format(field_atom, ~r/[0-9]+/, message: "must contains at least one number")
    |> validate_format(field_atom, Regex.compile!(password_format()))
  end

  def validate_email(changeset, field_atom \\ :email) do
    changeset
    |> validate_length(field_atom, max: email_max_length())
    |> validate_format(field_atom, Regex.compile!(email_format()))
    |> unique_constraint(field_atom)
  end

  def validate_username(changeset, field_atom \\ :username) do
    changeset
    |> validate_length(field_atom, min: username_min_length(), max: username_max_length())
    |> validate_exclusion(field_atom, ~w(admin superadmin passphrase reset_password) ++
      [User.system_username])
    |> validate_format(field_atom, Regex.compile!(username_format()),
      message: "can only contain alphanumeric characters and underscores")
    |> unique_constraint(field_atom)
  end

  def validate_company_branch(changeset, field_atom \\ :organization) do
    changeset
    |> validate_length(field_atom, min: company_branch_min_length(), max: company_branch_max_length())
    |> validate_format(field_atom, Regex.compile!(company_branch_format()),
      message: "can contain only English characters (upper and lower case), numbers and spaces")
  end

  def validate_company_tax_id_and_name_zh_tw(changeset) do
    nationality = get_field(changeset, :company_nationality)
    case nationality do
      "TW" -> 
        changeset
        |> validate_required(:company_tax_id)
        |> validate_required(:company_name_zh_tw)
      _ -> 
        changeset
    end
  end

  def validate_institute_tax_id_and_name_zh_tw(changeset) do
    nationality = get_field(changeset, :institute_nationality)
    case nationality do
      "TW" -> 
        changeset
        |> validate_required(:institute_tax_id)
        |> validate_required(:institute_name_zh_tw)
      _ -> 
        changeset
    end
  end

  def validate_agreeable_contract(changeset) do
    nda_contract_type = get_field(changeset, :nda_contract_type)
    case nda_contract_type do
      "unilateral" -> 
        changeset
        |> validate_inclusion(:unilateral_input_yr_01, 1..99)
        |> validate_inclusion(:unilateral_input_yr_02, 1..99)
        |> validate_inclusion(:unilateral_input_days, 1..99)
      "mutual" -> 
        changeset
        |> validate_inclusion(:mutual_input_yr_01, 1..99)
        |> validate_inclusion(:mutual_input_yr_02, 1..99)
        |> validate_inclusion(:mutual_input_days, 1..99)  
      _ -> 
        changeset
    end
  end


end
