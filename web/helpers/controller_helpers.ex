defmodule Ipseeds.ControllerHelpers do
  def put_flash_success(conn, alert_message) do
    put_flash_alert(conn, alert_message, "success")
  end

  def put_flash_info(conn, alert_message) do
    put_flash_alert(conn, alert_message, "info")
  end

  def put_flash_warning(conn, alert_message) do
    put_flash_alert(conn, alert_message, "warning")
  end

  def put_flash_danger(conn, alert_message) do
    put_flash_alert(conn, alert_message, "danger")
  end

  def put_flash_alert(conn, alert_message, alert_type \\ "info") do
    conn
    |> Phoenix.Controller.put_flash(:alert_message, alert_message)
    |> Phoenix.Controller.put_flash(:alert_type, alert_type)
  end
end