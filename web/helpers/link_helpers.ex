defmodule Ipseeds.LinkHelpers do
  import Phoenix.HTML.Link, only: [link: 2]

  @default_class "active"

  @doc """
  Helper for returning a classname based on the controller of the currently active page.

  Example: set class to "current" if the current page is in MyApp.PageController.

    <li class="<%= active_class_controller @conn, MyApp.PageController, "current" %>">

  Will return the following if the current page is MyApp.PageController#about:

    <li class="active">

  Otherwise:

    <li class="">

  The function also accepts a list of controllers, e.g.:

    <li class="<%= active_class_controller @conn,
      [MyApp.PageController, MyApp.DashboardController], "current" %>">

  """
  def active_class_controller(conn, controllers) do
    active_class_controller(conn, controllers, @default_class)
  end
  def active_class_controller(conn, controllers, class) when is_list(controllers) do
    case Enum.member? controllers, Phoenix.Controller.controller_module(conn) do
      true -> class
      _ -> ""
    end
  end
  def active_class_controller(conn, controller, class) do
    active_class_controller(conn, [controller], class)
  end

  @doc """
  Helper for returning a classname based on the path of the currently active page. Matches are exact.

  Example: set class to "current" if the current page page path is page_path(@conn, :about, "en")
  "/en/about".

    <li class="<%= active_class_path @conn, page_path(@conn, :about, "en"), "current" %>">

  Will return the following if the current page is "/en/about":

    <li class="active">

  Otherwise:

    <li class="">

  The function also accepts a list of paths, e.g.:

    <li class="<%= active_class_controller @conn,
      [page_path(@conn, :about, "en"), page_path(@conn, :dashboard, "en")], "current" %>">

  """
  def active_class_path(conn, paths) do
    active_class_path(conn, paths, @default_class)
  end
  def active_class_path(conn, paths, class) when is_list(paths) do
    case Enum.member? paths, conn.request_path do
      true -> class
      _ -> ""
    end
  end
  def active_class_path(conn, path, class) do
    active_class_path(conn, [path], class)
  end

  def link_unless_current(conn, opts, do: contents) do
    link_unless_current(conn, contents, opts)
  end
  def link_unless_current(conn, text, opts) do
    {to, opts} = Keyword.pop(opts, :to)

    unless to do
      raise ArgumentError, "expected non-nil value for :to in link_to_unless_current/3, got: #{inspect to}"
    end

    {active_class, opts} = Keyword.pop(opts, :active_class, @default_class)
    {classes, opts} = Keyword.pop(opts, :class, "")

    {final_to, final_classes} = 
      if to == conn.request_path do
        {"#", String.trim(classes <> " " <> active_class)}
      else
        {to, classes}
      end

    final_opts =
      opts
      |> Keyword.put(:to, final_to)
      |> Keyword.put(:class, final_classes)

    link(text, final_opts)
  end
end
