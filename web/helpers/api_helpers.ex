defmodule Ipseeds.APIHelpers do
  import Ipseeds.Gettext, only: [gettext: 1]

  require Logger

  def changeset_api_error, do: "#{gettext("error (unknown API error).")} #{contact_support()}"
  def changeset_api_error(error_code) when is_integer(error_code),
    do: "#{gettext("error (API error code")} #{error_code}). #{contact_support()}"
  def changeset_api_error(_), do: changeset_api_error()

  def full_api_error, do: "#{gettext("API error (unknown API error).")} #{contact_support()}"
  def full_api_error(error_code) when is_integer(error_code),
    do: "#{gettext("API error (API error code")} #{error_code}). #{contact_support()}"
  def full_api_error(_), do: full_api_error()

  def contact_support, do: gettext("Please contact us for further support.")

  @doc """
  Log info about the API request and return the metadata used to log the API response with additional stats.
  """
  def log_api_req(module, api_path, request_body, method) do
    method_string = method |> Atom.to_string |> String.upcase
    request_start_time = DateTime.utc_now |> DateTime.to_unix(:microseconds)
    api_url = module.process_url(api_path)

    Logger.info "#{module} Request Start: #{method_string} #{api_url}"
    if Mix.env != :prod, do: Logger.debug "Req: " <> inspect(request_body)

    %{module: module, api_url: api_url, method_string: method_string, request_start_time: request_start_time}
  end

  @doc """
  Log info about the API response based on the metadata from logging the request.
  """
  def log_api_res(response, %{module: Ipseeds.BlockTackAPI = module, api_url: api_url, method_string: method_string,
    request_start_time: request_start_time} = _req_metadata) do

    request_end_time = DateTime.utc_now |> DateTime.to_unix(:microseconds)
    response_time = (request_end_time - request_start_time) / 1_000

    case response do
      {:ok, %{status_code: status_code, body: %{"error_code" => error_code}} = error_body} when error_code == 9001 ->
        Logger.info "#{module} Request Complete: #{method_string} #{api_url} #{status_code} " <>
          "(Blockchain Transaction is unconfirmed) in #{response_time}ms"
        Logger.info "Res: " <> inspect(error_body)
      {:ok, %{status_code: status_code, body: %{"error_code" => error_code}} = error_body} when error_code != 0 ->
        Logger.error "#{module} Request Complete with API error: #{method_string} #{api_url} #{status_code} " <>
          "(API error_code: #{error_code}) in #{response_time}ms"
        Logger.error "Res: " <> inspect(error_body)
      {:ok, %{status_code: status_code}} when status_code in [200, 201] ->
        Logger.info "#{module} Request Complete: #{method_string} #{api_url} #{status_code} in #{response_time}ms"
      {:ok, %{status_code: status_code} = error_body} ->
        Logger.error "#{module} Request Error: #{method_string} #{api_url} #{status_code} in #{response_time}ms"
        Logger.error "Res: " <> inspect(error_body)
      {:ok, no_status_code_resp} ->
        Logger.error "#{module} Request Error: #{method_string} #{api_url} in #{response_time}ms"
        Logger.error "Res: " <> inspect(no_status_code_resp)
      {:error, reason} ->
        Logger.error "#{module} Request Error: #{method_string} #{api_url} in #{response_time}ms"
        Logger.error "Reason: " <> inspect(reason)
    end

    response
  end
  def log_api_res(response, %{module: module, api_url: api_url, method_string: method_string,
    request_start_time: request_start_time} = _req_metadata) do

    request_end_time = DateTime.utc_now |> DateTime.to_unix(:microseconds)
    response_time = (request_end_time - request_start_time) / 1_000

    case response do
      {:ok, %{status_code: status_code, body: %{"error_code" => error_code}} = error_body} when error_code != 0 ->
        Logger.error "#{module} Request Complete with API error: #{method_string} #{api_url} #{status_code} " <>
          "(API error_code: #{error_code}) in #{response_time}ms"
        Logger.error "Res: " <> inspect(error_body)
      {:ok, %{status_code: status_code}} when status_code in [200, 201] ->
        Logger.info "#{module} Request Complete: #{method_string} #{api_url} #{status_code} in #{response_time}ms"
      {:ok, %{status_code: status_code} = error_body} ->
        Logger.error "#{module} Request Error: #{method_string} #{api_url} #{status_code} in #{response_time}ms"
        Logger.error "Res: " <> inspect(error_body)
      {:ok, no_status_code_resp} ->
        Logger.error "#{module} Request Error: #{method_string} #{api_url} in #{response_time}ms"
        Logger.error "Res: " <> inspect(no_status_code_resp)
      {:error, reason} ->
        Logger.error "#{module} Request Error: #{method_string} #{api_url} in #{response_time}ms"
        Logger.error "Reason: " <> inspect(reason)
    end

    response
  end

end