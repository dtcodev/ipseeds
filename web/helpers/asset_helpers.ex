defmodule Ipseeds.AssetHelpers do
  defmacro __using__(opts) do
    asset_module = Keyword.fetch!(opts, :asset_module)
    asset_api_id_key = Keyword.get(opts, :asset_api_id_key, :asset_id)
    asset_db_id_key = Keyword.get(opts, :asset_db_id_key, :api_asset_id)

    quote do
      import unquote(__MODULE__)

      alias unquote(asset_module)

      @asset_module unquote(asset_module)
      @asset_api_id_key unquote(asset_api_id_key)
      @asset_db_id_key unquote(asset_db_id_key)
    end
  end

  defmacro dev_mode_asset_filter(assets) do
    quote bind_quoted: [assets: assets] do
      if Mix.env != :prod do
        asset_ids = assets |> get_in([Access.all, @asset_api_id_key])
        query = from p in @asset_module,
          select: field(p, ^@asset_db_id_key),
          where: field(p, ^@asset_db_id_key) in ^asset_ids

        existing_ids = Ipseeds.Repo.all(query)

        Enum.filter(assets, fn a -> a[@asset_api_id_key] in existing_ids end)
      else
        assets
      end
    end
  end

  defmacro add_assets_to_local_db(assets, conn) do
    quote bind_quoted: [assets: assets, conn: conn] do
      asset_ids = assets |> get_in([Access.all, @asset_api_id_key])
      query = from p in @asset_module,
        select: field(p, ^@asset_db_id_key),
        where: field(p, ^@asset_db_id_key) in ^asset_ids

      existing_ids = Ipseeds.Repo.all(query)

      assets_not_in_local_db = Enum.reject(assets, fn a -> a[@asset_api_id_key] in existing_ids end)
      assets_not_inserted = for asset <- assets_not_in_local_db do
        api_link_changeset = @asset_module.api_link_changeset(%@asset_module{}, %{
          user_id: conn.assigns.current_user.id,
          api_asset_id: asset[:asset_id]
        })

        case Ipseeds.Repo.insert(api_link_changeset) do
          {:ok, _} -> nil
          {:error, invalid_changeset} -> asset
        end
      end
      |> Enum.filter(fn a -> !is_nil(a) end)

      assets_not_inserted_ids = assets_not_inserted |> get_in([Access.all, @asset_api_id_key])
      Enum.reject(assets, fn a -> a[@asset_api_id_key] in assets_not_inserted_ids end)
    end
  end
end
