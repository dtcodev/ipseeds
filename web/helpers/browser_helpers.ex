defmodule Ipseeds.BrowserHelpers do
  import Ipseeds.Gettext

  def get_user_agent(conn) do
    ua_req_header = Plug.Conn.get_req_header(conn, "user-agent") |> List.first

    if is_binary(ua_req_header), do: ua_req_header, else: "Other"
  end

  def unsupported_browser?(conn) do
    ua = get_user_agent(conn)
    Browser.ie?(ua, 9) || Browser.ie?(ua, 10)
  end

  def unsupported_browser_message do
    gettext("We're sorry, but your browser is not supported. BioIPSeeds uses modern cryptographic tools to encrypt your messages and files, but these are not supported by your current browser. Google Chome and Mozilla Firefox browsers both support the modern cryptographic tools and are free to download.")
  end

  def partially_supported_browser?(conn) do
    ua = get_user_agent(conn)
    Browser.ie?(ua, 11)
  end

  def partially_supported_browser_message do
      gettext("Your browser is not fully supported by BioIPSeeds. Some functions will work, but many will be unavailable. BioIPSeeds uses modern cryptographic tools to encrypt your messages and files, but these are only partially implemented by your current brownser. Google Chome and Mozilla Firefox browsers both support the modern cryptographic tools and are free to download.")
    end
end