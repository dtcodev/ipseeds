defmodule Ipseeds.EnumHelpers do
  def atomize_keys(map) when is_map(map) do
    for {key, value} <- map, into: %{}, do: atomize_kv(key, value)
  end
  defp atomize_kv(key, value) when is_binary(key), do: {String.to_atom(key), value}
  defp atomize_kv(key, value) when is_atom(key), do: {key, value}
  defp atomize_kv(key, value), do: {String.to_atom(inspect(key)), value}

  def mapify_map_list([], _), do: %{}
  def mapify_map_list(list, field) when is_list(list) do
    list |> Enum.map(fn item -> {Map.get(item, field), item} end) |> Enum.into(%{})
  end

  def merge_map_fields(map1, map2, _fields) when map1 == %{} or map2 == %{}, do: %{}
  def merge_map_fields(map1 = %{}, _map2 = %{}, []), do: map1
  def merge_map_fields(map1 = %{}, map2 = %{}, field) when not is_list(field) do
    merge_map_fields(map1, map2, [field])
  end
  def merge_map_fields(map1 = %{}, map2 = %{}, fields) when is_list(fields) do
    Map.merge(map1, Map.take(map2, fields))
  end

end
