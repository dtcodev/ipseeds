defmodule Ipseeds.AuthHelpers do
  import Bitwise
  import Comeonin.Bcrypt, only: [dummy_checkpw: 0]
  import Ipseeds.Gettext

  require Logger

  alias Ipseeds.{API, Repo, User}

  def log_user_in(username, password) do
    user = Repo.get_by(User.with_application_query, username: username)

    with {:ok, success_resp} <- do_call_login_api(user, password),
      {:ok, real_user} <- check_status(user),
      do: {:ok, real_user, success_resp}
  end

  defp check_status(nil) do
    dummy_checkpw()
    {:error, gettext("Invalid username/password combination")}
  end
  defp check_status(%{user_registration: user_registration} = user) do
    dummy_checkpw()
    case user_registration do
      %{disapproved_at: nil, approved_at: nil} ->
        {:error, gettext("Sorry, your application is not approved yet")}
      %{disapproved_at: dt, approved_at: at} when at < dt ->
        {:error, gettext("Sorry, your application was disapproved")}
      _ -> {:ok, user}
    end
  end

  def do_call_login_api(user, password, retries \\ 0) do
    payload = login_payload(user, password)
    response = API.call_endpoint("/account/login", payload)
    case response do
      {:ok, %{body: %{"error_code" => 0} = success_resp}} ->
        {:ok, success_resp}
      {:ok, %{body: %{"error_code" => 204}}} ->
        {:error, gettext("Your email address has not been confirmed yet, please check your email")}
      {:ok, %{body: %{"error_code" => 7001}}} ->
        if retries == API.max_retries do
          {:error, gettext("Invalid username/password combination")}
        else
          do_call_login_api(user, password, retries + 1)
        end
      {:ok, _api_resp} ->
        {:error, gettext("Invalid username/password combination")}
      {:error, _api_resp} ->
        {:error, gettext("Unexpected error")}
    end
  end

  def login_payload(user = %User{:username => account_name}, password) do
    {company, branch} = User.get_api_company_and_branch(user)
    
    {auth_code, utc_time, sign_hash} = generate_auth_code(account_name, company, branch, password)

    API.default_auth_payload(account_name, company, branch, auth_code, utc_time)
    |> Map.put("signhash", sign_hash)
    |> Map.put("login_extend", 3)
  end
  def login_payload(nil, password) do
    {account_name, company, branch} = {"", "", ""}
    
    {auth_code, utc_time, sign_hash} = generate_auth_code(account_name, company, branch, password)

    API.default_auth_payload(account_name, company, branch, auth_code, utc_time)
    |> Map.put("signhash", sign_hash)
    |> Map.put("login_extend", 3)
  end

  def generate_auth_code(account_name, company, branch, password) do
    utc_time = DateTime.utc_now |> DateTime.to_unix 
    utc_time_hexstring = utc_time
      |> timestamp_to_bytes
      |> bytes_to_hexstring

    auth_string = company <> branch <> account_name <> API.software <> utc_time_hexstring
    auth_code_hash = sha256_2(auth_string)
    auth_code = auth_code_hash |> bytes_to_hexstring
    password_hash = sha256(password)

    sign_hash = 
      auth_code_hash <> password_hash
      |> sha256
      |> Base.encode16(case: :lower)

    {auth_code, utc_time, sign_hash}
  end

  defp timestamp_to_bytes(timestamp) do
    byte_1 = (timestamp >>> 56) &&& 0xFF
    byte_2 = (timestamp >>> 48) &&& 0xFF
    byte_3 = (timestamp >>> 40) &&& 0xFF
    byte_4 = (timestamp >>> 32) &&& 0xFF
    byte_5 = (timestamp >>> 24) &&& 0xFF
    byte_6 = (timestamp >>> 16) &&& 0xFF
    byte_7 = (timestamp >>> 8) &&& 0xFF
    byte_8 = timestamp &&& 0xFF

    [byte_1, byte_2, byte_3, byte_4,
     byte_5, byte_6, byte_7, byte_8]
  end
  
  defp bytes_to_hexstring(bytes) do
    bytes
    |> IO.iodata_to_binary
    |> Base.encode16(case: :lower)
  end

  defp sha256(bytes) do
    :crypto.hash_init(:sha256)
    |> :crypto.hash_update(bytes)
    |> :crypto.hash_final()
  end

  defp sha256_2(bytes) do
    tmp_hash = :crypto.hash_init(:sha256)
      |> :crypto.hash_update(bytes)
      |> :crypto.hash_final()
    
    :crypto.hash_init(:sha256)
    |> :crypto.hash_update(tmp_hash)
    |> :crypto.hash_final()
  end
end
