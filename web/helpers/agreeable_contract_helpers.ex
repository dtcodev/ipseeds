defmodule Ipseeds.AgreeableContractHelpers do
  def unilateral_plaintext(nda_data) do
    ~s"""
    THIS AGREEMENT (the “Agreement”) is entered into #{nda_data["show_date"]} by and between #{nda_data["sender_name"]}[#{nda_data["sender_nationality"]}], with address at #{nda_data["sender_address"]} (the “Discloser”) and #{nda_data["recipient_name"]}[#{nda_data["recipient_nationality"]}] with address at #{nda_data["recipient_address"]} (the “Recipient”).
    
    WHEREAS the Discloser possesses certain ideas and information relating to #{nda_data["unilateral_input_pnddo"]} that is confidential and proprietary to the Discloser and shall include all material, non-public information, written or oral, disclosed, directly or indirectly, through any means of communication or observation by the Discloser or any of its affiliates or representatives to or for the benefit of the Recipient (“Confidential Information”); 
    
    WHEREAS the Recipient is willing to receive disclosure of the Confidential Information pursuant to the terms of this Agreement for the purpose of #{nda_data["unilateral_input_ndd"]} (the “Purpose”). 
    
    NOW, THEREFORE, in consideration of the mutual undertakings herein, intending to be legally bound, the Discloser and the Recipient agree as follows:
    
    1 Disclosure. Discloser agrees to disclose, and Recipient agrees to receive the Confidential Information.
    
    2 Confidentiality.
    
    	a) Use. Recipient agrees not to use the Confidential Information in any way, or to manufacture or test any product embodying Confidential Information, except for the Purpose.
    
    	b) Nondisclosure and No Use Obligations. Recipient will maintain in confidence and will not disclose or disseminate any Confidential Information belonging to the Discloser. Recipient agrees that it shall treat all of Discloser’s Confidential Information with at least the same degree of care as Recipient accords its own confidential information. Recipient further represents that it will use its best efforts to prevent and protect the Confidential Information. If Recipient is not an individual, Recipient agrees that Recipient shall disclose Confidential Information only to those of its employees who need to know such information, and shall cause such employees to hold all Confidential Information of the Discloser in strict confidence. 
    
    	c) Recipient agrees to take all steps reasonably necessary to protect the secrecy of the Confidential Information, and to prevent the Confidential Information from falling into the public domain or into the possession of unauthorized persons.
    
    	d) Recipient may, however, disclose Confidential Information in accordance with judicial or other governmental order, provided that the Recipient gives the Discloser reasonable notice prior to such disclosure and shall comply with any applicable protective order or equivalent.
    
    	e) Neither this Agreement nor the supply of any information grants the Recipient any license, interest or right in respect of any intellectual property rights of the Discloser.
    
    3 Limits on Confidential Information. 
    
    	a) Confidential Information shall not be deemed proprietary and the Recipient shall have no obligation with respect to such information where the information:
    
    		(i) is or subsequently becomes publicly available without Recipient’s breach of any obligation owed to the Discloser;
    		(ii) became known to the Recipient prior to Discloser’s disclosure of such information to Recipient;
    		(iii) became known to the Recipient from a source other than the Discloser other than by the breach of an obligation of confidentiality owed to the Discloser; 
    		(iv) is independently developed by Recipient as substantiated by record, documentation or other evidence to be provided to the Discloser upon its request; or 
    		(v) is any information which is already known to the Recipient and which was not subject to any obligation of confidence before it was disclosed to the Recipient by the Discloser. 
    
    	b) Notwithstanding the preceding, Confidential Information shall not be deemed to be in the public domain merely because any part of the said information is embodied in general disclosures or because individual features, components or combinations thereof are now or thereafter become known to the public. 
    
    4 Ownership of Confidential Information. Recipient agrees that all Confidential Information shall remain the property of Discloser, and that Discloser may use such Confidential Information for any purposes without obligation to Recipient. Nothing contained herein shall be construed as granting or implying any transfer of rights to Recipient in the Confidential Information, or any patents or other intellectual property protecting or relating to the Confidential Information.
    
    5 Term and Termination. 
    
    	(a) This Agreement shall terminate as to the exchange of Confidential Information [[unilateral_input_yr_01]] years after the effective date hereof.
    
    	(b) The Recipient and the Discloser may terminate the exchange of Confidential Information under this Agreement at any time by providing #{nda_data["unilateral_input_days"]} days written notice to the other party specifically referencing this Agreement.
    
    	(c) Notwithstanding any termination, the obligations of each party to maintain the confidentiality of the Confidential Information it has received under this Nondisclosure Agreement shall continue for a period of [[unilateral_input_yr_02]] years after any termination.
    
    	(d) Upon termination of any relationship between the parties, Recipient will promptly deliver to the Discloser, without retaining any copies, all documents and other materials furnished to Recipient by the Discloser.
    
    6 Survival of Rights and Obligations. 
    
    	(a) This Agreement shall govern all communications between the Recipient and the Discloser, and be binding upon, inure to the benefit of, and be enforceable by the following: 
    
    		(i) Discloser, its successors, and assigns; and
    
    		(ii) Recipient, its successors and assigns.
    
    	(b) Recipient understands that its obligations under Section 2 ("Confidentiality") shall survive the termination of any other relationship between the parties.
    
    	(c) This Agreement may not be assigned by, delegated or transferred without the written approval of the Discloser.
    
    7 Governing Law. This Agreement is governed by, and is to be construed in accordance with, the law of #{nda_data["unilateral_nationality"]}.
    
    8 Remedies for Breach. The Recipient understands and acknowledges that the Confidential Information has been developed or obtained by the Discloser by the investment of significant time, effort and expenses. The Recipient understands and agrees that in the event of a breach of its obligations hereunder, monetary damages may not be a sufficient remedy and the Discloser may seek and obtain a permanent injunction in order to prevent or restrain any such breach by the Recipient and any and all person acting directly and indirectly with the Recipient. None of the remedies set forth above shall in any way limit the Discloser’s remedies available at law or in equity for such breach. 
    
    9 In the event of any suit, action or proceeding commenced by the Discloser in any court of competent jurisdiction to enforce any term or provision of this Agreement after default by a Recipient hereto, the prevailing party in such suit shall be entitled to receive from the other party, in addition to any other relief granted, reasonable attorney’s fees and costs. 
    
    10 Entire Agreement. This Agreement constitutes the entire agreement with respect to the Confidential Information disclosed herein and supersedes all prior or contemporaneous oral or written agreements concerning such Confidential Information. This Agreement shall not be modified except by a written agreement dated subsequent to the date of this Agreement and signed by both parties.
    
    11 Severability. If any provision of this Agreement shall be held by a court of competent jurisdiction to be illegal. Invalid or unenforceable, the remaining provision shall remain in full force and effect. 
    
    12 The Recipient warrants and represents that it has carefully read and understood this Agreement and the Discloser warrants and represents that it has carefully read and understood this Agreement, and each acknowledge receipt of a copy thereof. 
    
    IN WITNESS WHEREOF, the parties have executed this Agreement effective as of the date first written above.
    """
  end

  def mutual_plaintext(nda_data) do
    ~s"""
    THIS AGREEMENT (the “Agreement”) is entered into #{nda_data["show_date"]} by and between #{nda_data["sender_name"]}[#{nda_data["sender_nationality"]}], with address at #{nda_data["sender_address"]} (the “First Party”) and #{nda_data["recipient_name"]}[#{nda_data["recipient_nationality"]}] with address at #{nda_data["recipient_address"]} (the “Second Party”). The First Party and Second Party may be collectively referred to as the “Parties” or individually as a “Party”. 
    
    WHEREAS, the Parties mutually desire to enter frank and open discussions with respect to #{nda_data["mutual_input_apambbr"]} (the “Purpose”);
    
    WHEREAS, the Parties may exchange through correspondence and during discussions Confidential Information (as defined below) and materials;
    
    WHEREAS, the Parties, for their mutual benefit and in contemplation of the Purpose, may exchange Confidential Information during the term of their relationship; and
    
    WHEREAS, the Parties wish to define their respective rights and obligations with respect to such Confidential Information.
    
    NOW, THEREFORE, in consideration of the mutual undertakings herein, intending to be legally bound, the Parties agree as follows:
    
    1. The term “Confidential Information” means information which is of a non-public, proprietary or confidential nature to the disclosing Party and shall include all material, non-public information, written or oral, disclosed, directly or indirectly, through any means of communication or observation by the disclosing Party or any of its affiliates or representatives to or for the benefit of the receiving Party.
    
    2. Confidentiality.
    
    	a) Use. Both Parties agree not to use the Confidential Information in any way, or to manufacture or test any product embodying Confidential Information, except for the Purpose.
    
    	b) Nondisclosure and No Use Obligations. The receiving Party will maintain in confidence and will not disclose or disseminate any Confidential Information belonging to the disclosing Party. The receiving Party agrees that it shall treat all of disclosing Party’s Confidential Information with at least the same degree of care as the receiving Party accords its own confidential information. The receiving Party further represents that it will use its best efforts to prevent and protect the Confidential Information. If the receiving Party is not an individual, the receiving Party agrees that the receiving Party shall disclose Confidential Information only to those of its employees who need to know such information, and shall cause such employees to hold all Confidential Information of the disclosing Party in strict confidence. 
    
    	c) The receiving Party agrees to take all steps reasonably necessary to protect the secrecy of the Confidential Information, and to prevent the Confidential Information from falling into the public domain or into the possession of unauthorized persons.
    
    	d) The receiving Party may, however, disclose Confidential Information in accordance with judicial or other governmental order, provided that the receiving Party gives the disclosing Party reasonable notice prior to such disclosure and shall comply with any applicable protective order or equivalent.
    
    	e) Neither this Agreement nor the supply of any information grants the receiving Party any license, interest or right in respect of any intellectual property rights of the disclosing Party.
    
    4 Limits on Confidential Information. 
    
    	a) Confidential Information shall not be deemed proprietary and the receiving Party shall have no obligation with respect to such information where the information:
    
    		(i) is or subsequently becomes publicly available without the receiving Party’s breach of any obligation owed to the disclosing Party;
    		(ii) became known to the receiving Party prior to the disclosing Party’s disclosure of such information to the receiving Party;
    		(iii) became known to the receiving Party from a source other than the disclosing Party other than by the breach of an obligation of confidentiality owed to the disclosing Party; 
    		(iv) is independently developed by the receiving Party as substantiated by record, documentation or other evidence to be provided to the disclosing Party upon its request; or 
    		(v) is any information which is already known to the receiving Party and which was not subject to any obligation of confidence before it was disclosed to the receiving Party by the disclosing Party. 
    
    	b) Notwithstanding the preceding, Confidential Information shall not be deemed to be in the public domain merely because any part of the said information is embodied in general disclosures or because individual features, components or combinations thereof are now or thereafter become known to the public. 
    
    5 Ownership of Confidential Information. The receiving Party agrees that all Confidential Information shall remain the property of the disclosing Party, and that the disclosing Party may use such Confidential Information for any purposes without obligation to the receiving Party. Nothing contained herein shall be construed as granting or implying any transfer of rights to the receiving Party in the Confidential Information, or any patents or other intellectual property protecting or relating to the Confidential Information.
    
    6 Term and Termination. 
    
    	(a) This Agreement shall terminate as to the exchange of Confidential Information [[mutual_input_yr_01]] years after the effective date hereof.
    
    	(b) Both Parties may terminate the exchange of Confidential Information under this Agreement at any time by providing #{nda_data["mutual_input_days"]} days written notice to the other Party specifically referencing this Agreement.
    
    	(c) Notwithstanding any termination, the obligations of each Party to maintain the confidentiality of the Confidential Information it has received under this Nondisclosure Agreement shall continue for a period of [[mutual_input_yr_02]] years after any termination.
    
    	(d) Upon termination of any relationship between the Parties, the receiving Party will promptly deliver to the disclosing Party, without retaining any copies, all documents and other materials furnished to the receiving Party by the disclosing Party.
    
    7 Survival of Rights and Obligations. 
    
    	(a) This Agreement shall govern all communications between the Parties, and be binding upon, inure to the benefit of, and be enforceable by both Parties, its successors, and assigns.
    	 
    	(b) The receiving Party understands that its obligations under Section 2 ("Confidentiality") shall survive the termination of any other relationship between the Parties.
    
    	(c) This Agreement may not be assigned by, delegated or transferred without the written approval of the other Party.
    
    8 Governing Law. This Agreement is governed by, and is to be construed in accordance with, the law of #{nda_data["unilateral_nationality"]}.
    
    9 Remedies for Breach. Each Party understands and acknowledges that the Confidential Information has been developed or obtained by the other Party by the investment of significant time, effort and expenses. Each Party understands and agrees that in the event of a breach of its obligations hereunder, monetary damages may not be a sufficient remedy and the disclosing Party may seek and obtain a permanent injunction in order to prevent or restrain any such breach by the breaching Party and any and all person acting directly and indirectly with the breaching Party. None of the remedies set forth above shall in any way limit either Party’s remedies available at law or in equity for such breach.  
    
    10 In the event of any suit, action or proceeding commenced by either of the Parties in any court of competent jurisdiction to enforce any term or provision of this Agreement after default by a Party hereto, the prevailing Party in such suit shall be entitled to receive from the other Party, in addition to any other relief granted, reasonable attorney’s fees and costs. 
    
    11 Entire Agreement. This Agreement constitutes the entire agreement with respect to the Confidential Information disclosed herein and supersedes all prior or contemporaneous oral or written agreements concerning such Confidential Information. This Agreement shall not be modified except by a written agreement dated subsequent to the date of this Agreement and signed by both Parties.
    
    12 Severability. If any provision of this Agreement shall be held by a court of competent jurisdiction to be illegal. Invalid or unenforceable, the remaining provision shall remain in full force and effect. 
    
    13 Each of the Parties warrants and represents that it has carefully read and understood this Agreement and each acknowledges receipt of a copy thereof. 
    
    IN WITNESS WHEREOF, the Parties have executed this Agreement effective as of the date first written above.
    """
  end
end