defmodule Ipseeds.ScrivenerHelpers do
  alias Ipseeds.EnumHelpers

  def query_params(%{query_params: %{} = query_params} = _conn) when query_params != %{} do
    query_params
    |> EnumHelpers.atomize_keys()
    |> Map.delete(:page)
    |> Enum.into([])
  end
  def query_params(_conn), do: []
end