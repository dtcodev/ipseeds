defmodule Ipseeds.DateTimeHelpers do

  def format_ecto_dt(ecto_dt, timezone \\ "Asia/Taipei") do
    ecto_dt
    |> Ecto.DateTime.to_erl
    |> format_datetime(timezone)
  end

  def format_datetime(datetime, timezone \\ "Asia/Taipei") do
    datetime
    |> Timex.to_datetime
    |> Timex.Timezone.convert(timezone)
    |> Timex.format!("{ISOdate} {h24}:{m}:{s}")
  end

  def parse_api_timestamp(timestamp) when is_integer(timestamp) do
    case DateTime.from_unix(timestamp) do
      {:ok, dt} ->
        tw_dt = case Timex.Timezone.convert(dt, "Asia/Taipei") do
          %DateTime{} = new_dt -> new_dt
          %Timex.AmbiguousDateTime{after: new_dt} -> new_dt
        end
        date_string = tw_dt |> DateTime.to_date |> Date.to_string
        time_string = tw_dt |> DateTime.to_time |> Time.to_string
        Phoenix.HTML.raw("#{date_string} #{time_string}")
      _ -> timestamp
    end
  end
  def parse_api_timestamp(timestamp), do: to_string(timestamp)

  def api_timestamp_to_datestring(timestamp) when is_integer(timestamp) do
    case DateTime.from_unix(timestamp) do
      {:ok, dt} ->
        tw_dt = case Timex.Timezone.convert(dt, "Asia/Taipei") do
          %DateTime{} = new_dt -> new_dt
          %Timex.AmbiguousDateTime{after: new_dt} -> new_dt
        end
        tw_dt |> DateTime.to_date |> Date.to_string
      _ -> to_string(timestamp)
    end
  end
  def api_timestamp_to_datestring(timestamp), do: to_string(timestamp)

  def api_timestamp_to_datetimestring(timestamp) when is_integer(timestamp) do
    case DateTime.from_unix(timestamp) do
      {:ok, dt} -> api_timestamp_to_datetimestring(dt)
      _ -> to_string(timestamp)
    end
  end
  def api_timestamp_to_datetimestring(%Ecto.DateTime{} = timestamp) do
    timestamp
    |> Ecto.DateTime.to_erl
    |> Timex.to_datetime("Etc/UTC")
    |> api_timestamp_to_datetimestring
  end
  def api_timestamp_to_datetimestring(%DateTime{} = dt) do
    tw_dt = case Timex.Timezone.convert(dt, "Asia/Taipei") do
      %DateTime{} = new_dt -> new_dt
      %Timex.AmbiguousDateTime{after: new_dt} -> new_dt
    end
    tw_dt |> Timex.format!("{ISOdate} {h24}:{m}:{s}")
  end
  def api_timestamp_to_datetimestring(timestamp), do: to_string(timestamp)
end
