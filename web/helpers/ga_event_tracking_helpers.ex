defmodule Ipseeds.GaEventTrackingHelpers do
  require Logger
  def sign_in_tracking(conn, current_user) do
    if conn.cookies["trigger_ga_event"] == "Y" do
      " ga('send', 'event', 'User Sign In', '#{current_user |> Ipseeds.User.get_name}(#{current_user.username}-##{current_user.id})');Cookies.remove('trigger_ga_event');"
    else
      ""
    end
  end
end