defmodule Ipseeds.ExagonHelpers do
  def put(conn, kw_list) when is_list(kw_list) do
    put(conn, Enum.into(kw_list, %{}))
  end

  def put(conn, map) when is_map(map) do
    orig = get_exagon(conn)
    new = Map.merge(orig, map)
    Plug.Conn.assign(conn, :exagon, new)
  end

  def put(conn, key, value) do
    orig = get_exagon(conn)
    new = Map.put(orig, key, value)
    Plug.Conn.assign(conn, :exagon, new)
  end

  def get(conn, key) do
    Map.get(get_exagon(conn), key)
  end

  def get_exagon(conn) do
    Map.get(conn.assigns, :exagon, %{})
  end
end