defmodule Ipseeds.MembershipHelpers do
  def membership_class("individual_researcher_application"), do: "info"
  def membership_class("industry_application"), do: "primary"
  def membership_class("research_institute_application"), do: "success"
end