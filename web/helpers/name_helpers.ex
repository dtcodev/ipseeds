defmodule Ipseeds.NameHelpers do
  def full_name(given_names, last_name) do
    case is_chinese_name?(given_names, last_name) do
      true -> "#{last_name}#{given_names}"
      _ -> "#{given_names} #{last_name}"
    end
  end

  defp is_chinese_name?(given_names, last_name) do
    String.length(given_names) <= 2 && String.length(last_name) <= 2
  end
end