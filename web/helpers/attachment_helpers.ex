defmodule Ipseeds.AttachmentHelpers do
  require Arc.Ecto.Schema # required for Arc.Ecto.Schema.cast_attachments/3 macro
  import Ecto.Changeset # required for Arc.Ecto.Schema.cast_attachments/3 macro

  @doc """
  Set the :size_limit option when using to set the size limit for attachments that is different from the default.

      use Ipseeds.AttachmentHelpers, attachment_max_size_bytes: 1_048_576
  """
  defmacro __using__(opts) do
    attachment_max_size_bytes = Keyword.get(opts, :attachment_max_size_bytes, 2_097_152)
    quote do
      import Ipseeds.AttachmentHelpers

      @attachment_max_size_bytes unquote(attachment_max_size_bytes)

      def attachment_max_size_bytes(), do: @attachment_max_size_bytes
    end
  end

  def cast_attachments_if_valid(changeset_or_data, params, allowed, required \\ []) do
    required = Enum.map(required, fn key ->
      case key do
        key when is_binary(key) -> key
        key when is_atom(key) -> Atom.to_string(key)
      end
    end)

    failed_params =
      required
      |> Enum.filter(&!Map.has_key?(params, &1))
      |> Enum.map(&String.to_atom(&1))

    case validate_required(changeset_or_data, failed_params) do
      %Ecto.Changeset{valid?: true} ->
        Arc.Ecto.Schema.cast_attachments(changeset_or_data, params, allowed)
      failed_changeset -> failed_changeset
    end
  end

  defmacro validate_attachment_size(changeset_or_model, params, field) do
    quote bind_quoted: [changeset_or_model: changeset_or_model,
                        params: params,
                        field: field] do
      validate_attachment_size(changeset_or_model, params, field, @attachment_max_size_bytes)
    end
  end

  def validate_attachment_size(changeset_or_model, params, fields, size_limit) when is_list(fields) do
    Enum.reduce(fields, changeset_or_model, fn (field, changeset) ->
      validate_attachment_size(changeset, params, field, size_limit)
    end)
  end
  def validate_attachment_size(changeset_or_model, params, field, size_limit) when is_atom(field) do
    string_field = 
      case field do
        key when is_binary(key) -> key
        key when is_atom(key) -> Atom.to_string(key)
      end

    changeset = case changeset_or_model do
      %Ecto.Changeset{} -> changeset_or_model
      %{__meta__: _} -> Ecto.Changeset.change(changeset_or_model)
    end

    with true <- Map.has_key?(params, string_field),
         %Plug.Upload{path: path} <- Map.get(params, string_field),
         {:ok, %File.Stat{size: size}} when size <= size_limit <- File.stat(path)  do
      # if file size less than size limit, let it pass
      changeset
    else
      false -> changeset # if params does not contain the specified field, let it pass
      _ -> 
        Ecto.Changeset.add_error(changeset, field,
                                 "exceeds maximum allowed size of %{size_limit}",
                                 size_limit: Sizeable.filesize(size_limit))
    end
  end

  def human_readable_attachment_max_size(module) do
    module.attachment_max_size_bytes()
    |> Sizeable.filesize()
  end

  def cast_uuid_names(params, allowed) do
    # Cast supports both atom and string keys, ensure we're matching on both.
    allowed = Enum.map(allowed, fn key ->
      case key do
        key when is_binary(key) -> key
        key when is_atom(key) -> Atom.to_string(key)
      end
    end)

    new_params = case params do
      :invalid ->
        :invalid
      %{} ->
        params
        |> Arc.Ecto.Schema.convert_params_to_binary
        |> Map.take(allowed)
        |> Enum.map(&put_uuid_filename(&1))
        |> Enum.into(%{})
    end

    Map.merge(params, new_params)
  end

  def put_uuid_filename({k, %Plug.Upload{filename: orig_name} = image}) do
    new_image = %Plug.Upload{image | filename: uuid_filename(orig_name)}
    {k, new_image}
  end
  def put_uuid_filename(any), do: any

  def uuid_filename(orig_name) do
    UUID.uuid4 <> Path.extname(orig_name)
  end

end
