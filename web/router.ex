defmodule Ipseeds.Router do
  use Ipseeds.Web, :router

  pipeline :browser do
    plug :accepts, ["html", "json"]
    plug :fetch_session
    plug :fetch_flash
    plug :put_secure_browser_headers
    plug Ipseeds.LocalePlug, "en"
  end

  pipeline :csrf_protection do
    plug :protect_from_forgery
  end

  pipeline :public_layout do
    plug Ipseeds.CountingPlug
    plug :put_layout, {Ipseeds.LayoutView, :public}
  end

  pipeline :secure_app do
    plug Ipseeds.App.AuthPlug
    plug Ipseeds.App.KeypairCheckPlug
  end

  pipeline :app_layout do
    plug Ipseeds.CountingPlug
    plug Ipseeds.App.UnreadMessagesPlug
    plug Ipseeds.App.PendingContractsPlug
    plug :put_layout, {Ipseeds.LayoutView, :app}
  end

  pipeline :secure_admin do
    plug Ipseeds.Admin.AuthPlug, repo: Ipseeds.Repo
  end

  pipeline :admin_layout do
    plug :put_layout, {Ipseeds.LayoutView, :admin}
  end

  scope "/", Ipseeds do
    # Will redirect to /:locale/
    pipe_through [:browser, :csrf_protection]

    get "/", PageController, :dummy

    # add dummy route to allow links to this page with no locale to redirect to locale
    get "/users/reset_password", PageController, :dummy
  end

  scope "/:locale", Ipseeds do
    # Public routes (static pages, contact page)
    pipe_through [:browser, :csrf_protection, :public_layout]

    get "/", PageController, :index
    get "/privacy_policy", PageController, :privacy_policy
    get "/terms", PageController, :terms
    get "/features", PageController, :features
    get "/faq", PageController, :faq
    get "/about", PageController, :about
    get "/how_it_works", PageController, :how_it_works
    get "/pricing", PageController, :pricing
    get "/medium_rss_feed", PageController, :medium_rss_feed

    # Subscribe
    post "/subscribe", SubscribeController, :subscribe

    # Contact Page routes 
    get "/contact_us", ContactUsController, :new
    post "/contact_us", ContactUsController, :create

    # User Registration routes
    resources "/user_registrations", UserRegistrationController, only: [:new, :create]
    get "/user_registrations/thank_you", UserRegistrationController, :thank_you

    # Public User routes
    get "/users/forgot_password", UserController, :forgot_password
    post "/users/request_reset_password", UserController, :request_reset_password
    get "/users/reset_password", UserController, :reset_password
    post "/users/submit_new_password", UserController, :submit_new_password

    # Session routes
    resources "/sessions", SessionController, only: [:new, :create, :delete]

    # Explorer routes
    resources "/explorer", App.ExplorerController, only: [:index, :show]
  end

  scope "/:locale", Ipseeds do
    pipe_through [:browser]
    post "/_reset_password_hook", UserController, :api_password_reset_hook

    # For monitoring
    get "/health_check", PageController, :health_check
  end

  scope "/:locale/secure", Ipseeds, as: :app do
    # App routes (main ipseeds functionality)
    pipe_through [:browser, :csrf_protection, :secure_app, :app_layout]

    get "/", App.IpDiscoveryController, :index
    get "/features", PageController, :features
    get "/privacy_policy", PageController, :privacy_policy
    get "/terms", PageController, :terms
    get "/faq", PageController, :faq
    get "/about", PageController, :about
    get "/how_it_works", PageController, :how_it_works

    get "/contact_us", ContactUsController, :new
    post "/contact_us", ContactUsController, :create

    # User settings routes
    get "/users/passphrase", App.UserController, :passphrase
    post "/users/passphrase", App.UserController, :create_passphrase
    post "/users/reset_password", App.UserController, :reset_password
    resources "/users", App.UserController, only: [:show, :edit, :update]
    post "/users/:id/update_profile_photo", App.UserController, :update_profile_photo

    # Ip Notary routes
    resources "/ip_notaries", App.IpNotaryController, only: [:new, :create, :index, :show]
    resources "/ip_summaries", App.IpSummaryController, 
      only: [:new, :create, :index, :show, :edit, :update]
    post "/ip_summaries/ip_notaries_list", App.IpSummaryController, :ip_notaries_list
    patch "/ip_summaries/:asset_id/hide", App.IpSummaryController, :hide
    patch "/ip_summaries/:asset_id/unhide", App.IpSummaryController, :unhide

    # Messages and contact list
    resources "/messages", App.MessageController, except: [:show]
    get "/messages/:username/:subject", App.MessageController, :show
    patch "/messages/mark_as_read/:user_id/:subject", App.MessageController, :mark_as_read
    resources "/user_contacts", App.UserContactController, only: [:index, :create, :delete]

    # Contract/Licensing
    resources "/contracts", App.ContractController, only: [:new, :create, :index, :show]

    get "/sessions/update", SessionController, :update
    
    # Explorer routes
    resources "/explorer", App.ExplorerController, only: [:index, :show]

    # Agreeable Contract routes
    resources "/agreeable_contract", App.AgreeableContractController, except: [:index, :delete]
    patch "/agreeable_contract/:id/cancel", App.AgreeableContractController, :cancel
    patch "/agreeable_contract/:id/decline", App.AgreeableContractController, :decline
  end

  scope "/:locale/admin", Ipseeds, as: :public_admin do
    # Public admin routes (no auth needed to view)
    pipe_through [:browser, :csrf_protection, :admin_layout]

    resources "/sessions", Admin.SessionController, only: [:new, :create, :delete]
    resources "/password_reset", Admin.PasswordResetController,
      only: [:new, :create, :edit, :update]
  end

  scope "/:locale/admin", Ipseeds, as: :admin do
    # Admin routes (TRPMA admin functionality)
    pipe_through [:browser, :csrf_protection, :secure_admin, :admin_layout]

    get "/", Admin.PageController, :dashboard

    resources "/admin_users", Admin.AdminUserController, except: [:show]
    resources "/user_registrations", Admin.UserRegistrationController,
      only: [:index, :show, :edit, :update]
    post "/user_registrations/:id/approve", Admin.UserRegistrationController, :approve
    post "/user_registrations/:id/disapprove", Admin.UserRegistrationController, :disapprove
  end
end
