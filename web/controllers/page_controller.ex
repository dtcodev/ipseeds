defmodule Ipseeds.PageController do
  use Ipseeds.Web, :controller
  alias Ipseeds.MediumRSSFeed

  def index(conn, _params) do
    render conn, "index.html"
  end

  def terms(conn, _params) do
    render conn, "terms.html"
  end

  def privacy_policy(conn, _params) do
    render conn, "privacy_policy.html"
  end

  def features(conn, _params) do
    render conn, "features.html"
  end

  def faq(conn, _params) do
    render conn, "faq.html"
  end

  def about(conn, _params) do
    render conn, "about.html"
  end

  def how_it_works(conn, _params) do
    render conn, "how_it_works.html"
  end

  def pricing(conn, _params) do
    render conn, "pricing.html"
  end

  def medium_rss_feed(conn, _params) do

    {:ok, %{body: rss_content}} = MediumRSSFeed.get_xml()

    conn
     |> put_layout(:none)
     |> put_resp_content_type("application/xml")
     |> render "medium_rss_feed.xml", rss_content: rss_content
  end

  def health_check(conn, _params) do
    conn
     |> put_layout(:none)
     |> put_resp_content_type("text/plain")
     |> text "OK"
  end

end
