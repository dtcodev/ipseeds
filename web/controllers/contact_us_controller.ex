defmodule Ipseeds.ContactUsController do
  use Ipseeds.Web, :controller

  import Ipseeds.ParamPlug

  alias Ipseeds.ContactUsInfo

  plug :strip_tags, "contact_us_info" when action in [:create]
  plug :scrub_params, "contact_us_info" when action in [:create]

  def new(conn, _params) do
  	changeset = ContactUsInfo.changeset(%ContactUsInfo{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"contact_us_info" => contact_us_info_params, 
    "g-recaptcha-response" => g_recaptcha_response}) do
    
  	changeset = ContactUsInfo.changeset(%ContactUsInfo{}, contact_us_info_params)
    env = Mix.env

    case Recaptcha.verify(g_recaptcha_response) do
      {:ok, _} -> 
        changeset
        |> do_for_pass_reCaptcha(contact_us_info_params, conn)
      {:error, _} when env == :test -> 
        changeset
        |> do_for_pass_reCaptcha(contact_us_info_params, conn)
      {:error, _} -> 
        conn
        |> put_flash_danger(gettext("Failed to send message, please finish the Google reCaptcha."))
        |> render("new.html", changeset: %{changeset | action: :invalid})
    end
  end

  defp send_new_contact_us_email(%{"first_name" => first_name, "last_name" => last_name,
    "email" => user_email, "message" => message}) do

    Ipseeds.Email.new_contact_us_email(first_name, last_name, user_email, message)
    |> Ipseeds.Mailer.deliver_later()
  end

  defp do_for_pass_reCaptcha(changeset, contact_us_info_params, conn) do
    if changeset.valid? do
      send_new_contact_us_email(contact_us_info_params)
      conn
      |> put_flash_success(gettext("Message was sent successfully"))
      |> render("new.html", changeset: ContactUsInfo.changeset(%ContactUsInfo{}))
    else
      conn
      |> put_flash_danger(gettext("Failed to send message, please check the errors below"))
      |> render("new.html", changeset: %{changeset | action: :invalid})
    end
  end
end
