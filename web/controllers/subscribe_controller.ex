defmodule Ipseeds.SubscribeController do
  use Ipseeds.Web, :controller
  alias Ipseeds.MailchimpAPI

  require Logger

  def subscribe(conn, params) do
    response_from_mailchimp = MailchimpAPI.call_endpoint_need_auth("/3.0/lists/list_id/members", 
      email_address: params["email"])
    resp = %{}

    resp_json = case response_from_mailchimp do
      {:ok, %{body: %{"status" => "pending"} = resp_params}} ->
        resp
        |> Map.put("result", "ok")
        |> Map.put("status", 200)
        |> Map.put("msg", Gettext.gettext(Ipseeds.Gettext, "Successfully! One more step! We’ve sent a message to #{resp_params["email_address"]}. Open it up and click the confirm button."))
      {:ok, %{body: %{"status" => 400, "title" => "Member Exists"} = resp_params}}  ->
        resp
        |> Map.put("result", "error")
        |> Map.put("status", resp_params["status"])
        |> Map.put("title", resp_params["title"])
      {:ok, %{body: %{"status" => 400, "title" => _} = resp_params}}  ->
        resp
        |> Map.put("result", "error")
        |> Map.put("status", resp_params["status"])
        |> Map.put("title", resp_params["title"])
        |> Map.put("detail", resp_params["detail"])
      {_, %{body: resp_params}} ->
        resp
        |> Map.put("result", "unexpected")
        |> Map.put("status", resp_params["status"])
        |> Map.put("title", resp_params["title"])
        |> Map.put("detail", resp_params["detail"])  
    end    

    render conn, "subscribe.jsonx", resp_json: Poison.encode! resp_json
  end

end