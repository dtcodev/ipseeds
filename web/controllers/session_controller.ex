defmodule Ipseeds.SessionController do
  use Ipseeds.Web, :controller

  # update/2 already gets piped through secure_app in the router
  plug Ipseeds.App.AuthPlug when action in [:delete]

  alias Ipseeds.{API, AuthHelpers}
  alias Ipseeds.App.AuthPlug

  def new(conn, _params) do
    render conn, "new.html"
  end

  def create(conn, %{"user" => %{"username" => username, "password" => password}}) do
    case AuthHelpers.log_user_in(username, password) do
      {:ok, user, response} ->
        redirect_url = if get_session(conn, :redirect_url) do 
          get_session(conn, :redirect_url)
        else
          app_ip_discovery_path(conn, :index, conn.assigns.locale)
        end

        conn
        |> put_data_into_session_and_conn(user.id, response)
        |> configure_session(renew: true)
        |> delete_session(:redirect_url)
        |> put_resp_cookie("trigger_ga_event", "Y", http_only: false)
        |> redirect(to: redirect_url)
      {:error, translated_error_msg} ->
        # Login is invalid or Calling API has issue
        conn
        |> put_flash_danger(translated_error_msg)
        |> render("new.html")
    end
  end

  def update(conn, _params) do
    # User's auth token will have been automatically updated by auth plug
    # if the conn has made it this far. We simply need to add 30 minutes to
    # the current time and render the JSON response.
    render(conn, "update.json", expire_time_utc: AuthPlug.new_expire_time())
  end

  def delete(conn, _params) do
    {conn, _} = API.call_authed_endpoint(conn, "/account/logout")
    conn
    |> configure_session(drop: true)
    |> redirect(to: session_path(conn, :new, conn.assigns.locale))
  end

  defp put_data_into_session_and_conn(conn, user_id, login_resp) do
    auth_code = login_resp |> get_in(["account_auth", "auth_code"])
    expire_time_utc = login_resp |> get_in(["account_auth", "expire_time_utc"])
    addr_list = login_resp["addrlist"]
    issuer_address = get_address(addr_list, "Issuer")
    reciever_address = get_address(addr_list, "Receiver")
    archivist_address = get_address(addr_list, "Archivist")

    conn
    |> put_session(:user_id, user_id)
    |> put_session(:issuer_address, issuer_address)
    |> put_session(:receiver_address, reciever_address)
    |> put_session(:archivist_address, archivist_address)
    |> AuthPlug.update_auth_data(auth_code, expire_time_utc)
  end

  defp get_address(addr_list, address_type) do
    addr_list
    |> Enum.find(fn addr -> addr["atag"] == address_type end)
    |> Map.get("address")
  end
end
