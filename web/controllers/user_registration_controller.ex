defmodule Ipseeds.UserRegistrationController do
  use Ipseeds.Web, :controller

  import Ecto.Query, only: [from: 2]

  alias Ecto.Multi
  alias Ipseeds.{AdminUser, ApplicationData, API, Email, Mailer,
    Repo, User, UserRegistration, APIHelpers}

  def new(conn, _param) do
    render conn, "new.html", failed_changeset: nil
  end

  def create(conn, params) do
    user_params = params["user"]
    reg_params = params["user_registration"]
    application_type = UserRegistration.type_map[reg_params["application_type"]]
    application_params = params[application_type[:name]]

    action_batch = submit_application(application_type[:module],
      application_params, user_params, reg_params)

    case Repo.transaction(action_batch) do
      {:ok, changeset} -> register_user_on_api(conn, changeset)
      {:error, _failed_operations, failed_changeset, _changes_so_far} ->
        render(conn, "new.html", failed_changeset: failed_changeset)
    end
  end

  def thank_you(conn, _params) do
    render(conn, "thank_you.html")
  end

  defp register_user_on_api(conn, changeset) do
    # Making call to register API
    case call_register_api(changeset.user, changeset.application_data) do
      {:ok, %{body: %{"addrlist" => addrlist, "error_code" => 0}}} ->
        addrlist
        |> Enum.each(&update_receiver_asset_addr(&1, changeset.user))

        # Send notification email to Admin users
        admin_emails = Repo.all from u in AdminUser, select: u.email, where: is_nil(u.deleted_at)
        if !Enum.empty?(admin_emails) do
          review_link = admin_user_registration_url(conn, :show, conn.assigns.locale, 
                                                    changeset.user_registration)
          admin_emails
          |> Email.new_user_registration_email(review_link)
          |> Mailer.deliver_later
        end
        send_registration_email(conn, changeset.user)
        send_welcome_message(changeset.user)
        redirect(conn, to: user_registration_path(conn, :thank_you, conn.assigns.locale))
      {:ok, %{body: %{"error_code" => error_code}}} ->
        rollback_application(changeset)
        conn
        |> put_flash_danger(APIHelpers.full_api_error(error_code))
        |> render("new.html", failed_changeset: nil)
      {_, _response} ->
        rollback_application(changeset)
        conn
        |> put_flash_danger(gettext("Unexpected API Error! Please contact us for further support"))
        |> render("new.html", failed_changeset: nil)
    end
  end

  defp update_receiver_asset_addr(%{"atag" => "Receiver", "asset_Addr" => asset_addr}, user) do
    user
    |> Ecto.Changeset.change(receiver_asset_addr: asset_addr)
    |> Repo.update
  end
  defp update_receiver_asset_addr(_wallet, _user), do: nil 

  def submit_application(application_module, application_params, user_params, reg_params) do
    Multi.new
    |> Multi.insert(:user, User.registration_changeset(%User{}, user_params))
    |> Multi.insert(:application_data,
      application_module.changeset(struct(application_module), application_params))
    |> Multi.run(:user_registration, &insert_user_registration(reg_params, &1))
  end

  def rollback_application(%{application_data: application_data,
    user_registration: user_registration, user: user}) do

    Multi.new
    |> Multi.delete(:application_data, application_data)
    |> Multi.delete(:user_registration, user_registration)
    |> Multi.delete(:user, user)
    |> Repo.transaction
  end

  defp insert_user_registration(registration_params, changes) do
    changes.application_data
    |> Ecto.build_assoc(:user_registration, user_id: changes.user.id)
    |> UserRegistration.registration_changeset(registration_params)
    |> Repo.insert
  end

  defp call_register_api(user, application_data) do
    payload = %{
      "company" => ApplicationData.get_api_company(application_data),
      "branch" => ApplicationData.get_api_branch(application_data),
      "account" => user.username,
      "email" => user.email,
      "password" => user.password
    }
    API.call_endpoint("/account/register", payload)
  end

  def send_welcome_message(user) do
    full_name = User.get_name(user)
    message = welcome_message_content(full_name)
    subject = welcome_message_subject()
    Ipseeds.App.MessageController.send_system_message(user, subject, message)
  end

  defp send_registration_email(conn, user) do
    auth_letter_urls = %{
      en: static_url(conn, "/pdf/BioIPSeeds_Authorization_Letter.pdf"),
      zh_tw: static_url(conn, "/pdf/BioIPSeeds平台授權書.pdf")
    }

    Ipseeds.Email.user_registration_email(user, auth_letter_urls)
    |> Ipseeds.Mailer.deliver_later()
  end

  defp welcome_message_subject, do: "Welcome to BioIPSeeds"

  defp welcome_message_content(full_name) do
    ~s"""
    Dear #{full_name},

    Hello and welcome to BioIPSeeds.

    This is a system message to introduce the message function. You can send messages to any user
    on BioIPSeeds in a number of ways:

      1. Send a message directly from their user profile
      2. Send a Request for Information message from an IP Summary page (find these from "Home")
      3. Add a user to your contact list and send them a message from "+New Message"

    When sending and receiving messages, you will need to enter your passphrase in order to decrypt
    your messages. Please don't lose your passphrase!

    We hope you enjoy using BioIPSeeds. Please click the "Contact" link at the bottom of the page at any
    time if you have any questions and we will gladly assist you.

    Regards,
    The BioIPSeeds Team
    """
  end
end
