defmodule Ipseeds.Admin.PageController do
  use Ipseeds.Web, :controller

  def dashboard(conn, _params) do
    render(conn, "dashboard.html")
  end
end
