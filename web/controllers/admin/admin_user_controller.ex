defmodule Ipseeds.Admin.AdminUserController do
  use Ipseeds.Web, :controller
  
  import Ipseeds.Admin.AuthPlug, only: [authenticate_superadmin: 2]
  import Ipseeds.Gettext, only: [gettext: 1]

  alias Ipseeds.{AdminUser, Email, Mailer}

  plug :authenticate_superadmin

  def index(conn, _params) do
    admin_users = AdminUser.not_deleted_query
      |> order_by(asc: :id)
      |> Repo.all

    render(conn, "index.html", admin_users: admin_users)
  end

  def new(conn, _params) do
    changeset = AdminUser.changeset(%AdminUser{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"admin_user" => admin_user_params}) do
    now = DateTime.utc_now
    token = AdminUser.gen_token
    params = admin_user_params
             |> Map.merge(%{
                 "password" => UUID.uuid4(),
                 "invitation_token_hash" => Comeonin.Bcrypt.hashpwsalt(token),
                 "invitation_created_at" => now,
                 "invitation_sent_at" => now,
                 "invitation_accepted_at" => nil,
                 "invitation_created_by_admin_id" => conn.assigns.current_admin_user.id,
               })

    changeset = AdminUser.registration_changeset(%AdminUser{}, params)

    case Repo.insert(changeset) do
      {:ok, admin_user} ->
        conn
        |> send_invitation_email(admin_user, token)
        |> put_flash_success(gettext("Admin user created"))
        |> redirect(to: admin_admin_user_path(conn, :index, conn.assigns.locale))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => id}) do
    admin_user = Repo.get!(AdminUser, id)
    changeset = AdminUser.changeset(admin_user)
    render(conn, "edit.html", admin_user: admin_user, changeset: changeset)
  end

  def update(conn, %{"id" => id, "admin_user" => admin_user_params}) do
    admin_user = Repo.get!(AdminUser, id)
    changeset = AdminUser.changeset(admin_user, admin_user_params)

    case Repo.update(changeset) do
      {:ok, _admin_user} ->
        conn
        |> put_flash_success(gettext("Admin user updated successfully."))
        |> redirect(to: admin_admin_user_path(conn, :index, conn.assigns.locale))
      {:error, changeset} ->
        render(conn, "edit.html", admin_user: admin_user, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    admin_user = Repo.get!(AdminUser, id)
    changeset = AdminUser.delete_changeset(admin_user,
      %{deleted_at: DateTime.utc_now, deleted_by_admin_id: conn.assigns.current_admin_user.id})

    case Repo.update(changeset) do
      {:ok, _admin_user} ->
        conn
        |> put_flash_success(gettext("Admin user deleted successfully."))
        |> redirect(to: admin_admin_user_path(conn, :index, conn.assigns.locale))
      {:error, _changeset} ->
        conn
        |> put_flash_danger(gettext("Unable to delete admin user."))
        |> redirect(to: admin_admin_user_path(conn, :index, conn.assigns.locale))
    end
  end

  defp send_invitation_email(conn, admin_user, token) do
    password_setup_url = invitation_password_setup_url(conn, admin_user.email, token)

    Email.admin_invitation_email(admin_user.email, password_setup_url, admin_user.username)
    |> Mailer.deliver_later

    conn
  end

  defp invitation_password_setup_url(conn, email, token) do
    public_admin_password_reset_url(
      conn, :edit, conn.assigns.locale, token,
      email: email,
      type: "invitation")
  end
end
