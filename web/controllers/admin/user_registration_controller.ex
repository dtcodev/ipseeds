defmodule Ipseeds.Admin.UserRegistrationController do
  use Ipseeds.Web, :controller

  alias Ipseeds.{UserRegistration, User, Endpoint, Email, Mailer}

  def index(conn, params) do
    page_params = get_paging_params(params)
    page =
      UserRegistration
      |> order_by(desc: :id)
      |> preload(:user)
      |> Repo.paginate(page_params)

    render(conn, "index.html", page: page)
  end

  def show(conn, %{"id" => id}) do
    user_registration = UserRegistration |> Repo.get!(id) 
      |> Repo.preload(:user)
      |> Repo.preload(:approved_by_admin)
      |> Repo.preload(:disapproved_by_admin)

    user_application = user_registration
      |> UserRegistration.get_user_application

    render(conn, "show.html", 
      user_registration: user_registration,
      user_application: user_application)
  end

  def edit(conn, %{"id" => id}) do
    user_registration = UserRegistration
      |> Repo.get!(id)
      |> Repo.preload(:user)

    changeset = user_registration
      |> UserRegistration.get_user_application_changeset

    render(conn, "edit.html",
      user_registration: user_registration,
      changeset: changeset)
  end

  def update(conn, %{"id" => id, "application_data" => application_params}) do
    user_registration = UserRegistration |> Repo.get!(id) 
      |> Repo.preload(:user)

    changeset = user_registration
      |> UserRegistration.get_user_application_changeset(application_params)

    case Repo.update(changeset) do
      {:ok, _model} ->
        redirect(conn, to: admin_user_registration_path(conn, :show, conn.assigns.locale, user_registration))
      {:error, failed_changeset} ->
        render(conn, "edit.html",
          user_registration: user_registration,
          changeset: failed_changeset)
    end
  end

  def approve(conn, %{"id" => id}) do
    user_registration = UserRegistration |> Repo.get!(id)
    changeset = user_registration
      |> UserRegistration.changeset(%{"approved_at" => DateTime.utc_now(),
        "approved_by_admin_id" => conn.assigns.current_admin_user.id})

    case Repo.update(changeset) do
      {:ok, _} ->
        send_notification_email(user_registration.user_id, session_path(conn, :new, conn.assigns.locale))

        conn
        |> put_flash_success(gettext("Registration updated successfully."))
        |> redirect(to: admin_user_registration_path(conn, :show, conn.assigns.locale, user_registration))
      {:error, failed_changeset} ->
        IO.inspect failed_changeset

        conn
        |> put_flash_danger(gettext("Unexpected Error"))
        |> redirect(to: admin_user_registration_path(conn, :show, conn.assigns.locale, user_registration))
    end
  end

  def disapprove(conn, %{"id" => id, "disapproved_reason" => reason}) do
    user_registration = UserRegistration |> Repo.get!(id)
    changeset = user_registration
      |> UserRegistration.changeset(%{
        "disapproved_at" => DateTime.utc_now(),
        "disapproved_reason" => reason,
        "disapproved_by_admin_id" => conn.assigns.current_admin_user.id})

    case Repo.update(changeset) do
      {:ok, _} ->
        conn
        |> put_flash_success(gettext("Registration updated successfully."))
      {:error, failed_changeset} ->
        IO.inspect failed_changeset
        conn
        |> put_flash_danger(gettext("Unexpected Error"))
    end
    |> redirect(to: admin_user_registration_path(conn, :show,  conn.assigns.locale,user_registration))
  end

  defp get_paging_params(%{"page" => page}), do: [page: page, page_size: 10]
  defp get_paging_params(_), do: [page: 1, page_size: 10]

  defp send_notification_email(user_id, login_path) do
    user = Repo.get(User, user_id)
    Email.approval_notification_email(user.email, User.get_name(user), Endpoint.url <> login_path)
    |> Mailer.deliver_later
  end
end
