defmodule Ipseeds.Admin.SessionController do
  use Ipseeds.Web, :controller

  import Ipseeds.Gettext, only: [gettext: 1]

  alias Ipseeds.ControllerHelpers

  plug :put_layout, {Ipseeds.LayoutView, :admin}

  def new(conn, _params) do
    if conn.assigns[:current_admin_user] do
      redirect(conn, to: admin_page_path(conn, :dashboard, conn.assigns.locale))
    else
      render conn, "new.html"
    end
  end

  def create(conn, %{"session" => %{"username" => user, "password" => pass}}) do
    case Ipseeds.Admin.AuthPlug.login_by_username_and_pass(conn, user, pass, repo: Repo) do
      {:ok, conn} ->
        return_to = get_session(conn, :return_to) || admin_page_path(conn, :dashboard, conn.assigns.locale)

        conn
        |> ControllerHelpers.put_flash_success(gettext("Logged in successfully."))
        |> redirect(to: return_to)
      {:error, _reason, conn} ->
        conn
        |> ControllerHelpers.put_flash_danger(gettext("Invalid username/password combination."))
        |> render("new.html")
    end
  end

  def delete(conn, _params) do
    conn
    |> Ipseeds.Admin.AuthPlug.logout
    |> put_flash_success(gettext("Logged out successfully."))
    |> redirect(to: public_admin_session_path(conn, :new, conn.assigns.locale))
  end
end
