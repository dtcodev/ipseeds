defmodule Ipseeds.Admin.PasswordResetController do
  use Ipseeds.Web, :controller

  import Ipseeds.Gettext, only: [gettext: 1]

  alias Ipseeds.{AdminUser, Email, Mailer}

  @invitation "invitation"

  plug :assign_admin_user when action in [:edit, :update]
  plug :validate_token when action in [:edit, :update]

  def new(conn, _params) do
    changeset = AdminUser.changeset(%AdminUser{})
    render conn, "new.html", changeset: changeset
  end

  def create(conn, %{"admin_user" => %{"email" => raw_email}}) do
    email = String.downcase(raw_email)

    case Repo.get_by(AdminUser, %{email: email}) do
      admin_user = %AdminUser{} ->

        token = AdminUser.gen_token

        conn
        |> persist_forgot_password_props(admin_user, token)
        |> send_password_reset_email(email, token)
        |> put_flash_info(gettext("Email sent with password reset instructions"))
        |> redirect(to: public_admin_session_path(conn, :new, conn.assigns.locale))

      _ ->
        conn
        |> put_flash_danger(gettext("Email address not found"))
        |> render("new.html", changeset: AdminUser.changeset(%AdminUser{}))
    end
  end

  def edit(conn, params = %{"id" => id, "email" => _email}) do
    changeset = AdminUser.changeset(conn.assigns[:admin_user])
    type = Map.get(params, "type", nil)
    render(conn, "edit.html", changeset: changeset, token: id, type: type)
  end

  def update(conn, params = %{"id" => id, "admin_user" => admin_user_params}) do
    admin_user = conn.assigns[:admin_user]
    type = Map.get(params, "type")
    changeset = AdminUser.password_reset_changeset(admin_user, admin_user_params, type)

    case Repo.update(changeset) do
      {:ok, _admin_user} ->
        conn
        |> put_flash_success(gettext("Password successfully reset. Please login with your new password."))
        |> redirect(to: public_admin_session_path(conn, :new, conn.assigns.locale))
      {:error, changeset} ->
        conn
        |> put_flash_danger(gettext("Invalid username/password combination"))
        |> render(conn, "edit.html", changeset: changeset, token: id)
    end
  end

  defp send_password_reset_email(conn, email, token) do
    password_reset_url = public_admin_password_reset_url(
      conn, :edit, conn.assigns.locale, token, email: email)

    Email.admin_password_reset_email(email, password_reset_url)
    |> Mailer.deliver_later

    conn
  end

  defp persist_forgot_password_props(conn, admin_user, token) do
    changeset = admin_user
                |> AdminUser.forgot_password_changeset(%{token: token})

    case Repo.update(changeset) do
      {:ok, _admin_user} ->
        conn
      {:error, _changeset} ->
        conn
        |> redirect(to: public_admin_session_path(conn, :new, conn.assigns.locale))
        |> halt()
    end
  end

  # Plugs
  defp assign_admin_user(conn = %{params: params}, _) do
    email = case params do
       %{"email" => email} ->
         email
       %{"admin_user" => %{"email" => email}} ->
         email
    end

    case Repo.get_by(AdminUser, %{email: email}) do
      nil -> conn
      admin_user -> assign(conn, :admin_user, admin_user)
    end
  end

  defp validate_token(conn = %{assigns: %{admin_user: admin_user},
                               params: %{"id" => token} = params}, _) do

    type = Map.get(params, "type")

    if token_valid?(admin_user, token, type) do
      conn
    else
      response_error(conn, type)
    end
  end

  defp validate_token(conn = %{params: %{"type" => type}}, _) do
    response_error(conn, type)
  end

  defp token_valid?(admin_user, token, @invitation) do
    token_created_at = Map.get(admin_user, :invitation_created_at)
    token_accepted_at = Map.get(admin_user, :invitation_accepted_at, nil)

    not token_used(token_accepted_at) and
      not expired?(token_created_at, 48) and
      AdminUser.check_token(token, admin_user, :invitation_token_hash)
  end

  defp token_valid?(admin_user, token, _) do
    token_created_at = Map.get(admin_user, :reset_password_created_at)
    token_accepted_at = Map.get(admin_user, :password_reseted_at, nil)

    not token_used(token_accepted_at) and
      not expired?(token_created_at) and
      AdminUser.check_token(token, admin_user, :reset_password_token_hash)
  end

  defp token_used(nil), do: false
  defp token_used(_), do: true

  # Plug utilities
  defp expired?(date_time, period \\ 2, granularity \\ :hours)
  defp expired?(nil, _, _), do: true
  defp expired?(date_time, period, granularity) do
    date_time
    |> Ecto.DateTime.to_erl()
    |> Timex.diff(Timex.now, granularity)
    |> fn dif -> period < abs(dif) end.()
  end

  defp response_error(conn, @invitation) do
    conn
    |> put_flash_danger(gettext("Invitation token is expired or invalid,
                                 please contact the administrator."))
    |> redirect(to: public_admin_session_path(conn, :new, conn.assigns.locale))
    |> halt()
  end

  defp response_error(conn, _) do
    conn
    |> put_flash_danger(gettext("Password reset token is expired or invalid,
                                 please send the request again."))
    |> render("new.html", changeset: AdminUser.changeset(%AdminUser{}))
    |> halt()
  end
end
