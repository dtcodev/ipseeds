defmodule Ipseeds.App.IpDiscoveryController do
  use Ipseeds.Web, :controller
  use Ipseeds.AssetHelpers, asset_module: Ipseeds.IpSummary

  alias Ipseeds.{API, EnumHelpers, IpSummary}
  alias Ipseeds.App.{IpSummaryController}
  alias Scrivener.Page

  require Logger

  @assets_per_page 99999
  @page_size 9

  def index(conn, params) do
    current_page = get_current_page(params)
    keys = get_keys(params)
    payload = get_index_payload(keys, 1)
    query_result = API.call_endpoint("/asset/query/bulk", payload)

    case query_result do
      {:ok, %{body: body = %{"asset_list" => assets}}} when is_list(assets) ->
        ip_summaries = assets
        |> IpSummaryController.merge_db_fields()
        |> Enum.map(&EnumHelpers.atomize_keys/1)
        |> IpSummaryController.add_is_visible_to_assets()
        |> Enum.filter(fn asset -> asset[:id] != nil end)
        |> Enum.filter(fn asset -> asset[:is_visible] and !asset[:has_new_version] end)
        |> IpSummaryController.sort_assets()

        total_entries = length(ip_summaries)
        total_pages = round(Float.ceil(length(ip_summaries) / 9))
        scrivener_page = %Page{page_number: current_page, total_pages: total_pages}

        # Logger.info "#{ip_summaries |> Enum.slice(9,1)}"
        ip_summaries = ip_summaries
        |> Enum.slice((current_page - 1)*9, (if current_page == total_pages, do: rem(total_entries, 9), else: 9))



        render(conn, "index.html", ip_summaries: ip_summaries, current_page: current_page, 
          total_pages: total_pages, page_size: 9, scrivener_page: scrivener_page,
          total_entries: total_entries)
      {:error, err_resp} ->
        index_error_response(conn, err_resp)
      error ->
        index_error_response(conn, error)
    end
  end

  defp index_error_response(conn, _error) do
    render(conn, "index.html", ip_summaries: [], current_page: 1, total_pages: 1, page_size: @assets_per_page)
  end

  defp get_index_payload(keys, current_page) do
    %{
      keys: keys,
      time_utc: 0,
      type: IpSummary.asset_type(),
      page_index: current_page,
      assets_per_page: @assets_per_page
    }
  end

  defp get_keys(params = %{}) do
    params
    |> Map.take(["category", "search"])
    |> Map.values
    |> Enum.map(fn (key) -> "#" <> key end)
  end

  defp get_current_page(_params = %{"page" => page}), do: String.to_integer(page)
  defp get_current_page(_params), do: 1
end
