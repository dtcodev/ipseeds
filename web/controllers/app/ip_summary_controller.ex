defmodule Ipseeds.App.IpSummaryController do
  use Ipseeds.Web, :controller
  use Ipseeds.AssetHelpers, asset_module: Ipseeds.IpSummary

  import Ipseeds.ParamPlug, only: [strip_tags: 2]

  alias Ipseeds.{API, EnumHelpers, IpNotary, User, UserContact, APIHelpers, ExagonHelpers, AttachmentHelpers, 
    DateTimeHelpers}
  alias Ecto.{Changeset}

  require Logger

  plug :strip_tags, "ip_summary" when action in [:create]
  plug :scrub_params, "ip_summary" when action in [:create]
  plug :authorize_set_visibility! when action in [:hide, :unhide]

  def index(conn, params) do
    {conn, ip_summaries} = get_ip_summaries_list(conn, address: get_session(conn, :issuer_address))

    ip_summaries = 
      ip_summaries
      |> merge_db_fields()
      |> Enum.map(&EnumHelpers.atomize_keys/1)
      |> add_assets_to_local_db(conn)
      |> Enum.filter(fn asset -> !asset[:has_new_version] end)
      |> add_is_visible_to_assets()
      |> sort_assets()

    paging_config = get_paging_params(params)
    ip_summaries = Scrivener.paginate(ip_summaries, paging_config)
    
    render(conn, "index.html", ip_summaries: ip_summaries)
  end

  def new(conn, _params) do
    today = DateTime.utc_now |> DateTime.to_date |> Date.to_string
    changeset = IpSummary.changeset(%IpSummary{}, %{"valid_time" => today})

    conn 
    |> ExagonHelpers.put(attachment_max_size_bytes: IpSummary.attachment_max_size_bytes(),
        file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
          size_limit: AttachmentHelpers.human_readable_attachment_max_size(IpSummary))
      )
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"ip_summary" => ip_summary_params}) do
    changeset = IpSummary.create_changeset(%IpSummary{}, ip_summary_params)

    conn =
      conn
      |> ExagonHelpers.put(attachment_max_size_bytes: IpSummary.attachment_max_size_bytes(),
          file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
            size_limit: AttachmentHelpers.human_readable_attachment_max_size(IpSummary))
        )

    case submit_ip_summary_asset(conn, changeset) do
      {conn, {:ok, %{body: %{"asset_id" => asset_id, "error_code" => 0}}}} ->
        asset_updates = %{
          "api_asset_id" => asset_id,
          "user_id" => conn.assigns.current_user.id 
        }
        new_changeset = changeset |> IpSummary.changeset(asset_updates)
        case Repo.insert(new_changeset) do
          {:ok, ip_summary} ->
            redirect(conn, to: app_ip_summary_path(conn, :show, conn.assigns.locale, ip_summary))
          {:error, failed_changeset} ->
            render(conn, "new.html", changeset: failed_changeset)
          _ ->
            render(conn, "new.html", changeset: new_changeset)
        end
      {conn, {:ok, %{body: %{"error_code" => error_code}}}} ->
        changeset = changeset
          |> Map.put(:action, :insert)
          |> Changeset.add_error(:api_error, APIHelpers.changeset_api_error(error_code))

        render(conn, "new.html", changeset: changeset)
      _ ->
        Logger.debug "Asset not submitted to API, changeset:"
        Logger.debug inspect(changeset)
        changeset = Map.put(changeset, :action, :insert)
        render(conn, "new.html", changeset: changeset)
    end
  end

  def edit(conn, %{"id" => asset_id}) do
    # Must validate if owner
    ip_summary = IpSummary
      |> Repo.get_by!(api_asset_id: asset_id)
      |> Repo.preload(:user)

    case API.call_endpoint("/asset/query", asset_id: ip_summary.api_asset_id) do
      {:ok, %{body: %{} = asset_params}} ->

        linked_ips = asset_params["tag"] |> get_linked_ips() |> Enum.map(&EnumHelpers.atomize_keys/1)
        asset_params = asset_params |> EnumHelpers.atomize_keys
        changeset = IpSummary.edit_changeset(%IpSummary{}, edit_params(asset_params))

        conn
        |> ExagonHelpers.put(attachment_max_size_bytes: IpSummary.attachment_max_size_bytes(),
             file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
             size_limit: AttachmentHelpers.human_readable_attachment_max_size(IpSummary))
           )
        |> render("edit.html", ip_summary: asset_params, linked_ips: linked_ips, changeset: changeset, 
             asset_id: asset_id)
      _ ->
        conn
        |> put_flash_danger(gettext("API asset not found"))
        |> redirect(to: app_ip_summary_path(conn, :index, conn.assigns.locale))
    end
  end

  def update(conn, %{"ip_summary" => ip_summary_params, "id" => source_asset_id}) do
    # Must validate if owner

    
    changeset = %{}
    source_ip_summary = IpSummary
      |> Repo.get_by!(api_asset_id: source_asset_id)
    initial_ip_summary = IpSummary
      |> Repo.get_by!(api_asset_id: source_ip_summary.initial_asset_id || source_asset_id)  

    conn = 
      conn
        |> ExagonHelpers.put(attachment_max_size_bytes: IpSummary.attachment_max_size_bytes(),
             file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
             size_limit: AttachmentHelpers.human_readable_attachment_max_size(IpSummary))
           )
    with {conn, {:ok, %{body: %{"asset_id" => _, "enable" => _}}}} 
           <- do_set_visibility(conn, source_asset_id, false),
         {:ok, %{body: %{} = source_asset_params}} 
           <- API.call_endpoint("/asset/query", asset_id: source_asset_id),
         linked_ips = source_asset_params["tag"] |> get_linked_ips() |> Enum.map(&EnumHelpers.atomize_keys/1),
         source_asset_params = source_asset_params |> EnumHelpers.atomize_keys,
         changeset = IpSummary.new_version_changeset(%IpSummary{}, source_asset_params, 
           initial_ip_summary.api_asset_id, ip_summary_params),
         {conn, {:ok, %{body: %{"asset_id" => asset_id, "error_code" => 0}}}} 
           <- submit_ip_summary_asset_update(conn, changeset, source_asset_params),
         new_asset_updates = %{
           "api_asset_id" => asset_id,
           "user_id" => conn.assigns.current_user.id,
           "source_asset_id" => source_asset_id,
           "initial_asset_id" => initial_ip_summary.api_asset_id
         },
         new_changeset = changeset |> IpSummary.changeset(new_asset_updates),
         {:ok, new_ip_summary} <- Repo.insert(new_changeset),
         source_asset_updates = %{ "has_new_version" => true },
         souce_changeset = source_ip_summary |> IpSummary.old_version_changeset(source_asset_updates),
         {:ok, source_ip_summary} <- Repo.update(souce_changeset)
    do
      conn
      |> put_flash_success(gettext("Update successfully!!"))
      |> redirect(to: app_ip_summary_path(conn, :show, conn.assigns.locale, new_ip_summary))
    else
      resp ->
        Logger.debug "Asset not submitted to API, changeset:"
        Logger.debug inspect(resp)
        changeset = Map.put(changeset, :action, :update)
        conn
        |> put_flash_danger(gettext("Sorry, the process is fail! Please contact us!"))
        |> redirect(to: app_ip_summary_path(conn, :index, conn.assigns.locale))
    end
  end

  def show(conn, %{"id" => asset_id}) do
    ip_summary = IpSummary
      |> Repo.get_by!(api_asset_id: asset_id)
      |> Repo.preload(:user)

    case API.call_endpoint("/asset/query", asset_id: ip_summary.api_asset_id) do
      {:ok, %{body: %{} = asset_params}} ->
        linked_ips = get_linked_ips(asset_params["tag"])
          |> Enum.map(&EnumHelpers.atomize_keys/1)
        asset_params = EnumHelpers.atomize_keys(asset_params)
        current_user = conn.assigns.current_user
        current_user_active_keypair = User.active_keypair(current_user)
        issuer = Repo.get_by!(User.is_active_query, username: ip_summary.user.username)
        issuer_is_contact? = UserContact.is_contact?(current_user, issuer)
        other_user_active_keypair = User.active_keypair(%{id: ip_summary.user_id})
        {current_user_company, current_user_branch} = User.get_company_and_branch(current_user)

        conn
        |> Ipseeds.ExagonHelpers.put(current_user_id: current_user.id,
          other_user_id: ip_summary.user_id,
          current_user_public_key: current_user_active_keypair.ecc_public_key_hex,
          other_user_public_key: other_user_active_keypair.ecc_public_key_hex,
          ip_summary_name: asset_params[:name],
          ip_summary_case_id: asset_params[:name_short],
          current_user_name: User.get_name(current_user),
          current_user_company: current_user_company,
          current_user_branch: current_user_branch
          )
        |> render("show.html", ip_summary: asset_params, issuer: ip_summary.user, linked_ips: linked_ips, 
             issuer_is_contact?: issuer_is_contact?)
      _ ->
        conn
        |> put_flash_danger(gettext("API asset not found"))
        |> redirect(to: app_ip_summary_path(conn, :index, conn.assigns.locale))
    end
  end

  def hide(conn, %{"asset_id" => asset_id}) do
    set_visibility(conn, asset_id, false)
  end

  def unhide(conn, %{"asset_id" => asset_id}) do
    set_visibility(conn, asset_id, true)
  end

  defp authorize_set_visibility!(conn, _opts) do
    ip_summary = Repo.get_by(IpSummary, api_asset_id: conn.params["asset_id"])
    case ip_summary.user_id == conn.assigns.current_user.id do
      true -> conn
      _ -> raise Ipseeds.Forbidden
    end
  end

  defp set_visibility(conn, asset_id, is_visible) do
    toggle_visibility_verb_hash = %{true => :hide, false => :unhide}
    toggle_visibility_link = app_ip_summary_path(conn, toggle_visibility_verb_hash[is_visible],
      conn.assigns.locale, asset_id)

    case do_set_visibility(conn, asset_id, is_visible) do
      {conn, {:ok, %{body: %{"asset_id" => asset_id, "enable" => is_enabled}}}} ->
        conn
        |> put_status(:ok)
        |> render("set_visibility.json", asset_id: asset_id, is_visible: is_enabled == 1,
          toggle_visibility_link: toggle_visibility_link)
      {conn, {status, error_resp}} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Ipseeds.ErrorView, "api_error.json", status: status, error_resp: error_resp)
      {conn, _} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Ipseeds.ErrorView, "api_error.json", status: 0, error_resp: "Unknown error")
    end
  end

  defp do_set_visibility(conn, asset_id, is_visible) do
    toggle_visibility_int_hash = %{true => 1, false => 0}
    payload = %{
      address: get_session(conn, :issuer_address),
      asset_id: asset_id,
      enable: toggle_visibility_int_hash[is_visible]
    }
    {conn, api_response} = API.call_authed_endpoint(conn, "/tradechain/asset/enable", payload)
  end

  def ip_notaries_list(conn, _params) do
    payload = %{"address" => get_session(conn, :issuer_address)}
    {conn, resp} = API.call_authed_endpoint(conn, "/tradechain/asset/infos", payload)

    ip_notaries = case resp do
      {:ok, %{body: %{"list" => assets_list}}} ->
        assets_list
        |> Enum.filter(fn (asset) -> asset["type"] == IpNotary.asset_type end)
        |> Enum.sort(&(&1["create_time"] > &2["create_time"]))
      _error -> []
    end

    json(conn, ip_notaries)
  end

  def add_is_visible_to_assets([]), do: []
  def add_is_visible_to_assets(assets) when is_list(assets) do
    Enum.map(assets, fn asset ->
      if asset[:Enable] == 1 || asset[:enable] == 1 do
        Map.put(asset, :is_visible, true)
      else
        Map.put(asset, :is_visible, false)
      end
    end)
  end
  def add_is_visible_to_assets(_), do: []

  def sort_assets([]), do: []
  def sort_assets(assets) when is_list(assets) do
    Enum.sort(assets, fn (a, b) -> a[:create_time] > b[:create_time] end)
  end
  def sort_assets(_), do: []

  def merge_db_fields([]), do: []
  def merge_db_fields(ip_summary = %IpSummary{}), do: merge_db_fields([ip_summary])
  def merge_db_fields(ip_summaries) when is_list(ip_summaries) do
    asset_ids = Enum.map(ip_summaries, &(&1["asset_id"]))
    asset_map = EnumHelpers.mapify_map_list(ip_summaries, "asset_id")
    query = from ip in IpSummary,
      where: ip.api_asset_id in ^asset_ids
    Repo.all(query)
      |> Enum.map(fn db_asset ->
        asset_map[db_asset.api_asset_id]
        |> EnumHelpers.merge_map_fields(db_asset, [:has_new_version])
        |> EnumHelpers.merge_map_fields(db_asset, [:id])
      end)
  end

  defp get_linked_ips(""), do: []
  defp get_linked_ips(nil), do: []
  defp get_linked_ips(tag_string) do
    case Poison.decode(tag_string) do
      {:ok, tags} ->
        case get_in(tags, ["items", Access.all(), "asset_id"]) do
          nil -> []
          ids ->
            ids
            |> Enum.map(&query_for_asset(&1))
            |> Enum.reject(fn (el) -> is_nil(el) end)
        end
      {:error, _} -> []
    end
  end

  defp query_for_asset(asset_id) do
    case API.call_endpoint("/asset/query", asset_id: asset_id) do
      {:ok, %{body: %{} = asset_params}} -> asset_params
      _ -> nil
    end
  end

  defp get_paging_params(%{"page" => page}), do: [page: page, page_size: 25]
  defp get_paging_params(_), do: [page: 1, page_size: 25]

  defp submit_ip_summary_asset(conn, changeset) do
    with true <- changeset.valid?,
      asset = IpSummary.convert_to_api_payload(changeset),
      issuer_address = get_session(conn, :issuer_address),
      payload = Map.put(asset, "address", issuer_address),
      {conn, {:ok, success_resp}} <- API.call_authed_endpoint(conn, "/tradechain/asset/create",
        payload),
    do: {conn, {:ok, success_resp}}
  end

  defp submit_ip_summary_asset_update(conn, changeset, source_asset_params) do
    with true <- changeset.valid?,
      asset = IpSummary.convert_to_api_payload(changeset),
      issuer_address = get_session(conn, :issuer_address),
      Logger.error("asset: #{inspect asset}"),
      payload = asset 
                |> Map.put("address", issuer_address) 
                |> IpSummary.handle_file_url_for_update(source_asset_params),
      Logger.error("payload: #{inspect payload}"),
      {conn, {:ok, success_resp}} <- API.call_authed_endpoint(conn, "/tradechain/asset/create",
        payload),
    do: {conn, {:ok, success_resp}}
  end

  defp get_ip_summaries_list(conn, payload) do
    case API.call_authed_endpoint(conn, "/tradechain/asset/infos", payload) do
      {conn, {:ok, %{body: %{"list" => assets_list}}}} ->
        ip_notaries =
          assets_list
          |> Enum.filter(fn (asset) -> asset["type"] == IpSummary.asset_type end)
        {conn, ip_notaries}
      {conn, _error_body} ->
        {conn, []}
      error -> # Unexpected error
        Logger.error "Unexpected error:"
        Logger.error inspect(error)
        {conn, []}
    end
  end

  defp edit_params(asset_params) do
    %{
      "name" => asset_params[:name],
      "name_short" => asset_params[:name_short],
      "category" => Ipseeds.App.IpSummaryView.categories_map()[Ipseeds.App.IpSummaryView.parse_category(asset_params)],
      "keywords" => Ipseeds.App.IpSummaryView.parse_keywords_for_edit(asset_params),
      "inventors" => Poison.encode!(Ipseeds.App.IpSummaryView.get_inventors_list(asset_params)),
      "department" => Ipseeds.App.IpSummaryView.parse_text_description_for_edit(asset_params, :department),
      "background" => Ipseeds.App.IpSummaryView.parse_text_description_for_edit(asset_params, :background),
      "innovation" => Ipseeds.App.IpSummaryView.parse_text_description_for_edit(asset_params, :innovation),
      "application" => Ipseeds.App.IpSummaryView.parse_text_description_for_edit(asset_params, :application),
      "advantage" => Ipseeds.App.IpSummaryView.parse_text_description_for_edit(asset_params, :advantage),
      "state_of_development" => 
        Ipseeds.App.IpSummaryView.parse_text_description_for_edit(asset_params, :state_of_development),
      "clinical_trial" => Ipseeds.App.IpSummaryView.parse_text_description_for_edit(asset_params, :clinical_trial),
      "patent_status" => Ipseeds.App.IpSummaryView.parse_text_description_for_edit(asset_params, :patent_status),
      "type_of_partnership" => 
        Poison.encode!(Ipseeds.App.IpSummaryView.parse_text_description_for_edit(asset_params, :type_of_partnership)),
      "tag" => asset_params[:tag],
      "create_time" => DateTimeHelpers.api_timestamp_to_datestring(asset_params[:valid_time]),
      "valid_time" => DateTime.utc_now |> DateTime.to_date |> Date.to_string
    }
  end
end
