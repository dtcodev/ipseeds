defmodule Ipseeds.App.UserContactController do
  use Ipseeds.Web, :controller

  alias Ipseeds.UserContact
  
  def index(conn, _params) do
    current_user_id = conn.assigns.current_user.id
    query = from uc in UserContact,
            join: contact in assoc(uc, :contact_user),
            select: uc,
            where: uc.user_id == ^current_user_id,
            order_by: [asc: contact.username],
            preload: [contact_user: contact]
    user_contacts = Repo.all(query)

    render(conn, "index.html", user_contacts: user_contacts)
  end

  def create(conn, %{"user_contact" => user_contact_params}) do
    user_contact_params = Map.put(user_contact_params, "user_id", conn.assigns.current_user.id)
    changeset = UserContact.changeset(%UserContact{}, user_contact_params)

    case Repo.insert(changeset) do
      {:ok, user_contact} ->
        conn
        |> put_status(:created)
        |> render("show.json", user_contact: user_contact)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Ipseeds.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user_contact = Repo.get!(UserContact, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(user_contact)

    send_resp(conn, :no_content, "")
  end
end
