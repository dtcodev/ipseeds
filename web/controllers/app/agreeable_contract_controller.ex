defmodule Ipseeds.App.AgreeableContractController do
  use Ipseeds.Web, :controller

  import Ipseeds.ControllerHelpers
  import Ipseeds.ParamPlug

  alias Ecto.Multi
  alias Ipseeds.{AgreeableContract, API, User, EnumHelpers, AgreeableContractSenderSignature, ExagonHelpers,
    AttachmentHelpers}

  require Logger

  plug :strip_tags, "agreeable_contract" when action in [:create, :update]
  plug :scrub_params, "agreeable_contract" when action in [:create, :update]

  def new(conn, _params) do
  	changeset = AgreeableContract.changeset(%AgreeableContract{})
    conn |> render_new_html(changeset)
  end

  def create(conn, %{"agreeable_contract" => params}) do
    insert_nda = insert_nda_to_db(params, conn.assigns.current_user.id)
    case Repo.transaction(insert_nda) do
      {:ok, changeset_for_db} ->
        changeset_for_submit = AgreeableContract.issue_changeset(%AgreeableContract{}, 
          conn.assigns.current_user, AgreeableContract.issue_params(params))
        Logger.error "params====> #{inspect params}"
        if changeset_for_submit.valid? do
          
          case submit_nda_asset(conn, changeset_for_submit) do
            {conn, {:ok, %{body: %{"asset_id" => asset_id, "error_code" => 0}}}} ->
              contract_changes = changeset_for_db.agreeable_contract
                    |> AgreeableContract.changeset(%{"draft_asset_id" => asset_id})
              case Repo.update(contract_changes) do
                {:ok, contract} ->      
                  {conn, result} = send_asset(conn, asset_id, 
                    AgreeableContract.issue_params(params)["recipient_user_id"])
                  case result do
                    {:ok, %{body: %{"txid" => _tx_id, "error_code" => 0}}} ->

                      # Send mail to recipient
                      recipient_user = Repo.get(User, contract.recipient_user_id)
                      sender_user = conn.assigns.current_user
                      send_new_pending_agreeable_contract_email(recipient_user, sender_user, asset_id)
                      
                      conn
                      |> put_flash_success(gettext("The NDA was issued successfully and is now pending acceptance by the recipient."))
                      |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
                    _ ->
                      rollback_agreeable_contract(changeset_for_db.agreeable_contract)
                      Logger.debug "Asset not sent to receiver_asset_addr of recipient, changeset:"
                      Logger.debug inspect(changeset_for_db)
                      changeset = Map.put(changeset_for_db, :action, :insert)
                      conn
                      |> put_flash_danger(gettext("Failed to create the NDA, please try again."))
                      |> render_new_html(changeset)
                  end
                _ ->
                  rollback_agreeable_contract(changeset_for_db.agreeable_contract)
                  Logger.debug "Draft Asset ID not stored to DB, changeset:"
                  Logger.debug inspect(contract_changes)
                  changeset = Map.put(contract_changes, :action, :insert)
                  conn
                  |> put_flash_danger(gettext("Failed to create the NDA, please try again."))
                  |> render_new_html(changeset)
              end
              
            {conn, error_body} ->
              rollback_agreeable_contract(changeset_for_db.agreeable_contract)
              Logger.error "API Error:"
              Logger.error inspect(error_body)
              changeset = Map.put(changeset_for_submit, :action, :insert)
              conn
              |> put_flash_danger(gettext("Failed to create the NDA, please try again."))
              |> render_new_html(changeset)
            _ ->
              rollback_agreeable_contract(changeset_for_db.agreeable_contract)
              Logger.debug "Asset not created to API, changeset:"
              Logger.debug inspect(changeset_for_submit)
              changeset = Map.put(changeset_for_submit, :action, :insert)
              conn
              |> put_flash_danger(gettext("Failed to create the NDA, please try again."))
              |> render_new_html(changeset)
          end

        else

          rollback_agreeable_contract(changeset_for_db.agreeable_contract)
          Logger.error "Issue data is invalid:"
          Logger.error inspect(changeset_for_submit)
          changeset = Map.put(changeset_for_submit, :action, :insert)
          conn
          |> put_flash_danger(gettext("Failed to create the NDA, please ensure that you have filled in all the required fields and uploaded your signature."))
          |> render_new_html(changeset)

        end
        
      {:error, _failed_operations, failed_changeset, _changes_so_far} ->
        Logger.debug "Asset not created to DB, changeset:"
        Logger.debug inspect(failed_changeset)
        conn
        |> put_flash_danger(gettext("Failed to create the NDA, please try again."))
        |> render_new_html(failed_changeset)
    end
  end

  def show(conn, %{"id" => asset_id}) do
    agreeable_contract = AgreeableContract
      |> Repo.get_by(draft_asset_id: asset_id)
    if agreeable_contract do
      current_character = character(agreeable_contract, conn.assigns.current_user.id)
      case current_character do
        :unknow ->
          conn
          |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
        _->
          agreement_status = AgreeableContract.agreement_status(agreeable_contract)
          with :accepted <- agreement_status,
            {:ok, %{body: %{} = asset_params}} <- API.call_endpoint("/asset/query",
                asset_id: agreeable_contract.final_asset_id) do
              asset = asset_params |> EnumHelpers.atomize_keys()
              asset_description = asset.description 
                |> String.replace("'", "\"") 
                |> Poison.decode! 
                |> EnumHelpers.atomize_keys()
              sender_signature_url = asset_description.var_data["sender_signature_url"]
              recipient_signature_url = asset_description.var_data["recipient_signature_url"]
              render(conn, "show.html", agreement: asset, user_character: current_character, 
                sender_signature_url: sender_signature_url, 
                recipient_signature_url: recipient_signature_url,
                nda: asset_description.var_data |> EnumHelpers.atomize_keys(), 
                asset_id: asset_id)
          else
            _->
              Logger.info "AGREEMENT STATUS =====> #{agreement_status}"
              conn
              |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
          end
      end
    else
      Logger.info "============= AGREEMENT NOT FOUND!!! ==============="
      conn
        |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
    end
  end

  def edit(conn, %{"id" => asset_id}) do
    agreeable_contract = AgreeableContract
      |> Repo.get_by!(draft_asset_id: asset_id)

    
    current_character = character(agreeable_contract, conn.assigns.current_user.id)
    case current_character do
      :unknow ->
        conn
        |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
      _->
        agreement_status = AgreeableContract.agreement_status(agreeable_contract)
        with :pending <- agreement_status,
          {:ok, %{body: %{} = asset_params}} <- API.call_endpoint("/asset/query", asset_id: asset_id) do
            asset = asset_params |> EnumHelpers.atomize_keys()
            changeset = AgreeableContract.changeset(%AgreeableContract{})
            asset_description = asset.description 
              |> String.replace("'", "\"") 
              |> Poison.decode! 
              |> EnumHelpers.atomize_keys()
            sender_signature_url = asset_description.var_data["sender_signature_url"]

            conn
              |> ExagonHelpers.put(attachment_max_size_bytes: AgreeableContract.attachment_max_size_bytes(),
                file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
                size_limit: AttachmentHelpers.human_readable_attachment_max_size(AgreeableContract))
              )
              |> render("edit.html", agreement: asset, user_character: current_character, 
                sender_signature_url: sender_signature_url, 
                nda: asset_description.var_data |> EnumHelpers.atomize_keys(), 
                asset_id: asset_id, changeset: changeset)
        else
          _->
            conn
            |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
        end
    end
  end

  def update(conn,  %{"agreeable_contract" => params, "id" => asset_id}) do
    # Get the NDA record from DB (find by draft_asset_id)
    agreement_db = AgreeableContract
      |> Repo.get_by!(draft_asset_id: asset_id)
    agreement_status = agreement_db |> AgreeableContract.agreement_status

    if agreement_db do
      # Check whether the user is the NDA recipient or not
      # Check the NDA status
      with true <- agreement_db.recipient_user_id == conn.assigns.current_user.id,
        :pending <- agreement_status do
        # Can accept it.

        # get original_asset from API
        {:ok, %{body: %{} = asset_params}} = query_asset_by_API(asset_id)
        original_asset_atom = asset_params |> EnumHelpers.atomize_keys()
        sender_signature_url = AgreeableContractSenderSignature.url(original_asset_atom.image_url, agreement_db)

        # Do process of accepting...
        # Prepares the final agreement asset data
        Logger.info inspect params
        accept_changeset = AgreeableContract.accept_changeset(%AgreeableContract{}, original_asset_atom, params)

        # Check whether the data valid or not
        if accept_changeset.valid? do
          # Create, issue and send the final agreement asset to recipient and sender
          case submit_final_agreement_asset(conn, accept_changeset, agreement_db.sender_user_id) do
            {conn, {:ok, %{body: %{"final_asset_id"=> final_asset_id, "tx_id_sender" => _tx_id_sender, 
              "error_code" => 0}}}} ->
                Logger.debug "{{{{accept_changeset.changes}}}} ================> #{inspect accept_changeset.changes}"
                Logger.info "{{{{final_asset}}}} ================> #{final_asset_id}"
              # Update final_asset _id of agreement to DB 
              final_changes = agreement_db
                |> AgreeableContract.final_asset_changeset(%{"final_asset_id" => final_asset_id})
              case Repo.update(final_changes) do
                {:ok, agreement} ->
                  # Update data successfully

                  # Send e-mail to sender
                  recipient_user = Repo.get(User, agreement.sender_user_id)
                  sender_user = conn.assigns.current_user
                  send_status_changed_agreeable_contract_email(recipient_user, sender_user, asset_id)
                  # Show successful flash message and redirect to show page
                  conn 
                  |> put_flash_success(gettext("The NDA was accepted successfully."))
                  |> redirect(to: app_agreeable_contract_path(conn, :show, conn.assigns.locale, asset_id))
                _->
                  # Update data to DB is fail...
                  conn
                    |> put_flash_danger(gettext("Failed to accept the NDA, please try again."))
                    |> redirect(to: app_agreeable_contract_path(conn, :edit, conn.assigns.locale, asset_id))
              end

            _->
              # Fail to create or issue or send the final agreement asset...
              conn
                |> put_flash_danger(gettext("Failed to accept the NDA, please try again."))
                |> redirect(to: app_agreeable_contract_path(conn, :edit, conn.assigns.locale, asset_id))
          end

        else
          # The data of user input is invalid...
          current_character = character(agreement_db, conn.assigns.current_user.id)
          asset_description = original_asset_atom.description 
              |> String.replace("'", "\"") 
              |> Poison.decode! 
              |> EnumHelpers.atomize_keys()

          conn
          |> put_flash_danger(gettext("Failed to accept the NDA, please ensure that you have filled in all the required fields and uploaded your signature."))
          |> ExagonHelpers.put(attachment_max_size_bytes: AgreeableContract.attachment_max_size_bytes(),
              file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
                size_limit: AttachmentHelpers.human_readable_attachment_max_size(AgreeableContract))
            )
          |> render("edit.html", agreement: original_asset_atom,
               user_character: current_character,
               sender_signature_url: sender_signature_url,
               nda: asset_description.var_data |> EnumHelpers.atomize_keys(),
               asset_id: asset_id, changeset: accept_changeset)
        end

      else
        false -> 
          # Current user isn't the NDA sender.
          conn |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
        _ ->
          # Status isn't pending.
          if agreement_status == :accepted do
            conn
          else
            conn
            |> put_flash_danger(gettext("The status of the NDA has already been changed by the sender, you can no longer accept it. Please contact the sender of NDA for more details."))
          end
            |> redirect(to: app_agreeable_contract_path(conn, :show, conn.assigns.locale, asset_id))
      end
    else
      # Agreement record not found...
      conn |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
    end
  end

  def decline(conn, %{"id" => asset_id}) do
    # Get the NDA record from DB (find by draft_asset_id)
    agreeable_contract = AgreeableContract
      |> Repo.get_by!(draft_asset_id: asset_id)
    agreement_status = agreeable_contract |> AgreeableContract.agreement_status 
    # Check whether the user is the NDA recipient or not
    # Check the NDA status
    with true <- agreeable_contract.recipient_user_id == conn.assigns.current_user.id,
      :pending <- agreement_status do
      # Can decline it.

      # Write declined datetime to Database
      decline_nda = agreeable_contract 
        |> AgreeableContract.decline_changeset(%{"declined_at" => DateTime.utc_now})
        |> Repo.update

      case decline_nda do
        {:ok, updated_nda} ->

          # Send status changed mail to the "Sender"
          recipient_user = Repo.get(User, updated_nda.sender_user_id)
          sender_user = conn.assigns.current_user
          send_status_changed_agreeable_contract_email(recipient_user, sender_user, asset_id)

          # show successfully message
          conn
          |> put_flash_success(gettext("The NDA was declined successfully."))
        {:error, _changeset} ->
          # show fail message
          conn
          |> put_flash_danger(gettext("Failed to decline the NDA, please try again."))
      end
      |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))

    else
      false -> 
        # Current user isn't the NDA sender.
        conn |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
      _->
        # Status isn't pending.
        asset_id = if agreement_status == :accepted do
          agreeable_contract.final_asset_id
        else
          asset_id
        end
        conn
        |> put_flash_danger(gettext("The status of the NDA has already been changed by the sender, you can no longer decline it. Please contact the sender of NDA for more details."))
        |> redirect(to: app_agreeable_contract_path(conn, :show, conn.assigns.locale, asset_id))
    end
  end

  def cancel(conn, %{"id" => asset_id}) do
    # Get the NDA record from DB (find by draft_asset_id)
    agreeable_contract = AgreeableContract
      |> Repo.get_by!(draft_asset_id: asset_id)
    agreement_status = agreeable_contract |> AgreeableContract.agreement_status 
    # Check whether the user is the NDA sender or not
    # Check the NDA status
    with true <- agreeable_contract.sender_user_id == conn.assigns.current_user.id,
      :pending <- agreement_status do
      # Can cancel it.

      # Write cancelled datetime to Database
      cancel_nda = agreeable_contract 
        |> AgreeableContract.cancel_changeset(%{"cancelled_at" => DateTime.utc_now})
        |> Repo.update
      case cancel_nda do
        {:ok, updated_nda} ->

          # Send status changed mail to the "Recipient"
          recipient_user = Repo.get(User, updated_nda.recipient_user_id)
          sender_user = conn.assigns.current_user
          send_status_changed_agreeable_contract_email(recipient_user, sender_user, asset_id)

          # show successfully message
          conn
          |> put_flash_success(gettext("The NDA was cancelled successfully."))
          |> redirect(to: app_agreeable_contract_path(conn, :show, conn.assigns.locale, asset_id))
        {:error, _changeset} ->
          # show fail message
          conn
          |> put_flash_danger(gettext("Failed to cancel the NDA, please try again."))
          |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
      end
    else
      false -> 
        # Current user isn't the NDA sender.
        conn |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
      _->
        # Status isn't pending.
        conn
        |> put_flash_danger(gettext("The status of the NDA has already been changed by the recipient, you can no longer cancel it."))
        |> redirect(to: app_contract_path(conn, :index, conn.assigns.locale))
    end
  end

  defp submit_nda_asset(conn, changeset) do
    with true <- changeset.valid?,
      asset = changeset.changes,
      stringified_map = API.stringify_map(asset),
      issuer_address = get_session(conn, :issuer_address),
      payload = Map.put(stringified_map, "address", issuer_address),
      {conn, {:ok, success_resp}} <- API.call_authed_endpoint(conn, "/tradechain/asset/create", payload),
    do: {conn, {:ok, success_resp}}
  end

  # renew process of submit to API..
  defp submit_final_agreement_asset(conn, changeset, sender_user_id) do
    with true <- changeset.valid?,
      asset = changeset.changes,
      stringified_map = API.stringify_map(asset),
      issuer_address = get_session(conn, :issuer_address),
      payload = Map.put(stringified_map, "address", issuer_address),
      {conn, {:ok, %{body: %{"asset_id" => asset_id, "error_code" => 0}}}} 
        <- API.call_authed_endpoint(conn, "/tradechain/asset/create", payload),
      {conn, {:ok, %{body: %{"txid" => tx_id_sender, "error_code" => 0}}}} 
        <- send_asset(conn, asset_id, sender_user_id),
    do: {conn, {:ok, %{body: %{"final_asset_id"=> asset_id, "tx_id_sender" => tx_id_sender, "error_code" => 0}}}}
  end

  defp sender(user) do
    %{
      :name => User.get_name(user),
      :address => User.get_address(user),
      :nationality => User.get_nationality_name(user)
    }
  end

  defp insert_nda_to_db(%{"recipient_user_id" => recipient_user_id}, sender_user_id) do
    Multi.new
    |> Multi.insert(:agreeable_contract, AgreeableContract.changeset(%AgreeableContract{}, 
         %{"recipient_user_id" => recipient_user_id, "sender_user_id" => sender_user_id}))
  end

  defp send_asset(conn, asset_id, recipient_user_id) do
    payload = %{
      "address" => get_session(conn, :issuer_address)
    }
    {conn, balance_resp} = API.call_authed_endpoint(conn, "/tradechain/asset/balance", payload)
    case balance_resp do
      {:ok, %{body: %{"list" => asset_list}}} ->
        has_asset = Enum.any?(asset_list, fn asset ->
          asset["asset_id"] == asset_id
        end)

        {conn, _} = 
          case has_asset do
            false -> call_issue_api(conn, asset_id)
            _ -> {conn, "nothing happened"}
          end

        call_send_api(conn, asset_id, recipient_user_id)
      _ -> {conn, balance_resp}
    end
  end

  defp call_send_api(conn, asset_id, recipient_user_id) do
    recipient_user = Repo.get_by(User.is_active_query, id: recipient_user_id)
    if recipient_user do
      recipient_addr = recipient_user.receiver_asset_addr
      payload = %{
        "address" => get_session(conn, :issuer_address),
        "asset_id" => asset_id,
        "asset_addr_to" => recipient_addr,
        "quantity" => "1"
      }

      API.call_authed_endpoint(conn, "/tradechain/asset/send", payload)
    else
      {conn, {:error, gettext("Error!")}}
    end
  end

  defp call_issue_api(conn, asset_id) do
    payload = %{
      "asset_id" => asset_id,
      "address" => get_session(conn, :issuer_address),
      "quantity" => "10000"
    }
    API.call_authed_endpoint(conn, "/tradechain/asset/issue", payload)
  end

  defp rollback_agreeable_contract(agreeable_contract) do
    Multi.new
    |> Multi.delete(:agreeable_contract, agreeable_contract)
    |> Repo.transaction
  end

  defp character(agreeable_contract, current_user_id) do
    case agreeable_contract do
      %{sender_user_id: s_id} when s_id == current_user_id ->
        :sender
      %{recipient_user_id: r_id} when r_id == current_user_id ->
        :recipient
      _->
        :unknow
    end  
  end

  defp query_asset_by_API(asset_id) do
    API.call_endpoint("/asset/query", asset_id: asset_id)
  end

  defp send_new_pending_agreeable_contract_email(recipient_user, sender_user, asset_id) do
    Ipseeds.Email.new_pending_agreeable_contract_email(recipient_user, sender_user, asset_id)
    |> Ipseeds.Mailer.deliver_later()
  end

  defp send_status_changed_agreeable_contract_email(recipient_user, sender_user, asset_id) do
    Ipseeds.Email.status_changed_agreeable_contract_email(recipient_user, sender_user, asset_id)
    |> Ipseeds.Mailer.deliver_later()
  end

  defp render_new_html(conn, changeset) do
    conn
      |> ExagonHelpers.put(attachment_max_size_bytes: AgreeableContract.attachment_max_size_bytes(),
        file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
          size_limit: AttachmentHelpers.human_readable_attachment_max_size(AgreeableContract))
      )
      |> render("new.html", changeset: changeset, sender: sender(conn.assigns.current_user))
  end
end
