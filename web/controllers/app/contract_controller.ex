defmodule Ipseeds.App.ContractController do
  use Ipseeds.Web, :controller
  use Ipseeds.AssetHelpers, asset_module: Ipseeds.Contract,
    asset_api_id_key: :tx_id, asset_db_id_key: :api_transaction_id

  import Ipseeds.ParamPlug
  
  require Logger

  alias Ecto.Multi
  alias Ipseeds.{API, Contract, ContractAttachment, EnumHelpers, User, AgreeableContract, ExagonHelpers,
    AttachmentHelpers}

  plug :strip_tags, "contract" when action in [:create, :update]
  plug :scrub_params, "contract" when action in [:create, :update]

  def index(conn, _params) do
    user = conn.assigns.current_user
    receiver_address = get_session(conn, :receiver_address)
    {conn, contracts_from_api} = 
      with {conn, sent_contracts} <- query_for_sent_contracts(conn),
           {conn, received_contracts} <- query_for_received_contracts(conn, receiver_address),
      do: {conn, Enum.concat(sent_contracts, received_contracts)}

    mix_contracts =
      case Enum.empty?(contracts_from_api) do
        true -> []
        _ ->
          contracts_query = contracts_from_api
            |> Enum.map(fn c -> c["tx_id"] end)
            |> get_contracts_query(user)

          contracts_results = contracts_from_api
            |> Enum.map(&EnumHelpers.atomize_keys/1)
            |> merge_db_fields(contracts_query)

          contracts_from_api
          |> Enum.map(&EnumHelpers.atomize_keys/1)
          |> merge_recipient_user_id_by_receive_address()
          |> Enum.filter(fn(x) -> Map.has_key?(x, :status) end)
          |> Enum.concat(contracts_results)
          |> Enum.sort(fn (a, b) -> a[:tx_time] > b[:tx_time] end)

      end

    render(conn, "index.html", contracts: mix_contracts)
  end

  def new(conn, _params) do
    changeset = Contract.changeset(%Contract{})

    conn
    |> ExagonHelpers.put(attachment_max_size_bytes: Contract.attachment_max_size_bytes(),
        file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
          size_limit: AttachmentHelpers.human_readable_attachment_max_size(Contract))
      )
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"contract" => contract_params, "ip_notary_asset_id" => asset_id,
    "main_attachment" => attachment}) do

    conn =
      conn
      |> ExagonHelpers.put(attachment_max_size_bytes: Contract.attachment_max_size_bytes(),
          file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
          size_limit: AttachmentHelpers.human_readable_attachment_max_size(Contract))
        )

    params = contract_params
      |> Map.put("issuer_user_id", conn.assigns.current_user.id)
      |> Map.put("api_sent_asset_id", asset_id)
      |> Map.put("api_transaction_id", "!TEMP_TX_ID!")

    action_batch = insert_contract_to_db(params, attachment)

    case Repo.transaction(action_batch) do
      {:ok, changeset} ->
        {conn, result} = send_asset(conn, asset_id, contract_params["recipient_user_id"])
        case result do
          {:ok, %{body: %{"txid" => tx_id}}} ->
            contract_changes = changeset.contract
              |> Contract.changeset(%{"api_transaction_id" => tx_id})

            case Repo.update(contract_changes) do
              {:ok, contract} ->
                recipient_user = Repo.get(User, contract.recipient_user_id)
                send_new_data_contract_email(recipient_user, contract)

                conn
                |> put_status(:created)
                |> render("create_success.json",
                  redirect_to: app_contract_path(conn, :index, conn.assigns.locale))
              _ ->
                conn
                |> put_status(:unprocessable_entity)
                |> render(Ipseeds.ChangesetView, "error.json", changeset: contract_changes)
            end
          _ ->
            rollback_contract(changeset)
            render(conn, Ipseeds.ErrorView, "403.json", %{message: gettext("API error")})
        end
      {:error, _failed_operations, failed_changeset, _changes_so_far} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Ipseeds.ChangesetView, "error.json", changeset: failed_changeset)
    end
  end

  def show(conn, %{"id" => api_transaction_id}) do
    current_user = conn.assigns.current_user
    contract_query = get_contract_query(api_transaction_id, current_user)
    contract = Repo.one!(contract_query)
    asset = get_contract_asset(contract)
    current_user_active_keypair = User.active_keypair(current_user)
    recipient_name = User.get_name(contract.recipient)
    issuer_name = User.get_name(contract.issuer)

    conn
    |> Ipseeds.ExagonHelpers.put(%{
        current_user_bip32_public_key: current_user_active_keypair.bip32_hd_public_key,
        main_attachment: Map.take(contract.main_attachment, [:mime_type, :original_filename, :s3_filename]),
        main_attachment_url: Ipseeds.ContractAttachmentUploader.url(
          {contract.main_attachment.s3_filename, contract.main_attachment}, :original),
        main_attachment_hash: asset.hash_attachment
      })
    |> render("show.html", contract: contract, asset: asset, recipient_name: recipient_name,
      issuer_name: issuer_name)
  end

  def edit(conn, %{"id" => id}) do
    contract = Repo.get!(Contract, id)
    changeset = Contract.changeset(contract)
    render(conn, "edit.html", contract: contract, changeset: changeset)
  end

  def update(conn, %{"id" => id, "contract" => contract_params}) do
    contract = Repo.get!(Contract, id)
    changeset = Contract.changeset(contract, contract_params)

    case Repo.update(changeset) do
      {:ok, contract} ->
        conn
        |> put_flash(:info, "Contract updated successfully.")
        |> redirect(to: app_contract_path(conn, :show, conn.assigns.locale, contract))
      {:error, changeset} ->
        render(conn, "edit.html", contract: contract, changeset: changeset)
    end
  end

  def merge_db_fields([], _query), do: []
  def merge_db_fields(contract = %{}, query), do: merge_db_fields([contract], query)
  def merge_db_fields(contracts, contracts_query) when is_list(contracts) do
    contract_map = EnumHelpers.mapify_map_list(contracts, :tx_id)

    contracts_query
    |> Repo.all
    |> Enum.map(fn db_record ->
      contract_map[db_record.api_transaction_id]
      |> Map.merge(%{status: nil})
      |> EnumHelpers.merge_map_fields(db_record, [:recipient_user_id, :recipient, :issuer])
    end)
  end

  defp send_asset(conn, asset_id, recipient_user_id) do
    payload = %{address: get_session(conn, :issuer_address)}
    {conn, balance_resp} = API.call_authed_endpoint(conn, "/tradechain/asset/balance", payload)
    case balance_resp do
      {:ok, %{body: %{"list" => asset_list}}} ->
        has_asset = Enum.any?(asset_list, fn asset ->
          asset["asset_id"] == asset_id
        end)

        {conn, _} = 
          case has_asset do
            false -> call_issue_api(conn, asset_id)
            _ -> {conn, "nothing happened"}
          end

        call_send_api(conn, asset_id, recipient_user_id)
      _ -> {conn, balance_resp}
    end
  end

  defp call_send_api(conn, asset_id, recipient_user_id) do
    recipient_user = Repo.get_by(User.is_active_query, id: recipient_user_id)
    if recipient_user do
      recipient_addr = recipient_user.receiver_asset_addr
      payload = %{
        "address" => get_session(conn, :issuer_address),
        "asset_id" => asset_id,
        "asset_addr_to" => recipient_addr,
        "quantity" => "1"
      }

      API.call_authed_endpoint(conn, "/tradechain/asset/send", payload)
    else
      {conn, {:error, gettext("Error!")}}
    end
  end

  defp call_issue_api(conn, asset_id) do
    payload = %{
      "asset_id" => asset_id,
      "address" => get_session(conn, :issuer_address),
      "quantity" => "10000"
    }
    API.call_authed_endpoint(conn, "/tradechain/asset/issue", payload)
  end

  defp insert_contract_to_db(contract_params, attachment_params) do
    Multi.new
    |> Multi.insert(:contract, Contract.changeset(%Contract{}, contract_params))
    |> Multi.run(:main_attachment, &insert_main_attachment(attachment_params, &1))
    |> Multi.run(:add_main_attachment_id, &add_main_attachment_id(&1))
  end

  defp insert_main_attachment(attachment_params, changes) do
    changes.contract
    |> build_assoc(:attachments)
    |> ContractAttachment.create_changeset(attachment_params)
    |> Repo.insert
  end

  defp add_main_attachment_id(changes) do
    main_attachment_id = changes.main_attachment.id

    changes.contract
    |> Ecto.Changeset.change(main_attachment_id: main_attachment_id)
    |> Repo.update
  end

  defp rollback_contract(%{contract: contract, main_attachment: attachment}) do
    Multi.new
    |> Multi.delete(:main_attachment, attachment)
    |> Multi.delete(:contract, contract)
    |> Repo.transaction
  end

  defp get_contract_asset(contract) do
    case API.call_endpoint("/asset/query", asset_id: contract.api_sent_asset_id) do
      {:ok, %{body: %{} = asset_params}} -> EnumHelpers.atomize_keys(asset_params)
      _ -> nil
    end
  end

  defp get_contract_query(api_transaction_id, current_user) do
    from c in Contract,
      join: i in assoc(c, :issuer),
      join: r in assoc(c, :recipient),
      join: ma in assoc(c, :main_attachment),
      where: c.api_transaction_id == ^api_transaction_id and
        (c.issuer_user_id == ^current_user.id or c.recipient_user_id == ^current_user.id),
      preload: [issuer: i, recipient: r, main_attachment: ma]
  end

  defp send_new_data_contract_email(recipient_user, contract) do
    issuer_user = Repo.get!(User, contract.issuer_user_id)
    Ipseeds.Email.new_data_contract_email(recipient_user, issuer_user)
    |> Ipseeds.Mailer.deliver_later()
  end

  defp query_for_sent_contracts(conn) do
    payload = %{
      "address" => get_session(conn, :issuer_address),
      "tx_time" => 0
    }

    {conn, resp} = API.call_authed_endpoint(conn, "/tradechain/asset/txs", payload)
    case resp do
      {:ok, %{body: %{"list" => contracts_list, "error_code" => 0}}} ->
        {conn, contracts_list}
      _ -> {conn, []}
    end
  end

  defp query_for_received_contracts(conn, receiver_asset_addr) do
    payload = %{
      "address" => receiver_asset_addr,
      "tx_time" => 0
    }

    {conn, resp} = API.call_authed_endpoint(conn, "/tradechain/asset/txs", payload)
    case resp do
      {:ok, %{body: %{"list" => contracts_list}}} ->
        {conn, contracts_list}
      _ -> {conn, []}
    end
  end

  defp get_contracts_query(api_transaction_ids, _current_user) do
    from c in Contract,
      join: i in assoc(c, :issuer),
      join: r in assoc(c, :recipient),
      where: c.api_transaction_id in ^api_transaction_ids,
      preload: [issuer: i, recipient: r]
  end

  defp merge_recipient_user_id_by_receive_address(nda_map) do
    nda_map
    |> Enum.map(fn tx ->
      nda =  get_agreeable_contract_by_draft_asset_id(tx.asset_id)
      if nda do
        Map.merge(tx, %{recipient: nda.recipient, issuer: nda.sender, 
          recipient_user_id: nda.recipient_user_id, sender_user_id: nda.sender_user_id, 
          status: AgreeableContract.agreement_status(nda) })
      else
        tx
      end
    end)
    |> Enum.filter(fn(x) -> 
        if Map.has_key?(x, :status) do
          x.recipient_user_id != nil && (x.category != "AssetIssue") 
        else
          x
        end
       end)
  end

  defp get_agreeable_contract_by_draft_asset_id(asset_id) do
    Repo.get_by(AgreeableContract, draft_asset_id: asset_id)
    |> Repo.preload([:recipient, :sender])
  end
end
