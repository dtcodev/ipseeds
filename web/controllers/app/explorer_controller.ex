defmodule Ipseeds.App.ExplorerController do
  use Ipseeds.Web, :controller

  alias Ipseeds.API

  @default_api_error_message gettext("An API error occurred, please try again later")

  def index(conn, params) do
    page = Map.get(params, "page", "1")

    requset_params = %{
      "page_index" => page,
      "entries_per_page" => 20
    }

    case API.call_endpoint("/explore/entries/page", requset_params) do
      {:ok, %{body: %{"entry_list" => entries, "current_page" => current_page, "entries_per_page" => entries_per_page,
        "total_pages" => total_pages}}} ->

        render(conn, "index.html", entries: entries,
          pages: %{page_number: current_page, page_size: entries_per_page, total_pages: total_pages})

      {:error, message} ->
        render_error_page(conn, message)

      _ ->
        render_error_page(conn, @default_api_error_message)
    end
  end

  def show(conn, %{"id" => entry_id}) do
    with \
      {:ok, entry_verify} <- get_entry_verify(entry_id),
      {:ok, block_id} <- get_block_id(entry_id),
      {:ok, block_verify} <- get_block_verify(block_id),
      {:ok, anchorinfo} <- get_anchorinfo(block_id)
    do
      render(conn, "show.html", entry_id: entry_id, entry_verify: entry_verify, block_verify: block_verify,
        anchorinfo: anchorinfo)
    else
      {:error, message} -> render_error_page_show(conn, message)
    end
  end

  defp get_entry_verify(entry_id) do
    # GET API/explore/entry_verify/#{entry_id} => entry_verify
    case API.call_endpoint("/explore/entry_verify", %{entry_id: entry_id}) do
      {:ok, %{body: %{"raw" => _} = entry_verify}} -> {:ok, entry_verify}

      {:error, message} -> {:error, message}

      _ -> {:error, @default_api_error_message}
    end
  end

  defp get_block_id(entry_id) do
    # GET API/explore/block_with_entry/#{entry_id} => block_with_entry
    case API.call_endpoint("/explore/block_with_entry", entry_id: entry_id) do
      {:ok, %{body: %{"id" => block_id}}} -> {:ok, block_id}

      {:error, message} -> {:error, message}

      _ -> {:error, @default_api_error_message}
    end
  end

  defp get_block_verify(block_id) do
    # GET API/explore/block_verify/#{block_with_entry[:id]} => block_verify
    case API.call_endpoint("/explore/block_verify", block_id: block_id) do
      {:ok, %{body: %{"pre_hash" => _} = block_verify}} -> {:ok, block_verify}

      {:ok, %{status_code: status_code}} when status_code == 404 -> 
        {:ok, %{"pre_hash" => "", "full_hash" => "", "entries" => []}}

      {:error, message} -> {:error, message}

      _ -> {:error, @default_api_error_message}
    end
  end

  defp get_anchorinfo(block_id) do
    # GET API/explore/anchorinfo/#{block_with_entry[:id]} => anchorinfo
    case API.call_endpoint("/explore/anchorinfo", block_id: block_id) do
      {:ok, %{body: %{"address" => _} = anchorinfo}} -> {:ok, anchorinfo}

      {:ok, %{status_code: status_code}} when status_code == 404 -> {:ok, %{"txid" => ""}}

      {:error, message} -> {:error, message}

      _ -> {:error, @default_api_error_message}
    end
  end

  defp render_error_page(conn, message) do
    conn
    |> put_flash_danger(message)
    |> render("index.html", entries: [], pages: %{page_number: 1, page_size: 20, total_pages: 0})
  end

  defp render_error_page_show(conn, message) do
    conn
    |> put_flash_danger(message)
    |> redirect(to: explorer_index_page(conn))
  end

  defp explorer_index_page(%Plug.Conn{private: %{phoenix_layout: {Ipseeds.LayoutView, :public}}} = conn) do
    explorer_path(conn, :index, conn.assigns.locale)
  end
  defp explorer_index_page(%Plug.Conn{private: %{phoenix_layout: {Ipseeds.LayoutView, :app}}} = conn) do
    app_explorer_path(conn, :index, conn.assigns.locale)
  end
  defp explorer_index_page(conn), do: explorer_path(conn, :index, conn.assigns.locale)
end
