defmodule Ipseeds.App.IpNotaryController do
  use Ipseeds.Web, :controller
  use Ipseeds.AssetHelpers, asset_module: Ipseeds.IpNotary

  import Ipseeds.ParamPlug

  alias Ipseeds.{API, BlockTackAPI, EnumHelpers, ExagonHelpers, IpNotary, AttachmentHelpers}

  require Logger

  plug :strip_tags, "ip_notary" when action in [:create]
  plug :scrub_params, "ip_notary" when action in [:create]

  def index(conn, params) do
    {conn, ip_notaries} = get_ip_notaries_list(conn, address: get_session(conn, :issuer_address))

    ip_notaries =
      ip_notaries
      |> Enum.map(&EnumHelpers.atomize_keys/1)
      |> add_assets_to_local_db(conn)
      |> sort_assets()

    paging_config = get_paging_params(params)
    ip_notaries = Scrivener.paginate(ip_notaries, paging_config)

    render(conn, "index.html", ip_notaries: ip_notaries)
  end

  def new(conn, _params) do
    today = DateTime.utc_now |> DateTime.to_date |> Date.to_string
    changeset = IpNotary.changeset(%IpNotary{}, %{"valid_time" => today})
    conn
    |> ExagonHelpers.put(attachment_max_size_bytes: IpNotary.attachment_max_size_bytes(),
        file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
          size_limit: AttachmentHelpers.human_readable_attachment_max_size(IpNotary))
        )
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"ip_notary" => ip_notary_params}) do
    changeset = IpNotary.create_changeset(%IpNotary{}, ip_notary_params)

    conn =
      conn
      |> ExagonHelpers.put(attachment_max_size_bytes: IpNotary.attachment_max_size_bytes(),
        file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
          size_limit: AttachmentHelpers.human_readable_attachment_max_size(IpNotary))
        )

    
    today = DateTime.utc_now |> DateTime.to_date |> Date.to_string
    for_error_changeset = IpNotary.changeset(%IpNotary{}, Map.merge(%{"valid_time" => today}, ip_notary_params))
     
    case submit_ip_notary_asset(conn, changeset) do
      {conn, {:ok, %{body: %{"asset_id" => asset_id, "tack_id_BTC" => tack_id_BTC, "error_code" => 0}}}} ->
        conn
          |> put_flash_success(gettext("IP Notary created successfully. The blockchain information of this IP Notary is shown below."))
          |> finished_ip_notary_creating(changeset, asset_id, tack_id_BTC)
      {conn, {:ok, error_body}} ->
        if error_body.body["anchor_process"] == nil do
          conn
          |> put_flash_danger(gettext("Sorry, something went wrong. Please try again later. If this message appear continually, please contact us for support."))
          |> render("new.html", changeset: for_error_changeset)
        else
          %{"asset_id" => asset_id} = error_body.body
          conn |> finished_ip_notary_creating(changeset, asset_id)
        end

      {conn, {:invalid, fail_changeset}} ->
        conn 
        |> put_flash_danger(gettext("You must fill all of following fields and choose a file."))
        |> render("new.html", changeset: fail_changeset)

        
      {conn, {:error, e}} ->
        Logger.debug "Asset not submitted to API, there is an unexpected response:"
        Logger.debug "The changeset: #{inspect(changeset)}"
        conn
        |> put_flash_danger(gettext("Sorry, there is an unexpected error. Please try again later. If this message appears continually, please contact us for support."))
        |> render("new.html", changeset: for_error_changeset)
    end
    
  end

  def show(conn, %{"id" => asset_id}) do
    ip_notary = 
      IpNotary
      |> Repo.get_by!(api_asset_id: asset_id, user_id: conn.assigns.current_user.id)
    
    case show_ip_notary_detail(ip_notary.api_asset_id, ip_notary.tack_id_BTC) do
      {:ok, %{body: %{} = show_params}} ->
        asset = show_params
          |> EnumHelpers.atomize_keys()
          |> Map.merge(%{inserted_at: ip_notary.inserted_at})
          |> put_btc_tx_check_url()

        render(conn, "show.html", ip_notary: asset)
      _ ->
        conn
        |> put_flash_danger(gettext("API asset not found"))
        |> redirect(to: app_ip_notary_path(conn, :index, conn.assigns.locale))
    end

  end

  defp btc_explorer_tx_query_url do
    if Mix.env == :prod do
      "https://btc.com"
    else
      "https://live.blockcypher.com/btc-testnet/tx"
    end
  end

  defp put_btc_tx_check_url(asset) do
    if Map.has_key?(asset, :btc_tx_id), 
      do: Map.put(asset, :btc_tx_check_url, "#{btc_explorer_tx_query_url()}/#{asset.btc_tx_id}"),
      else: asset
  end
    

  defp get_paging_params(%{"page" => page}), do: [page: page, page_size: 25]
  defp get_paging_params(_), do: [page: 1, page_size: 25]

  defp submit_ip_notary_asset(conn, changeset) do
    try do
      with true <- changeset_valid?(conn, changeset),
        asset = changeset.changes,
        stringified_map = API.stringify_map(asset),
        issuer_address = get_session(conn, :issuer_address),
        payload = Map.put(stringified_map, "address", issuer_address),
        {conn, {:ok, %{body: %{} = success_resp_asset}}} <- 
          API.call_authed_endpoint(conn, "/tradechain/asset/create", payload),
        {conn, {:ok, %{body: %{"tack_id" => tack_id_BTC, "error_code" => error_code}}}} <- 
          anchor_to_BTC_blockchain(conn, issuer_address, stringified_map["hash_attachment"])
      do 
        {conn, {:ok, %{body: Map.merge(%{"tack_id_BTC" => tack_id_BTC, "error_code" => error_code}, 
          success_resp_asset)}}}
      end
    rescue
      e in MatchError -> {conn, {:error, e}}
    end  
  end

  defp changeset_valid?(conn, changeset) do
    if changeset.valid?, do: true, else: {conn, {:invalid, changeset}}
  end

  defp anchor_to_BTC_blockchain(conn, issuer_address, file_hash) do
    payload = %{"address" => issuer_address, "blockchain" => "bitcoin", "software" => "IPSeeds", 
        "hash_type" => "SHA256", "hash" => file_hash}
    {status, %{body: response}} = BlockTackAPI.call_endpoint("/proof", payload)
    {conn, {status, %{body: Map.merge(%{"anchor_process" => "BTC"}, response)}}}
  end

  defp finished_ip_notary_creating(conn, changeset, asset_id, tack_id_BTC \\ nil) do
    asset_updates = %{
      "api_asset_id" => asset_id,
      "tack_id_BTC" => tack_id_BTC,
      "user_id" => conn.assigns.current_user.id
    }
    {:ok, ip_notary} = 
      changeset
      |> IpNotary.changeset(asset_updates)
      |> Repo.insert()
    redirect(conn, to: app_ip_notary_path(conn, :show, conn.assigns.locale, ip_notary))
  end

  defp show_ip_notary_detail(api_asset_id, tack_id_BTC) do
    if tack_id_BTC != nil do
      with {:ok, %{body: %{} = asset_params}} <- API.call_endpoint("/asset/query", asset_id: api_asset_id),
         payload = %{"tack_id" => tack_id_BTC}, 
         {:ok, %{body: %{"tx_id" => btc_tx_id}}} <- BlockTackAPI.call_endpoint("/query", payload),
      do: {:ok, %{body: Map.merge(%{"btc_tx_id" => btc_tx_id}, asset_params)}}
    else
      API.call_endpoint("/asset/query", asset_id: api_asset_id)
    end
    
  end

  defp get_ip_notaries_list(conn, payload) do
    case API.call_authed_endpoint(conn, "/tradechain/asset/infos", payload) do
      {conn, {:ok, %{body: %{"list" => assets_list}}}} ->
        ip_notaries = assets_list 
          |> Enum.filter(fn (asset) -> asset["type"] == IpNotary.asset_type end)
        {conn, ip_notaries}
      {conn, _error_body} ->
        {conn, []}
      error ->
        Logger.error "Unexpected error:"
        Logger.error inspect(error)
        {conn, []}
    end
  end

  def sort_assets([]), do: []
  def sort_assets(assets) when is_list(assets) do
    Enum.sort(assets, fn (a, b) -> a[:create_time] > b[:create_time] end)
  end
  def sort_assets(_), do: []
end
