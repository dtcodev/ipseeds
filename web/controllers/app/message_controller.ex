defmodule Ipseeds.App.MessageController do
  use Ipseeds.Web, :controller
  import Ipseeds.ParamPlug, only: [strip_tags: 2]

  alias Ipseeds.{DateTimeHelpers, Message, User}

  plug :strip_tags, "message" when action in [:create, :update]

  def index(conn, _params) do
    current_user = conn.assigns.current_user
    current_user_active_keypair = User.active_keypair(current_user)
    query = index_query(current_user, current_user_active_keypair)

    conversations = Repo.all(query)
      |> Enum.map(fn conversation ->
          {:ok, last_message_at} = Ecto.DateTime.load(conversation[:last_message_at])
          last_message_at_timex = DateTimeHelpers.format_ecto_dt(last_message_at)
          Map.put(conversation, :last_message_at, last_message_at_timex)
        end)

    render(conn, "index.html", conversations: conversations, changeset: Message.changeset(%Message{}))
  end

  def new(conn, _params) do
    changeset = Message.changeset(%Message{})
    current_user = Repo.preload(conn.assigns.current_user, :contacts)
    current_user_public_key = User.active_keypair(current_user).ecc_public_key_hex
    contacts_for_select = current_user.contacts
      |> Stream.map(fn(contact) ->
        active_keypair = User.active_keypair(contact) || %{}
        public_key = Map.get(active_keypair, :ecc_public_key_hex)
        %{id: contact.id, username: contact.username, name: User.get_name(contact),
          public_key: public_key}
      end)
      |> Stream.reject(fn(contact_mapped) -> is_nil(contact_mapped[:public_key]) end)
      |> Enum.to_list

    render(conn, "new.html", changeset: changeset, contacts: contacts_for_select,
      current_user_public_key: current_user_public_key)
  end

  def create(conn, %{"message" => message_params}) do
    current_user = conn.assigns.current_user
    sender_keypair = User.active_keypair(current_user)
    recipient_keypair = User.active_keypair(%User{id: message_params["recipient_user_id"]})
    message_params = Map.merge(message_params, %{
      "sender_user_id" => current_user.id,
      "sender_keypair_id" => sender_keypair.id,
      "recipient_keypair_id" => recipient_keypair.id
    })
    changeset = Message.changeset(%Message{}, message_params)

    case Repo.insert(changeset) do
      {:ok, message} ->
        recipient_user = Repo.get(User, message.recipient_user_id)
        if recipient_user.message_notify_by_email, do: send_new_message_email(current_user, recipient_user)

        conn
        |> put_status(:created)
        |> render("create_success.json",
          redirect_to: app_message_path(conn, :index, conn.assigns.locale))
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Ipseeds.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"username" => username, "subject" => subject}) do
    system_user = User.system_username
    case username do
      ^system_user -> show_system_conversation(conn, subject)
      _ -> show_regular_conversation(conn, username, subject)
    end
  end

  defp show_regular_conversation(conn, username, subject) do
    current_user = conn.assigns.current_user
    current_user_active_keypair = User.active_keypair(current_user)
    other_user = Repo.get_by!(User, username: username)
    other_user_active_keypair = User.active_keypair(other_user)
    conversation = conversation_query(current_user, current_user_active_keypair, other_user, subject)
    messages = Repo.all(from m in conversation, order_by: [desc: m.inserted_at])

    if length(messages) > 0 do
      conn
      |> Ipseeds.ExagonHelpers.put(%{current_user_id: conn.assigns.current_user.id,
        other_user_id: other_user.id,
        current_user_bip32_public_key: current_user_active_keypair.bip32_hd_public_key,
        current_user_public_key: current_user_active_keypair.ecc_public_key_hex,
        other_user_public_key: other_user_active_keypair.ecc_public_key_hex,
        subject: subject,
        mark_as_read_path: app_message_path(conn, :mark_as_read, conn.assigns.locale, other_user.id, subject),
        messages: Ipseeds.App.MessageView.render("messages.json", messages: messages).data})
      |> render("show.html", messages: messages, current_user_name: User.get_name(current_user),
        other_user_name: User.get_name(other_user), other_user: other_user, subject: subject)
    else
      redirect(conn, to: app_message_path(conn, :new, conn.assigns.locale))
    end
  end

  defp show_system_conversation(conn, subject) do
    current_user = conn.assigns.current_user
    conversation = system_conversation_query(current_user, subject)
    messages = Repo.all(from m in conversation, order_by: [desc: m.inserted_at])
    if length(messages) > 0 do
      current_time = Timex.now
      from(m in conversation) |> Repo.update_all(set: [read_at: current_time])

      render(conn, "show_system.html", messages: messages, current_user_name: User.get_name(current_user),
        other_user_name: User.system_username, subject: subject)
    else
      redirect(conn, to: app_message_path(conn, :index, conn.assigns.locale))
    end
  end

  def edit(conn, %{"id" => id}) do
    message = Repo.get!(Message, id)
    changeset = Message.changeset(message)
    render(conn, "edit.html", message: message, changeset: changeset)
  end

  def update(conn, %{"id" => id, "message" => message_params}) do
    message = Repo.get!(Message, id)
    changeset = Message.changeset(message, message_params)

    case Repo.update(changeset) do
      {:ok, message} ->
        recipient_user = Repo.get(User, message.recipient_user_id)
        conn
        |> put_flash(:info, "Message updated successfully.")
        |> redirect(to: app_message_path(conn, :show, conn.assigns.locale, recipient_user.username,
            message.subject))
      {:error, changeset} ->
        render(conn, "edit.html", message: message, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    message = Repo.get!(Message, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(message)

    conn
    |> put_flash(:info, "Message deleted successfully.")
    |> redirect(to: app_message_path(conn, :index, conn.assigns.locale))
  end

  def mark_as_read(conn, %{"user_id" => user_id, "subject" => subject}) do
    current_user = conn.assigns.current_user
    current_user_active_keypair = User.active_keypair(current_user)
    other_user = Repo.get(User, user_id)

    all_messages_in_conversation =
      conversation_query(current_user, current_user_active_keypair, other_user, subject)

    unread_messages_to_current_user_in_conversation = from m in all_messages_in_conversation,
      where: m.recipient_user_id == ^current_user.id and is_nil(m.read_at)

    update_dt = DateTime.utc_now
    Repo.update_all(unread_messages_to_current_user_in_conversation,
      set: [read_at: update_dt, updated_at: update_dt])

    conn |> put_status(:ok) |> render("mark_as_read.json")
  end

  def send_system_message(recipient, subject, message) do
    message_params = %{recipient_user_id: recipient.id, recipient_encrypted_content: message,
      subject: subject}

    Message.system_message_changeset(%Message{}, message_params)
    |> Repo.insert
  end

  def index_query(current_user, current_user_active_keypair) do
    from m in Message,
      left_join: su in User, on: su.id == m.sender_user_id,
      left_join: ru in User, on: ru.id == m.recipient_user_id,
      select: %{
        user_id: fragment("(CASE WHEN (? IS TRUE) THEN ? WHEN (? = ?) THEN ? ELSE ? END) AS user_id",
          m.is_system_message, 0, m.sender_user_id, ^current_user.id,
          m.recipient_user_id, m.sender_user_id),
        user_username: fragment(
          "MAX(CASE WHEN (? IS TRUE) THEN ? WHEN (? = ?) THEN ? ELSE ? END) AS user_username",
          m.is_system_message, ^User.system_username, m.sender_user_id, ^current_user.id, ru.username,
          su.username),
        subject: m.subject,
        last_message_at: fragment("MAX(?) AS last_message_at", m.inserted_at),
        unread_count: fragment("SUM(CASE WHEN (? IS TRUE AND ? IS NULL) THEN 1 WHEN (? != ? AND ? IS NULL) THEN 1 ELSE 0 END) AS unread_count",
          m.is_system_message, m.read_at, m.sender_user_id, ^current_user.id, m.read_at)
      },
      where: (m.sender_user_id == ^current_user.id
          and m.sender_keypair_id == ^current_user_active_keypair.id)
        or (m.recipient_user_id == ^current_user.id
          and m.recipient_keypair_id == ^current_user_active_keypair.id)
        or (m.recipient_user_id == ^current_user.id and m.is_system_message == true),
      group_by: fragment("user_id, subject"),
      order_by: fragment("last_message_at DESC")
  end

  def conversation_query(current_user, current_user_active_keypair, other_user, subject) do
    from m in Message,
      where: (m.sender_user_id == ^current_user.id
          and m.recipient_user_id == ^other_user.id
          and m.sender_keypair_id == ^current_user_active_keypair.id
          and m.subject == ^subject)
        or (m.sender_user_id == ^other_user.id
          and m.recipient_user_id == ^current_user.id
          and m.recipient_keypair_id == ^current_user_active_keypair.id
          and m.subject == ^subject)
  end

  def system_conversation_query(current_user, subject) do
    from m in Message,
      where: (m.recipient_user_id == ^current_user.id
          and m.is_system_message == true
          and m.subject == ^subject)
  end

  defp send_new_message_email(sender_user, recipient_user) do
    Ipseeds.Email.new_message_email(sender_user, recipient_user)
    |> Ipseeds.Mailer.deliver_later()
  end
end
