defmodule Ipseeds.App.UserController do
  use Ipseeds.Web, :controller

  import Ipseeds.ParamPlug

  alias Ipseeds.{API, User, UserKeypair, UserContact, APIHelpers, ExagonHelpers, AttachmentHelpers}

  plug :strip_tags, "user" when action in [:update]
  plug :scrub_params, "user" when action in [:update]
  plug :scrub_params, "user_keypair" when action in [:create_passphrase]
  plug :check_profile_ownership when action in [:edit, :update]

  def show(conn, %{"id" => username}) do
    user = Repo.get_by!(User.is_active_query, username: username)
    current_user = conn.assigns.current_user
    is_contact? = UserContact.is_contact?(current_user, user)
    current_user_active_keypair = User.active_keypair(current_user)
    other_user_active_keypair = User.active_keypair(user)

    conn
    |> ExagonHelpers.put(current_user_id: current_user.id,
      other_user_id: user.id,
      current_user_public_key: current_user_active_keypair.ecc_public_key_hex,
      other_user_public_key: other_user_active_keypair.ecc_public_key_hex)
    |> render("show.html", user: user, is_contact?: is_contact?)
  end

  def edit(conn, %{"id" => username}) do
    user = Repo.get_by!(User.is_active_query, username: username)
    changeset = User.changeset(user)
    conn
    |> ExagonHelpers.put(profile_photo_size_limit: User.attachment_max_size_bytes(),
        file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
          size_limit: AttachmentHelpers.human_readable_attachment_max_size(User))
      )
    |> render("edit.html", user: user, changeset: changeset)
  end

  def update(conn, %{"id" => username, "user" => user_params}) do
    user = User |> Repo.get_by!(username: username)
    user_settings = user |> User.settings_changeset(user_params)

    case Repo.update(user_settings) do
      {:ok, _updated_user} ->
        conn
        |> put_flash_success(gettext("Settings updated successfully"))
        |> redirect(to: app_user_path(conn, :edit, conn.assigns.locale, username))
      {:error, changeset} ->
        conn
        |> ExagonHelpers.put(profile_photo_size_limit: User.attachment_max_size_bytes(),
            file_too_big_error: gettext("File exceeds maximum allowed size of %{size_limit}",
              size_limit: AttachmentHelpers.human_readable_attachment_max_size(User))
          )
        |> render("edit.html", user: user, changeset: changeset)
    end
  end

  def reset_password(conn, _params) do
    message = 
      case API.call_endpoint("/account/reset_password", email: conn.assigns.current_user.email) do
        {:ok, %{body: %{"error_code" => 0}}} ->
          gettext "Reset password request has been sent.\nPlease check your email for further instructions."
        {:ok, %{body: %{"error_code" => error_code}}} ->
          APIHelpers.full_api_error(error_code)
        _ ->
          APIHelpers.full_api_error()
      end

    render conn, "reset_password.json", message: message
  end

  def passphrase(conn, _params) do
    render(conn, "passphrase.html")
  end

  def create_passphrase(conn, %{"user_keypair" => keypair_params}) do
    new_keypair = 
      conn.assigns.current_user
      |> Ecto.build_assoc(:keypairs)
      |> UserKeypair.changeset(keypair_params)

    case Repo.insert(new_keypair) do
      {:ok, _keypair} ->
        conn
        |> redirect(to: app_user_path(conn, :show, conn.assigns.locale, conn.assigns.current_user.username))
      {:error, failed_changeset} ->
        IO.inspect failed_changeset
        conn
        |> redirect(to: app_user_path(conn, :passphrase, conn.assigns.locale))
    end
  end

  def update_profile_photo(conn, %{"id" => username, "user" => user_params}) do
    user = Repo.get_by!(User, username: username)
    changeset = User.profile_photo_changeset(user, user_params)

    case Repo.update(changeset) do
      {:ok, user} ->
        conn |> render("upload_success.json", user: user)
      {:error, failed_changeset} ->
        conn |> render("upload_failed.json", changeset: failed_changeset)
    end
  end

  defp check_profile_ownership(%{params: %{"id" => username}} = conn, _) do
    if conn.assigns.current_user.username != username do
      conn
      |> put_flash_danger(gettext("You can only edit your profile"))
      |> redirect(to: app_user_path(conn, :show, conn.assigns.locale, username))
      |> halt()
    else
      conn
    end
  end

end
