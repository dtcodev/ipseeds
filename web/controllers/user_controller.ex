defmodule Ipseeds.UserController do
  use Ipseeds.Web, :controller

  alias Ipseeds.{API, User, APIHelpers}

  @reset_password_success_response {:ok, "Reset Password Request Sent"}

  def reset_password(conn, %{"info" => reset_token}) do
    pw_changeset = 
      %User{}
      |> User.reset_password_changeset(%{"reset_password_token" => reset_token})

    render(conn, "create_new_password.html", changeset: pw_changeset)
  end

  def forgot_password(conn, _params) do
    changeset = User.changeset(%User{})
    render(conn, "forgot_password.html", changeset: changeset)
  end

  def request_reset_password(conn, %{"user" => user_params}) do
    changeset = 
      %User{}
      |> User.forgot_password_changeset(user_params)
      |> Map.put(:action, :update)

    case process_request(changeset) do
      {:ok, _} ->
        render(conn, "reset_password_request_sent.html")
      {:error, api_errors} ->
        changeset = changeset
          |> Map.put(:errors, changeset.errors ++ api_errors)

        render(conn, "forgot_password.html", changeset: changeset)
    end
  end

  def api_password_reset_hook(conn, %{"Email" => email, "Token" => token}) do
    user = Repo.get_by(User, email: email)
    if !is_nil(user) do
      send_reset_password_email(user, reset_password_link(conn, token))
    end
    render(conn, "api_password_reset_hook.json")
  end

  defp process_request(%{:valid? => false}), do: {:error, []}
  defp process_request(changeset) do
    case Repo.get_by(User, email: changeset.changes.email) do
      nil ->
        @reset_password_success_response
      user ->
        payload = %{"email" => user.email}
        send_reset_password_request(payload, Mix.env)
    end
  end

  defp send_reset_password_request(_payload, :test), do: @reset_password_success_response
  defp send_reset_password_request(payload, _env) do
    case API.call_endpoint("/account/reset_password", payload) do
      {:ok, %{body: %{"error_code" => 0}}} ->
        @reset_password_success_response
      {:ok, %{body: %{"error_code" => error_code}}} ->
        {:error, [{:api, {APIHelpers.changeset_api_error(error_code), []}}]}
      _ ->
        {:error, [{:api, {APIHelpers.changeset_api_error(), []}}]}
    end
  end

  def submit_new_password(conn, %{"user" => reset_password_params}) do
    changeset = 
      %User{}
      |> User.reset_password_changeset(reset_password_params)
      |> Map.put(:action, :update)

    case create_new_password(changeset) do
      {:ok, flash_msg} ->
        conn
        |> put_flash(:info, flash_msg)
        |> redirect(to: session_path(conn, :new, conn.assigns.locale))
      {:error, api_errors} ->
        changeset = changeset |> Map.put(:errors, changeset.errors ++ api_errors)
        render(conn, "create_new_password.html", changeset: changeset)
    end
  end

  defp create_new_password(%{:valid? => false}), do: {:error, []}
  defp create_new_password(changeset) do
    payload = %{
      "reset_info" => changeset.changes.reset_password_token,
      "new_password" => changeset.changes.password
    }

    call_reset_password_api(payload)
  end

  defp call_reset_password_api(payload) do
    case API.call_endpoint("/account/change_password", payload) do
      {:ok, %{body: %{"error_code" => 0}}} ->
        {:ok, "New password has been set. Now you can login using your new password"}
      {:ok, %{body: %{"error_code" => 1012}}} ->
        {:error, [{:reset_password_token, {"is expired", []}}]}
      {:ok, %{body: %{"error_code" => error_code}}} ->
        {:error, [{:api, {APIHelpers.changeset_api_error(error_code), []}}]}
      {:error, _reason} ->
        {:error, [{:api, {APIHelpers.changeset_api_error(), []}}]}
    end
  end

  defp send_reset_password_email(user, reset_password_link) do
    Ipseeds.Email.reset_password_email(user.email, User.get_name(user), reset_password_link)
    |> Ipseeds.Mailer.deliver_later()
  end

  defp reset_password_link(conn, token) do
    user_url(conn, :reset_password, conn.assigns.locale, info: token)
  end
end
