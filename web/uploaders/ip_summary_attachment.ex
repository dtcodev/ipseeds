defmodule Ipseeds.IpSummaryAttachment do
  use Arc.Definition
  use Arc.Ecto.Definition
  use Ipseeds.BaseUploader

  @versions [:original]
  @acl :public_read

  def storage_dir(_, _) do
    "uploads/ip_summary/attachments"
  end
end
