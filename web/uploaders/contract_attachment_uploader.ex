defmodule Ipseeds.ContractAttachmentUploader do
  use Arc.Definition
  use Arc.Ecto.Definition
  use Ipseeds.BaseUploader

  @versions [:original]
  @acl :public_read

  # Override the storage directory:
  def storage_dir(_, _) do
    "uploads/contract/attachments"
  end
end
