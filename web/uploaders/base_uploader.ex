defmodule Ipseeds.BaseUploader do
  defmacro __using__(_) do
    quote do
      def __storage do
        Application.get_env(:arc, :storage, Arc.Storage.S3)
      end
    end
  end
end