defmodule Ipseeds.ProfilePhoto do
  use Arc.Definition
  use Arc.Ecto.Definition
  use Ipseeds.BaseUploader

  alias Ipseeds.Endpoint
  alias Ipseeds.Router.Helpers

  @versions [:original, :thumb]
  @extension_whitelist ~w(.jpg .jpeg .gif .png)
  @acl :public_read

  def validate({file, _}) do   
    file_extension = file.file_name |> Path.extname |> String.downcase
    Enum.member?(@extension_whitelist, file_extension)
  end

  # Define a thumbnail transformation:
  def transform(:thumb, _) do
    {:convert,
      "-strip -thumbnail 200x200 -gravity center -extent 200x200 -auto-orient -format jpg -quality 85",
      :jpg}
  end

  # Override the persisted filenames:
  def filename(version, {file, _}) do
    basename = Path.basename(file.file_name, Path.extname(file.file_name))
    "#{basename}_#{version}"
  end

  # Override the storage directory:
  def storage_dir(_, _) do
    "uploads/user/profile_photos"
  end

  # Provide a default URL if there hasn't been a file uploaded
  def default_url(_, _) do
    Helpers.static_path(Endpoint, "/images/dtco_logo.png")
  end
end
