defmodule Ipseeds.IpSummaryLogo do
  use Arc.Definition
  use Arc.Ecto.Definition
  use Ipseeds.BaseUploader

  @versions [:original]
  @extension_whitelist ~w(.jpg .jpeg .gif .png)
  @acl :public_read

  def validate({file, _}) do   
    file_extension = file.file_name |> Path.extname |> String.downcase
    Enum.member?(@extension_whitelist, file_extension)
  end

  # Define a thumbnail transformation:
  def transform(:original, _) do
    {:convert,
      "-strip -resize 260x260 -gravity center -extent 260x260 -auto-orient -format jpg -quality 85",
      :jpg}
  end

  # Override the storage directory:
  def storage_dir(_, _) do
    "uploads/ip_summary/logos"
  end
end
