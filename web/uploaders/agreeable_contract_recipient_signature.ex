defmodule Ipseeds.AgreeableContractRecipientSignature do
  use Arc.Definition
  use Arc.Ecto.Definition
  use Ipseeds.BaseUploader

  @versions [:original]
  @acl :public_read

  def storage_dir(_, _) do
    "uploads/agreeable/recipient_signatures"
  end
end