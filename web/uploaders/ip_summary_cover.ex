defmodule Ipseeds.IpSummaryCover do
  use Arc.Definition
  use Arc.Ecto.Definition
  use Ipseeds.BaseUploader

  @versions [:original]
  @extension_whitelist ~w(.jpg .jpeg .gif .png)
  @acl :public_read

  def validate({file, _}) do   
    file_extension = file.file_name |> Path.extname |> String.downcase
    Enum.member?(@extension_whitelist, file_extension)
  end

  # Define a thumbnail transformation:
  def transform(:original, _) do
    {:convert,
      "-strip -resize 1280x720 -gravity center -extent 1280x720 -auto-orient -format jpg -quality 85",
      :jpg}
  end

  # Override the storage directory:
  def storage_dir(_, _) do
    "uploads/ip_summary/cover_photos"
  end
end
