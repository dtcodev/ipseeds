defmodule Ipseeds.App.PendingContractsPlug do
  import Plug.Conn
  import Ecto.Query, only: [from: 2]

  alias Ipseeds.{AgreeableContract, Repo, User}

  def init(default), do: default

  def call(conn, _opts) do
    current_user = conn.assigns.current_user
    current_user_active_keypair = User.active_keypair(current_user)
    case current_user_active_keypair do
      nil -> assign(conn, :pending_contract_count, 0)
      _ ->
        pending_contract_count = get_pending_contract_count(current_user) |> Repo.one()
        assign(conn, :pending_contract_count, pending_contract_count)
    end
  end

  defp get_pending_contract_count(current_user) do
    _query = from ac in AgreeableContract,
      where: is_nil(ac.final_asset_id) 
               and is_nil(ac.cancelled_at) and is_nil(ac.declined_at) 
               and (ac.recipient_user_id == ^current_user.id or ac.sender_user_id == ^current_user.id),
      select: count(ac.id)
  end
end