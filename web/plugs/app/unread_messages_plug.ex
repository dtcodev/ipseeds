defmodule Ipseeds.App.UnreadMessagesPlug do
  import Plug.Conn
  import Ecto.Query, only: [from: 2]

  alias Ipseeds.{Message, Repo, User}

  def init(default), do: default

  def call(conn, _opts) do
    current_user = conn.assigns.current_user
    current_user_active_keypair = User.active_keypair(current_user)
    case current_user_active_keypair do
      nil -> assign(conn, :unread_message_count, 0)
      _ ->
        unread_message_count = unread_query(current_user, current_user_active_keypair) |> Repo.one()
        assign(conn, :unread_message_count, unread_message_count)
    end
  end

  defp unread_query(current_user, current_user_active_keypair) do
    _query = from m in Message,
      where: ((m.recipient_user_id == ^current_user.id
              and m.recipient_keypair_id == ^current_user_active_keypair.id)
            or (m.recipient_user_id == ^current_user.id
              and m.is_system_message == true))
            and is_nil(m.read_at),
      select: count(m.id)
  end
end
