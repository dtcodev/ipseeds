defmodule Ipseeds.App.AuthPlug do
  import Plug.Conn
  import Phoenix.Controller, only: [redirect: 2]
  import Ipseeds.ControllerHelpers

  alias Ipseeds.Repo
  alias Ipseeds.User
  alias Ipseeds.Router.Helpers

  require Logger

  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    user = conn.assigns[:current_user] || get_valid_user_from_session(conn)

    conn
    |> put_current_user(user)
    |> authenticated_users_only(conn) #=> allow only authenticated (logged in) users to continue
  end

  def authenticated_users_only(conn, _opts \\ []) do
    if conn.assigns[:current_user] do
      # refresh the user's API auth token on all requests
      refresh_api_auth(conn, Mix.env)
    else
      deny_access!(conn)
    end
  end

  def refresh_api_auth(conn, :test), do: conn
  def refresh_api_auth(conn, _) do
    {conn, resp} = Ipseeds.API.call_authed_endpoint(conn, "/account/refresh_auth")
    case resp do
      {:ok, _} -> conn
      _ -> deny_access!(conn)
    end
  end

  def update_auth_data(conn, auth_code, expire_time_utc \\ new_expire_time()) do
    conn
    |> put_auth_code_to_session(auth_code)
    |> put_session(:expire_time_utc, expire_time_utc)
    |> Ipseeds.ExagonHelpers.put(:expire_time_utc, expire_time_utc)
  end

  def put_auth_code_to_session(conn, auth_code) do
    if auth_code != "", do: put_session(conn, :auth_code, auth_code), else: conn
  end

  def new_expire_time do
    Timex.now()
    |> Timex.shift(minutes: 30)
    |> Timex.to_unix()
  end

  defp deny_access!(conn) do
    conn
    |> clear_session
    |> put_session(:redirect_url, conn.request_path)
    |> put_flash_danger("You must be logged in to access that page")
    |> redirect(to: Helpers.session_path(conn, :new, conn.assigns.locale))
    |> halt()
  end

  defp get_valid_user_from_session(conn) do
    user_id = get_session(conn, :user_id)
    auth_code = get_session(conn, :auth_code)
    auth_code_expiry = get_session(conn, :expire_time_utc)

    (user_id && auth_code && auth_code_expiry
      && auth_code_active?(auth_code_expiry)
      && Repo.get_by(User.is_active_query, id: user_id))
  end

  defp put_current_user(conn, user) do
    conn
    |> assign(:current_user, user)
  end

  defp auth_code_active?(auth_code_expiry) do
    curr_time = DateTime.utc_now |> DateTime.to_unix
    auth_code_expiry > curr_time
  end
end
