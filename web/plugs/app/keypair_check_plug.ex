defmodule Ipseeds.App.KeypairCheckPlug do
  import Plug.Conn
  import Phoenix.Controller, only: [redirect: 2, put_flash: 3]

  alias Ipseeds.Router.Helpers
  alias Ipseeds.User

  def init(default), do: default

  def call(conn, _) do
    if conn.request_path in exception_paths(conn) do
      conn
    else
      conn |> authorize
    end
  end

  defp authorize(conn) do
    if is_nil(User.active_keypair(conn.assigns.current_user)) do
      conn
      |> put_flash(:error, "You must setup your passphrase before having access to the app") 
      |> redirect(to: Helpers.app_user_path(conn, :passphrase, conn.assigns.locale))
      |> halt
    else
      conn
    end
  end

  defp exception_paths(conn) do
    [Helpers.app_user_path(conn, :passphrase, conn.assigns.locale),
     Helpers.app_user_path(conn, :create_passphrase, conn.assigns.locale)]
  end

end
