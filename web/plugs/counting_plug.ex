defmodule Ipseeds.CountingPlug do
  import Plug.Conn
  import Ecto.Query, only: [from: 2]

  alias Ipseeds.{IpNotary, IpSummary, UserRegistration, Repo, API}

  def init(default), do: default

  def call(conn, _opts) do
    ip_notary_count = Repo.one(from ip_notary in IpNotary, select: count(ip_notary.id))
    #ip_summary_count = Repo.one(from ip_summary in IpSummary, select: count(ip_summary.id))
    ip_summary_count = get_current_ip_summary_count
    user_count = Repo.one(from user_registration in UserRegistration, select: count(user_registration.id), 
      where: not is_nil(user_registration.approved_at))
    conn
    |> assign(:asset_count, ip_notary_count + ip_summary_count)
    |> assign(:research_case_count, ip_summary_count)
    |> assign(:user_count, user_count)
  end

  defp get_current_ip_summary_count do
    payload = %{
      keys: [],
      time_utc: 0,
      type: IpSummary.asset_type(),
      page_index: 1,
      assets_per_page: 1
    }
    query_result = API.call_endpoint("/asset/query/bulk", payload)

    case query_result do
      {:ok, %{body: body = %{"asset_list" => assets}}} when is_list(assets) ->
        body["total_amount"]
      _ ->
        0
    end
  end
end