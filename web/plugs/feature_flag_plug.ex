defmodule Ipseeds.FeatureFlagPlug do
  @moduledoc ~S"""
  This plug is used to disable the use of controllers and controller endpoints conditionally.

  Disable a whole controller in prod env:

      # some_controller.ex
      use Ipseeds.Web, :controller
      plug Ipseeds.FeatureFlagPlug, disable_if: Mix.env == :prod

  Disable only the show action in the staging env:

      # some_controller.ex
      use Ipseeds.Web, :controller
      plug Ipseeds.FeatureFlagPlug, [disable_if: Mix.env == :staging] when action in [:show]
  """

  def init(opts), do: opts

  def call(conn, opts) do
    case opts[:disable_if] do
      true -> raise Ipseeds.Forbidden
      _ -> conn
    end
  end
end