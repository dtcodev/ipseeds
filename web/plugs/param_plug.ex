defmodule Ipseeds.ParamPlug do
  def strip_tags(conn, required_key) when is_binary(required_key) do
    param = Map.get(conn.params, required_key) |> do_strip_tags()

    unless param do
      raise Phoenix.MissingParamError, key: required_key
    end

		params = Map.put(conn.params, required_key, param)
		%{conn | params: params}
  end

  defp do_strip_tags(param) when is_binary(param), do: HtmlSanitizeEx.strip_tags(param)
  defp do_strip_tags(params) when is_list(params), do: Enum.map(params, &do_strip_tags/1)
  defp do_strip_tags(%{__struct__: _} = param), do: param
  defp do_strip_tags(%{} = params) do
    Enum.reduce(params, %{}, fn({k, v}, acc) ->
      Map.put(acc, k, do_strip_tags(v))
    end)
  end
  defp do_strip_tags(param), do: param
end
