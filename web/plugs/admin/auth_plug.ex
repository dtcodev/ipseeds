defmodule Ipseeds.Admin.AuthPlug do
  import Plug.Conn
  import Comeonin.Bcrypt, only: [checkpw: 2, dummy_checkpw: 0]
  import Phoenix.Controller
  import Ipseeds.ControllerHelpers
  import Ipseeds.Gettext, only: [gettext: 1]

  alias Ipseeds.Router.Helpers
  alias Ipseeds.AdminUser

  def init(opts) do
    Keyword.fetch!(opts, :repo)
  end

  def call(conn, repo) do
    admin_user = conn.assigns[:current_admin_user] 
      || get_valid_admin_user_from_session(conn, repo)

    conn
    |> assign(:current_admin_user, admin_user)
    |> authenticate_login
  end

  def login(conn, admin_user) do
    conn
    |> assign(:current_admin_user, admin_user)
    |> put_session(:admin_user_id, admin_user.id)
    |> configure_session(renew: true)
  end

  def login_by_username_and_pass(conn, username, given_pass, opts) do
    repo = Keyword.fetch!(opts, :repo)
    admin_user = repo.get_by(AdminUser.is_active_query, username: username)

    cond do
      admin_user && checkpw(given_pass, admin_user.password_hash) ->
        {:ok, login(conn, admin_user)}
      admin_user ->
        {:error, :unauthorized, conn}
      true ->
        dummy_checkpw()
        {:error, :not_found, conn}
    end
  end

  def logout(conn) do
    configure_session(conn, drop: true)
  end

  def authenticate_login(conn, _opts \\ []) do
    if conn.assigns[:current_admin_user] do
      conn
    else
      conn
      |> put_flash_danger(gettext("You must be logged into access that page."))
      |> put_session(:return_to, conn.request_path)
      |> redirect(to: Helpers.public_admin_session_path(conn, :new, conn.assigns.locale))
      |> halt()
    end
  end

  def authenticate_superadmin(conn, _opts \\ []) do
    current_admin_user = conn.assigns[:current_admin_user]

    if current_admin_user && current_admin_user.is_superadmin do
      conn
    else
      conn
      |> put_flash_danger(gettext("You are not permitted to perform that action."))
      |> redirect(to: Helpers.admin_page_path(conn, :dashboard, conn.assigns.locale))
      |> halt()
    end
  end

  defp get_valid_admin_user_from_session(conn, repo) do
    admin_user_id = get_session(conn, :admin_user_id)
    admin_user_id && repo.get_by(AdminUser.is_active_query, id: admin_user_id)
  end
end
