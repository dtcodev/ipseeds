defmodule Ipseeds.APITimestamp do
  import Ecto.DateTime.Utils, only: [is_date: 3, to_i: 1]

  @behaviour Ecto.Type
  defstruct [:year, :month, :day]

  def type, do: :integer

  # Provide our own casting rules.
  def cast(<<year::4-bytes, ?-, month::2-bytes, ?-, day::2-bytes>>),
    do: from_parts(to_i(year), to_i(month), to_i(day))

  # We should still accept integers
  def cast(integer) when is_integer(integer) do
    case DateTime.from_unix(integer) do
      {:ok, dt} ->
        from_parts(dt.year, dt.month, dt.day)
      _ -> :error
    end
  end

  # Everything else is a failure though
  def cast(_), do: :error

  # When loading data from the database, we are guaranteed to
  # receive an integer (as databases are strict) and we will
  # just return it to be stored in the schema struct.
  def load(integer) when is_integer(integer), do: {:ok, integer}

  # When dumping data to the database, we *expect* an integer
  # but any value could be inserted into the struct, so we need
  # guard against them.
  def dump(%__MODULE__{} = dt) do
    {:ok, to_unix(dt)}
  end
  def dump(_), do: :error

  def to_string(%__MODULE__{year: year, month: month, day: day}) do
    %Date{year: year, month: month, day: day}
    |> Date.to_string
  end

  def to_unix(%__MODULE__{year: year, month: month, day: day}) do
    dt = %DateTime{year: year, month: month, day: day, zone_abbr: "UTC", 
          hour: 0, minute: 0, second: 7, microsecond: {0, 0},
          utc_offset: 0, std_offset: 0, time_zone: "Etc/UTC"}
    DateTime.to_unix(dt)
  end

  defp from_parts(year, month, day) when is_date(year, month, day) do
    {:ok, %__MODULE__{year: year, month: month, day: day}}
  end
end

defimpl String.Chars, for: Ipseeds.APITimestamp do
  def to_string(dt) do
    @for.to_string(dt)
  end
end

defimpl Inspect, for: Ipseeds.APITimestamp do
  @inspected inspect(@for)

  def inspect(dt, _opts) do
    "#" <> @inspected <> "<" <> @for.to_string(dt) <> ">"
  end
end

defimpl Phoenix.HTML.Safe, for: Ipseeds.APITimestamp do
  defdelegate to_iodata(data), to: Ipseeds.APITimestamp, as: :to_string
end

defimpl Poison.Encoder, for: Ipseeds.APITimestamp do
  def encode(data, _opts) do
    Ipseeds.APITimestamp.to_unix(data)
    |> Integer.to_string
  end
end
