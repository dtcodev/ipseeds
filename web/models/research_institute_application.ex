defmodule Ipseeds.ResearchInstituteApplication do
  use Ipseeds.Web, :model

  import Ipseeds.ValidationHelpers

  @allowed_fields ~w(institute_name_zh_tw institute_name_en institute_tax_id institute_nationality 
    institute_website user_given_names user_last_name user_department user_title user_telephone
    user_address contact_given_names contact_last_name contact_department contact_title contact_telephone
    contact_address contact_email)a

  @required_fields ~w(institute_name_en institute_nationality
    institute_website user_given_names user_last_name user_department user_title user_telephone
    user_address contact_given_names contact_last_name contact_department contact_title contact_telephone
    contact_email contact_address)a

  schema "research_institute_applications" do
    field :institute_name_zh_tw, :string
    field :institute_name_en, :string
    field :institute_tax_id, :string
    field :institute_nationality, :string
    field :institute_website, :string
    field :user_given_names, :string
    field :user_last_name, :string
    field :user_department, :string
    field :user_title, :string
    field :user_telephone, :string
    field :user_address, :string
    field :contact_given_names, :string
    field :contact_last_name, :string
    field :contact_department, :string
    field :contact_title, :string
    field :contact_telephone, :string
    field :contact_address, :string
    field :contact_email, :string
    has_one :user_registration, Ipseeds.UserRegistration, foreign_key: :application_id

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
    |> validate_company_branch(:institute_name_en)
    |> validate_company_branch(:user_department)
    |> validate_email(:contact_email)
    |> validate_institute_tax_id_and_name_zh_tw
  end

  defimpl Ipseeds.ApplicationData do
    alias Ipseeds.ResearchInstituteApplication

    def get_company(%ResearchInstituteApplication{institute_name_en: company_name}), do: company_name

    def get_branch(%ResearchInstituteApplication{user_department: branch_name}), do: branch_name

    def get_address(%ResearchInstituteApplication{user_address: address}), do: address

    def get_api_company(%ResearchInstituteApplication{institute_name_en: company_name}) do
      company_name |> String.replace(~r/\s+/, "_")
    end

    def get_api_branch(%ResearchInstituteApplication{user_department: branch_name}) do
      branch_name |> String.replace(~r/\s+/, "_")
    end

    def get_name(%ResearchInstituteApplication{user_given_names: given_names, user_last_name: last_name}) do
      Ipseeds.NameHelpers.full_name(given_names, last_name)
    end

    def get_nationality_name(%ResearchInstituteApplication{institute_nationality: institute_nationality}) do
      country = Ipseeds.CountriesHelper.all
        |> Enum.find(fn (c) -> Keyword.get(c, :alpha2) == institute_nationality end)

      if country do
        Keyword.get(country, :name)
      else
        institute_nationality
      end
    end

    def get_website(%ResearchInstituteApplication{institute_website: website}), do: website

  end
end
