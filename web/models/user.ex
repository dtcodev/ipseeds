defmodule Ipseeds.User do
  use Ipseeds.Web, :model
  use Ipseeds.AttachmentHelpers

  import Ecto.Query, only: [from: 2, last: 2]
  import Ipseeds.ValidationHelpers

  alias Ipseeds.{ApplicationData, IpNotary, Message, ProfilePhoto, Repo, UserContact, UserKeypair, UserRegistration}

  @allowed_fields ~w(username email is_paid receiver_asset_addr
    biography picture profile_show_email message_notify_by_email)a
  @required_fields ~w()a

  @allowed_registration_fields ~w(username email email_confirmation password password_confirmation
    message_notify_by_email)a
  @required_registration_fields ~w(username email email_confirmation password password_confirmation)a

  @allowed_login_fields ~w(username password)a
  @required_login_fields ~w(username password)a

  @allowed_setting_fields ~w(biography profile_show_email message_notify_by_email)a

  @reset_password_fields ~w(reset_password_token password password_confirmation)a

  schema "users" do
    field :username, :string
    field :email, :string
    field :email_confirmation, :string, virtual: true
    has_one :user_registration, UserRegistration
    field :is_paid, :boolean, default: false
    field :receiver_asset_addr, :string
    has_many :keypairs, UserKeypair
    field :biography, :string
    field :picture, ProfilePhoto.Type
    field :profile_show_email, :boolean, default: false
    field :message_notify_by_email, :boolean, default: true
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    field :reset_password_token, :string, virtual: true

    has_many :ip_notaries, IpNotary

    has_many :user_contacts, UserContact
    has_many :contacts, through: [:user_contacts, :contact_user]

    has_many :sent_messages, Message, foreign_key: :sender_user_id
    has_many :received_messages, Message, foreign_key: :recipient_user_id

    timestamps()
  end

  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
  end

  def registration_changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_registration_fields)
    |> validate_required(@required_registration_fields)
    |> validate_username
    |> unique_constraint(:username)
    |> validate_email
    |> validate_confirmation(:email, message: "does not match email")
    |> validate_password
    |> validate_confirmation(:password, message: "does not match password")
  end

  def login_changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_login_fields)
    |> validate_required(@required_login_fields)
    |> validate_username
    |> validate_password
  end

  def settings_changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_setting_fields)
  end

  def profile_photo_changeset(model, params \\ %{}) do
    params = params |> cast_uuid_names([:picture])

    model
    |> validate_attachment_size(params, :picture)
    |> cast_attachments_if_valid(params, [:picture], [:picture])
  end

  def forgot_password_changeset(model, params \\ %{}) do
    model
    |> cast(params, [:email])
    |> validate_required([:email])
    |> validate_email
  end

  def reset_password_changeset(model, params \\ %{}) do
    model
    |> cast(params, @reset_password_fields)
    |> validate_required(@reset_password_fields)
    |> validate_password
    |> validate_confirmation(:password, message: "does not match password")
  end

  def system_username, do: "BioIPSeeds"

  def get_company_and_branch(user) do
    application = get_user_application(user)

    {ApplicationData.get_company(application),
     ApplicationData.get_branch(application)}
  end

  def get_api_company_and_branch(user) do
    application = get_user_application(user)

    {ApplicationData.get_api_company(application), 
     ApplicationData.get_api_branch(application)}
  end

  def get_company(user) do
    user
    |> get_user_application
    |> ApplicationData.get_company
  end

  def get_branch(user) do
    user
    |> get_user_application
    |> ApplicationData.get_branch
  end

  def get_name(user) do
    user
    |> get_user_application
    |> ApplicationData.get_name
  end

  def get_nationality_name(user) do
    user
    |> get_user_application
    |> ApplicationData.get_nationality_name
  end

  def get_address(user) do
    user
    |> get_user_application
    |> ApplicationData.get_address
  end

  def get_website(user) do
    user
    |> get_user_application
    |> ApplicationData.get_website
  end

  def active_keypair(%{id: user_id}) do
    from([k] in UserKeypair,
      where: k.user_id == ^user_id)
    |> last(:inserted_at) 
    |> Repo.one
  end

  def is_active_query(query \\ default_query()) do
    query
    |> is_approved_query
  end

  def is_approved_query(query \\ default_query()) do
    from user in query,
      join: r in assoc(user, :user_registration),
      where: not(is_nil(r.approved_at)),
      preload: [user_registration: r]
  end

  def with_application_query(query \\ default_query()) do
    from user in query,
      join: r in assoc(user, :user_registration),
      preload: [user_registration: r]
  end

  def get_user_application(user) do
    user = Repo.preload(user, :user_registration)

    UserRegistration.get_user_application(user.user_registration)
  end

  def get_user_application_type(user) do
    user = Repo.preload(user, :user_registration)

    UserRegistration.get_application_type(user.user_registration)
  end

  defp default_query() do
    from user in __MODULE__, select: user
  end

  def is_industry_member?(user), do: is_application_type?(user, "industry_application")

  def is_individual_member?(user), do: is_application_type?(user, "individual_researcher_application")

  def is_research_institute_member?(user), do: is_application_type?(user, "research_institute_application")

  defp is_application_type?(user, application_type) do
    UserRegistration.get_application_type(user.user_registration)[:name] == application_type
  end
end
