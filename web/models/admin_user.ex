defmodule Ipseeds.AdminUser do
  use Ipseeds.Web, :model
  import Ecto.Query, only: [from: 2]

  alias Ipseeds.AdminUser

  @allowed_fields ~w(username email is_superadmin
                    invitation_token_hash invitation_created_at
                    invitation_created_by_admin_id invitation_sent_at
                    invitation_accepted_at reset_password_token_hash
                    reset_password_created_at reset_password_sent_at
                    disabled_at disabled_by_admin_id
                    deleted_at deleted_by_admin_id)a
  @required_fields ~w()a

  @allowed_registration_fields [:password | @allowed_fields]
  @required_registration_fields ~w(username password email)a

  @allowed_forgot_password_fields ~w(reset_password_token_hash
                                     reset_password_created_at
                                     reset_password_sent_at)a

  @allowed_password_reset_fields ~w(password
                                    password_confirmation
                                    password_reseted_at
                                    reset_password_token_hash
                                    invitation_accepted_at
                                    invitation_token_hash)

  @required_delete_fields ~w(deleted_at deleted_by_admin_id)a

  @invitation "invitation"

  schema "admin_users" do
    field :username, :string
    field :email, :string
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    field :password_hash, Comeonin.Ecto.Password
    field :is_superadmin, :boolean, default: false
    field :invitation_token_hash, :string
    field :invitation_created_at, Ecto.DateTime
    belongs_to :invitation_created_by_admin, AdminUser
    field :invitation_sent_at, Ecto.DateTime
    field :invitation_accepted_at, Ecto.DateTime
    field :reset_password_token_hash, :string
    field :reset_password_created_at, Ecto.DateTime
    field :reset_password_sent_at, Ecto.DateTime
    field :password_reseted_at, Ecto.DateTime
    field :disabled_at, Ecto.DateTime
    belongs_to :disabled_by_admin, AdminUser
    field :deleted_at, Ecto.DateTime
    belongs_to :deleted_by_admin, AdminUser

    timestamps()
  end

  @doc """
  Builds a changeset based on the `model` and `params`.
  """
  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
  end

  def registration_changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_registration_fields)
    |> validate_required(@required_registration_fields)
    |> put_password_hash()
  end

  def delete_changeset(model, params \\ %{}) do
    model
    |> changeset(params)
    |> validate_required(@required_delete_fields)
  end

  def forgot_password_changeset(model, params = %{token: token}) do
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    now = DateTime.utc_now

    params_with_hash = params
                       |> Map.delete(:token)
                       |> Map.put_new(:reset_password_token_hash, hash)
                       |> Map.put(:reset_password_created_at, now)
                       |> Map.put(:reset_password_sent_at, now)
                       |> Map.put(:password_reseted_at, nil)

    model
    |> cast(params_with_hash, @allowed_forgot_password_fields)
  end

  def password_reset_changeset(model, input_params, type \\ nil) do
    params = put_token_used_params(input_params, type)

    model
    |> cast(params, @allowed_password_reset_fields)
    |> put_password_hash()
    |> validate_confirmation(:password, message: "does not match password!")
  end

  def is_active_query(query \\ default_query()) do
    query
    |> not_deleted_query
    |> not_disabled_query
  end

  def not_deleted_query(query \\ default_query()) do
    from admin in query, where: is_nil(admin.deleted_at)
  end

  def not_disabled_query(query \\ default_query()) do
    from admin in query, where: is_nil(admin.disabled_at)
  end

  def is_superadmin?(%__MODULE__{is_superadmin: true}), do: true
  def is_superadmin?(_), do: false

  def gen_token do
    SecureRandom.urlsafe_base64
  end

  def check_token(token, admin_user, key \\ :reset_password_token_hash)
  def check_token(token, admin_user, _) when is_nil(token) or is_nil(admin_user), do: false
  def check_token(token, _admin_user, _) when token == "", do: false
  def check_token(token, admin_user, key) do
    case admin_user do
      %AdminUser{} ->
        token
        |> Comeonin.Bcrypt.checkpw(Map.get(admin_user, key))
      _ ->
        Comeonin.Bcrypt.dummy_checkpw()
    end
  end

  defp default_query do
    from admin in __MODULE__, select: admin
  end

  defp put_password_hash(changeset) do
    case changeset do
      %Ecto.Changeset{valid?: true, changes: %{password: password}} ->
        put_change(changeset, :password_hash, Comeonin.Bcrypt.hashpwsalt(password))
      _ ->
        changeset
    end
  end

  defp put_token_used_params(params, @invitation) do
    Map.merge(params, %{"invitation_accepted_at" => DateTime.utc_now,
                        "invitation_token_hash" => nil})
  end
  defp put_token_used_params(params, _) do
    Map.merge(params, %{"password_reseted_at" => DateTime.utc_now,
                        "reset_password_token_hash" => nil})
  end
end
