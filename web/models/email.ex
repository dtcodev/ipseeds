defmodule Ipseeds.Email do
  use Bamboo.Phoenix, view: Ipseeds.EmailView

  alias Ipseeds.{User, Repo}

  def status_changed_agreeable_contract_email(recipient_user, sender_user, asset_id) do
    recipient_name = User.get_name(recipient_user)
    sender_name = User.get_name(sender_user)

    base_email()
    |> to(recipient_user.email)
    |> subject("NDA contract status updated by #{sender_name}")
    |> render("status_changed_agreeable_contract_email.html", 
         recipient_name: recipient_name, sender_name: sender_name, asset_id: asset_id)
  end

  def new_pending_agreeable_contract_email(recipient_user, sender_user, asset_id) do
    recipient_name = User.get_name(recipient_user)
    sender_name = User.get_name(sender_user)

    base_email()
    |> to(recipient_user.email)
    |> subject("New NDA contract issued by #{sender_name}")
    |> render("new_pending_agreeable_contract_email.html", 
         recipient_name: recipient_name, sender_name: sender_name, asset_id: asset_id)
  end

  def new_user_registration_email(to_emails, review_link) do
    base_email()
    |> to(to_emails)
    |> subject("New user registration waiting for approval")
    |> render("new_user_registration_email.html", review_link: review_link)
  end

  def new_admin_registration_email(to_email, password_reset_url) do
    base_email()
    |> to(to_email)
    |> subject("Ipseeds Admin account invitation")
    |> render("new_admin_registration_email.html",
              username: to_email,
              password_reset_url: password_reset_url)
  end

  def admin_password_reset_email(to_emails, password_reset_url) do
    base_email()
    |> to(to_emails)
    |> subject("Password reset")
    |> render("admin_password_reset_email.html", password_reset_url: password_reset_url)
  end

  def admin_invitation_email(to_email, password_reset_url, username, valid_hours \\ 48) do
    base_email()
    |> to(to_email)
    |> subject("BioIPSeeds Admin invitation")
    |> render("admin_invitation_email.html",
              password_reset_url: password_reset_url,
              username: username,
              valid_hours: valid_hours)
  end

  def new_message_email(sender_user, recipient_user) do
    recipient_name = User.get_name(recipient_user)
    sender_name = User.get_name(sender_user)

    base_email()
    |> to(recipient_user.email)
    |> subject("New message from #{sender_name} (#{sender_user.username})")
    |> render("new_message_email.html", full_name: recipient_name, sender_user: sender_user, sender_name: sender_name)
  end

  def new_data_contract_email(recipient_user, issuer_user) do
    recipient_name = User.get_name(recipient_user)
    issuer_name = User.get_name(issuer_user)

    base_email()
    |> to(recipient_user.email)
    |> subject("New data contract from #{issuer_name} (#{issuer_user.username})")
    |> render("new_data_contract_email.html", full_name: recipient_name,
      issuer_user: issuer_user, issuer_name: issuer_name)
  end

  def approval_notification_email(to_email, name, login_url) do
    base_email()
    |> to(to_email)
    |> subject("Your BioIPSeeds application has been approved")
    |> render("approval_notification_email.html", name: name, login_url: login_url)
  end

  def new_contact_us_email(first_name, last_name, user_email, message) do
    base_email()
    |> to(mailer_config()[:contact_us_receiver])
    |> subject("New contact us message from #{first_name} #{last_name}")
    |> render("new_contact_us_email.html", first_name: first_name, last_name: last_name,
      user_email: user_email, message: message)
  end

  def reset_password_email(email, full_name, reset_password_link) do
    base_email()
    |> to(email)
    |> subject("BioIPSeeds Password Reset")
    |> render("reset_password_email.html", full_name: full_name,
      reset_password_link: reset_password_link)
  end

  def user_registration_email(user, auth_letter_urls) do
    user = Repo.preload(user, :user_registration)
    individual_application? = user.user_registration.application_type == "individual"

    base_email()
    |> to(user.email)
    |> subject("Thank you for registering for BioIPSeeds")
    |> render("user_registration_email.html", full_name: User.get_name(user),
      individual_application?: individual_application?, auth_letter_urls: auth_letter_urls)
  end

  defp base_email do
    new_email()
    |> from(mailer_config()[:default_from])
    |> put_html_layout({Ipseeds.LayoutView, "email.html"})
  end

  defp mailer_config do
    Application.get_env(:ipseeds, Ipseeds.Mailer)
  end
end
