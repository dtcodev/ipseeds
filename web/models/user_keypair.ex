defmodule Ipseeds.UserKeypair do
  use Ipseeds.Web, :model

  @allowed_fields ~w(bip32_hd_public_key ecc_public_key_hex user_id)a
  @required_fields ~w(bip32_hd_public_key ecc_public_key_hex user_id)a

  schema "user_keypairs" do
    field :bip32_hd_public_key, :string
    field :ecc_public_key_hex, :string
    belongs_to :user, Ipseeds.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
  end
end
