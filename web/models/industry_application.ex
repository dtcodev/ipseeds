defmodule Ipseeds.IndustryApplication do
  use Ipseeds.Web, :model

  import Ipseeds.ValidationHelpers

  @allowed_fields ~w(company_name_zh_tw company_name_en company_tax_id company_nationality 
    company_website user_given_names user_last_name user_department user_title user_telephone
    user_address contact_given_names contact_last_name contact_department contact_department 
    contact_title contact_telephone contact_address contact_email)a

  @required_fields ~w(company_name_en company_nationality
    company_website user_given_names user_last_name user_department user_title user_telephone 
    user_address contact_given_names contact_last_name contact_department contact_department 
    contact_title contact_telephone contact_email contact_address)a

  schema "industry_applications" do
    field :company_name_zh_tw, :string
    field :company_name_en, :string
    field :company_tax_id, :string
    field :company_nationality, :string
    field :company_website, :string
    field :user_given_names, :string
    field :user_last_name, :string
    field :user_department, :string
    field :user_title, :string
    field :user_telephone, :string
    field :user_address, :string
    field :contact_given_names, :string
    field :contact_last_name, :string
    field :contact_department, :string
    field :contact_title, :string
    field :contact_telephone, :string
    field :contact_address, :string
    field :contact_email, :string
    has_one :user_registration, Ipseeds.UserRegistration, foreign_key: :application_id

    timestamps()
  end

  @listed_statuses [
    {"No", "no"},
    {"Emerging Stock Market", "esm"},
    {"OTC", "otc"},
    {"Stock Exchange Market", "sem"}
  ]

  def listed_statuses, do: @listed_statuses

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
    |> validate_company_branch(:company_name_en)
    |> validate_company_branch(:user_department)
    |> validate_email(:contact_email)
    |> validate_company_tax_id_and_name_zh_tw
  end

  defimpl Ipseeds.ApplicationData do
    alias Ipseeds.IndustryApplication

    def get_company(%IndustryApplication{company_name_en: company_name}), do: company_name

    def get_branch(%IndustryApplication{user_department: branch_name}), do: branch_name

    def get_address(%IndustryApplication{user_address: address}), do: address

    def get_api_company(%IndustryApplication{company_name_en: company_name}) do
      company_name |> String.replace(~r/\s+/, "_")
    end

    def get_api_branch(%IndustryApplication{user_department: branch_name}) do
      branch_name |> String.replace(~r/\s+/, "_")
    end

    def get_name(%IndustryApplication{user_given_names: given_names, user_last_name: last_name}) do
      Ipseeds.NameHelpers.full_name(given_names, last_name)
    end

    def get_nationality_name(%IndustryApplication{company_nationality: company_nationality}) do
      country = Ipseeds.CountriesHelper.all
        |> Enum.find(fn (c) -> Keyword.get(c, :alpha2) == company_nationality end)

      if country do
        Keyword.get(country, :name)
      else
        company_nationality
      end
    end

    def get_website(%IndustryApplication{company_website: website}), do: website
  end
end
