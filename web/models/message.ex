defmodule Ipseeds.Message do
  use Ipseeds.Web, :model

  @allowed_fields ~w(sender_user_id recipient_user_id recipient_keypair_id
    sender_keypair_id recipient_encrypted_content sender_encrypted_content subject)a
  @required_fields ~w(sender_user_id recipient_user_id recipient_keypair_id
    sender_keypair_id recipient_encrypted_content sender_encrypted_content subject)a

  @allowed_system_message_fields ~w(recipient_user_id recipient_encrypted_content subject)a
  @required_system_message_fields ~w(recipient_user_id recipient_encrypted_content subject is_system_message)a

  schema "messages" do
    belongs_to :sender, Ipseeds.User, foreign_key: :sender_user_id
    belongs_to :sender_keypair, Ipseeds.UserKeypair
    belongs_to :recipient, Ipseeds.User, foreign_key: :recipient_user_id
    belongs_to :recipient_keypair, Ipseeds.UserKeypair
    field :subject, :string
    field :content, :string, virtual: true
    field :sender_encrypted_content, :string
    field :recipient_encrypted_content, :string
    field :read_at, Ecto.DateTime
    field :is_system_message, :boolean

    timestamps()
  end

  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
    |> validate_recipient_is_not_sender()
  end

  def system_message_changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_system_message_fields)
    |> change(%{is_system_message: true})
    |> validate_required(@required_system_message_fields)
  end

  defp validate_recipient_is_not_sender(changeset) do
    sender_user_id = get_field(changeset, :sender_user_id)
    recipient_user_id = get_field(changeset, :recipient_user_id)
    case sender_user_id != recipient_user_id do
      true -> changeset
      _ -> add_error(changeset, :recipient_user, "cannot be the same as sender")
    end
  end
end
