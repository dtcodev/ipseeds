defmodule Ipseeds.ContactUsInfo do
  use Ipseeds.Web, :model

  @allowed_fields ~w(first_name last_name email message)a
  @required_fields ~w(first_name last_name email message)a

  embedded_schema do
    field :first_name
    field :last_name
    field :email
    field :message
  end

  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
    |> validate_format(:email, Regex.compile!(Ipseeds.ValidationHelpers.email_format))
  end

  
end
