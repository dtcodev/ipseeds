defmodule Ipseeds.AgreeableContract do
  use Ipseeds.Web, :model
  use Ipseeds.AttachmentHelpers

  import Ipseeds.ValidationHelpers

  alias Ipseeds.{User, AgreeableContractSenderSignature, AgreeableContractRecipientSignature, Repo, EnumHelpers,
    AgreeableContractHelpers}

  require Logger

  @allowed_original_asset_fields ~w(sender_user_id recipient_user_id draft_asset_id)a
  @allowed_final_asset_fields ~w(final_asset_id)a

  @allowed_issue_unilateral_fields ~w(nda_contract_type recipient_user_id sender_signature_url sender_signer_name 
    sender_signer_title unilateral_input_pnddo unilateral_input_ndd unilateral_input_yr_01 unilateral_input_yr_02 
    unilateral_input_days unilateral_nationality)a
  @required_issue_unilateral_fields ~w(nda_contract_type recipient_user_id sender_signature_url sender_signer_name 
    sender_signer_title unilateral_input_pnddo unilateral_input_ndd unilateral_input_yr_01 unilateral_input_yr_02 
    unilateral_input_days unilateral_nationality)a

  @allowed_issue_mutual_fields ~w(nda_contract_type recipient_user_id sender_signature_url sender_signer_name 
    sender_signer_title mutual_input_apambbr mutual_input_yr_01 mutual_input_yr_02 
    mutual_input_days mutual_nationality)a
  @required_issue_mutual_fields ~w(nda_contract_type recipient_user_id sender_signature_url sender_signer_name 
    sender_signer_title mutual_input_apambbr mutual_input_yr_01 mutual_input_yr_02 
    mutual_input_days mutual_nationality)a

  @attachment_sender_fields ~w(sender_signature_url)a
  @attachment_sender_required_fields ~w(sender_signature_url)a
  @attachment_recipient_fields ~w(recipient_signature_url)a
  @attachment_recipient_required_fields ~w(recipient_signature_url)a

  @allowed_accept_fields ~w(recipient_signature_url recipient_signer_name recipient_signer_title)a
  @required_accept_fields ~w(recipient_signature_url recipient_signer_name recipient_signer_title)a

  @allowed_decline_fields ~w(declined_at)a

  @allowed_cancel_fields ~w(cancelled_at)a

  schema "agreeable_contracts" do
    belongs_to :sender,    User, foreign_key: :sender_user_id 
    belongs_to :recipient, User, foreign_key: :recipient_user_id
    
    field :draft_asset_id,          :string
    field :final_asset_id,          :string

    field :declined_at,             Ecto.DateTime
    field :cancelled_at,            Ecto.DateTime

    field :name,                    :string,              virtual: true
    field :name_short,              :string,              virtual: true
    field :type,                    :string,              virtual: true
    field :description,             :string,              virtual: true
    field :image_url,               :string,              virtual: true
    field :tag,                     :string,              virtual: true
    field :valid_time,              Ipseeds.APITimestamp, virtual: true
    field :invalid_time,            Ipseeds.APITimestamp, virtual: true
    field :nda_contract_type,       :string,              virtual: true

    field :sender_signature_url,    Ipseeds.AgreeableContractSenderSignature.Type, virtual: true
    field :sender_signer_name,      :string,              virtual: true
    field :sender_signer_title,     :string,              virtual: true

    field :recipient_signature_url, Ipseeds.AgreeableContractRecipientSignature.Type, virtual: true
    field :recipient_signer_name,   :string,              virtual: true
    field :recipient_signer_title,  :string,              virtual: true 

    field :unilateral_input_pnddo,  :string,              virtual: true
    field :unilateral_input_ndd,    :string,              virtual: true 
    field :unilateral_input_yr_01,  :integer,             virtual: true 
    field :unilateral_input_yr_02,  :integer,             virtual: true
    field :unilateral_input_days,   :integer,             virtual: true
    field :unilateral_nationality,  :string,              virtual: true 

    field :mutual_input_apambbr,  :string,              virtual: true 
    field :mutual_input_yr_01,    :integer,             virtual: true 
    field :mutual_input_yr_02,    :integer,             virtual: true
    field :mutual_input_days,     :integer,             virtual: true
    field :mutual_nationality,    :string,              virtual: true 

    timestamps()
  end

  def asset_type(%{"nda_contract_type" => nda_type}) do
    if Mix.env == :prod do
      "contract_nda_#{nda_type}"
    else
      "contract_nda_#{nda_type}_#{Mix.env}"
    end
  end

  def changeset(struct, params \\ %{}) do
    struct 
    |> cast(params, @allowed_original_asset_fields)
  end
  def final_asset_changeset(struct, params \\ %{}) do
    struct 
    |> cast(params, @allowed_final_asset_fields)
  end

  def issue_changeset(struct, current_user, params \\ %{}) do
    %{"allowed_issue_fields" => allowed_issue_fields, 
      "required_issue_fields" => required_issue_fields} = issue_fields(params["nda_contract_type"])

    origin_struct = struct
      |> cast(params, allowed_issue_fields)
      |> validate_required(required_issue_fields)
      |> validate_agreeable_contract

    if origin_struct.valid? do
      params = cast_uuid_names(params, @attachment_sender_required_fields)
      current_dt = DateTime.utc_now
      show_data = Timex.now("Asia/Taipei") |> Timex.format!("{ISOdate}")
      name_for_nda = "#{asset_type(params)}_#{UUID.uuid4}"

      origin_struct
      |> Ecto.Changeset.put_change(:name, name_for_nda)
      |> Ecto.Changeset.put_change(:name_short, name_for_nda)
      |> Ecto.Changeset.put_change(:type, asset_type(params))
      |> Ecto.Changeset.put_change(:valid_time, current_dt |> DateTime.to_unix)
      |> Ecto.Changeset.put_change(:invalid_time, Timex.shift(current_dt, days: 3) |> DateTime.to_unix)
      |> Ecto.Changeset.put_change(:image_url, 
        AgreeableContractSenderSignature.url(params["sender_signature_url"].filename))
      |> Ecto.Changeset.put_change(:description, 
        issue_plaintext_of_nda(Map.merge(params, %{"show_date" => show_data}), current_user))
      |> validate_attachment_size(params, @attachment_sender_fields)
      |> cast_attachments_if_valid(params, @attachment_sender_fields, @attachment_sender_required_fields)

    else
      origin_struct
    end
  end

  def accept_changeset(struct, original_asset_atom, params \\ %{}) do
    params = cast_uuid_names(params, @attachment_recipient_required_fields)
    struct
      |> cast(params, @allowed_accept_fields)
      |> validate_required(@required_accept_fields)
      |> validate_length(:recipient_signer_name, max: 256)
      |> validate_length(:recipient_signer_title, max: 256)
      |> validate_attachment_size(params, @attachment_recipient_fields)
      |> accept_put_change_if_valid(original_asset_atom, params)
      |> cast_attachments_if_valid(params, @attachment_recipient_fields, @attachment_recipient_required_fields)
  end

  def cancel_changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_cancel_fields)
  end

  def decline_changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_decline_fields)
  end

  def agreement_status(agreeable_contract) do
    case agreeable_contract do
      %{cancelled_at: nil, declined_at: nil, final_asset_id: fa_id} when fa_id == nil ->
        :pending
      %{cancelled_at: nil, declined_at: nil, final_asset_id: _} ->
        :accepted  
      %{cancelled_at: c_at} when c_at != nil ->
        :cancelled
      %{declined_at: d_at} when d_at != nil ->
        :declined  
      _->
        :unknown
    end
  end

  def get_nationality_name(nationality) do
    country = Ipseeds.CountriesHelper.all
      |> Enum.find(fn (c) -> Keyword.get(c, :alpha2) == nationality end)

    if country do
      Keyword.get(country, :name)
    else
      nationality
    end
  end

  defp final_plaintext_of_nda(orginal_description, params) do
    map_desc = orginal_description
      |> String.replace("'", "\"") 
      |> Poison.decode! 
      |> EnumHelpers.atomize_keys()


    # Merge Signer information to var data
    final_var_data = map_desc.var_data
      |> Map.merge(%{"recipient_signature_url" =>  
           AgreeableContractRecipientSignature.url(params["recipient_signature_url"].filename),
           "recipient_signer_name" => params["recipient_signer_name"],
           "recipient_signer_title" => params["recipient_signer_title"]})

    Logger.info inspect final_var_data

    Poison.encode!(%{"plaintext" => map_desc.plaintext, 
      "var_data" => final_var_data})
  end

  def issue_params(%{"nda_contract_type" => nda_type} = params) do
    case nda_type do
      "unilateral" ->
        params
          |> Map.take(~w(nda_contract_type recipient_user_id sender_signature_url 
                         sender_signer_name sender_signer_title unilateral_input_pnddo unilateral_input_ndd 
                         unilateral_input_yr_01 unilateral_input_yr_02 unilateral_input_days unilateral_nationality))
          |> Map.put("unilateral_nationality", get_nationality_name(params["unilateral_nationality"]))
      "mutual" -> 
        params
          |> Map.take(~w(nda_contract_type recipient_user_id sender_signature_url 
                         sender_signer_name sender_signer_title mutual_input_apambbr 
                         mutual_input_yr_01 mutual_input_yr_02 mutual_input_days mutual_nationality))
          |> Map.put("mutual_nationality", get_nationality_name(params["mutual_nationality"]))
      _ ->
        %{}
    end
  end

  defp issue_plaintext_of_nda(%{"nda_contract_type" => nda_type} = params, sender_user) do
    var_data = issue_input_data_of_nda(params, sender_user)
    plain_text = case nda_type do
      "unilateral" -> AgreeableContractHelpers.unilateral_plaintext(var_data)
      "mutual" -> AgreeableContractHelpers.mutual_plaintext(var_data)
      _ -> ""
    end

    Poison.encode!(%{"plaintext" => plain_text, 
      "var_data" => issue_input_data_of_nda(params, sender_user)})
  end

  defp issue_input_data_of_nda(%{"recipient_user_id" => recipient_user_id} = params, sender_user) do

    u_sender = %{
      "sender_name" => User.get_name(sender_user),
      "sender_address" => User.get_address(sender_user),
      "sender_nationality" => User.get_nationality_name(sender_user),
      "sender_signature_url" => AgreeableContractSenderSignature.url(params["sender_signature_url"].filename)
    }

    recipient_user = Repo.get_by(User.is_active_query, id: recipient_user_id)

    u_recipient = %{
      "recipient_name" => User.get_name(recipient_user),
      "recipient_address" => User.get_address(recipient_user),
      "recipient_nationality" => User.get_nationality_name(recipient_user)
    }

    params
    |> Map.drop(["recipient_user_id"])
    |> Map.merge(u_sender)
    |> Map.merge(u_recipient)

  end

  defp issue_fields(nda_type) do
    case nda_type do
      "unilateral" ->
        %{"allowed_issue_fields" =>  @allowed_issue_unilateral_fields,
          "required_issue_fields" => @required_issue_unilateral_fields}
      "mutual" ->  
        %{"allowed_issue_fields" => @allowed_issue_mutual_fields,
          "required_issue_fields" => @required_issue_mutual_fields}
      _ -> %{}    
    end
  end

  defp accept_put_change_if_valid(struct, original_asset_atom, params) do
    if struct.valid? do
      struct
      |> Ecto.Changeset.put_change(:name, original_asset_atom.name)
      |> Ecto.Changeset.put_change(:name_short, original_asset_atom.name_short)
      |> Ecto.Changeset.put_change(:type, original_asset_atom.type)
      |> Ecto.Changeset.put_change(:valid_time, original_asset_atom.valid_time)
      |> Ecto.Changeset.put_change(:invalid_time, original_asset_atom.invalid_time)
      |> Ecto.Changeset.put_change(:tag, "{\"items\":[{\"asset_id\":\"#{original_asset_atom.asset_id}\"}]}")
      |> Ecto.Changeset.put_change(:description, final_plaintext_of_nda(original_asset_atom.description, params))
      |> Ecto.Changeset.put_change(:image_url, 
           AgreeableContractRecipientSignature.url(params["recipient_signature_url"].filename))
    else
      struct
    end
  end
end
