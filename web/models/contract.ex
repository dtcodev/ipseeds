defmodule Ipseeds.Contract do
  use Ipseeds.Web, :model
  use Ipseeds.AttachmentHelpers

  alias Ipseeds.ContractAttachment
  alias Ipseeds.User

  @allowed_fields ~w(issuer_user_id recipient_user_id main_attachment_id api_transaction_id
    api_sent_asset_id)a
  @required_fields ~w(issuer_user_id recipient_user_id api_transaction_id)a

  schema "contracts" do
    belongs_to :issuer, User, foreign_key: :issuer_user_id 
    belongs_to :recipient, User, foreign_key: :recipient_user_id
    field :api_transaction_id, :string
    field :api_sent_asset_id, :string

    has_many :attachments, ContractAttachment
    belongs_to :main_attachment, ContractAttachment

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
  end

  def visible_to_user_query(user_id, query \\ default_query()) do
    from contract in query,
      left_join: a in assoc(contract, :main_attachment),
      join: i in assoc(contract, :issuer),
      join: r in assoc(contract, :recipient),
      where: contract.issuer_user_id == ^user_id or contract.recipient_user_id == ^user_id,
      preload: [main_attachment: a, issuer: i, recipient: r]
  end 

  defp default_query() do
    from contract in __MODULE__, select: contract
  end
end
