defmodule Ipseeds.IpSummary do
  use Ipseeds.Web, :model
  use Ipseeds.AttachmentHelpers, attachment_max_size_bytes: 26_214_400

  alias Ipseeds.{IpSummaryAttachment, IpSummaryCover, IpSummaryLogo}

  @allowed_fields ~w(api_asset_id user_id name_short name valid_time category tag 
    source_asset_id initial_asset_id)a
  @required_fields ~w(api_asset_id user_id name_short name valid_time category tag)a

  @allowed_old_version_fields ~w(api_asset_id has_new_version)a
  @required_old_version_fields ~w(api_asset_id has_new_version)a

  @allowed_create_fields ~w(keywords name_short name category tag
    inventors department background innovation application advantage state_of_development
    clinical_trial patent_ids patent_status type_of_partnership hash_attachment)a
  @required_create_fields ~w(name_short name valid_time category tag)a

  @allowed_edit_fields ~w(keywords name_short name category tag
    inventors department background innovation application advantage state_of_development
    clinical_trial patent_ids patent_status type_of_partnership hash_attachment)a
  @required_edit_fields ~w(name_short name valid_time category tag)a

  @allowed_api_link_fields ~w(user_id api_asset_id)a
  @required_api_link_fields ~w(user_id api_asset_id)a

  @attachment_fields ~w(icon_url cover_url attachment_url)a
  @attachment_required_fields ~w(icon_url cover_url)a
  @attachment_map [
    icon_url: Ipseeds.IpSummaryLogo,
    cover_url: Ipseeds.IpSummaryCover,
    attachment_url: Ipseeds.IpSummaryAttachment
  ]

  @description_fields ~w(inventors department background innovation 
    application advantage state_of_development clinical_trial patent_ids patent_status
    type_of_partnership version_info)
  @comment_fields ~w(category keywords)
  @array_fields ~w(inventors type_of_partnership patent_ids)

  @categories [
    {"Small Molecules", "#small-molecules"},
    {"Biologics", "#biologics"},
    {"Botanical Drugs", "#botanical-drugs"},
    {"Regenerative Medicine", "#regenerative-medicine"},
    {"Medical Devices", "#medical-devices"},
    {"Diagnostics", "#diagnostics"},
    {"Technology Platform", "#technology-platform"},
    {"Others", "#others"}
  ]

  @states_of_development %{
    "invitro" => "In vitro",
    "invivo" => "In vivo",
    "preclinical" => "Pre-clinical"
  }

  @clinical_trials %{
    "phase1" => "Phase 1",
    "phase2" => "Phase 2",
    "phase3" => "Phase 3"
  }

  @types_of_partnership %{
    "codevelopment" => "Co-development",
    "techtransfer" => "Technology transfer",
    "licenseout" => "License out",
    "fundraising" => "Fund raising"
  }

  @human_readable_attr %{
    :icon_url => "Logo Image",
    :cover_url => "Cover Image",
    :attachment_url => "File Attachment",
    :name_short => "Case ID",
    :name => "Title",
    :linked_ip_seeds => "Linked IP Seeds",
    :valid_time => "Date",
    :tag => "Category",
    :api_error => "API Error"
  }

  @derive {Phoenix.Param, key: :api_asset_id}
  schema "ip_summaries" do
    field :api_asset_id, :string
    belongs_to :user, Ipseeds.User
    field :has_new_version, :boolean
    field :source_asset_id, :string
    field :initial_asset_id, :string
    field :icon_url, IpSummaryLogo.Type, virtual: true
    field :cover_url, IpSummaryCover.Type, virtual: true
    field :attachment_url, IpSummaryAttachment.Type, virtual: true
    field :name_short, :string, virtual: true
    field :valid_time, Ipseeds.APITimestamp, virtual: true
    field :name, :string, virtual: true
    field :type, :string, virtual: true
    field :inventors, :string, virtual: true
    field :department, :string, virtual: true
    field :background, :string, virtual: true
    field :innovation, :string, virtual: true
    field :application, :string, virtual: true
    field :advantage, :string, virtual: true
    field :state_of_development, :string, virtual: true
    field :clinical_trial, :string, virtual: true
    field :patent_ids, :string, virtual: true
    field :patent_status, :string, virtual: true
    field :type_of_partnership, :string, virtual: true
    field :create_time, Ipseeds.APITimestamp, virtual: true
    field :comment, :string, virtual: true
    field :category, :string, virtual: true
    field :keywords, :string, virtual: true
    field :tag, :string, virtual: true
    field :hash_attachment, :string, virtual: true

    timestamps()
  end

  def asset_type do
    case Mix.env do
      :prod -> "ip_summary"
      :staging -> "ip_summary_staging"
      :dev -> "ip_summary_dev_#{dev_api_suffix()}"
      :test -> "ip_summary_test"
    end
  end

  def categories, do: @categories

  def states_of_development, do: @states_of_development

  def clinical_trials, do: @clinical_trials
  
  def types_of_partnership, do: @types_of_partnership

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
  end

  def edit_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_edit_fields)
    |> validate_required(@required_edit_fields)
  end

  def create_changeset(model, params \\ %{}) do
    params = cast_uuid_names(params, @attachment_required_fields)
    current_dt = DateTime.utc_now |> DateTime.to_unix

    model
    |> cast(params, @allowed_create_fields)
    |> Ecto.Changeset.put_change(:type, asset_type())
    |> Ecto.Changeset.put_change(:valid_time, current_dt)
    |> validate_required(@required_create_fields)
    |> validate_length(:name, max: 500)
    |> validate_length(:name_short, max: 32)
    |> validate_format(:keywords, ~r/^[a-z0-9-_# ]+$/i)
    |> validate_length(:background, max: 2000)
    |> validate_length(:innovation, max: 2000)
    |> validate_length(:application, max: 1000)
    |> validate_length(:advantage, max: 1000)
    |> validate_length(:patent_status, max: 1000)
    |> validate_attachment_size(params, @attachment_fields, 26_214_400)
    |> cast_attachments_if_valid(params, @attachment_fields, @attachment_required_fields)
  end

  def new_version_changeset(model, old_asset_params, initial_asset_id, params \\ %{}) do
    
    params = cast_uuid_names(params, @attachment_required_fields)
    current_dt = DateTime.utc_now |> DateTime.to_unix

    model
    |> cast(params, @allowed_edit_fields)
    |> Ecto.Changeset.put_change(:type, asset_type())
    |> Ecto.Changeset.put_change(:valid_time, current_dt)
    |> validate_required(@required_edit_fields)
    |> validate_length(:name, max: 500)
    |> validate_length(:name_short, max: 32)
    |> validate_format(:keywords, ~r/^[a-z0-9-_# ]+$/i)
    |> validate_length(:background, max: 2000)
    |> validate_length(:innovation, max: 2000)
    |> validate_length(:application, max: 1000)
    |> validate_length(:advantage, max: 1000)
    |> validate_length(:patent_status, max: 1000)
    |> validate_attachment_size(params, @attachment_fields, 26_214_400)
    |> cast_attachments_if_valid(params, @attachment_fields)
    |> add_version_info(old_asset_params, initial_asset_id)

  end

  def old_version_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_old_version_fields)
    |> validate_required(@required_old_version_fields)
  end

  def api_link_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_api_link_fields)
    |> validate_required(@required_api_link_fields)
  end

  def readable_attr(attribute) do
    if Map.has_key?(@human_readable_attr, attribute) do
      @human_readable_attr[attribute]
    else
      Phoenix.Naming.humanize(attribute)
    end
  end

  def convert_to_api_payload(changeset) do
    params = changeset
      |> resolve_urls
      |> Enum.filter(fn {_, v} -> v && v != nil end)
      |> Enum.into(%{})

    # Process "comment" fields
    comment = params
      |> Map.take(@comment_fields)
      |> Map.values
      |> Enum.map(&Regex.scan(~r/#[0-9a-zA-Z-_]+/, &1)) 
      |> List.flatten
      |> Enum.uniq
      |> Enum.join

    params = Map.put(params, :comment, comment)

    # Process "description" fields
    description = Map.take(params, @description_fields)
      |> Enum.map(fn 
        {k, v} when k in @array_fields ->
          {k, Poison.decode!(v)}
        any -> any
      end)
      |> Enum.into(%{})

    params = if Enum.empty?(description) do
      params
    else
      params
      |> Map.put(:description, Poison.encode!(description))
      |> Map.put(:description_mime, "application/json")
    end

    params
    |> Map.drop(@comment_fields)
    |> Map.drop(@description_fields)
  end

  def handle_file_url_for_update(pay_load, old_asset_params) do
    unless Map.has_key?(pay_load, "icon_url"), 
      do: pay_load = pay_load |> Map.put("icon_url", old_asset_params[:icon_url])

    unless Map.has_key?(pay_load, "cover_url"), 
      do: pay_load = pay_load |> Map.put("cover_url", old_asset_params[:cover_url])

    unless Map.has_key?(pay_load, "attachment_url"), 
      do: pay_load = pay_load |> Map.put("attachment_url", old_asset_params[:attachment_url])  
      
    pay_load
  end

  def add_version_info(changeset, old_asset_params, initial_asset_id) do
    version_info = %{
      initial_asset_id: initial_asset_id,
      source_asset_id: old_asset_params[:asset_id]
    }
    changeset 
      |> Ecto.Changeset.put_change(:version_info, version_info)
  end

  defp resolve_urls(changeset) do
    model =  Ecto.Changeset.apply_changes(changeset)

    Enum.reduce(changeset.changes, %{}, fn
       {key, %{file_name: file_name}}, acc when key in @attachment_fields ->
         full_url = 
           @attachment_map 
           |> Keyword.get(key)
           |> apply(:url, [{file_name, model}])
         Map.put(acc, to_string(key), full_url)
       {key, value}, acc -> Map.put(acc, to_string(key), value)
    end)
  end

  defp dev_api_suffix, do: Application.get_env(:ipseeds, :dev_api_suffix, nil)
end
