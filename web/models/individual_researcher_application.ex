defmodule Ipseeds.IndividualResearcherApplication do
  use Ipseeds.Web, :model
  import Ipseeds.ValidationHelpers

  @allowed_fields ~w(given_names last_name id_number id_type nationality date_of_birth gender
    organization department title telephone mobile address)a
  @required_fields ~w(given_names last_name id_number id_type nationality date_of_birth gender
    organization department title telephone mobile address)a

  schema "individual_researcher_applications" do
    field :given_names, :string
    field :last_name, :string
    field :id_number, :string
    field :id_type, :string
    field :nationality, :string
    field :date_of_birth, Ecto.Date
    field :gender, :string
    field :organization, :string
    field :department, :string
    field :title, :string
    field :telephone, :string
    field :mobile, :string
    field :address, :string
    has_one :user_registration, Ipseeds.UserRegistration, foreign_key: :application_id

    timestamps()
  end

  @id_type_list [
    {"National ID Card", "id_card"},
    {"Passport", "passport"}
  ]

  @gender_list [
    {"Male", "m"},
    {"Female", "f"},
    {"Other", "o"}
  ]

  def id_types, do: @id_type_list

  def genders, do: @gender_list

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
    |> validate_company_branch(:organization)
    |> validate_company_branch(:department)
  end

  def get_id_type(%__MODULE__{id_type: id_type}) do
    id_types()
    |> Enum.map(fn {k, v} -> {v, k} end)
    |> Enum.into(%{})
    |> Map.get(id_type)
  end

  def get_country_name(%__MODULE__{nationality: nationality}) do
    country = Ipseeds.CountriesHelper.all
      |> Enum.find(fn (c) -> Keyword.get(c, :alpha2) == nationality end)

    if country do
      Keyword.get(country, :name)
    else
      nationality
    end
  end

  def get_gender(%__MODULE__{gender: gender_value}) do
    gender = Enum.find(genders(), fn {_k, v} ->  v == gender_value end)

    if gender do
      elem(gender, 0)
    else
      gender_value
    end
  end

  defimpl Ipseeds.ApplicationData do
    alias Ipseeds.IndividualResearcherApplication

    def get_company(%IndividualResearcherApplication{organization: company_name}), do: company_name

    def get_branch(%IndividualResearcherApplication{department: branch_name}), do: branch_name

    def get_address(%IndividualResearcherApplication{address: address}), do: address

    def get_api_company(%IndividualResearcherApplication{organization: company_name}) do
      company_name |> String.replace(~r/\s+/, "_")
    end

    def get_api_branch(%IndividualResearcherApplication{department: branch_name}) do
      branch_name |> String.replace(~r/\s+/, "_")
    end

    def get_name(%IndividualResearcherApplication{given_names: given_names, last_name: last_name}) do
      Ipseeds.NameHelpers.full_name(given_names, last_name)
    end

    def get_nationality_name(%IndividualResearcherApplication{nationality: nationality}) do
      country = Ipseeds.CountriesHelper.all
        |> Enum.find(fn (c) -> Keyword.get(c, :alpha2) == nationality end)

      if country do
        Keyword.get(country, :name)
      else
        nationality
      end
    end

    def get_website(_), do: ""

  end
end
