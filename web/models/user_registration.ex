defmodule Ipseeds.UserRegistration do
  use Ipseeds.Web, :model

  alias Ipseeds.Repo

  @allowed_fields ~w(application_id application_type approved_at
    approved_by_admin_id disapproved_at disapproved_by_admin_id
    disapproved_reason user_id)a
  @required_fields ~w(user_id application_id application_type)a

  @allowed_registration_fields ~w(application_id application_type terms_of_service user_id)a
  @required_registration_fields ~w(application_id application_type terms_of_service user_id)a

  schema "user_registrations" do
    field :application_id, :integer
    field :application_type, :string
    field :terms_of_service, :boolean, virtual: true
    field :approved_at, Ecto.DateTime
    field :disapproved_at, Ecto.DateTime
    field :disapproved_reason, :string
    belongs_to :approved_by_admin, Ipseeds.AdminUser
    belongs_to :disapproved_by_admin, Ipseeds.AdminUser
    belongs_to :user, Ipseeds.User

    timestamps()
  end

  @type_map %{
    "individual" => [module: Ipseeds.IndividualResearcherApplication,
      name: "individual_researcher_application",
      display_name: "Individual Researcher"],
    "industry" => [module: Ipseeds.IndustryApplication,
      name: "industry_application",
      display_name: "Industry Member"],
    "institute" => [module: Ipseeds.ResearchInstituteApplication,
      name: "research_institute_application",
      display_name: "Research Institute"]
  }

  def type_map, do: @type_map

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
  end

  def registration_changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_registration_fields)
    |> validate_required(@required_registration_fields)
    |> validate_inclusion(:terms_of_service, [true],
      message: "you must read and agree with this agreement")
  end

  def is_approved?(%__MODULE__{approved_by_admin: approved_by, approved_at: time, disapproved_at: d_time}) do

    if is_nil(d_time) do 
      !is_nil(approved_by) && !is_nil(time)
    else
      !is_nil(approved_by) && !is_nil(time) && time > d_time
    end
  end

  def is_disapproved?(%__MODULE__{disapproved_at: time, approved_at: a_time}) do
    !is_nil(time) && time > a_time
  end

  def is_unapproved?(%__MODULE__{approved_at: nil, disapproved_at: nil}), do: true
  def is_unapproved?(_), do: false

  def get_user_application(nil), do: nil
  def get_user_application(%__MODULE__{application_type: type, application_id: id}) do
    type_map()[type][:module]
    |> Repo.get!(id)
  end

  def get_user_application_changeset(model, params \\ %{}) do
    model_module = type_map()[model.application_type][:module]

    model_module
    |> Repo.get!(model.application_id)
    |> model_module.changeset(params)
  end
  
  def get_application_type(user_registration) do
    Map.get(type_map(), user_registration.application_type)
  end
end
