defmodule Ipseeds.UserContact do
  use Ipseeds.Web, :model

  alias Ipseeds.Repo

  @allowed_fields ~w(user_id contact_user_id)a
  @required_fields ~w(user_id contact_user_id)a

  schema "user_contacts" do
    belongs_to :user, Ipseeds.User
    belongs_to :contact_user, Ipseeds.User
    
    timestamps()
  end

  def changeset(model, params \\ %{}) do
    model
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
    |> unique_constraint(:unique_contact, name: :user_contacts_user_id_contact_user_id_index,
      message: "already on contact list")
  end

  def is_contact?(current_user, user) do
    # checks if user is contact of current_user
    query = from uc in __MODULE__,
            select: uc,
            where: uc.user_id == ^current_user.id and uc.contact_user_id == ^user.id
    !is_nil Repo.one(query)
  end
end
