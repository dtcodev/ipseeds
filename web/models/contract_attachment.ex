defmodule Ipseeds.ContractAttachment do
  use Ipseeds.Web, :model
  use Ipseeds.AttachmentHelpers

  alias Ipseeds.{Contract, ContractAttachmentUploader}

  @allowed_create_fields ~w(original_filename mime_type)a
  @required_create_fields ~w(original_filename mime_type)a

  @attachment_fields ~w(s3_filename)a
  @attachment_required_fields ~w(s3_filename)a

  schema "contract_attachments" do
    belongs_to :contract, Contract
    field :s3_filename, ContractAttachmentUploader.Type
    field :original_filename, :string
    field :mime_type, :string

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:contract_id, :s3_filename, :original_filename, :mime_type])
    |> validate_required([:contract_id, :s3_filename, :original_filename, :mime_type])
  end

  def create_changeset(model, params \\ %{}) do
    params = cast_uuid_names(params, @attachment_fields)

    model
    |> cast(params, @allowed_create_fields)
    |> validate_required(@required_create_fields)
    |> validate_attachment_size(params, @attachment_fields)
    |> cast_attachments_if_valid(params, @attachment_fields, @attachment_required_fields)
  end
end
