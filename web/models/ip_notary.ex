defmodule Ipseeds.IpNotary do
  use Ipseeds.Web, :model
  use Ipseeds.AttachmentHelpers

  @allowed_fields ~w(api_asset_id tack_id_BTC user_id name_short valid_time name hash_attachment create_time)a
  @required_fields ~w(api_asset_id user_id name_short valid_time name type hash_attachment)a

  @allowed_create_fields ~w(name_short name hash_attachment)a
  @required_create_fields ~w(name_short valid_time name type hash_attachment)a

  @allowed_api_link_fields ~w(user_id api_asset_id)a
  @required_api_link_fields ~w(user_id api_asset_id)a

  @human_readable_attr %{
    :name_short => "Case ID",
    :name => "Title",
    :valid_time => "Date",
    :hash_attachment => "Digital Signature"
  }

  @derive {Phoenix.Param, key: :api_asset_id}
  schema "ip_notaries" do
    field :api_asset_id, :string
    belongs_to :user, Ipseeds.User
    field :name_short, :string, virtual: true
    field :valid_time, Ipseeds.APITimestamp, virtual: true
    field :name, :string, virtual: true
    field :type, :string, virtual: true
    field :hash_attachment, :string, virtual: true
    field :create_time, Ipseeds.APITimestamp, virtual: true
    field :tack_id_BTC, :string

    timestamps()
  end

  def asset_type do
    if Mix.env == :prod do
      "ip_notary"
    else
      "ip_notary_#{Mix.env}"
    end
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_fields)
    |> validate_required(@required_fields)
  end

  def create_changeset(model, params \\ %{}) do
    current_dt = DateTime.utc_now |> DateTime.to_unix
    model
    |> cast(params, @allowed_create_fields)
    |> Ecto.Changeset.put_change(:type, asset_type())
    |> Ecto.Changeset.put_change(:valid_time, current_dt)
    |> validate_required(@required_create_fields)
    |> validate_length(:name, max: 128)
    |> validate_length(:name_short, max: 32)
    |> validate_length(:type, max: 64)
    |> validate_length(:hash_attachment, max: 256)
  end

  def api_link_changeset(struct, params \\ %{}) do
    struct
    |> cast(params, @allowed_api_link_fields)
    |> validate_required(@required_api_link_fields)
  end

  def readable_attr(attribute) do
    if Map.has_key?(@human_readable_attr, attribute) do
      @human_readable_attr[attribute]
    else
      Phoenix.Naming.humanize(attribute)
    end
  end
end
