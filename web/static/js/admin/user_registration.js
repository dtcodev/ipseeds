let UserRegistration = {
  init: () => {
    if($('.admin.user_registrations').length > 0) {
      $('#disapproved-reason-form').submit(function(evt) {
        $('textarea[name="disapproved_reason"]').parent().removeClass("has-error");
        $(this).find(".text-danger").addClass("hidden");
        if ($('textarea[name="disapproved_reason"]').val().replace(/^\s+|\s+$/g, '') == "") {
          $(this).find(".text-danger").removeClass("hidden");
          $('textarea[name="disapproved_reason"]').parent().addClass("has-error");
          $('textarea[name="disapproved_reason"]').focus();
          return false;
        }
      });
    }
  }
}

module.exports = UserRegistration
