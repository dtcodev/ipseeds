// Import dependencies
import "phoenix_html"
import UserRegistration from "./public/user_registration"
import FAQ from "./shared/faq"
import Explorer from "./shared/explorer"
import MediumRSSFeed from "./shared/medium_rss_feed"
import SubscribeNews from "./public/subscribe_news"

// Import local files
// import socket from "./shared/socket"

function close_box(){
  $(".warnbox").fadeOut(300);
}
function close_box(){
  $(".msetting").fadeOut(300);
}
function close_box2(){
  $(".msetting2").fadeOut(300);
}
function open_box(evt){
  var position = $(this).offset();
  var $msetting = $(".msetting");
  $msetting.fadeIn(300);
  $msetting.offset({ 'top': position.top - 5, 'left': position.left - $msetting.width() });
}
function open_box2(evt){
  var position = $(this).offset();
  var $msetting = $(".msetting2");
  $msetting.fadeIn(300);
  $msetting.offset({ 'top': position.top - 5, 'left': position.left - $msetting.width() });
}

let Public = {
  init: () => {
    $(".listlink").on("click", function (e) {
      var $this = $(this);
      if (!$this.hasClass("on")) {
        var index = $this.index(".listlink");
        console.log(index);
        console.log($(".layer:not(.hide),.layer:eq(" + index + ")"));
        $(".layer:not(.hide),.layer:eq(" + index + ")").toggleClass("hide");
        $this.add(".listlink.on").toggleClass("on");
      }
    });
    $(".listlink2").on("click", function (e) {
      var $this = $(this);
      if (!$this.hasClass("on")) {
        var index = $this.index(".listlink2");
        console.log(index);
        console.log($(".layer2:not(.hide),.layer2:eq(" + index + ")"));
        $(".layer2:not(.hide),.layer2:eq(" + index + ")").toggleClass("hide");
        $this.add(".listlink2.on").toggleClass("on");
      }
    });
    $(".listlink3").on("click", function (e) {
      var $this = $(this);
      if (!$this.hasClass("on")) {
        var index = $this.index(".listlink3");
        console.log(index);
        console.log($(".layer:not(.hide),.layer:eq(" + index + ")"));
        $(".layer:not(.hide),.layer:eq(" + index + ")").toggleClass("hide");
        $this.add(".listlink3.on").toggleClass("on");
      }
    });
    $(".listlink4").on("click", function (e) {
      var $this = $(this);
      if (!$this.hasClass("on")) {
        var index = $this.index(".listlink4");
        console.log(index);
        console.log($(".layer3:not(.hide),.layer3:eq(" + index + ")"));
        $(".layer3:not(.hide),.layer3:eq(" + index + ")").toggleClass("hide");
        $this.add(".listlink4.on").toggleClass("on");
      }
    });
    $(".thumbnail.feature_link").on("click", function (e) {
      var $this = $(this);
      var linkUrl = $this.data("link");
      var anchor = $this.data("anchor");
      location.href = linkUrl + anchor;
    });

    marqueeInit({
      uniqueid: 'mycrawler',
      style: {
        'width': '100%',
      },
      inc: 5, //speed - pixel increment for each iteration of this marquee's movement
      mouse: 'cursor driven', //mouseover behavior ('pause' 'cursor driven' or false)
      moveatleast: 3,
      neutral: 150,
      persist: true,
      savedirection: true
    });

    // Initialize modules of other pages
    UserRegistration.init();
    FAQ.init();
    Explorer.init();
    MediumRSSFeed.init();
    SubscribeNews.init();

    // Placeholder fix for older browsers
    $('input, textarea').placeholder();
  }
};

module.exports = Public;
