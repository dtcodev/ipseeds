// Import dependencies
import "phoenix_html"

// Import local files
// import socket from "./shared/socket"
import UserRegistration from "./admin/user_registration"

let Admin = {
  init: () => {
    // Initialize modules of other pages
    UserRegistration.init();

    // Placeholder fix for older browsers
    $('input, textarea').placeholder();

    console.log("Admin initialized.");
  }
};

module.exports = Admin;
