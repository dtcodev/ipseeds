// Import dependencies
import "phoenix_html"

import PassPhrase from "./app/passphrase"
import UserProfile from "./app/user_profile"
import IpDiscovery from "./app/ip_discovery"
import IpNotary from "./app/ip_notary"
import IpSummary from "./app/ip_summary"
import UserContact from "./app/user_contact"
import Message from "./app/message"
import Session from "./app/session"
import Contract from "./app/contract"
import FAQ from "./shared/faq"
import Explorer from "./shared/explorer"
import AgreeableContract from "./app/agreeable_contract"

// Import local files
// import socket from "./shared/socket"


let App = {
  init: () => {
    console.log("App initialized.");

    // Initialize modules of other pages
    PassPhrase.init();
    UserProfile.init();
    IpDiscovery.init();
    IpNotary.init();
    IpSummary.init();
    UserContact.init();
    Message.init();
    Session.init();
    Contract.init();
    FAQ.init();
    Explorer.init();
    AgreeableContract.init();

    // Placeholder fix for older browsers
    $('input, textarea').placeholder();
  }
};

module.exports = App;
