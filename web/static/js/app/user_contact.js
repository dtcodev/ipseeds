import ErrorHelper from "../shared/error_helper"

let UserContact = {
  init: () => {
    $(".add-to-contact-list-btn").on("click", UserContact._handleAddToContactsClick);
    $(".delete-from-contact-list-btn").on("click", UserContact._handleDeleteFromContactsClick)
  },

  _handleDeleteFromContactsClick: (event) => {
    event.preventDefault();
    let $el = $(event.target);
    let userContactPath = $el.data("userContactPath");
    let csrfToken = $el.data("csrfToken");
    let defaultFailureText = $el.data("defaultFailureText");
    $.ajax({
      url: userContactPath,
      type: "delete",
      headers: {"X-CSRF-TOKEN": csrfToken},
      dataType: "json"
    })
      .done((data, textStatus) => {
        $($el.data("target")).modal('hide');
        return $el.closest("tr").detach();
      })
      .fail((jqXHR, textStatus) => {
        if (jqXHR.responseJSON && jqXHR.responseJSON.errors) {
          return alert(ErrorHelper.getErrorMessages(jqXHR.responseJSON.errors).join(', '))
        }
        return alert(defaultFailureText);
      });
  },

  _handleAddToContactsClick: (event) => {
    event.preventDefault();
    let $el = $(event.currentTarget);
    let userContactPath = $el.data("userContactPath");
    let contactUserId = $el.data("contactUserId");
    let csrfToken = $el.data("csrfToken");
    let successText = $el.data("successText");
    let contactIndexLink = $el.data("contactIndexLink");
    let onProfile = $el.data("onProfile");
    let defaultFailureText = $el.data("defaultFailureText");
    $.ajax({
      url: userContactPath,
      type: "post",
      data: {user_contact: {contact_user_id: contactUserId}},
      headers: {"X-CSRF-TOKEN": csrfToken},
      dataType: "json"
    })
      .done((data, textStatus) => {
        if(onProfile){
          return UserContact._handleAddToContactsSuccess_onProfile($el, successText);
        }
        return UserContact._handleAddToContactsSuccess($el, successText, contactIndexLink);
      })
      .fail((jqXHR, textStatus) => {
        if (jqXHR.responseJSON && jqXHR.responseJSON.errors) {
          return UserContact._handleAddToContactsError(jqXHR.responseJSON.errors, defaultFailureText)
        }
        return UserContact._handleAddToContactsError(null, defaultFailureText)
      });
  },

  _handleAddToContactsError: (errors, defaultFailureText) => {
    let $errorContainer = $("#add-to-contact-list-errors").empty();
    if (!errors) return $errorContainer.append(defaultFailureText);
    $errorContainer.append(ErrorHelper.getErrorList(errors));
  },

  _handleAddToContactsSuccess: ($el, successText, successLink) => {
    let $link = $("<a></a>").text(successText).attr({href: successLink});
    let $parent = $("<p></p>").append($link);

    $el.replaceWith($parent);
  },

  _handleAddToContactsSuccess_onProfile: ($el, successText) => {
    let $newEl = $el.find("span");
    $newEl.attr("data-original-title", successText)
          .find("i.fa")
          .removeClass("fa-user-plus")
          .addClass("fa-address-book");
    $el.replaceWith($newEl);
    $newEl.tooltip('show');
  }
};

module.exports = UserContact;
