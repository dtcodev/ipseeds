import HashHelper from "../shared/hash_helper"
import FormHelper from "../shared/form_helper"
import {sprintf} from "sprintf-js";

let IpNotary = {
  init: () => {
    if ($('.secure.ip_notaries').length > 0) {
      // Objects are passed by reference in Javascript, so this object will persist
      // even inside the event handlers. This prevents tampering that might be possible
      // if we stored data inside a hidden element or data- attribute
      let state = {droppedFile: null, droppedFileHash: null, isProcessing: false};

      $('#add-ip-notary #proof-file').change(IpNotary.getHandleProofFileChange(state));

      $('#dropzone').on('drag dragstart dragend dragover dragenter dragleave drop', (event) => {
        event.preventDefault();
        event.stopPropagation();
      }).on('dragover dragenter', () => $(this).addClass('is-dragover'))
        .on('dragleave dragend drop', () => $(this).removeClass('is-dragover'))
        .on('drop', IpNotary.getHandleFileDrop(state));

      $('#ip-notary-form').on("submit", IpNotary.getHandleIpNotarySubmit(state));
      $('#confirmationModal .submit-btn').click((event) => {
        event.preventDefault();
        event.stopPropagation();
        $('#confirmationModal').modal('hide');
        $('#ip-notary-form').submit();
      });
    }
  },
  getHandleIpNotarySubmit: (state) => {
    return (event) => {
      event.preventDefault();
      event.stopPropagation();
      let $form = $("#ip-notary-form");

      // Remove any previously added hash_attachment fields
      $form.find("input[name='ip_notary[hash_attachment]']").remove();

      if (!state.isProcessing && state.droppedFile && state.droppedFileHash) {
        let hashString = state.droppedFileHash;
        let $newHashAttachmentField = $("<input>")
          .attr({type: "hidden", name: "ip_notary[hash_attachment]"}).val(hashString);
        $form.append($newHashAttachmentField);
        $form[0].submit(); // submit HTML node, not jQuery object to prevent calling event handler
      } else {
        $form[0].submit(); // submit HTML node, not jQuery object to prevent calling event handler
      }
    }
  },
  getHandleProofFileChange: (state) => {
    // Modifies droppedFile in order to prevent tampering
    return (event) => {
      let files = event.target.files;
      if (state.isProcessing || !files || files.length < 1) return false;
      if (files[0].size > window.exagon.attachment_max_size_bytes) {
        alert(window.exagon.file_too_big_error);
        FormHelper.clearInputFile(evt.target);
        return false;
      }
      state.droppedFile = files[0];

      IpNotary.setIsProcessing(state);
      HashHelper.hashFile(state.droppedFile,
        (hashString) => IpNotary.handleHashResult(state, hashString));
    }
  },
  getHandleFileDrop: (state) => {
    return (event) => {
      if (state.isProcessing) return false;
      if (event.originalEvent.dataTransfer.files[0].size > window.exagon.attachment_max_size_bytes) {
        alert(window.exagon.file_too_big_error);
        FormHelper.clearInputFile(evt.target);
        return false;
      }
      state.droppedFile = event.originalEvent.dataTransfer.files[0];

      IpNotary.setIsProcessing(state);
      HashHelper.hashFile(state.droppedFile,
        (hashString) => IpNotary.handleHashResult(state, hashString));
    }
  },
  setIsProcessing: (state) => {
    state.isProcessing = true;
    let processingText = $("#ip-notary-form").data("processingText");
    $('#dropzone .choose-file-link label').text(processingText);
    $("#hashed-file").text("");
  },
  handleHashResult: (state, hashString) => {
    state.isProcessing = false;
    state.droppedFileHash = hashString;
    let fileName = state.droppedFile.name;
    let changeFileText = $("#ip-notary-form").data("changeFileText");
    let $fileName = $("<strong></strong>").text(sprintf("%s (%s)", fileName, changeFileText));
    $('#dropzone .choose-file-link label').empty().append($fileName);
    $("#hashed-file").text(hashString);
  }
};

export default IpNotary;
