import {Buffer} from "buffer"

let Mnemonic = bcRequire("bitcore-mnemonic");
let eccrypto = require("eccrypto");

let PassPhrase = {
  init: () => {
    if ($('.secure.users.passphrase').length > 0) {
      let code = new Mnemonic();
      $('#preview-passphrase').text(code.toString());

      $('.passbox button#change-passphrase').click(function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        code = new Mnemonic();
        $('#preview-passphrase').text(code.toString());
      });

      $('.passbox #show-passphrase-again').click(function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        $('#passphrase-confirmation').val(code.toString());
        alert("Please make sure to write it down and keep it safe.\nThe passphrase won't be stored anywhere on the system.");
      });

      $('.passbox button[type="submit"]').click(function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        let $passbox = $(".passbox");
        let code_confirmation = $('#passphrase-confirmation').val();
        if (code_confirmation != code.toString()) {
          alert("Passphrase confirmation is incorrect.\nYou can use \"Show me my passphrase\" button to show your passphrase again");
          return false;
        }
        let hdPrivKey = code.toHDPrivateKey();

        let bip_input = $("<input>")
          .attr("type", "hidden")
          .attr("name", "user_keypair[bip32_hd_public_key]").val(hdPrivKey.hdPublicKey);
        $passbox.append(bip_input);
        
        let privKey = hdPrivKey.privateKey.toBuffer();
        let pubKey = eccrypto.getPublic(privKey);
        let pubKeyHex = Buffer.from(pubKey).toString("hex");

        let ecc_pub_key_input = $("<input>")
          .attr("type", "hidden")
          .attr("name", "user_keypair[ecc_public_key_hex]").val(pubKeyHex);
        $passbox.append(ecc_pub_key_input);
        $('#submit-passphrase').submit();
      });
    }
  }
};

export default PassPhrase;