import filesize from "filesize"

let UserProfile = {
  init: () => {
    if($('.secure.users #user-profile').length > 0) {
      $('#update-profile-photo').fileupload({
        url: $('#update-profile-photo').data("action"),
        dataType: 'json',
        method: 'POST',
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        headers: {
          "X-CSRF-TOKEN": $('input[name="_csrf_token"]').val()
        },
        add: function(e, data) {
          $('.photo-upload').siblings('.loading-indicator').show();
          $('.photo-upload').hide();
          $('.upload-note').hide();
          $('.photo-upload-wrap .errors').empty();
          let size_limit = window.exagon.profile_photo_size_limit;
          if (data.files[0].size <= size_limit) {
            data.submit();
          } else {
            let error_str = "<ul>";
            error_str += "<li>" + window.exagon.file_too_big_error + "</li>";
            error_str += "</ul>";
            $('.photo-upload-wrap .errors').html(error_str);
            $('.photo-upload').siblings('.loading-indicator').hide();
            $('.photo-upload').show();
          }
        },
        done: function (e, data) {
          $('.photo-upload').siblings('.loading-indicator').hide();
          $('.photo-upload').show();
          if (data.result.success) {
            var new_photo = $("<img/>").addClass('in-queue').load(function(){
              $(this).siblings('img').remove();
              $(this).removeClass('in-queue');
            });
            $('.upload-note').show();
            new_photo.attr("src", data.result.file).appendTo("#profile-photo-container");
          } else {
            $('.upload-note').hide();
            var error_str = "<ul>";
            $.each(data.result.errors, function(idx, msg){
              error_str += "<li>" + msg + "</li>";
            });
            error_str += "</ul>";
            $('.photo-upload-wrap .errors').html(error_str);
          }
        },
        fail: function (e, data) {
          $('.photo-upload').siblings('.loading-indicator').hide();
          $('.photo-upload').show();
          var error_str = "<ul><li>Unable to connect to server</li></ul>";
          if (data.textStatus == "parsererror") {
            error_str = "<ul><li>Session Expired. Please refresh the page to sign-in again.</li></ul>";
          }
          $('.photo-upload-wrap .errors').html(error_str);
        }
      });

      $('#reset-password-btn').click(function(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        var loading_indicator = $(this).siblings('.loading-indicator');
        loading_indicator.show();

        $.ajax({
          url: $(this).data('action'),
					headers: {
						"X-CSRF-TOKEN": $('input[name="_csrf_token"]').val()
					},
          method: 'POST'
        })
          .done(function(data) {
            alert(data.message);
          })
          .always(function() {
            loading_indicator.hide();
          })
      })
    }
  }
}

module.exports = UserProfile
