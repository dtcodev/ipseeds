import {Buffer} from "buffer"
import EncryptionHelper from "../shared/encryption_helper"
import HashHelper from "../shared/hash_helper"
import FormHelper from "../shared/form_helper"
import {sprintf} from "sprintf-js"
import sha256 from "js-sha256"
import jBinary from "jbinary"
import ErrorHelper from "../shared/error_helper"

let Mnemonic = bcRequire("bitcore-mnemonic");
let Promise = require("bluebird");
let eccrypto = require("eccrypto");
let download = require("downloadjs");

let Contract = {
  init: () => {
    if ($(".secure.contracts").length > 0) {
      $(".table select").select2();

      let state = {
        mainAttachmentFile: null, mainAttachmentHash: null, encryptedMainAttachment: null,
        extraAttachmentFiles: null, encryptedExtraAttachments: null, isProcessing: false
      };
      let $proofFile = $("#proof-file");

      $("#ip-notary-signature").on("drag dragstart dragend dragover dragenter dragleave drop",
        (event) => {
          event.preventDefault();
          event.stopPropagation();
        }).on("dragover dragenter", () => $(this).addClass("is-dragover"))
        .on("dragleave dragend drop", () => $(this).removeClass("is-dragover"))
        .on("drop", handleMainFileDrop(state));

      if ($("#ip-notary-list").length > 0) {
        loadIpNotaryList();

        $("#ip-notary-select").on("click", ".select-ip", handleSelectIp);
        $(".addblock").on("click", ".remove-ip-notary", handleRemoveIp(state));
      }

      $proofFile.on("change", handleMainFileInput(state));
      $("#contract-submit").on("click", handleIssueContract(state));

      //  #show
      let showState = {
        encryptedMainAttachmentObject: null,
        decryptedMainAttachmentBuffer: null
      };
      $("#verify-attached-file-btn").on("click", handleVerifyFileClick);
      $("#submit-passphrase-btn").on("click", handleSubmitPassphrase(showState));
      $("#download-file-btn").on("click", handleDownloadClick(showState));
    }
  }
};

export default Contract;

function loadIpNotaryList() {
  $.ajax({
    url: $("#ip-notary-list").data("source"),
    headers: {
      "X-CSRF-TOKEN": $("input[name=\"_csrf_token\"]").val()
    },
    method: "POST"
  }).done(function (data) {
    let result = {
      ip_notaries: data,
      formatEntryTime: function () {
        return function (createTime, render) {
          createTime = render(createTime);
          return moment.unix(createTime).utcOffset(8).format("YYYY-MM-DD hh:mm:ss");
        }
      }
    };
    let template = $("#table-row-template").html(),
      rendered = Mustache.render(template, result);
    $("#ip-notary-list tr:first-child").after(rendered);
  });
}

function handleSelectIp() {
  let self = $(this);
  let parent = self.parent().parent(); // find the parent <tr>
  let datarecord = {
    asset_id_full: parent.find(".name_short").data("asset-id"),
    hash_attachment: parent.data("hashAttachment") == "" ?
      "No digital signature provided." : parent.data("hashAttachment"),
    name_short: parent.find(".name_short").text(),
    name: parent.find(".name").text(),
    create_time: parent.find(".create_time").text()
  };
  let template = $("#datarecord-template").html();
  let rendered = Mustache.render(template, datarecord);

  parent.hide();
  $("#add-ip-notary").hide();
  $("#add-ip-notary").before(rendered);
  $("#ip-notary-select").modal("hide");

  $("#select-ip-notary-prompt").hide();
  $("#original-file-signature").data("hashAttachment", datarecord.hash_attachment)
    .text(datarecord.hash_attachment.slice(0, 30) + "...");
  $("#ip-notary-signature").show();
}

function handleRemoveIp(state) {
  return (event) => {
    event.preventDefault();
    event.stopPropagation();
    let self = $(event.target).closest("a");
    let parent = self.parent();
    let asset_id = parent.data("asset-id");
    $(sprintf("#ip-notary-list [data-asset-id=\"%s\"]", asset_id)).parent().show();
    parent.remove();
    $("#add-ip-notary").show();
    $("#select-ip-notary-prompt").show();
    $("#ip-notary-signature").hide();
    state.mainAttachmentHash = null;
    state.mainAttachmentFile = null;
    $("#selected-files-list").empty();
    $("#no-files").show();
    state.isProcessing = false;
    $(".file-signature-alert").hide();
    $("#selected-file-filename").text("");
    $("#selected-file-signature").text("");
  };

}

function handleMainFileDrop(state) {
  return (event) => {
    let files = event.originalEvent.dataTransfer.files;
    handleMainFileSelect(state, files);
  }
}

function handleMainFileInput(state) {
  return (event) => {
    let files = event.target.files;
    handleMainFileSelect(state, files);
  }
}

function handleMainFileSelect(state, files) {
  if (state.isProcessing || !files || files.length < 1) return false;
  if (files[0].size > window.exagon.attachment_max_size_bytes) {
    alert(window.exagon.file_too_big_error);
    return false;
  }
  $(".file-signature-alert").hide();
  state.mainAttachmentFile = files[0];

  setIsProcessing(state);
  $("#selected-file-filename").text(state.mainAttachmentFile.name);
  HashHelper.hashFile(state.mainAttachmentFile,
    (hashString) => handleHashResult(state, hashString));
  $("#selected-files-list").empty().append(getFileListItem(state.mainAttachmentFile.name));
  $("#no-files").hide();
}

function getFileListItem(filename) {
  let $li = $("<li></li>").text(filename);
  let $fileIcon = $("<i></i>").addClass("fa fa-file-o");
  $li.prepend($fileIcon);
  return $li;
}

function setIsProcessing(state) {
  state.isProcessing = true;
  $("#selected-file-signature").text("Processing...");
}

function handleHashResult(state, hashString) {
  state.isProcessing = false;
  state.mainAttachmentHash = hashString;
  let origHash = $("#original-file-signature").data("hashAttachment");

  $("#selected-file-signature").text(hashString.slice(0, 30) + "...");
  if (hashString == origHash) {
    $("#file-signature-match").show();
  } else {
    $("#file-signature-not-match").show();
  }
}

function handleIssueContract(state) {
  return (event) => {
    event.preventDefault();
    event.stopPropagation();
    $('#contractFormErrors').remove();

    let $form = $("#issue-contract-form");

    let $recipientSelect = $("#contract_recipient_user_id");
    let recipientUserId = $recipientSelect.val();
    let noRecipientMessage = $form.data("noRecipientMessage");
    if (recipientUserId == "") return alert(noRecipientMessage);

    let $dataRecords = $(".addblock .datarecords");
    let noIpMessage = $form.data("noIpMessage");
    if ($dataRecords.length == 0) return alert(noIpMessage);

    let noMainFileMessage = $form.data("noMainFileMessage");
    if (!state.mainAttachmentFile) return alert(noMainFileMessage);

    let $selectedRecipientOption = $recipientSelect.find("option:selected").first();
    let linkedAssetId = $dataRecords.data("assetId");
    let csrfToken = $form.find("input[name=\"_csrf_token\"]").first().val();
    let recipientPublicKeyHex = $selectedRecipientOption.data("publicKey");
    let recipientPublicKey = Buffer.from(recipientPublicKeyHex, "hex");

    let formData = new FormData();
    formData.append("ip_notary_asset_id", linkedAssetId);
    formData.append("contract[recipient_user_id]", recipientUserId);
    toggleIsSubmitting(true);

    Promise.resolve(readFileAsBufferAsync(state.mainAttachmentFile))
      .then((mainFileBuffer) => eccrypto.encrypt(recipientPublicKey, mainFileBuffer))
      .then((mainFileEncryptedObject) => {
        let mainFileEncryptedBuffer = EncryptionHelper.getEncryptedBufferFromEncryptedObject(
          mainFileEncryptedObject);
        let mainFileEncryptedBlob = new Blob([mainFileEncryptedBuffer], {type: "application/octet-stream"});
        formData.append("main_attachment[s3_filename]", mainFileEncryptedBlob);
        formData.append("main_attachment[original_filename]", state.mainAttachmentFile.name);
        formData.append("main_attachment[mime_type]", state.mainAttachmentFile.type || "application/octet-stream");
      }).then(() => $.ajax({
      url: $form.attr("action"),
      method: "POST",
      headers: {"X-CSRF-TOKEN": csrfToken},
      data: formData,
      processData: false,
      contentType: false
    }).done((res, textStatus, jqXHR) => {
      if (jqXHR.status == 201) {
        return window.location.assign(res.data.redirect_to);
      } else {
        return window.location.reload(true);
      }
    }).fail((jqXHR, textStatus, errorThrown) => {
      let $sendButton = $("#contract-submit").parent();
      if (!jqXHR.responseJSON || !jqXHR.responseJSON.errors) {
        $("<p></p>").addClass("danger").text(errorThrown).insertAfter($sendButton);
      } else {
        let errors = jqXHR.responseJSON.errors;
        let $errorList = ErrorHelper.getErrorList(errors).attr({id: "contractFormErrors"});
        $errorList.insertAfter($sendButton);
      }
      toggleIsSubmitting(false);
    }));
  }
}

function toggleIsSubmitting(isSubmitting = true) {
  if (isSubmitting) {
    $("#contract-submit").hide();
    $("#contract-is-submitting").show();
  } else {
    $("#contract-submit").show();
    $("#contract-is-submitting").hide();
  }
}

function handleVerifyFileClick(event) {
  event.stopPropagation();
  event.preventDefault();
  $("#passphrase-input-modal").modal("show");
  $("#passphrase").select();
}

function handleSubmitPassphrase(state) {
  return (event) => {
    event.stopPropagation();
    event.preventDefault();
    $("#passphrase-error").addClass("hidden");

    let $passphraseInput = $("#passphrase");
    let invalidPassphraseErrorMessage = $passphraseInput.data("invalidPassphraseErrorMessage");
    let passphraseMismatchErrorMessage = $passphraseInput.data("passphraseMismatchErrorMessage");
    let passphrase = $passphraseInput.val().trim();

    if (!Mnemonic.isValid(passphrase)) return handlePassphraseError(invalidPassphraseErrorMessage);

    let mnCode = new Mnemonic(passphrase);
    let hdPrivKey = mnCode.toHDPrivateKey();
    let hdPublicKey = hdPrivKey.hdPublicKey.toString();

    if (hdPublicKey != window.exagon.current_user_bip32_public_key) {
      return handlePassphraseError(passphraseMismatchErrorMessage)
    }

    $("#passphrase-input-modal").modal("hide");
    $("#verify-attached-file-btn").hide();
    $("#file-is-verifying").show();

    // Create demo encrypted binary file
    Promise.resolve(jBinary.loadData(window.exagon.main_attachment_url))
      .then((encryptedArrayBuffer) => {
        let encryptedBuffer = Buffer.from(encryptedArrayBuffer);
        let encryptedObject = EncryptionHelper.getEncryptedObjectFromEncryptedBuffer(encryptedBuffer);
        state.encryptedMainAttachmentObject = encryptedObject;
        return eccrypto.decrypt(hdPrivKey.privateKey.toBuffer(), encryptedObject);
      })
      .then((decryptedBuffer) => {
        $("#file-is-verifying").hide();
        state.decryptedMainAttachmentBuffer = decryptedBuffer;
        let sharedFileHash = sha256(decryptedBuffer);
        let originalFileHash = window.exagon.main_attachment_hash;
        $("#shared-file-hash").text(sharedFileHash);
        $("#shared-file-hash-container").show();

        // Show success or failure message depending on whether or not the file matches
        if (sharedFileHash == originalFileHash) {
          $("#file-verification-success").show();
        } else {
          $("#file-verification-failure").show();
        }

        $("#download-file-btn").show();
      });

  }
}

function handleDownloadClick(state) {
  return (event) => {
    event.preventDefault();

    let bufferToDownload = state.decryptedMainAttachmentBuffer;
    let mimeType = window.exagon.main_attachment.mime_type;
    let fileName = window.exagon.main_attachment.original_filename;
    let blob = new Blob([bufferToDownload], {type: mimeType});

    download(blob, fileName, mimeType)
  }
}

function handlePassphraseError(errorMessage) {
  $("#passphrase-error").text(errorMessage).removeClass("hidden");
}

function downloadAndDecryptAttachment(event) {
  event.preventDefault();
  event.stopPropagation();

  let $submitButton = $(this);
  let downloadURL = $submitButton.data("target");
  let fileName = $submitButton.data("filename");
  let mimeType = $submitButton.data("mimetype");
  let $passphraseInput = $("#passphrase");
  let passphrase = $passphraseInput.val().trim();

  let mnCode = new Mnemonic(passphrase);
  let hdPrivKey = mnCode.toHDPrivateKey();
  let privateKey = hdPrivKey.privateKey.toBuffer();

  downloadAttachment(downloadURL)
    .then((fileBlob) => {
      return readFileAsBufferAsync(fileBlob);
    })
    .then((fileBuffer) => {
      let encryptedContentBuffer = Buffer.from(fileBuffer);
      let encryptedContentObject = EncryptionHelper.getEncryptedObjectFromEncryptedBuffer(encryptedContentBuffer);
      return eccrypto.decrypt(privateKey, encryptedContentObject);
    })
    .then((decryptedContent) => {
      let decryptedContentBlob = new Blob([decryptedContent], {type: mimeType});
      download(decryptedContentBlob, fileName, mimeType);
    });
}

function downloadAttachment(downloadURL) {
  return new Promise((resolve, reject) => {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", downloadURL, true);
    xhr.responseType = "blob";
    xhr.onload = function () {
      if (xhr.status == 200) {
        resolve(xhr.response);
      } else {
        reject(Error(xhr.statusText));
      }
    };
    xhr.onerror = reject;
    xhr.send();
  });
}

function readFileAsBufferAsync(file) {
  return new Promise((resolve, reject) => {
    try {
      let reader = new FileReader();
      reader.onload = (event) => resolve(event.target.result);
      reader.readAsArrayBuffer(file);
    } catch (e) {
      reject(e);
    }
  });
}
