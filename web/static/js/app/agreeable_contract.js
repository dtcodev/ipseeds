import FormHelper from "../shared/form_helper"

let AgreeableContract = {
  init: () => {
    if ($(".secure.agreeable_contract").length > 0) {
      $(".table select.contact-select").select2();
      $(".nda_contract_type").on("change", function(){
      	AgreeableContract.changeType($(this).val());
      });
      $("#agreeable_contract_recipient_user_id").on("change", function(){
        AgreeableContract.changeContact(this);
      });
      $("#agreeable_contract_sender_signature_url").change(function (event) {
        let $this = $(this); 
        if (event.target.files && event.target.files[0]) {
          if (event.target.files[0].size <= window.exagon.attachment_max_size_bytes) {
            let reader = new FileReader();
            reader.onload = function (e) {
              $(".signature-preview").remove();
              $("<img class='signature-preview' width='200' src='" + e.target.result + "' alt='cover images'/>").insertBefore($this)
              $this.parent().find(".but06").removeClass("but06").addClass("but04 but-inline").text("Change")
            };
            reader.readAsDataURL(event.target.files[0]);
          } else {
            alert(window.exagon.file_too_big_error);
            FormHelper.clearInputFile(event.target);
            return false;
          }
        }
      });
      $("#agreeable_contract_recipient_signature_url").change(function (event) {
        let $this = $(this); 
        if (event.target.files && event.target.files[0]) {
          if (event.target.files[0].size <= window.exagon.attachment_max_size_bytes) {
            let reader = new FileReader();
            reader.onload = function (e) {
              $(".recipient-signature-preview").remove();
              $("<img class='recipient-signature-preview' width='200' src='" + e.target.result + "' alt='cover images'/>").insertBefore($this)
              $this.parent().find(".but06").removeClass("but06").addClass("but04 but-inline").text("Change")
            };
            reader.readAsDataURL(event.target.files[0]);
          } else {
            alert(window.exagon.file_too_big_error);
            FormHelper.clearInputFile(event.target);
            return false;
          }
        }
      });
      $("#nda-submit").on("click", function(event){
        AgreeableContract.handleIssueContract(event);
      });
      $("#nda-cancel-submit").on("click", function(event){
        AgreeableContract.handleCancelContract(event);
      });
      $("#nda-decline-submit").on("click", function(event){
        AgreeableContract.handleDeclineContract(event);
      });
      $("#nda-accept-submit").on("click", function(event){
        AgreeableContract.handleAcceptContract(event);
      });
    }	
  },
  changeType: (nda_contract_type) => {
  	$(".nda-plaintext-tab").hide();
    $("." + nda_contract_type + "-nda").show();
  	$("#nda-title").text(nda_contract_type.toUpperCase());
  },
  changeContact: (select_elem) => {
    let selected_option = $(select_elem).find(":selected");
    let name = selected_option.data("name");
    let nationality = selected_option.data("nationality");
    let address = selected_option.data("address");
    
    $(".input-recipient-name").val(name + " [" + nationality + "]");
    $(".input-recipient-address").val(address);
    $(".recipient-name").text(name + " [" + nationality + "]")
  },
  handleIssueContract: (event) => {
    event.preventDefault();
    event.stopPropagation();

    $("#issue-agreeable-contract-form").submit();
  },
  handleCancelContract: (event) => {
    event.preventDefault();
    event.stopPropagation();

    $("#cancel-agreeable-contract-form").submit();
  },
  handleDeclineContract: (event) => {
    event.preventDefault();
    event.stopPropagation();

    $("#decline-agreeable-contract-form").submit();
  }
  ,
  handleAcceptContract: (event) => {
    event.preventDefault();
    event.stopPropagation();

    $("#accept-agreeable-contract-form").submit();
  }
}

export default AgreeableContract;
