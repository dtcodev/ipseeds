import HashHelper from "../shared/hash_helper"
import FormHelper from "../shared/form_helper"
import {sprintf} from "sprintf-js"
import _ from "lodash"

let IpSummary = {
  init: () => {

    if(window.location.href.indexOf("edit") > -1) handleClearFileInputs();

    $(".warnbox .close").on("click", handleCloseWarningBox);
    $("a.toggle-ip-summary-visibility").on("click", handleToggleIpSummaryVisibility);

    $("span.inventor-name[title], .add-to-contact").tooltip();

    if ($(".secure.ip_summaries").length > 0) {
      if ($("#ip-notary-list").length > 0) {
        $.ajax({
          url: $("#ip-notary-list").data("source"),
          headers: {
            "X-CSRF-TOKEN": $('input[name="_csrf_token"]').val()
          },
          method: "POST"
        }).done(function (data) {
          let result = {
            ip_notaries: data,
            formatEntryTime: function () {
              return function (createTime, render) {
                createTime = render(createTime);
                return moment.unix(createTime).utcOffset(8).format("YYYY-MM-DD hh:mm:ss");
              }
            }
          };
          let template = $("#table-row-template").html(),
            rendered = Mustache.render(template, result);
          $("#ip-notary-list tr:first-child").after(rendered);
        });
      }

      $("#ip_summary_icon_url").change(function (evt) {
        if (evt.target.files && evt.target.files[0]) {
          if (evt.target.files[0].size <= window.exagon.attachment_max_size_bytes) {
            let reader = new FileReader();
            reader.onload = function (e) {
              $(".logo-preview").attr("src", e.target.result)
                .parent(".preview-pane").css("display", "inline-block")
                .siblings(".but04").removeClass(".but-inline");
            };
            reader.readAsDataURL(evt.target.files[0]);
          } else {
            alert(window.exagon.file_too_big_error);
            FormHelper.clearInputFile(evt.target);
            return false;
          }
        }
      });
      $("#ip_summary_cover_url").change(function (evt) {
        if (evt.target.files && evt.target.files[0]) {
          if (evt.target.files[0].size <= window.exagon.attachment_max_size_bytes) {
            let reader = new FileReader();
            reader.onload = function (e) {
              $(".cover-preview").attr("src", e.target.result)
                .parent(".preview-pane").css("display", "inline-block")
                .siblings(".but04").removeClass(".but-inline");
            }
            reader.readAsDataURL(evt.target.files[0]);
          } else {
            alert(window.exagon.file_too_big_error);
            FormHelper.clearInputFile(evt.target);
            return false;
          }
        }
      });

      $("#ip_summary_attachment_url").change(function (evt) {
        if (evt.target.files && evt.target.files[0] &&
          evt.target.files[0].size > window.exagon.attachment_max_size_bytes) {

          alert(window.exagon.file_too_big_error);
          FormHelper.clearInputFile(evt.target);
          return false;
        }
      });

      $("#submit-ip-summary").click(function (evt) {
        evt.preventDefault();
        evt.stopPropagation();

        // Proccess Linked IP Notaries
        let linked_ip_notaries = {
          items: []
        }
        linked_ip_notaries.items = $(".addblock .datarecords").map(function (idx, el) {
          return {asset_id: $(el).data("asset-id")};
        }).get();
        let tags_string = JSON.stringify(linked_ip_notaries);
        $('input[name="ip_summary[tag]"]').val(tags_string);

        // Process Inventors
        let inventors = $(".inventor_name").map(function (idx, el) {
          if ($(el).val() == '') {
            return;
          } else {
            return {name: $(el).val(), department: $(el).next(".inventor_dept").val()};
          }
        }).get();
        $('input[name="ip_summary[inventors]"]').val(JSON.stringify(inventors));

        // Process Inventors
        let patent_ids = $(".patent_ids_input").map(function (idx, el) {
          if ($(el).val() == '') {
            return;
          } else {
            return {patent_id: $(el).val()};
          }
        }).get();
        $('input[name="ip_summary[patent_ids]"]').val(JSON.stringify(patent_ids));

        // Process Type of Partnership
        let partnerships = $(".partnership_opt:checked").map(function (idx, el) {
          if ($(el).val() == "other") {
            if ($("#other_partnership").val() == '') {
              return;
            } else {
              return $("#other_partnership").val();
            }
          } else {
            return $(el).val();
          }
        }).get();
        $('input[name="ip_summary[type_of_partnership]"]').val(JSON.stringify(partnerships));

        // Generate hash for attachment
        if ($("#ip_summary_attachment_url")[0].files && $("#ip_summary_attachment_url")[0].files[0]) {
          HashHelper.hashFile($("#ip_summary_attachment_url")[0].files[0], function (hash_string) {
            $("#ip_summary_hash_attachment").val(hash_string);
            $("#ip-summary-form").submit();
          });
        } else {
          $("#ip-summary-form").submit();
        }
      });

      $("#ip-notary-select").on("click", ".select-ip", function (evt) {
        let self = $(this),
          parent = self.parent().parent(), // find the parent <tr>
          datarecord = {
            asset_id_full: parent.find(".name_short").data("asset-id"),
            name_short: parent.find(".name_short").text(),
            name: parent.find(".name").text(),
            create_time: parent.find(".create_time").text()
          },
          template = $("#datarecord-template").html(),
          preview_template = $("#datarecord-preview-template").html(),
          rendered = Mustache.render(template, datarecord),
          preview_rendered = Mustache.render(preview_template, datarecord);

        parent.hide();
        $("#add-ip-notary").before(rendered);
        $("#records-preview").append(preview_rendered);
      });
      $(".addblock").on("click", ".remove-ip-notary", function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        let self = $(this),
          parent = self.parent(),
          asset_id = parent.data("asset-id");
        $(sprintf('#ip-notary-list [data-asset-id="%s"]', asset_id)).parent().show();
        parent.remove();
        $(sprintf('#records-preview [data-asset-id="%s"]', asset_id)).remove();
      });

      $("#add-more-inventor").click(function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        $("#ip_summary_inventors").before(' \
            <div class="inventors_input_container"> \
              <input class="inventors_input inventor_name" type="text" value="" placeholder="Name" /> \
              <input class="inventors_input inventor_dept" type="text" value="" placeholder="Department" /> \
            </div>');
      });

      $("#add-more-patent_id").click(function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        $("#ip_summary_patent_ids").before(' \
            <div class="patent_ids_input_container"> \
              <input class="patent_ids_input" type="text" value="" placeholder="Patent ID" /> \
            </div>');
      });

      $("#previewModal").on("show.bs.modal", function (evt) {
        $(".case-id-preview").text($("#ip_summary_name_short").val());
        $("#title-preview").text($("#ip_summary_name").val());
        $("#type-preview").text($("#ip_summary_category option:selected").text());
        $("#date-preview").text($("#ip_summary_date").val());
        $("#background-preview").text($("#ip_summary_background").val());
        $("#innovation-preview").text($("#ip_summary_innovation").val());
        $("#application-preview").text($("#ip_summary_application").val());
        $("#advantage-preview").text($("#ip_summary_advantage").val());
        $("#entry-time-preview").text(moment().format("YYYY-MM-DD hh:mm:ss"));

        // Process inventors
        let inventors = $(".inventor_name").map(function (idx, el) {
          if ($(el).val() == "") {
            return;
          } else {
            return $("<span></span>").text($(el).val())
              .attr("class", "inventor-name inventor-tool-tip")
              .attr("title", $(el).next(".inventor_dept").val());
          }
        }).get();
        $("#inventor-preview").html("");
        let $inventorSpans = _.map(inventors, (inventorString) => inventorString);
        $("#inventor-preview").append($inventorSpans);
        $("#inventor-preview > span:not(:last-child)").after(", ");
        $("#inventor-preview > span").tooltip();

        // Process State of Development
        let $selectedSOD = $('input[name="ip_summary[state_of_development]"]:checked');
        if ($selectedSOD.val() != "") {
          $("#sod-preview").text($selectedSOD.parent().text());
        }

        // Process Clinical Trial status
        let $selectedClinicalTrial = $('input[name="ip_summary[clinical_trial]"]:checked');
        if ($selectedClinicalTrial.val() != "") {
          $("#clinical-preview").text($selectedClinicalTrial.parent().text());
        }

        // Process Type of Partnership
        let selectedPartnershipTypes = _.map($("input.partnership_opt:checked"), function (selectedOpt) {
          let $selectedOpt = $(selectedOpt);
          if ($selectedOpt.val() == "other") {
            return $("#other_partnership").val().trim();
          } else {
            return $selectedOpt.parent().text().trim();
          }
        });
        $("#partnership-preview").text(selectedPartnershipTypes.join(", "));

        // Process Patent IDs
        let patent_html_components = $(".patent_ids_input").map(function (idx, el) {
          if ($(el).val() == "") {
            return;
          } else {
            return {
              span: $("<span></span>").text($(el).val()).attr("class", "patent_id"),
              link_button: $("<a class='but03 but03_patent' target='_blank'>Espacenet</a>")
                .attr("href", "#" + encodeURIComponent($(el).val()))
            };
            // [NEED CHECK] link_buttons' href need a correct URL reference!! - BrownJhang 20170922
          }
        }).get();
        $("#patent_ids-preview").html("").hide();
        if (patent_html_components.length > 0) {
          let patents_table = $("<table></table>");
          _.map(patent_html_components, (pc) => 
            patents_table.append($("<tr></tr>").append($("<td></td>").append(pc.span)).append($("<td></td>").append(pc.link_button))));
          $("#patent_ids-preview").append(patents_table);
          $("#patent_ids-preview > a:not(:last-child)").after("<br/>");
          $("#patent_ids-preview").show();
        }

        // Process Patent Status
        $("#patent_status-preview").html("").hide();
        if ($('textarea[name="ip_summary[patent_status]"]').val().trim() != "") {
          let patentStatuses = $('textarea[name="ip_summary[patent_status]"]').val().split(/\n/);
          let statusSpans = _.map(patentStatuses, (status) => $("<span></span>").text(status));
          $("#patent_status-preview").append(statusSpans);
          $("#patent_status-preview > span:not(last-child)").after("<br/>");
          $("#patent_status-preview").show();
        }

        // Process Keywords
        $("#keyword-preview").html("");
        if ($('input[name="ip_summary[keywords]"]').val().trim() != "") {
          let keywords = $('input[name="ip_summary[keywords]"]').val().split(/\s/);
          let keywordSpans = _.map(keywords, (kw) => $("<span></span>").text(kw));
          $("#keyword-preview").append(keywordSpans);
        }

        // Process File Attachment
        if ($('input[name="ip_summary[attachment_url]"]').val() != "") {
          $("#attachment-preview a").text($('input[name="ip_summary[attachment_url]"]').val());
          $("#attachment-preview").show();
        } else {
          $("#attachment-preview").hide();
        }
      });
    }
  },
};

export default IpSummary;

function handleToggleIpSummaryVisibility(clickEvent) {
  clickEvent.preventDefault();
  clickEvent.stopPropagation();

  let $link = $(this);
  let $icon = $link.find("i");
  let $form = $link.parent();
  let action = $form.attr("action");
  let csrfToken = $form.find('input[name="_csrf_token"]').val();
  let method = $form.find('input[name="_method"]').val();
  let currentlyVisible = !$icon.hasClass("fa-eye-slash");

  $icon.removeClass().addClass("fa " +
    (currentlyVisible ? "fa-eye-slash" : "fa-eye"));

  $.ajax({
    url: action,
    type: method.toUpperCase(),
    data: {
      "_csrf_token": csrfToken
    }
  }).done((res, textStatus, jqXHR) => {
    $form.attr({action: res.data.toggle_visibility_link});
    $icon.removeClass().addClass("fa " +
      (res.data.is_visible ? "fa-eye" : "fa-eye-slash"));
  }).fail((jqXHR, textStatus, errorThrown) => {
    if (jqXHR.status == 403) return window.location.reload(true);
    console.log(jqXHR, textStatus, errorThrown);
    $icon.removeClass().addClass("fa " +
      (currentlyVisible ? "fa-eye" : "fa-eye-slash"));
  });
}

function handleCloseWarningBox() {
  $(this).parent().hide();
}

function handleClearFileInputs(){
  FormHelper.clearInputFile(document.getElementById("ip_summary_icon_url"));
  FormHelper.clearInputFile(document.getElementById("ip_summary_cover_url"));
  FormHelper.clearInputFile(document.getElementById("ip_summary_attachment_url"));
}

