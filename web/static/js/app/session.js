let Session = {
  init() {
    $("#extend_session").on("click", () => {
      $.ajax("/en/secure/sessions/update")
        .then(resp => updateExagon(resp));
    });

    monitorSessionExpiration();
  }
};

export default Session;

function monitorSessionExpiration(checkInterval = 10, warningWhen = 300, redirectPath = "/en") {
  setInterval(() => {
    let sec = expireAfterSec();
    if (sec <= 0) {
      window.location.replace(redirectPath);
    } else if (sec <= warningWhen) {
      $("#sessionExtendModal").modal("show");
    }
  }, checkInterval * 1000);
}

function updateExagon({status, data}, redirectPath = "/en") {
  if (status !== "ok" || !data.exagon) {
    return window.location.replace(redirectPath);
  }
  window.exagon = Object.assign({}, window.exagon, data.exagon);
}

function expireAfterSec() {
  let sessionExpiration = moment.unix(exagon.expire_time_utc);

  return sessionExpiration.diff(moment.utc(), "seconds")
}