import {Buffer} from "buffer"
import {sprintf} from "sprintf-js"
import ErrorHelper from "../shared/error_helper"
import EncryptionHelper from "../shared/encryption_helper"

let Promise = require("bluebird");
let Mnemonic = bcRequire("bitcore-mnemonic");
let eccrypto = require("eccrypto");

let Message = {
  init: () => {
    $("#message_form").on("submit", messageFormSubmitHandler);
    $("#decrypt-messages-btn").on("click", handleDecryptMessagesClick);
    $("#send-message-btn").on("click", handleSendMessageClick);
    $("#send-rfiq-btn").on("click", handleSendRfiqClick);
    $("#send-message-from-user-profile-btn").on("click", handleSendMessageFromUserProfileClick);

    setTimeout(() => {
      $("#decrypt-messages-modal").modal();
      $("#passphrase").select();
    }, 500);
  }
};

export default Message;

function handleDecryptMessagesClick(event) {
  event.preventDefault();
  $("#passphrase-error").addClass("hidden");

  let $passphraseInput = $("#passphrase");
  let invalidPassphraseErrorMessage = $passphraseInput.data("invalidPassphraseErrorMessage");
  let passphraseMismatchErrorMessage = $passphraseInput.data("passphraseMismatchErrorMessage");
  let passphrase = $passphraseInput.val().trim();

  if (!Mnemonic.isValid(passphrase)) return handleError(invalidPassphraseErrorMessage);

  let mnCode = new Mnemonic(passphrase);
  let hdPrivKey = mnCode.toHDPrivateKey();
  let hdPublicKey = hdPrivKey.hdPublicKey.toString();

  if (hdPublicKey != window.exagon.current_user_bip32_public_key) {
    return handleError(passphraseMismatchErrorMessage)
  }

  let privateKey = hdPrivKey.privateKey.toBuffer();

  Promise.map(window.exagon.messages, (message) => {
    let encryptedContentHex = isRecipient(window.exagon.current_user_id, message) ?
      message.recipient_encrypted_content : message.sender_encrypted_content;
    let encryptedContentBuffer = Buffer.from(encryptedContentHex, "hex");
    let encryptedContentObject = EncryptionHelper.getEncryptedObjectFromEncryptedBuffer(encryptedContentBuffer);

    return Promise.resolve(eccrypto.decrypt(privateKey, encryptedContentObject))
      .then((decryptedBuffer) => {
        let decryptedContent = Buffer.from(decryptedBuffer).toString("utf8");
        $(sprintf("#message-%s", message["id"])).text(decryptedContent).removeClass("encrypted");
      });
  })
    .then(() => $("#decrypt-messages-modal").modal("hide"))
    .then(() => {
      let csrfToken = $("form").find("input[name='_csrf_token']").first().val();
      $.ajax({
        url: window.exagon.mark_as_read_path,
        method: "PATCH",
        headers: {"X-CSRF-TOKEN": csrfToken}
      })
        .done((data) => console.log(data))
        .fail((jqXHR, textStatus, errorThrown) => {
          console.log(jqXHR, textStatus, errorThrown);
        });
    });
}

function messageFormSubmitHandler(event) {
  event.preventDefault();
  $("#messageFormErrors").remove();
  let $form = $(event.target);
  let formAction = $form.attr("action");
  let csrfToken = $form.find("input[name='_csrf_token']").val();
  let noRecipientErrorMsg = $form.data("noRecipientErrorMsg");
  let noSubjectErrorMsg = $form.data("noSubjectErrorMsg");
  let emptyMessageErrorMsg = $form.data("emptyMessageErrorMsg");
  let $selectedRecipientOption = $("#message_recipient_user_id").find("option:selected");
  let messageContent = $("#message_content").val().trim();
  let subject = $("#message_subject").val().trim();
  if ($selectedRecipientOption.length < 1 || $selectedRecipientOption.val() == "") {
    return alert(noRecipientErrorMsg);
  }
  if (subject.length < 1) return alert(noSubjectErrorMsg);
  if (messageContent.length < 1) return alert(emptyMessageErrorMsg);

  let senderPublicKeyHex = $form.find("#sender_public_key").val();
  let senderPublicKey = Buffer.from(senderPublicKeyHex, "hex");
  let recipientPublicKeyHex = $selectedRecipientOption.data("publicKey");
  let recipientPublicKey = Buffer.from(recipientPublicKeyHex, "hex");
  let recipientUserId = $selectedRecipientOption.val();

  sendMessage({
    formAction: formAction,
    csrfToken: csrfToken,
    senderPublicKey: senderPublicKey,
    recipientPublicKey: recipientPublicKey,
    recipientUserId: recipientUserId,
    messageContent: messageContent,
    subject: subject
  }, (res, textStatus, jqXHR) => {
    if (jqXHR.status == 201) {
      return window.location.assign(res.data.redirect_to);
    } else {
      return window.location.reload(true);
    }
  }, (jqXHR) => {
    if (!jqXHR.responseJSON.errors) return;
    let errors = jqXHR.responseJSON.errors;
    let $errorList = ErrorHelper.getErrorList(errors).attr({id: "messageFormErrors"});
    $errorList.insertAfter($("#message_content"))
  })
}

function handleSendMessageClick(event) {
  event.preventDefault();
  event.stopPropagation();

  let $form = $(this).parent();
  let formAction = $form.attr("action");
  let csrfToken = $form.find("input[name='_csrf_token']").val();
  let recipientUserId = window.exagon.other_user_id;
  let recipientPublicKeyHex = window.exagon.other_user_public_key;
  let recipientPublicKey = Buffer.from(recipientPublicKeyHex, "hex");
  let senderPublicKeyHex = window.exagon.current_user_public_key;
  let senderPublicKey = Buffer.from(senderPublicKeyHex, "hex");
  let subject = window.exagon.subject;
  let messageContent = $("#message-content").val().trim();

  sendMessage({
    formAction: formAction,
    csrfToken: csrfToken,
    recipientUserId: recipientUserId,
    recipientPublicKey: recipientPublicKey,
    senderPublicKey: senderPublicKey,
    messageContent: messageContent,
    subject: subject
  }, (res, textStatus, jqXHR) => {
    if (jqXHR.status == 201) {
      let $message = $("#demo-sent-message").clone();
      $message.removeClass("hidden").insertAfter("#message-head")
        .find(".mbox").text(stripTags(messageContent));
      $("#message-content").val("");
    } else {
      console.log(res, textStatus, jqXHR);
    }
  });
}

function handleSendMessageFromUserProfileClick(event) {
  event.preventDefault();
  event.stopPropagation();
  $("#messageFormErrors").remove();

  let $container = $("#user-profile-message-container");
  let $link = $(this);
  let $form = $link.parent();
  let formAction = $form.attr("action");
  let successMessage = $link.data("successMessage");
  let csrfToken = $form.find("input[name='_csrf_token']").val();
  let recipientUserId = window.exagon.other_user_id;
  let recipientPublicKeyHex = window.exagon.other_user_public_key;
  let recipientPublicKey = Buffer.from(recipientPublicKeyHex, "hex");
  let senderPublicKeyHex = window.exagon.current_user_public_key;
  let senderPublicKey = Buffer.from(senderPublicKeyHex, "hex");
  let $messageContent = $("#message-content");
  let $subject = $("#message-subject");
  let noSubjectErrorMsg = $link.data("noSubjectErrorMsg");
  let emptyMessageErrorMsg = $link.data("emptyMessageErrorMsg");
  let messageContent = $messageContent.val().trim();
  let subject = $subject.val().trim();

  if (subject.length < 1) return alert(noSubjectErrorMsg);
  if (messageContent.length < 1) return alert(emptyMessageErrorMsg);

  sendMessage({
    formAction: formAction,
    csrfToken: csrfToken,
    recipientUserId: recipientUserId,
    recipientPublicKey: recipientPublicKey,
    senderPublicKey: senderPublicKey,
    messageContent: messageContent,
    subject: subject
  }, (res, textStatus, jqXHR) => {
    if (jqXHR.status == 201) {
      $container.children().hide();
      let $successMessage = $("<p></p>").text(successMessage).insertBefore($form);
    } else {
      console.log(res, textStatus, jqXHR);
    }
  }, (jqXHR) => {
    if (!jqXHR.responseJSON.errors) return;
    let errors = jqXHR.responseJSON.errors;
    let $errorList = ErrorHelper.getErrorList(errors).attr({id: "messageFormErrors"});
    $errorList.insertAfter($messageContent);
  });
}

function handleSendRfiqClick(event) {
  event.preventDefault();
  event.stopPropagation();
  $("#messageFormErrors").remove();

  let $form = $(this).parent();
  let $comments = $("#rfiq-comments");
  let formAction = $form.attr("action");
  let csrfToken = $form.find("input[name='_csrf_token']").val();
  let successMessage = $(this).data("successMessage");
  let recipientUserId = window.exagon.other_user_id;
  let recipientPublicKeyHex = window.exagon.other_user_public_key;
  let recipientPublicKey = Buffer.from(recipientPublicKeyHex, "hex");
  let senderPublicKeyHex = window.exagon.current_user_public_key;
  let senderPublicKey = Buffer.from(senderPublicKeyHex, "hex");
  let userCommentsContent = $comments.val();
  let ipSummaryName = window.exagon.ip_summary_name;
  let caseId = window.exagon.ip_summary_case_id;
  let senderName = window.exagon.current_user_name;
  let senderCompany = window.exagon.current_user_company;
  let senderBranch = window.exagon.current_user_branch;
  let messageContent = sprintf("RFIQ Sent from IP Summary: %s\n", ipSummaryName) +
    sprintf("Case ID: %s\n\n", caseId) +
    "User Details:\n" +
    sprintf("Name: %s\n", senderName) +
    sprintf("Organization: %s\n", senderCompany) +
    sprintf("Branch: %s\n\n", senderBranch) +
    sprintf("Comments:\n%s", userCommentsContent);

  sendMessage({
    formAction: formAction,
    csrfToken: csrfToken,
    recipientUserId: recipientUserId,
    recipientPublicKey: recipientPublicKey,
    senderPublicKey: senderPublicKey,
    messageContent: messageContent,
    subject: caseId
  }, (res, textStatus, jqXHR) => {
    if (jqXHR.status == 201) {
      $form.hide();
      $comments.hide();
      let $successMessage = $("<p></p>").text(successMessage).insertBefore($form);
    } else {
      console.log(res, textStatus, jqXHR);
    }
  }, (jqXHR) => {
    if (!jqXHR.responseJSON.errors) return;
    let errors = jqXHR.responseJSON.errors;
    let $errorList = ErrorHelper.getErrorList(errors).attr({id: "messageFormErrors"});
    $errorList.insertAfter($comments);
  });
}

function sendMessage(spec, successCallback, failureCallback = null) {
  let sanitizedMessage = stripTags(spec.messageContent);
  let sanitizedSubject = stripTags(spec.subject);
  let messageContentBuffer = Buffer.from(sanitizedMessage, "utf8");
  let senderEncryptedMessageContentHex;
  let recipientEncryptedMessageContentHex;

  eccrypto.encrypt(spec.senderPublicKey, messageContentBuffer)
    .then((senderEncrypted) => {
      let senderEncryptedBuffer = EncryptionHelper.getEncryptedBufferFromEncryptedObject(senderEncrypted);
      senderEncryptedMessageContentHex = Buffer.from(senderEncryptedBuffer).toString("hex");
    })
    .then(() => eccrypto.encrypt(spec.recipientPublicKey, messageContentBuffer))
    .then((recipientEncrypted) => {
      let recipientEncryptedBuffer = EncryptionHelper.getEncryptedBufferFromEncryptedObject(recipientEncrypted);
      recipientEncryptedMessageContentHex = Buffer.from(recipientEncryptedBuffer).toString("hex");
    })
    .then(() => {
      $.ajax({
        url: spec.formAction,
        method: "POST",
        headers: {"X-CSRF-TOKEN": spec.csrfToken},
        data: {
          "message[sender_encrypted_content]": senderEncryptedMessageContentHex,
          "message[recipient_user_id]": spec.recipientUserId,
          "message[recipient_encrypted_content]": recipientEncryptedMessageContentHex,
          "message[subject]": sanitizedSubject
        }
      })
        .done(successCallback)
        .fail(failureCallback || ((jqXHR, textStatus, errorThrown) => {
            console.log(jqXHR, textStatus, errorThrown);
          }));
    });
}

function isRecipient(current_user_id, message) {
  return parseInt(current_user_id) === parseInt(message.recipient_user_id);
}

function handleError(errorMessage) {
  $("#passphrase-error").text(errorMessage).removeClass("hidden");
}

function stripTags(string) {
  return window.sanitizeHtml(string, {
    allowedTags: [],
    allowedAttributes: []
  })
}
