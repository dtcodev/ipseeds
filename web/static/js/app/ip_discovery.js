let IpDiscovery = {
  init: () => {
    if ($('.secure .ip-discovery').length > 0) {
      $("#ip-search a").click(function(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        $("form#ip-search").submit();
      });
    }
  }
}

module.exports = IpDiscovery
