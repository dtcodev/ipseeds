import {Buffer} from "buffer"

let EncryptionHelper = {
  /**
   * Eccrypto takes a buffer and encrypts it into a JS object.
   * In order to reliably store this in the db, we need convert the JS object into
   * a buffer. The structure of the encrypted object is like this:
   *
   * { iv: Uint8Array(16), ephemPublicKey: Uint8Array(65), mac: Uint8Array(32),
 *   ciphertext: Uint8Array(variable length, depending on content) }
   *
   * We can take the integer arrays and append them together to make one long buffer.
   * The ciphertext must come last, otherwise there is no way to put the object back
   * together for decryption later.
   *
   * encBuffer = Buffer.concat([enc.iv, enc.mac, enc.ephemPublicKey, enc.ciphertext])
   * encObject = sliced up encBuffer
   */
  getEncryptedBufferFromEncryptedObject: (encryptedObject) => {
    return Buffer.concat([
      encryptedObject.iv, encryptedObject.mac, encryptedObject.ephemPublicKey,
      encryptedObject.ciphertext
    ])
  },
  getEncryptedObjectFromEncryptedBuffer: (encryptedBuffer) => {
    let iv = encryptedBuffer.slice(0, 16);
    let mac = encryptedBuffer.slice(16, 48);
    let ephemPublicKey = encryptedBuffer.slice(48, 113);
    let ciphertext = encryptedBuffer.slice(113);
    return {iv: iv, mac: mac, ephemPublicKey: ephemPublicKey, ciphertext: ciphertext}
  }
};

export default EncryptionHelper;
