let Explorer = {
  init: () => {
    $(".explorer .btn-show-all").on("click", function(){
      let $this = $(this);
      $this.parent().text($this.data("wholeRaw"));
    });
  }
};
module.exports = Explorer;