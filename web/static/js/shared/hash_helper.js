import sha256 from "js-sha256"

let Promise = require("bluebird");


let HashHelper = {
  hashFile: hashFile
};

export default HashHelper;

function hashFile(file, callback) {
  let reader = new FileReader();

  reader.onload = function (event) {
    let fileAsArrayBuffer = event.target.result;
    let sha256Hash = sha256(fileAsArrayBuffer);

    if (typeof callback === "function") {
      callback(sha256Hash);
    }
  };

  reader.readAsArrayBuffer(file);
}
