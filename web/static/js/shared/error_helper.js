import _ from "lodash"
import {sprintf} from "sprintf-js"

let ErrorHelper = {
  getErrorList: getErrorList,
  getErrorMessages: getErrorMessages
};

export default ErrorHelper;

function getErrorList(errors) {
  let errorMessages = ErrorHelper.getErrorMessages(errors);
  let $ul = $("<ul></ul>").addClass("errors");
  _.each(errorMessages, (errorMessage) => {
    let $li = $("<li></li>").append(errorMessage);
    $ul.append($li);
  });
  return $ul;
}

function getErrorMessages(errors) {
  return _.map(errors, (error, field) => {
    return sprintf("%s %s", field, error);
  });
}
