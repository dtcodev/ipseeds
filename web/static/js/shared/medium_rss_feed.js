let MediumRSSFeed = {
  init: () => {

    // GET RSS feed from: https://medium.com/feed/blog-ipseeds-net (AJAX)
    $.get($(".blog").data("rssUrl"), function(data) {
	    var $xml = $(data);
	    $xml.find("item").each(function(i, _item) {
	        var $this = $(this),
	            item = {
	                title: $this.find("title").text(),
	                link: $this.find("link").text(),
	                content: $this.find("encoded").text() || $this.find("content\\:encoded").text(),
	        	}

	        let imgSrc = $(".hidden-container").html(item.content).find('img').attr("src");	

	        // Success: Show the items on the page.
	        $(".blog").append(MediumRSSFeed.genFeedItemHTML(item.link, imgSrc, item.title));

	        return i < 2;
	    });

	    // Final: Hide the loading
	    $(".blog-loading").hide();
	});
    
  },
  genFeedItemHTML: (link, imageUrl, title) => {
  	return "<a href=\"" + link + "\" target=\"_blank\"><div><div class=\"post-image\" style=\"background-image: url(" + imageUrl + ");\"><img src=\"" + imageUrl + "\" /></div><h2>" + title + "</h2></div></a>";
  }
};
module.exports = MediumRSSFeed;