let FAQ = {
  init: () => {
    $(".faq .list-group-item").on("click", function(){
      let $this = $(this);
      $(".faq .list-group-item.active").removeClass("active");
      $this.addClass("active");
      $(".faq .panel-group.active").removeClass("active");
      $(".faq .panel-group#" + $this.data("tab")).addClass("active");
    });
  }
};
module.exports = FAQ;