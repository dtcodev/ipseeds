import {sprintf} from "sprintf-js"

let UserRegistration = {
  init: () => {
    if ($(".user_registrations").length < 1) return;
    
    $("[data-toggle='popover']").popover();

    $("input[name='user_registration[application_type]']").click(function () {
      $(this).tab('show');
    });

    $("input[type='checkbox'].copy-info").change(function () {
      let self = $(this),
        target = self.data("target"),
        new_value = self.prop("checked");
      if (new_value) {
        $(sprintf("input[name^='%s[user_']", target)).each(function () {
          let cur_self = $(this),
            dest = $(sprintf("input[name='%s']", cur_self.attr("name").replace("[user_", "[contact_")));
          dest.val(cur_self.val());
        });
        let user_email = $("input[name='user[email]']").val();
        $(sprintf("input[name='%s[contact_email]']", target)).val(user_email);
      }
      $(sprintf("input[name^='%s[contact_']", target)).prop("readonly", new_value);
    });

    $("#industry_application_company_nationality").change(function () {
      let selectedNationality = $(this).val();

      if (selectedNationality == "TW") {
        $(".only-needed-for-tw-company").removeClass("hidden");
      } else {
        $(".only-needed-for-tw-company").addClass("hidden");
      }
    });

    $("#research_institute_application_institute_nationality").change(function () {
      let selectedNationality = $(this).val();

      if (selectedNationality == "TW") {
        $(".only-needed-for-tw-research-institute").removeClass("hidden");
      } else {
        $(".only-needed-for-tw-research-institute").addClass("hidden");
      }
    });

    $("#research_institute_application_institute_nationality").trigger("change");
    $("#industry_application_company_nationality").trigger("change");
  }
};
module.exports = UserRegistration;
