let SubscribeNews = {
  init: () => {
  	
  	// Bind the subscribe click event
  	$("#subscribe-news").off("click").on("click", function(){
  		var validateEmail = function(email) {
		  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		  return re.test(email);
		};
  		var $this = $(this);
  		$this.button("loading");
  		var $data = {"email": $("input[name=email]").val()}
  		$(".newsletterinput .alert").text("");
  		$(".newsletterinput .alert").hide();

  		// Check user input value
  		if($data.email == ""){
  		  $("#subscribe-error-msg").text("Please enter your email address!").show();
  		  $this.button("reset");
  		  return;
  		}
  		if(!validateEmail($data.email)){
  		  $("#subscribe-error-msg").text("Please check your email address format!").show();
  		  $this.button("reset");
  		  return;
  		}

  		// POST the subscribe data to /subscribe
  		$.ajax({
          url: $this.data("subscribeUrl"),
          type: "POST",
          dataType: "json",
          beforeSend: function(xhr) {
            xhr.setRequestHeader("X-CSRF-Token", $this.data("csrfToken"));
          },
          data: $data,
          success: function(resp){
            if(resp.result == "ok"){
            	$("#subscribe-success-msg").text(resp.msg).show();
            	$(".newsletterinput input[name=email]").val("");
            }else{
            	var msg = "";
            	if(resp.title == "Member Exists"){
            		msg = "This email address already subscribe our news. If you can not receive our news, please contact us."
            	}else{
            	    msg = "Sorry, there is an unexpected error! Please contact us and let us know this issue.";
            	}
            	$("#subscribe-error-msg").text(msg).show()
            }
          },
          error: function(){
          	$("#subscribe-error-msg").text("Sorry, there is an unexpected error! Please contact us and let us know this issue.").show();
          },
          complete: function(){
          	$this.button("reset");
          }

	    });
  	});

    
    
  }
};
module.exports = SubscribeNews;