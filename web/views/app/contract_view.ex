defmodule Ipseeds.App.ContractView do
  use Ipseeds.Web, :view

  alias Ipseeds.{Repo, User, Contract}

  def render("create_success.json", %{redirect_to: redirect_to}) do
    %{data: %{redirect_to: redirect_to}}
  end

  def contact_select(form, field, %User{} = user, opts \\ []) do
		opts =
      opts
      |> Keyword.put_new(:id, field_id(form, field))
			|> Keyword.put_new(:name, field_name(form, field))

    user = Repo.preload(user, :contacts)

    active_contacts = user.contacts
      |> Stream.map(fn(contact) ->
        active_keypair = User.active_keypair(contact) || %{}
        public_key = Map.get(active_keypair, :ecc_public_key_hex)
        %{id: contact.id, username: contact.username, name: User.get_name(contact),
          public_key: public_key}
      end)
      |> Stream.reject(fn(contact_mapped) -> is_nil(contact_mapped[:public_key]) end)
      |> Enum.to_list

    initial_option = content_tag(:option, "", value: "")

    options = contact_options(active_contacts, initial_option)

    content_tag :select, options, opts
  end 

  def recipient_name(%{} = contract) do
    contract.recipient |> User.get_name()
  end 

  def sender_name(%{} = contract) do
    contract.issuer |> User.get_name()
  end

  def status_tag(%{} = contract) do
    case contract do
      %{id: id,recipient_user_id: ru_id, status: nil} when ru_id == id ->
        content_tag :a, gettext("RECEIVED"), class: "status-label-received"
      %{status: status} when status != nil->
        case contract.status do
           :accepted ->
             content_tag :a, gettext("ACCEPTED"), class: "status-label-#{contract.status}" 
           :cancelled ->
             content_tag :a, gettext("CANCELLED"), class: "status-label-#{contract.status}"
           :declined ->
             content_tag :a, gettext("DECLINED"), class: "status-label-#{contract.status}" 
           :pending ->
             content_tag :a, gettext("PENDING"), class: "status-label-#{contract.status}" 
           _ ->
             content_tag :a, gettext("UNKNOWN"), class: "status-label-#{contract.status}" 
        end  
      _ ->
        content_tag :a, gettext("ISSUED"), class: "status-label-issued"
    end
  end

  def detail_link(contract, field_name, conn, locale) do
    {link_path, show_text} = if contract.status == nil do
      {app_contract_path(conn, :show, locale, contract.tx_id), contract[field_name]}
    else
      lp = case contract.status do
        :accepted -> 
          app_agreeable_contract_path(conn, :show, locale, contract.asset_id)
        :pending -> 
          app_agreeable_contract_path(conn, :edit, locale, contract.asset_id)
        _ -> "#"   
      end

      st = case field_name do
        :name ->
          # separate the name. (NDAs show the type.)
          show_name(contract.name)
        _ ->
          "NDA"
      end
      
      {lp, st}
    end
    link show_text, to: link_path
  end

  def file_size_limit_note() do
    gettext "Maximum allowed size is %{size_limit}", size_limit: Sizeable.filesize(Contract.attachment_max_size_bytes())
  end

  defp contact_options(contacts, empty_option) do
    Enum.reduce contacts, empty_option, fn (contact, acc) ->
      option_key = html_escape("#{contact[:name]} (#{contact[:username]})")
      option = content_tag(:option, option_key, value: contact[:id], data: ["public-key": contact[:public_key]])
      html_escape([acc, option])
    end
  end

  defp show_name(name) do
     
    name = if String.starts_with?(name, "contract_nda_unilateral"), 
      do: "Unilateral Non-Disclosure Agreement", else: name
    name = if String.starts_with?(name, "contract_nda_mutual"), 
      do: "Mutual Non-Disclosure Agreement", else: name

    name  
  end
end
