defmodule Ipseeds.App.IpNotaryView do
  use Ipseeds.Web, :view
  import Scrivener.HTML

  alias Ipseeds.IpNotary

  def resource_add_path(conn, locale) do
    app_ip_notary_path(conn, :new, locale)
  end

  def default_valid_time() do
    Timex.now("Asia/Taipei") |> Timex.format!("{ISOdate}")
  end

  def file_size_limit_note() do
    gettext "Maximum allowed size is %{size_limit}", size_limit: Sizeable.filesize(IpNotary.attachment_max_size_bytes())
  end
  def link_btc_txid(txid, arguments) do
    if txid == "", do: "(Processing...)", else: link(txid, arguments)
  end
end
