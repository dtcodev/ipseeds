defmodule Ipseeds.App.IpSummaryView do
  use Ipseeds.Web, :view
  require Logger
  import Scrivener.HTML

  alias Ipseeds.IpSummary

  def render("set_visibility.json", %{asset_id: asset_id, is_visible: is_visible,
    toggle_visibility_link: link}) do

    %{data: %{
      toggle_visibility_link: link,
      asset_id: asset_id,
      is_visible: is_visible
    }}
  end

  def render("ip_summary.json", %{ip_summary: ip_summary}) do
    Map.take(ip_summary, [:id, :user_id, :api_asset_id, :is_visible])
  end

  def readable_attr(attr), do: IpSummary.readable_attr(attr)

  def resource_add_path(conn, locale) do
    app_ip_summary_path(conn, :new, locale)
  end

  def categories() do
    IpSummary.categories
    |> Enum.reduce([], fn {k, v}, acc ->
      Enum.concat(acc, [{Gettext.gettext(Ipseeds.Gettext, k), v}])
    end)
  end

  def categories_map() do
    Enum.into(IpSummary.categories, %{})
  end

  def get_inventors_list(asset) do
    case parse_description(asset) do
      %{:inventors => inventors} ->
        inventors
      _ -> ""
    end
  end

  def parse_inventor_list(asset) do
    raw(case parse_description(asset) do
      %{:inventors => inventors} ->
        inventors
        |> Enum.map(&parse_inventor/1)
        |> Enum.join(",&nbsp;")
      _ -> ""
    end)
  end

  defp parse_inventor(%{} = inventor) do
    if Map.has_key?(inventor, :department) do
      safe_to_string(content_tag(:span, inventor.name, class: "inventor-name", title: inventor.department))
    else
      safe_to_string(content_tag(:span, inventor.name, class: "inventor-name"))
    end
  end
  defp parse_inventor(inventor) do
    safe_to_string(content_tag(:span, inventor, class: "inventor-name"))
  end

  def get_patent_ids_list(asset) do
    case parse_description(asset) do
      %{:patent_ids => patent_ids} ->
        patent_ids
      _ -> %{}
    end
  end

  def parse_patent_ids_list(asset) do
    raw(case parse_description(asset) do
      %{:patent_ids => patent_ids} ->
        patent_ids
        |> Enum.map(&parse_patent_id/1)
      _ -> ""
    end)
  end

  defp parse_patent_id(%{} = p_id) do
    "<tr>"
      <> "<td>" 
      <> safe_to_string(content_tag(:span, p_id.patent_id, class: "patent_id")) 
      <> "</td>" 
      <> "<td>" 
      <> safe_to_string(content_tag(:a, "Espacenet", class: "but03 but03_patent", 
           href: "https://worldwide.espacenet.com/searchResults?#{URI.encode_query %{query: p_id.patent_id}}", 
           target: "_blank")) 
      <> "</td>"
      <> "</tr>"
  end

  def parse_state_of_development(asset) do
    case parse_description(asset) do
      %{:state_of_development => value} -> 
        if Map.has_key?(IpSummary.states_of_development, value) do
          state = IpSummary.states_of_development[value]
          Gettext.gettext(Ipseeds.Gettext, state)
        else
          text_to_html("N/A")
        end
      _ -> text_to_html("N/A")
    end
  end

  def parse_clinical_trial(asset) do
    case parse_description(asset) do
      %{:clinical_trial => value} -> 
        if Map.has_key?(IpSummary.clinical_trials, value) do
          state = IpSummary.clinical_trials[value]
          Gettext.gettext(Ipseeds.Gettext, state)
        else
          text_to_html("N/A")
        end
      _ -> text_to_html("N/A")
    end
  end

  def parse_partnership(asset) do
    case parse_description(asset) do
      %{:type_of_partnership => []} -> text_to_html("N/A")
      %{:type_of_partnership => types} -> 
        types
        |> Enum.map(fn type -> 
          if Map.has_key?(IpSummary.types_of_partnership, type) do
            key = IpSummary.types_of_partnership[type]
            Gettext.gettext(Ipseeds.Gettext, key)
          else
            type
          end
        end)
        |> Enum.join(", ")
      _ -> text_to_html("N/A")
    end
  end

  def parse_category(asset) do
    case asset[:comment] do
      nil -> ""
      value ->
        all_kw_set = Regex.scan(~r/#[0-9a-zA-Z-_]+/, value)
          |> List.flatten
          |> MapSet.new

        categories_map = IpSummary.categories
          |> Enum.reduce(%{}, fn {k, v}, acc -> 
            Map.put(acc, v, k)
          end)

        categories_set = categories_map
          |> Map.keys
          |> MapSet.new

        if MapSet.disjoint?(all_kw_set, categories_set) do
          ""
        else
          MapSet.intersection(all_kw_set, categories_set) 
          |> MapSet.to_list
          |> Enum.map(&Gettext.gettext(Ipseeds.Gettext, categories_map[&1]))
          |> Enum.join(", ")
        end
    end
  end

  def parse_keywords(asset, conn) do
    case asset[:comment] do
      nil -> ""
      value ->
        all_kw_list = Regex.scan(~r/#[0-9a-zA-Z-_]+/, value)
          |> List.flatten

        categories_list = IpSummary.categories
          |> Enum.map(&elem(&1, 1))

        (all_kw_list -- categories_list)
        |> Enum.map(&content_tag(:a, &1,
          [href: app_ip_discovery_path(conn, :index, conn.assigns.locale, search: String.slice(&1, 1..-1))]
          ))
    end
  end

  def parse_keywords_for_edit(asset) do
    case asset[:comment] do
      nil -> ""
      value ->
        all_kw_list = Regex.scan(~r/#[0-9a-zA-Z-_]+/, value)
          |> List.flatten

        categories_list = IpSummary.categories
          |> Enum.map(&elem(&1, 1))

        (all_kw_list -- categories_list)
    end
  end

  def parse_text_description(asset, attribute, attributes \\ []) do
    case parse_description(asset) do
      %{^attribute => value} -> text_to_html(value, [attributes: attributes])
      _ -> text_to_html("N/A", [attributes: attributes])
    end
  end

  def parse_text_description_for_edit(asset, attribute, attributes \\ []) do
    case parse_description(asset) do
      %{^attribute => value} -> value
      _ -> ""
    end
  end

  def parse_attachment_filename(attachment_url) do
    attachment_url
    |> String.split("/")
    |> List.last
  end

  def parse_description(%{:description => ""}), do: %{}
  def parse_description(%{:description => description_string}) do
    case Poison.decode(description_string, keys: :atoms) do
      {:ok, description} -> description
      {:error, _} -> %{}
    end
  end 
  def parse_description(_), do: %{}

  def default_valid_time() do
    Timex.now("Asia/Taipei") |> Timex.format!("{ISOdate}")
  end

  def parse_list_from_changeset!(changeset, field) do
    case Ecto.Changeset.get_change(changeset, field) do
      nil -> []
      change -> Poison.Parser.parse!(change)
    end
  end

  def get_other_partnership(partnerships) when is_list(partnerships) do
    all_partnerships = Map.keys(IpSummary.types_of_partnership())
    other_partnership = partnerships
      |> Enum.reject(fn partnership -> partnership in all_partnerships end)

    if length(other_partnership) > 0, do: List.first(other_partnership), else: nil
  end
  def get_other_partnership([]), do: nil
  def get_other_partnership(_), do: nil

  def file_size_limit_note() do
    gettext "Maximum allowed size is %{size_limit}", size_limit: Sizeable.filesize(IpSummary.attachment_max_size_bytes())
  end
end
