defmodule Ipseeds.App.UserContactView do
  use Ipseeds.Web, :view

  def render("show.json", %{user_contact: user_contact}) do
    %{data: render_one(user_contact, __MODULE__, "user_contact.json")}
  end

  def render("user_contact.json", %{user_contact: user_contact}) do
    %{id: user_contact.id}
  end
end
