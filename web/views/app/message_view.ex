defmodule Ipseeds.App.MessageView do
  use Ipseeds.Web, :view

  def render("create_success.json", %{redirect_to: redirect_to}) do
    %{data: %{redirect_to: redirect_to}}
  end

  def render("messages.json", %{messages: messages}) do
    %{data: render_many(messages, __MODULE__, "message.json")}
  end

  def render("message.json", %{message: message}) do
    Map.take(message, [:id, :recipient_user_id, :recipient_encrypted_content, :sender_user_id,
      :sender_encrypted_content, :inserted_at])
  end

  def render("mark_as_read.json", %{}) do
    %{status: "success"}
  end

  def message_box_class(message, current_user) do
    case message.sender_user_id == current_user.id do
      true -> "mbox_b"
      false -> "mbox_a"
    end
  end
end
