defmodule Ipseeds.App.AgreeableContractView do
  use Ipseeds.Web, :view

  alias Ipseeds.{Repo, User, AgreeableContract}

  def render_static_plaintext_html(agreement, assigns) do
    if agreement.type == AgreeableContract.asset_type(%{"nda_contract_type" => "unilateral"}) do
      render "_static_unilateral_plaintext.html", Map.put(assigns, :agreement, agreement) 
    else
      render "_static_mutual_plaintext.html", Map.put(assigns, :agreement, agreement)
    end
  end

  def singner_chacater_field_name(side, nda_type) do
    type_mutual = AgreeableContract.asset_type(%{"nda_contract_type" => "mutual"})
    type_unilateral = AgreeableContract.asset_type(%{"nda_contract_type" => "unilateral"})
    case side do
       :left when nda_type == type_mutual ->
         gettext "FIRST PARTY"
       :left when nda_type == type_unilateral ->
         gettext "DISCLOSER"
       :right when nda_type == type_mutual ->
         gettext "SECOND PARTY"
       :right when nda_type == type_unilateral ->
         gettext "RECIPIENT"
    end
  end

  def nda_type_name(agreement) do
    if agreement.type == AgreeableContract.asset_type(%{"nda_contract_type" => "unilateral"}) do
      "unilateral"
    else
      "mutual"
    end
  end

  def countries_list do
    Ipseeds.CountriesHelper.all
    |> Enum.map(fn(country) -> {country[:name], country[:alpha2]} end)
  end

  def contact_select(form, field, %User{} = user, opts \\ []) do
	  opts =
      opts
      |> Keyword.put_new(:id, field_id(form, field))
	    |> Keyword.put_new(:name, field_name(form, field))

    user = Repo.preload(user, :contacts)

    active_contacts = user.contacts
      |> Stream.map(fn(contact) ->
        active_keypair = User.active_keypair(contact) || %{}
        public_key = Map.get(active_keypair, :ecc_public_key_hex)
        %{id: contact.id, username: contact.username, name: User.get_name(contact),
          public_key: public_key, application_type: User.get_user_application_type(contact), 
          nationality: User.get_nationality_name(contact), address: User.get_address(contact)}
      end)
      |> Stream.reject(fn(contact_mapped) -> is_nil(contact_mapped[:public_key]) end)
      |> Enum.to_list

    initial_option = content_tag(:option, "", value: "")

    options = contact_options(active_contacts, initial_option)

    content_tag :select, options, opts
  end

  def default_valid_time() do
    Timex.now("Asia/Taipei") |> Timex.format!("{ISOdate}")
  end

  def file_size_limit_note() do
    gettext "Maximum allowed size for signature file is %{size_limit}", size_limit: Sizeable.filesize(AgreeableContract.attachment_max_size_bytes())
  end

  defp contact_options(contacts, empty_option) do
    Enum.reduce contacts, empty_option, fn (contact, acc) ->
      option_key = html_escape("#{contact[:name]} (#{contact[:username]})")
      option = content_tag(:option, option_key, value: contact[:id], 
      	data: ["application-type": contact[:application_type], 
      	  name: contact[:name], nationality: contact[:nationality], address: contact[:address]])
      html_escape([acc, option])
    end
  end
  
end
