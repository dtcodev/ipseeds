defmodule Ipseeds.App.IpDiscoveryView do
  use Ipseeds.Web, :view

  alias Ipseeds.IpSummary

  def categories_menu(conn) do
    [{"New", ""} | IpSummary.categories]
    |> Enum.map(fn {k, v} ->
      value = String.replace(v, "#", "")
      Gettext.gettext(Ipseeds.Gettext, k)
      |> link(to: category_path(conn, value), class: menu_classes(conn.query_params, value))
      |> safe_to_string
    end)
    |> Enum.join
    |> raw
  end

  def parse_keywords(asset) do
    case asset["comment"] do
      nil -> ""
      value ->
        all_kw_list = Regex.scan(~r/#[0-9a-zA-Z-_]+/, value)
          |> List.flatten

        categories_list = IpSummary.categories
          |> Enum.map(&elem(&1, 1))

        (all_kw_list -- categories_list)
        |> Enum.map(&content_tag(:span, &1))
    end
  end

  def category_params(conn) do
    conn.query_params 
    |> Map.take(["category"])
    |> Enum.reduce(%{}, fn({k, v}, acc) ->
      acc |> Map.put(String.to_atom(k), v)
    end)
    |> Map.to_list
  end

  def entry_range([] = _assets, _current_page, _page_size), do: "0-0"
  def entry_range(assets, current_page, page_size) do
    start_entry = (current_page - 1) * page_size + 1
    end_entry = start_entry + length(assets) - 1
    "#{start_entry}-#{end_entry}"
  end

  def total_entries([] = _assets, _current_page, _page_size, _total_pages), do: 0
  def total_entries(assets, 1 = _current_page, page_size, 1 = _total_pages) do
    if length(assets) < page_size, do: length(assets), else: page_size
  end
  def total_entries(assets, current_page, page_size, total_pages) do
    if current_page < total_pages, do: total_pages * page_size, else: (current_page - 1) * page_size + length(assets)
  end

  defp category_path(conn, ""), do: app_ip_discovery_path(conn, :index, conn.assigns.locale)
  defp category_path(conn, value) do
    app_ip_discovery_path(conn, :index, conn.assigns.locale, category: value)
  end

  defp menu_classes(%{"category" => category}, category), do: "on"
  defp menu_classes(%{"category" => _category}, ""), do: ""
  defp menu_classes(_params, ""), do: "on"
  defp menu_classes(_, _), do: ""
end
