defmodule Ipseeds.App.UserView do
  use Ipseeds.Web, :view

  alias Ipseeds.{ProfilePhoto, User, ErrorHelpers}

  def user_profile_photo(user) do
    ProfilePhoto.url({user.picture, user}, :thumb)
  end

  def render("reset_password.json", %{message: message}) do
    %{
      message: Gettext.gettext(Ipseeds.Gettext, message)
    }
  end

  def render("upload_success.json", %{user: user}) do
    %{
      success: true,
      file: user_profile_photo(user)
    }
  end

  def render("upload_failed.json", %{changeset: failed_changeset}) do
    errors = 
      failed_changeset.errors 
      |> Enum.map(&render_error_detail(&1))

    %{
      success: false,
      errors: errors
    }
  end

  def file_size_limit_note() do
    gettext "Maximum allowed size is %{size_limit}", size_limit: Sizeable.filesize(User.attachment_max_size_bytes())
  end

  defp render_error_detail({attr, message}) do
    "#{humanize(attr)} #{ErrorHelpers.translate_error(message)}"
  end
end
