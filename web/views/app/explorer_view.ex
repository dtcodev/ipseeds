defmodule Ipseeds.App.ExplorerView do
  use Ipseeds.Web, :view
  import Scrivener.HTML

  def api_timestamp_to_local_string(timestamp) when is_integer(timestamp) do
    timestamp
    |> api_timestamp_to_datetimestring
  end

  def api_timestamp_to_local_string(time) do
    Timex.parse!(time, "%FT%T%z", :strftime)
    |> Timex.Timezone.convert("Asia/Taipei")
    |> Timex.format!("{ISOdate} {ISOtime}")
  end

  def get_path_function(%{path_info: path_info} = _conn) do
    if Enum.member?(path_info, "secure"), do: &app_explorer_path/4, else: &explorer_path/4
  end

  def get_current_explorer_path(%{path_info: path_info} = conn, locale) do
    if Enum.member?(path_info, "secure"), do: app_explorer_path(conn, :index, locale), 
      else: explorer_path(conn, :index, locale)
  end

  def get_current_explorer_detail_path(%{path_info: path_info} = conn, locale, entry_id) do
    if Enum.member?(path_info, "secure"), do: app_explorer_path(conn, :show, locale, entry_id), 
      else: explorer_path(conn, :show, locale, entry_id)
  end

  def link_btc_txid(txid, arguments) do
    if txid == "", do: "(Unconfirmed)", else: link(txid, arguments)
  end
end
