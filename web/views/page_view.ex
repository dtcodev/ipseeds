defmodule Ipseeds.PageView do
  use Ipseeds.Web, :view
  import Ipseeds.Pow
  
  @doc """
  Provides counting text for homepage.
  """
  def counting_text(count) do
    if div(count, 10) > 0, do: "#{div(count, 10) * 10}+", else: "#{count}"
  end
end
