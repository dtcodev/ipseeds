defmodule Ipseeds.Admin.UserRegistrationView do
  use Ipseeds.Web, :view
  import Scrivener.HTML

  alias Ipseeds.{IndividualResearcherApplication, UserRegistration}

  def parse_type(value) do
    type_name = UserRegistration.type_map |> get_in([value, :display_name])
    Gettext.gettext(Ipseeds.Gettext, type_name)
  end

  def application_detail_template(%UserRegistration{application_type: application_type}) do
    "_" <> application_type <> "_application_detail.html"
  end

  def application_edit_template(%UserRegistration{application_type: application_type}) do
    "_" <> application_type <> "_application_edit.html"
  end

  def parse_id_type(application_data) do
    type_name = IndividualResearcherApplication.get_id_type(application_data)
    Gettext.gettext(Ipseeds.Gettext, type_name)
  end

  def parse_nationality(application_data) do
    country_name = IndividualResearcherApplication.get_country_name(application_data)
    Gettext.gettext(Ipseeds.Gettext, country_name)
  end

   def parse_gender(application_data) do
     gender = IndividualResearcherApplication.get_gender(application_data)
     Gettext.gettext(Ipseeds.Gettext, gender)
   end

   def approval_status(registration) do
     cond do
       UserRegistration.is_disapproved?(registration) ->
         content_tag :span, gettext("Disapproved"), class: "label label-danger"
       UserRegistration.is_approved?(registration) ->
         content_tag :span, gettext("Approved"), class: "label label-success"
       UserRegistration.is_unapproved?(registration) -> 
         content_tag :span, gettext("Pending"), class: "label label-default"
     end
   end

  def countries_list do
    Ipseeds.CountriesHelper.all
    |> Enum.map(fn(country) -> {country[:name], country[:alpha2]} end)
  end
end
