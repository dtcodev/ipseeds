defmodule Ipseeds.Admin.PasswordResetView do
  use Ipseeds.Web, :view

  def title(type) do
    if type == "invitation", do: "Setup password", else: "Reset password"
  end

  def action_path(conn, token, "invitation") do
    public_admin_password_reset_path(conn, :update, conn.assigns.locale, token, type: "invitation")
  end

  def action_path(conn, token, _) do
    public_admin_password_reset_path(conn, :update, conn.assigns.locale, token)
  end
end
