defmodule Ipseeds.UserView do
  use Ipseeds.Web, :view

  def render("api_password_reset_hook.json", %{}) do
    %{status: "ok"}
  end
end
