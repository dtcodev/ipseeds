defmodule Ipseeds.LayoutView do
  use Ipseeds.Web, :view

  @doc """
  Renders current locale.
  """
  def locale do
    Gettext.get_locale(Ipseeds.Gettext)
  end

  @doc """
  Provides tuples for all alternative languages supported.
  """
  def fb_locales do
    Ipseeds.Gettext.supported_locales
    |> Enum.map(fn l ->
      # Cannot call `locale/0` inside guard clause
      current = locale()
      case l do
        l when l == current -> {"og:locale", l}
        l -> {"og:locale:alternate", l}
      end
    end)
  end

  @doc """
  Provides tuples for all alternative languages supported.
  """
  def language_annotations(conn) do
    Ipseeds.Gettext.supported_locales
    |> Enum.reject(fn l -> l == locale() end)
    |> Enum.concat(["x-default"])
    |> Enum.map(fn l ->
      case l do
        "x-default" -> {"x-default", localized_url(conn, "")}
        l -> {l, localized_url(conn, "/#{l}")}
      end
    end)
  end

  @doc """
  Provides class names from controller name and action name
  """
  def body_classes(conn) do
    case conn.path_info do
      [] -> ""
      [_head | tail] ->
        tail |> Enum.join(" ")
    end
  end

  def render("exagon.html", %{conn: conn}) do
    exagon_encoded = Ipseeds.ExagonHelpers.get_exagon(conn)
      |> Poison.encode!()

    ~E"""
    <script type="text/javascript">
      window.exagon = <%= {:safe, exagon_encoded} %>;
    </script>
    """
  end

  def current_date do
    Timex.now("Asia/Taipei") |> Timex.format!("{D} {Mshort}, {YYYY}")
  end

  defp localized_url(conn, alt) do
    # Replace current locale with alternative
    path = ~r/\/#{locale()}(\/(?:[^?]+)?|$)/
    |> Regex.replace(conn.request_path, "#{alt}\\1")

    Phoenix.Router.Helpers.url(Ipseeds.Router, conn) <> path
  end
end
