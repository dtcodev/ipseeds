defmodule Ipseeds.SessionView do
  use Ipseeds.Web, :view

  def errors(conn) do
    conn.assigns.changeset.errors ++ conn.assigns.errors
  end

  def render("update.json", %{expire_time_utc: expire_time_utc}) do
    %{
      status: :ok,
      data: %{exagon: %{expire_time_utc: expire_time_utc}}
     }
  end
end
