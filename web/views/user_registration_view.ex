defmodule Ipseeds.UserRegistrationView do
  use Ipseeds.Web, :view

  def countries_list do
    Ipseeds.CountriesHelper.all
    |> Enum.map(fn(country) -> {country[:name], country[:alpha2]} end)
  end

  def get_field_value(conn, model, field, default \\ "") do
    field_value = 
      case Map.get(conn, :params) do
        %{^model => values} ->
          values |> Map.get(field)
        _ -> nil
      end

    unless field_value do
      case field do
        "date_of_birth" -> "1970-01-01"
        _ -> default
      end
    else
      field_value
    end
  end

  def signup_text_input(conn, model_atom, field_atom, opts \\ []) do
    options = build_value_options(conn, model_atom, field_atom, opts)
    text_input(model_atom, field_atom, options)
  end

  def signup_select(conn, model_atom, field_atom, choices, opts \\ []) do
    options = build_value_options(conn, model_atom, field_atom, opts)

    localized_choices = Enum.map choices, fn({k, v}) ->
      {k, Gettext.gettext(Ipseeds.Gettext, v)}
    end

    select(model_atom, field_atom, localized_choices, options)
  end

  def application_type_radio(conn, model_atom, field_atom, value, opts) do
    model = Atom.to_string(model_atom)
    field = Atom.to_string(field_atom)

    field_value = get_field_value(conn, model, field, "industry")
    
    options = if value == field_value do
      Keyword.merge([checked: "checked"], opts)
    else
      opts
    end

    radio_button(model_atom, field_atom, value, options)
  end

  def tab_class(conn, model_atom, field_atom, value) do
    model = Atom.to_string(model_atom)
    field = Atom.to_string(field_atom)

    field_value = get_field_value(conn, model, field, "industry")

    if value == field_value do
      "tab-pane active"
    else
      "tab-pane"
    end
  end

  def signup_checkbox(conn, model_atom, field_atom, opts \\ []) do
    options = build_value_options(conn, model_atom, field_atom, opts)
    checkbox(model_atom, field_atom, options)
  end

  defp build_value_options(conn, model_atom, field_atom, opts) do
    model = Atom.to_string(model_atom)
    field = Atom.to_string(field_atom)

    field_value = get_field_value(conn, model, field)

    Keyword.merge([value: field_value], opts)
  end
end
