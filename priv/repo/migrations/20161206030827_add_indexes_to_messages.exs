defmodule Ipseeds.Repo.Migrations.AddIndexesToMessages do
  use Ecto.Migration

  def change do
    drop index(:messages, [:sender_user_id])
    drop index(:messages, [:recipient_user_id])
    create index(:messages, [:sender_user_id, :sender_keypair_id])
    create index(:messages, [:recipient_user_id, :recipient_keypair_id])
    create index(:messages, [:read_at])
  end
end
