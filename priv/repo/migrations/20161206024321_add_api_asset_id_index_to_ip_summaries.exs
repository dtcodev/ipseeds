defmodule Ipseeds.Repo.Migrations.AddApiAssetIdIndexToIpSummaries do
  use Ecto.Migration

  def change do
    create index(:ip_summaries, [:api_asset_id])
  end
end
