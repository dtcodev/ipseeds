defmodule Ipseeds.Repo.Migrations.RemoveUniqueIndexFromUserRegistration do
  use Ecto.Migration

  def change do
    drop_if_exists unique_index(:user_registrations, [:application_id])
  end
end
