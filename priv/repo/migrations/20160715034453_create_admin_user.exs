defmodule Ipseeds.Repo.Migrations.CreateAdminUser do
  use Ecto.Migration

  def change do
    create table(:admin_users) do
      add :username, :string, null: false
      add :email, :string, null: false
      add :password_hash, :string
      add :is_superadmin, :boolean, default: false, null: false
      add :invitation_token_hash, :string
      add :invitation_created_at, :datetime
      add :invitation_created_by_admin_id, references(:admin_users)
      add :invitation_sent_at, :datetime
      add :invitation_accepted_at, :datetime
      add :reset_password_token_hash, :string
      add :reset_password_created_at, :datetime
      add :reset_password_sent_at, :datetime
      add :disabled_at, :datetime
      add :disabled_by_admin_id, references(:admin_users)
      add :deleted_at, :datetime
      add :deleted_by_admin_id, references(:admin_users)

      timestamps()
    end

    create unique_index(:admin_users, [:username])
    create unique_index(:admin_users, [:email])
  end
end
