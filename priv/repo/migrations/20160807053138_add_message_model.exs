defmodule Ipseeds.Repo.Migrations.AddMessageModel do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :sender_user_id, references(:users)
      add :recipient_user_id, references(:users)
      add :recipient_keypair_id, references(:user_keypairs)
      add :recipient_encrypted_content, :text
      add :sender_keypair_id, references(:user_keypairs)
      add :sender_encrypted_content, :text
    end

    create index(:messages, [:sender_user_id])
    create index(:messages, [:recipient_user_id])
  end
end
