defmodule Ipseeds.Repo.Migrations.CreateAgreeableContract do
  use Ecto.Migration

  def change do
	create table(:agreeable_contracts) do
	  add :sender_user_id, references(:users)
	  add :recipient_user_id, references(:users)
	  add :draft_asset_id, :string
	  add :final_asset_id, :string
	  add :declined_at, :datetime
	  add :cancelled_at, :datetime

	  timestamps  # inserted_at and updated_at
	end
	create index(:agreeable_contracts, [:sender_user_id])
    create index(:agreeable_contracts, [:recipient_user_id])
    create index(:agreeable_contracts, [:draft_asset_id])
    create index(:agreeable_contracts, [:final_asset_id])
  end
end
