defmodule Ipseeds.Repo.Migrations.AddReadAtToMessages do
  use Ecto.Migration

  def change do
    alter table(:messages) do
      add :read_at, :datetime
    end
  end
end
