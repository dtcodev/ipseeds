defmodule Ipseeds.Repo.Migrations.CreateApp.IpNotary do
  use Ecto.Migration

  def change do
    create table(:ip_notaries) do
      add :api_asset_id, :string
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end
    create index(:ip_notaries, [:user_id])

  end
end
