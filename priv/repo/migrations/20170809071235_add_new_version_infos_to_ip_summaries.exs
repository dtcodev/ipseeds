defmodule Ipseeds.Repo.Migrations.AddNewVersionInfosToIpSummaries do
  use Ecto.Migration

  def change do
  	alter table(:ip_summaries) do
      add :has_new_version, :boolean, default: false
      add :source_asset_id, :string
      add :initial_asset_id, :string
    end

    create index(:ip_summaries, [:initial_asset_id])
  end
end
