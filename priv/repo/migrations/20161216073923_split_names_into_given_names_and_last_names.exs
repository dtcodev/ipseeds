defmodule Ipseeds.Repo.Migrations.SplitNamesIntoGivenNamesAndLastNames do
  use Ecto.Migration

  def up do
    do_up(:individual_researcher_applications)
    do_up(:industry_applications, "user_")
    do_up(:industry_applications, "contact_")
    do_up(:research_institute_applications, "user_")
    do_up(:research_institute_applications, "contact_")
  end

  def down do
    do_down(:individual_researcher_applications)
    do_down(:industry_applications, "user_")
    do_down(:industry_applications, "contact_")
    do_down(:research_institute_applications, "user_")
    do_down(:research_institute_applications, "contact_")
  end

  defp do_down(table, prefix \\ "") do
    alter table(table) do
      add String.to_atom("#{prefix}name"), :string
    end

    execute down_sql(table, prefix)

    alter table(table) do
      remove String.to_atom("#{prefix}given_names")
      remove String.to_atom("#{prefix}last_name")
    end
  end

  defp do_up(table, prefix \\ "") do
    alter table(table) do
      add String.to_atom("#{prefix}given_names"), :string
      add String.to_atom("#{prefix}last_name"), :string
    end

    execute up_sql(table, prefix)

    alter table(table) do
      remove String.to_atom("#{prefix}name")
    end
  end

  defp up_sql(table, prefix) do
   ~s"""
   UPDATE #{Atom.to_string table} t SET
      #{prefix}given_names = CASE WHEN char_length(#{prefix}name) > 3
        THEN regexp_replace(#{prefix}name, ' [^\ ]*$', '')
        ELSE substring(#{prefix}name from 2 for (char_length(#{prefix}name) - 1))
      END,
      #{prefix}last_name = CASE WHEN char_length(#{prefix}name) > 3
        THEN regexp_replace(#{prefix}name, '^.* ', '')
        ELSE substring(#{prefix}name from '^.')
      END
    """
  end

  defp down_sql(table, prefix) do
    ~s"""
      UPDATE #{Atom.to_string table} t SET
        #{prefix}name = CASE WHEN char_length(#{prefix}last_name) > 1
          THEN #{prefix}given_names || ' ' || #{prefix}last_name
          ELSE #{prefix}last_name || #{prefix}given_names
        END
    """
  end
end
