defmodule Ipseeds.Repo.Migrations.RevertMessageContentFieldsToText do
  use Ecto.Migration

  def change do
    alter table(:messages) do
      remove :recipient_encrypted_content
      remove :sender_encrypted_content
      add :recipient_encrypted_content, :text
      add :sender_encrypted_content, :text
    end
  end
end
