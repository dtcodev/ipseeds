defmodule Ipseeds.Repo.Migrations.ChangeMessageContentFieldsToBytea do
  use Ecto.Migration

  def change do
    alter table(:messages) do
      remove :recipient_encrypted_content
      remove :sender_encrypted_content
      add :recipient_encrypted_content, :bytea
      add :sender_encrypted_content, :bytea
    end
  end
end
