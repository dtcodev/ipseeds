defmodule Ipseeds.Repo.Migrations.AddUserIdIndexToUserKeypairs do
  use Ecto.Migration

  def change do
    create index(:user_keypairs, [:user_id])
  end
end
