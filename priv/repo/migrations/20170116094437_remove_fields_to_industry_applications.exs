defmodule Ipseeds.Repo.Migrations.RemoveFieldsToIndustryApplications do
  use Ecto.Migration

  def up do
  	alter table(:industry_applications) do
      remove :company_capital_usd
      remove :company_listed_status
      remove :company_employee_count
      remove :company_rnd_personnel_count
    end
  end

  def down do
  	alter table(:industry_applications) do
      add :company_capital_usd, :integer
      add :company_listed_status, :string
      add :company_employee_count, :integer
      add :company_rnd_personnel_count, :integer
    end
  end
end
