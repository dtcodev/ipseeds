defmodule Ipseeds.Repo.Migrations.AddPasswordResetedAt do
  use Ecto.Migration

  def change do
    alter table(:admin_users) do
      add :password_reseted_at, :datetime
    end
  end
end
