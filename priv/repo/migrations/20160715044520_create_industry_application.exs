defmodule Ipseeds.Repo.Migrations.CreateIndustryApplication do
  use Ecto.Migration

  def change do
    create table(:industry_applications) do
      add :company_name_zh_tw, :string
      add :company_name_en, :string, null: false
      add :company_tax_id, :string, null: false
      add :company_website, :string
      add :company_capital_usd, :integer, null: false
      add :company_listed_status, :string, null: false
      add :company_employee_count, :integer, null: false
      add :company_rnd_personnel_count, :integer, null: false
      add :user_name, :string, null: false
      add :user_department, :string, null: false
      add :user_title, :string, null: false
      add :user_telephone, :string, null: false
      add :user_address, :string, null: false
      add :contact_name, :string, null: false
      add :contact_department, :string, null: false
      add :contact_title, :string, null: false
      add :contact_telephone, :string, null: false
      add :contact_address, :string, null: false

      timestamps()
    end

  end
end
