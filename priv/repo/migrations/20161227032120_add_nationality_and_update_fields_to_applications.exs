defmodule Ipseeds.Repo.Migrations.AddNationalityAndUpdateFieldsToApplications do
  use Ecto.Migration

  def up do
    alter table(:industry_applications) do
      add    :company_nationality,   :string
      modify :company_tax_id,        :string, null: true
    end

    alter table(:research_institute_applications) do
      add    :institute_nationality, :string
      modify :institute_name_zh_tw,  :string, null: true
      modify :institute_tax_id,      :string, null: true
    end
  end

  def down do
    alter table(:industry_applications) do
      remove :company_nationality
      modify :company_tax_id,        :string, null: false
    end

    alter table(:research_institute_applications) do
      remove :institute_nationality
      modify :institute_name_zh_tw,  :string, null: false
      modify :institute_tax_id,      :string, null: false
    end
  end

end
