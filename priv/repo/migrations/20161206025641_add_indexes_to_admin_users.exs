defmodule Ipseeds.Repo.Migrations.AddIndexesToAdminUsers do
  use Ecto.Migration

  def change do
    create index(:admin_users, [:deleted_at])
    create index(:admin_users, [:disabled_at])
    create index(:admin_users, [:is_superadmin])
  end
end
