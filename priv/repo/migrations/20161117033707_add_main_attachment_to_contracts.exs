defmodule Ipseeds.Repo.Migrations.AddMainAttachmentToContracts do
  use Ecto.Migration

  def change do
    alter table(:contracts) do
      add :main_attachment_id, references(:contract_attachments)
    end
  end
end
