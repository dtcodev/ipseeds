defmodule Ipseeds.Repo.Migrations.AddIndexesToContracts do
  use Ecto.Migration

  def change do
    create index(:contracts, [:api_transaction_id])
    create index(:contracts, [:recipient_user_id])
    create index(:contracts, [:issuer_user_id])
  end
end
