defmodule Ipseeds.Repo.Migrations.AddCreatedAtIndexToUserRegistrations do
  use Ecto.Migration

  def change do
    create index(:user_registrations, [:approved_at])
  end
end
