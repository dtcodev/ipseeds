defmodule Ipseeds.Repo.Migrations.AddTackIdBtcToIpNotaries do
  use Ecto.Migration

  def change do
    alter table(:ip_notaries) do
      add :tack_id_BTC, :string
    end
  end
end
