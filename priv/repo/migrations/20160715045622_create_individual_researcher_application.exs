defmodule Ipseeds.Repo.Migrations.CreateIndividualResearcherApplication do
  use Ecto.Migration

  def change do
    create table(:individual_researcher_applications) do
      add :name, :string, null: false
      add :id_number, :string, null: false
      add :id_type, :string, null: false
      add :nationality, :string, null: false
      add :date_of_birth, :date, null: false
      add :gender, :char, null: false
      add :organization, :string, null: false
      add :department, :string, null: false
      add :title, :string, null: false
      add :telephone, :string, null: false
      add :mobile, :string
      add :address, :string, null: false

      timestamps()
    end

  end
end
