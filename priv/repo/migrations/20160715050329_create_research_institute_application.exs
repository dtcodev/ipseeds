defmodule Ipseeds.Repo.Migrations.CreateResearchInstituteApplication do
  use Ecto.Migration

  def change do
    create table(:research_institute_applications) do
      add :institute_name_zh_tw, :string, null: false
      add :institute_name_en, :string, null: false
      add :institute_tax_id, :string, null: false
      add :institute_website, :string
      add :user_name, :string, null: false
      add :user_department, :string, null: false
      add :user_title, :string, null: false
      add :user_telephone, :string, null: false
      add :user_address, :string, null: false
      add :contact_name, :string, null: false
      add :contact_department, :string, null: false
      add :contact_title, :string, null: false
      add :contact_telephone, :string, null: false
      add :contact_address, :string, null: false

      timestamps()
    end

  end
end
