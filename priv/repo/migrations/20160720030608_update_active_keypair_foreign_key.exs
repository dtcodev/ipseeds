defmodule Ipseeds.Repo.Migrations.UpdateActiveKeypairForeignKey do
  use Ecto.Migration

  def change do
    execute "ALTER TABLE users DROP CONSTRAINT users_active_keypair_id_fkey"
    alter table(:users) do
      modify :active_keypair_id, references(:user_keypairs, on_delete: :nilify_all, on_update: :update_all), null: true
    end
  end
end
