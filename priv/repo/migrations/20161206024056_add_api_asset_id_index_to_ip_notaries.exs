defmodule Ipseeds.Repo.Migrations.AddApiAssetIdIndexToIpNotaries do
  use Ecto.Migration

  def change do
    create index(:ip_notaries, [:api_asset_id])
  end
end
