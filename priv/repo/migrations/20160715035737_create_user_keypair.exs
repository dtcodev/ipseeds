defmodule Ipseeds.Repo.Migrations.CreateUserKeypair do
  use Ecto.Migration

  def change do
    create table(:user_keypairs) do
      add :bip32_hd_public_key, :string
      add :rsa_public_key_pem, :text

      timestamps()
    end

  end
end
