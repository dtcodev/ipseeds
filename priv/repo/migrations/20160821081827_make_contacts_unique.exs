defmodule Ipseeds.Repo.Migrations.MakeContactsUnique do
  use Ecto.Migration

  def change do
    create index(:user_contacts, [:user_id, :contact_user_id], unique: true)
  end
end
