defmodule Ipseeds.Repo.Migrations.AddIsVisibleToIpSummary do
  use Ecto.Migration

  def change do
    alter table(:ip_summaries) do
      add :is_visible, :boolean, default: true
    end
  end
end
