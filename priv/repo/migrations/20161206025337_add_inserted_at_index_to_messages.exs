defmodule Ipseeds.Repo.Migrations.AddInsertedAtIndexToMessages do
  use Ecto.Migration

  def change do
    create index(:messages, [:inserted_at])
  end
end
