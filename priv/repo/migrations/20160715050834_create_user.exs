defmodule Ipseeds.Repo.Migrations.CreateUser do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :username, :string, null: false
      add :email, :string, null: false
      add :user_registration_id, :integer
      add :is_paid, :boolean, default: false, null: false
      add :receiver_asset_addr, :string
      add :active_keypair_id, references(:user_registrations), null: true
      add :biography, :text
      add :picture, :string
      add :profile_show_email, :boolean, default: false, null: false
      add :message_notify_by_email, :boolean, default: true, null: false

      timestamps()
    end

    create unique_index(:users, [:username])
    create unique_index(:users, [:email])
    create unique_index(:users, [:user_registration_id])
    create index(:users, [:is_paid])
  end
end
