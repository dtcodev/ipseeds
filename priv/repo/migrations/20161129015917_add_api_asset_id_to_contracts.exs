defmodule Ipseeds.Repo.Migrations.AddApiAssetIdToContracts do
  use Ecto.Migration

  def change do
    alter table(:contracts) do
      add :api_sent_asset_id, :string
    end
  end
end
