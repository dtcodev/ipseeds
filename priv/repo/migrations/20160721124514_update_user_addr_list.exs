defmodule Ipseeds.Repo.Migrations.UpdateUserAddrList do
  use Ecto.Migration

  def change do
    alter table(:users) do
      modify :receiver_asset_addr, :text
    end
  end
end
