defmodule Ipseeds.Repo.Migrations.RemoveIsVisibleFromIpSummaries do
  use Ecto.Migration

  def up do
    alter table(:ip_summaries) do
      remove :is_visible
    end
  end

  def down do
    alter table(:ip_summaries) do
      add :is_visible, :boolean, default: true
    end
  end
end
