defmodule Ipseeds.Repo.Migrations.CreateContract do
  use Ecto.Migration

  def change do
    create table(:contracts) do
      add :issuer_user_id, references(:users)
      add :recipient_user_id, references(:users)
      add :api_transaction_id, :string

      timestamps()
    end

  end
end
