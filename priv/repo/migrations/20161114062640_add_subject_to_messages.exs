defmodule Ipseeds.Repo.Migrations.AddSubjectToMessages do
  use Ecto.Migration

  def change do
    alter table(:messages) do
      add :subject, :string
    end

    create index(:messages, [:subject])
    # Add default subject
    execute "UPDATE messages SET subject = 'Empty Subject'"
  end
end
