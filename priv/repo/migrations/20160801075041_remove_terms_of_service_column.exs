defmodule Ipseeds.Repo.Migrations.RemoveTermsOfServiceColumn do
  use Ecto.Migration

  def change do
    alter table(:user_registrations) do
      remove :terms_of_service
    end
  end
end
