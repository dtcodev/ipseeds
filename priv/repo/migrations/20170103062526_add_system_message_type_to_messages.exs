defmodule Ipseeds.Repo.Migrations.AddSystemMessageTypeToMessages do
  use Ecto.Migration

  def change do
    alter table(:messages) do
      add :is_system_message, :boolean, default: false
    end
  end
end
