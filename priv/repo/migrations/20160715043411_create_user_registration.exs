defmodule Ipseeds.Repo.Migrations.CreateUserRegistration do
  use Ecto.Migration

  def change do
    create table(:user_registrations) do
      add :application_id, :integer, null: false
      add :application_type, :string, null: false
      add :terms_of_service, :boolean, default: false, null: false
      add :approved_at, :datetime
      add :approved_by_admin_id, references(:admin_users)
      add :disapproved_at, :datetime
      add :disapproved_by_admin_id, references(:admin_users)
      add :disapproved_reason, :text

      timestamps()
    end

    create unique_index(:user_registrations, [:application_id])
    create index(:user_registrations, [:application_type])
  end
end
