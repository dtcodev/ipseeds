defmodule Ipseeds.Repo.Migrations.CorrectUserModelReferences do
  use Ecto.Migration

  def change do
    alter table(:users) do
      remove :user_registration_id
      remove :active_keypair_id
    end
    drop_if_exists unique_index(:users, [:user_registration_id])

    alter table(:user_registrations) do
      add :user_id, references(:users)
    end

    alter table(:user_keypairs) do
      add :user_id, references(:users)
    end
  end
end
