defmodule Ipseeds.Repo.Migrations.CreateContractAttachment do
  use Ecto.Migration

  def change do
    create table(:contract_attachments) do
      add :contract_id, references(:contracts)
      add :s3_filename, :string
      add :original_filename, :string
      add :mime_type, :string

      timestamps()
    end

  end
end
