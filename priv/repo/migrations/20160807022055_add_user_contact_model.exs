defmodule Ipseeds.Repo.Migrations.AddUserContactModel do
  use Ecto.Migration

  def change do
    create table(:user_contacts) do
      add :user_id, references(:users, on_delete: :delete_all)
      add :contact_user_id, references(:users, on_delete: :delete_all)
    end

    create index(:user_contacts, [:user_id])
  end
end
