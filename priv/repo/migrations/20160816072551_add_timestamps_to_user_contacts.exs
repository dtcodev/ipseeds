defmodule Ipseeds.Repo.Migrations.AddTimestampsToUserContacts do
  use Ecto.Migration

  def change do
    alter table(:user_contacts) do
      timestamps()
    end
  end
end
