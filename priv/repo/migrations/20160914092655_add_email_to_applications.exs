defmodule Ipseeds.Repo.Migrations.AddEmailToApplications do
  use Ecto.Migration

  def change do
    alter table(:industry_applications) do
      add :contact_email, :string
    end

    alter table(:research_institute_applications) do
      add :contact_email, :string
    end
  end
end
