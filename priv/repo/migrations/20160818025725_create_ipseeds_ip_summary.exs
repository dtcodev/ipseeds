defmodule Ipseeds.Repo.Migrations.CreateApp.IpSummary do
  use Ecto.Migration

  def change do
    create table(:ip_summaries) do
      add :user_id, references(:users)
      add :api_asset_id, :string

      timestamps()
    end

    create index(:ip_summaries, [:user_id])
  end
end
