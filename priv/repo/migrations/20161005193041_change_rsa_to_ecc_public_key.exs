defmodule Ipseeds.Repo.Migrations.ChangeRsaToEccPublicKey do
  use Ecto.Migration

  def change do
    alter table(:user_keypairs) do
      remove :rsa_public_key_pem
      add :ecc_public_key_hex, :string
    end
  end
end
