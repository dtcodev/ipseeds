use Mix.Config

config :ipseeds, :config_path, "../conf/app_config.yml"

config :ipseeds, Ipseeds.Endpoint,
  http: [port: {:system, "PORT"}],
  cache_static_manifest: "priv/static/manifest.json"

# Print debug messages in staging
config :phoenix, :stacktrace_depth, 20
config :logger,
  level: :debug,
  utc_log: true,
  backends: [{LoggerFileBackend, :debug_log}]

config :logger, :debug_log,
  format: "$date $time+0000 $metadata[$level] $message\n",
  path: Path.absname("../logs/ipseeds_debug.log", System.cwd),
  level: :debug