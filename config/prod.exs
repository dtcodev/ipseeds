use Mix.Config

config :ipseeds, :config_path, "../conf/app_config.yml"

config :ipseeds, Ipseeds.Endpoint,
  http: [port: {:system, "PORT"}],
  cache_static_manifest: "priv/static/manifest.json"

# Do not print debug messages in production
config :logger,
  level: :error,
  utc_log: true,
  backends: [{LoggerFileBackend, :error_log}]

config :logger, :error_log,
  format: "$date $time+0000 $metadata[$level] $message\n",
  path: Path.absname("../logs/ipseeds_error.log", System.cwd),
  level: :error