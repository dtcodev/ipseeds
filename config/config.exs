# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :ipseeds,
  ecto_repos: [Ipseeds.Repo]

# Configures the endpoint
config :ipseeds, Ipseeds.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "rBPva/a5Nx8O9pPQztsM0dLF4P9kseK0H0o50gQBohoe5vH6dCWaDpZSKWsyY/Lr",
  render_errors: [view: Ipseeds.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Ipseeds.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger,
  format: "$date $time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configures Gettext
config :ipseeds, Ipseeds.Gettext,
  locales: ~w(en zh-tw ja-jp)

config :ipseeds, Ipseeds.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "ipseeds_dev",
  hostname: "localhost",
  pool_size: 10,
  timeout: 31_000

config :arc,
  version_timeout: 30_000

config :ex_aws,
  s3: [
    scheme: "https://",
    host: "s3-accelerate.amazonaws.com"]

# Configures API's Endpoint
# Important: endpoint must include trailing slash
config :ipseeds, Ipseeds.API,
  software_name: "IPseeds",
  endpoint: "http://apitest.example.com/"

# Configures Bamboo Mailler
config :ipseeds, Ipseeds.Mailer,
  adapter: Bamboo.LocalAdapter

# Configures Comeonin encrypt library
config :comeonin, Ecto.Password, Comeonin.Bcrypt

# Configures Arc library
config :arc,
  storage: Arc.Storage.S3,
  bucket: "uploads"

# Configures ExAWS library
config :ex_aws,
  access_key_id: "aws_access_key_id",
  secret_access_key: "aws_secret_access_key"

# Configures Scrivener.HTML library
config :scrivener_html,
  routes_helper: Ipseeds.Router.Helpers

config :ipseeds, Ipseeds.Mailer,
    default_from: "no-reply-dev@ipseeds.net",
    default_bcc: [],
    contact_us_receiver: "support@ipseeds.net"

# Configures Google reCaptcha library
config :recaptcha,
    public_key: {:system, "RECAPTCHA_PUBLIC_KEY"},
    secret: {:system, "RECAPTCHA_PRIVATE_KEY"}    

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
