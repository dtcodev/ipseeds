use Mix.Config

config :comeonin, :bcrypt_log_rounds, 4
config :comeonin, :pbkdf2_rounds, 1

config :ipseeds, :config_path, "test_config.yml"

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :ipseeds, Ipseeds.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :ipseeds, Ipseeds.Repo,
  database: "ipseeds_test",
  pool: Ecto.Adapters.SQL.Sandbox

# Configures Bamboo Mailler
config :ipseeds, Ipseeds.Mailer,
  adapter: Bamboo.TestAdapter,
  contact_us_receiver: "null@dev.com"

config :arc,
  storage: Arc.Storage.Local

