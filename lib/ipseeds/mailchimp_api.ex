defmodule Ipseeds.MailchimpAPI do 
  use HTTPoison.Base

  alias Ipseeds.{APIHelpers}

  @default_subscribe_payload %{
    "status" => "pending"
  }

  def list_id, do: config()[:list_id]
  def software, do: config()[:software_name]
  def api_key, do: config()[:api_key]

  def process_url(api_path) do
    config()[:endpoint] <> api_path
  end

  def process_request_headers(headers) do
    [{"content-type", "application/json"} | headers]
  end

  def process_request_body(body) do
    Poison.encode!(body)
  end

  def process_response_body(body) do
    try do
      Poison.decode!(body)
    rescue
      _ -> body
    end
  end

  def call_endpoint_need_auth(path, request_body) when is_list(request_body) do
    call_endpoint_need_auth(path, Enum.into(request_body, %{}))
  end
  def call_endpoint_need_auth("/3.0/lists/list_id/members", request_body) do
    payload = Map.merge(@default_subscribe_payload, request_body)
    do_call_endpoint_need_auth("/3.0/lists/#{list_id()}/members", payload, :post)
  end
  def call_endpoint_need_auth(api_path, request_body) do
    do_call_endpoint_need_auth(api_path, request_body, :post)
  end

  defp do_call_endpoint_need_auth(api_path, request_body, method) do
    req_metadata = APIHelpers.log_api_req(__MODULE__, api_path, request_body, method)
    auth = [hackney: [basic_auth: {software(), api_key()}]]
    _response =
      request(method, api_path, request_body, [], auth)
      |> APIHelpers.log_api_res(req_metadata)
  end

  defp config, do: Application.get_env(:ipseeds, __MODULE__)

end