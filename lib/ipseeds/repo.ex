defmodule Ipseeds.Repo do
  use Ecto.Repo, otp_app: :ipseeds
  use Scrivener, page_size: 10
end
