defmodule Ipseeds.BlockTackAPI do
  require Logger

  use HTTPoison.Base, otp_app: :ipseeds

  alias Ipseeds.{APIHelpers}

  @max_retries 1

  def software, do: config()[:software_name]

  def max_retries, do: @max_retries

  def process_url(api_path) do
    config()[:endpoint] <> api_path
  end

  def process_request_headers(headers) do
    [{"content-type", "application/json"} | headers]
  end

  def process_request_body(body) do
    Poison.encode!(body)
  end

  def process_response_body(body) do
    try do
      Poison.decode!(body)
    rescue
      _ -> body
    end
  end

  def call_endpoint(path, request_body) when is_list(request_body) do
    call_endpoint(path, Enum.into(request_body, %{}))
  end
  def call_endpoint("/proof", request_body) do
    %{sign_hash: sign_hash, timestamp: timestamp} = genrate_sign_hash()
    permission = %{"apikey" => api_key(), "time_utc" => timestamp, "signhash" => sign_hash}
    request_body = request_body |> Map.put("permission", permission)

    do_call_endpoint("/proof", request_body, :post)
  end
  def call_endpoint(api_path, request_body) do
    do_call_endpoint(api_path, request_body, :post)
  end

  defp do_call_endpoint(api_path, request_body, method) do
    req_metadata = APIHelpers.log_api_req(__MODULE__, api_path, request_body, method)
    _response =
      request(method, api_path, request_body)
      |> APIHelpers.log_api_res(req_metadata)
  end

  def stringify_map(map) do
    map
    |> Enum.reduce(%{}, &stringify_key(&1, &2))
  end

  defp stringify_key({k, v}, acc) when is_atom(k) do
    acc |> Map.put(Atom.to_string(k), v)
  end
  defp stringify_key({k, v}, acc) do
    acc |> Map.put(k, v)
  end
  defp stringify_key(element, _acc), do: element

  defp config, do: Application.get_env(:ipseeds, __MODULE__)

  defp api_key, do: config()[:api_key]

  defp api_secret, do: config()[:api_secret]

  defp genrate_sign_hash do
    timestamp = Timex.now |> Timex.to_unix
    pre_hash = timestamp |> Integer.to_string(16) |> String.rjust(16, ?0) |> String.downcase
    sign_hash =
      :crypto.hash(:sha256, :crypto.hash(:sha256, "#{api_key()}#{pre_hash}#{api_secret()}"))
      |> Base.encode16()

    %{sign_hash: sign_hash, timestamp: timestamp}
  end
  
end
