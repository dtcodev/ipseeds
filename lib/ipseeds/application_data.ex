defprotocol Ipseeds.ApplicationData do
	def get_company(application)
	def get_branch(application)
	def get_address(application)
	def get_api_company(application)
	def get_api_branch(application)
	def get_name(application)
	def get_nationality_name(application)
	def get_website(application)
end
