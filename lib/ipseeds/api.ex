defmodule Ipseeds.API do
  require Logger

  use HTTPoison.Base, otp_app: :ipseeds

  alias Ipseeds.{User, APIHelpers}

  @default_register_payload %{
    "accounttype" => 0,
    "regtype" => 2,
    "address_info" => true
  }

  @max_retries 1

  def software, do: config()[:software_name]

  def max_retries, do: @max_retries

  def process_url(api_path) do
    config()[:endpoint] <> api_path
  end

  def process_request_headers(headers) do
    [{"content-type", "application/json"} | headers]
  end

  def process_request_body(body) do
    Poison.encode!(body)
  end

  def process_response_body(body) do
    try do
      Poison.decode!(body)
    rescue
      _ -> body
    end
  end

  def call_endpoint(path, request_body) when is_list(request_body) do
    call_endpoint(path, Enum.into(request_body, %{}))
  end
  def call_endpoint("/account/register", request_body) do
    payload = Map.merge(default_register_payload(), request_body)
    do_call_endpoint("/account/register", payload, :post)
  end
  def call_endpoint("/asset/query" = path, %{asset_id: asset_id}) do
    do_call_endpoint("#{path}/#{asset_id}", %{}, :get)
  end
  def call_endpoint("/asset/page" = path, %{page: page}) do
    do_call_endpoint("#{path}/#{page}", %{}, :get)
  end
  def call_endpoint("/explore/block_with_entry" = path, %{tx_id: tx_id}) do
    do_call_endpoint("#{path}/#{tx_id}", %{}, :get)
  end
  def call_endpoint("/explore/entry_verify" = path, %{entry_id: entry_id}) do
    do_call_endpoint("#{path}/#{entry_id}", %{}, :get)
  end
  def call_endpoint("/explore/block_with_entry" = path, %{entry_id: entry_id}) do
    do_call_endpoint("#{path}/#{entry_id}", %{}, :get)
  end
  def call_endpoint("/explore/block_verify" = path, %{block_id: block_id}) do
    do_call_endpoint("#{path}/#{block_id}", %{}, :get)
  end
  def call_endpoint("/explore/anchorinfo" = path, %{block_id: block_id}) do
    do_call_endpoint("#{path}/#{block_id}", %{}, :get)
  end
  def call_endpoint(api_path, request_body) do
    do_call_endpoint(api_path, request_body, :post)
  end

  def call_authed_endpoint(conn, api_path), do: call_authed_endpoint(conn, api_path, %{}, 0)
  def call_authed_endpoint(conn, api_path, request_body) when is_list(request_body) do
    call_authed_endpoint(conn, api_path, Enum.into(request_body, %{}), 0)
  end
  def call_authed_endpoint(conn, api_path, %{} = request_body) do
    call_authed_endpoint(conn, api_path, request_body, 0)
  end
  def call_authed_endpoint(conn, api_path, request_body, retries) when is_list(request_body) do
    call_authed_endpoint(conn, api_path, Enum.into(request_body, %{}), retries)
  end
  def call_authed_endpoint(conn, api_path, %{} = request_body, retries) when is_integer(retries) do
    authed_payload =
      conn
      |> build_auth_payload
      |> Map.merge(request_body)

    api_resp = do_call_endpoint(api_path, authed_payload, :post)

    case api_resp do
      {:ok, %{body: %{"account_auth" => %{"auth_code" => auth_code,
        "expire_time_utc" => expire_time_utc}}}} ->
        new_conn = conn
          |> Ipseeds.App.AuthPlug.update_auth_data(auth_code, expire_time_utc)
        {new_conn, api_resp}
      {:ok, %{body: %{"error_code" => error_code}}} when error_code == 7001 or error_code == -1 ->
        if retries == @max_retries do
          new_conn = Plug.Conn.clear_session(conn)
          {new_conn, api_resp}
        else
          call_authed_endpoint(conn, api_path, request_body, retries + 1)
        end
      _ ->
        {conn, api_resp}
    end
  end

  defp do_call_endpoint(api_path, request_body, method) do
    req_metadata = APIHelpers.log_api_req(__MODULE__, api_path, request_body, method)
    _response =
      request(method, api_path, request_body)
      |> APIHelpers.log_api_res(req_metadata)
  end

  def default_auth_payload(account_name, company, branch, auth_code) do
    time_utc = DateTime.utc_now |> DateTime.to_unix
    default_auth_payload(account_name, company, branch, auth_code, time_utc)
  end
  def default_auth_payload(account_name, company, branch, auth_code, time_utc) do
    %{
      "account_key" => %{
        "company" => company,
        "branch" => branch,
        "account" => account_name,
        "software" => software(),
      },
      "account_auth" => %{
        "auth_code" => auth_code,
        "time_utc" => time_utc
      }
    }
  end

  defp build_auth_payload(conn) do
    current_user = conn.assigns.current_user
    auth_code = conn |> Plug.Conn.get_session(:auth_code)

    {company, branch} = User.get_api_company_and_branch(current_user)
    default_auth_payload(current_user.username, company, branch, auth_code)
  end

  def stringify_map(map) do
    map
    |> Enum.reduce(%{}, &stringify_key(&1, &2))
  end

  defp stringify_key({k, v}, acc) when is_atom(k) do
    acc |> Map.put(Atom.to_string(k), v)
  end
  defp stringify_key({k, v}, acc) do
    acc |> Map.put(k, v)
  end
  defp stringify_key(element, _acc), do: element

  defp config, do: Application.get_env(:ipseeds, __MODULE__)

  defp default_register_payload do
    Map.put(@default_register_payload, "software", software())
  end
end
