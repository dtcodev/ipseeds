defmodule Ipseeds.MediumRSSFeed do 
  use HTTPoison.Base

  def get_xml do
  	request(:get, "https://medium.com/feed/blog-ipseeds-net")
  end
end
