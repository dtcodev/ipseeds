defmodule Ipseeds.Endpoint do
  use Phoenix.Endpoint, otp_app: :ipseeds

  socket "/socket", Ipseeds.UserSocket

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phoenix.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/", from: :ipseeds, gzip: false,
    only: ~w(css fonts images js pdf
      favicon.ico robots.txt
      b70cef088681c7a4e411e8055ae442f0.txt d00cd2a90c9bb23b3b1021a14ce7d82cd7d4fa10e8c2daafbf3acf0e3b913525.txt)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Poison,
    length: 20_000_000 # set max body size to 20mb

  plug Plug.MethodOverride
  plug Plug.Head

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  plug Plug.Session,
    store: :cookie,
    key: "_ipseeds_key",
    signing_salt: "FL8D/Ef9"

  plug Ipseeds.Router
end
