defmodule Mix.Tasks.Ipseeds.Drop do
  use Mix.Task

  @shortdoc "Drop the DB with runtime config from file"

  def run(args) do
    Application.ensure_all_started(:yaml_elixir)
    Ipseeds.load_config_from_file()

    Mix.Tasks.Ecto.Drop.run(args)
  end
end