defmodule Mix.Tasks.Ipseeds.Rollback do
  use Mix.Task

  @shortdoc "Run the repo migrations with runtime config from file"

  def run(args, migrator \\ &Ecto.Migrator.run/4) do
    Application.ensure_all_started(:yaml_elixir)
    Ipseeds.load_config_from_file()

    Mix.Tasks.Ecto.Rollback.run(args, migrator)
  end
end