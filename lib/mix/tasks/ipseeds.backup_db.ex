defmodule Mix.Tasks.Ipseeds.BackupDb do
  use Mix.Task

  @shortdoc "Back up the database to S3"

  def run(_args) do
    Application.ensure_all_started(:yaml_elixir)
    Application.ensure_all_started(:ex_aws)
    Application.ensure_all_started(:httpoison)
    Ipseeds.load_config_from_file()

    repo_config = Application.get_env(:ipseeds, Ipseeds.Repo) |> Enum.into(Map.new)
    tmp_dir = System.tmp_dir!
    time_now = Timex.now
    timestamp = Timex.format!(time_now, "{YYYY}-{M}-{D}-{h24}-{m}-{s}")
    backup_filename = "ipseeds-db-backup-#{Mix.env}-#{timestamp}.sql.gz"
    backup_path = "#{tmp_dir}/#{backup_filename}"
    s3_bucket = Application.get_env(:ipseeds, :backups_bucket)
    s3_prefix = Timex.format!(time_now, "{YYYY}/{M}")
    s3_key = "#{s3_prefix}/#{backup_filename}"
    s3_uri = "s3://#{s3_bucket}/#{s3_key}"
    s3_options = []

    Mix.shell.info "Dumping database to #{backup_path}..."
    backup_command = "pg_dump -h #{repo_config[:hostname]} -U #{repo_config[:username]} " <>
      "-d #{repo_config[:database]} | gzip > #{backup_path}"
    dump_cmd_status = Mix.shell.cmd(backup_command)
    Mix.shell.info "Dump command complete with exit status #{dump_cmd_status}.\n"

    Mix.shell.info "Uploading #{backup_filename} to S3..."
    ExAws.S3.put_object!(s3_bucket, s3_key, File.read!(backup_path), s3_options)
    Mix.shell.info "#{s3_uri} uploaded successfully.\n"

    Mix.shell.info "Removing backup file #{backup_path}..."
    File.rm! backup_path
    Mix.shell.info "#{backup_path} removed successfully.\n"
  end
end