defmodule Mix.Tasks.Ipseeds.EnsureData do
  use Mix.Task
  alias Ipseeds.Repo
  alias Ipseeds.AdminUser

  @shortdoc "Run idempotent tasks to ensure data inside db"

  def run(_args) do
    Application.ensure_all_started(:yaml_elixir)
    Ipseeds.load_config_from_file()
    Mix.Task.run "app.start"

    seed_admin_users(Mix.env)
  end

  def add_admin(email, input_password \\ nil, is_superadmin \\ false) do
    case Mix.env do
      :prod ->
        Mix.shell.info("Manually create admin in #{Mix.env} is forbidden.")
      _ ->
        password = if input_password, do: input_password, else: UUID.uuid4()

        [%{email: email, password: password, is_superadmin: is_superadmin}]
        |> ensure_admin_users_exist
    end
  end

  defp seed_admin_users(:prod) do
    admin = [
      %{email: "admin@ipseeds.net", is_superadmin: true},
      %{email: "jacoblee@dtco.co", is_superadmin: true},
      %{email: "nickylu@trpma.org.tw", is_superadmin: true}
    ]

    admin
    |> ensure_admin_users_exist()
  end

  defp seed_admin_users(:dev) do
    admins = [
      %{email: "admin@ipseeds.net", is_superadmin: true},
      %{email: "percy@cogini.com", is_superadmin: true},
      %{email: "nghitpm@cogini.com", is_superadmin: true},
      %{email: "jacoblee@dtco.co", is_superadmin: true},
      %{email: "nickylu@trpma.org.tw", is_superadmin: true}
    ]

    admins
    |> ensure_admin_users_exist()
  end

  defp ensure_admin_users_exist(admins) do
    admins
    |> Enum.map(&to_params/1)
    |> Enum.reject(&is_exist/1)
    |> Enum.each(&insert_admin_user/1)
  end

  defp is_exist(%{email: email}) do
     Repo.get_by(AdminUser, %{email: email})
  end

  defp insert_admin_user(params = %{email: email}) do
    %{
      username: email,
      email: email,
      password: UUID.uuid4(),
      is_superadmin: false
    }
    |> Map.merge(params)
    |> fn chg -> AdminUser.registration_changeset(%AdminUser{}, chg) end.()
    |> Repo.insert!()

    Mix.shell.info("Create #{email} as #{role(params.is_superadmin)} in #{Mix.env} database")
  end

  defp to_params(admin) when is_bitstring(admin) do
    %{email: admin}
  end
  defp to_params(admin) when is_map(admin) do
    admin
  end

  defp role(is_superadmin) do
    if is_superadmin, do: "super admin", else: "admin"
  end
end
