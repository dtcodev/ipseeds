defmodule Ipseeds do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    load_config_from_file()

    children = [
      supervisor(Ipseeds.Repo, []),
      supervisor(Ipseeds.Endpoint, []),
    ]

    opts = [strategy: :one_for_one, name: Ipseeds.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    Ipseeds.Endpoint.config_change(changed, removed)
    :ok
  end

  def load_config_from_file do
    # Iterate over each application in config file
    Enum.each(env_config(), fn({app_atom, app_conf}) ->
      put_app_conf(app_atom, app_conf)
    end)
  end
  
  defp put_app_conf(app_atom, app_conf) do
    Enum.each(app_conf, fn({conf_key, conf_value}) ->
      case conf_value do
        # Phoenix-style config where the main app has sub-apps with keyword list
        # configurations. In this case, the config from the file should be merged
        # with the config inside config/*.exs
        conf_map when is_map(conf_value) -> merge_conf(app_atom, conf_key, conf_map)

        # Regular application config without sub-apps
        _ -> Application.put_env(app_atom, conf_key, conf_value)
      end
    end)
  end

  defp merge_conf(app_atom, conf_key, conf_map) do
    orig_conf = Application.get_env(app_atom, conf_key) || []
    # convert the YAML map into keyword list recursively
    new_conf = conf_map
      |> Stream.map(fn {k,v} -> if is_map(v), do: {k, Enum.into(v, [])}, else: {k,v} end)
      |> Enum.to_list
    merged_conf = Keyword.merge(orig_conf, new_conf)
    Application.put_env(app_atom, conf_key, merged_conf)
  end

  defp env_config do
    config_path = Application.fetch_env!(:ipseeds, :config_path)
    case File.exists?(config_path) do
      true -> YamlElixir.read_from_file(Path.absname(config_path, System.cwd), atoms: true)
      _ -> %{}
    end
  end
end
