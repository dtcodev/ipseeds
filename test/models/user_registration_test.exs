defmodule Ipseeds.UserRegistrationTest do
  use Ipseeds.ModelCase

  alias Ipseeds.UserRegistration

  @valid_attrs %{user_id: 42, application_id: 42, application_type: "some content",
  approved_at: Ecto.DateTime.utc, approved_by_admin_id: 42,
  disapproved_at: Ecto.DateTime.utc, disapproved_by_admin_id: 42,
  disapproved_reason: "some content", terms_of_service: true}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = UserRegistration.changeset(%UserRegistration{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = UserRegistration.changeset(%UserRegistration{}, @invalid_attrs)
    refute changeset.valid?
  end
end
