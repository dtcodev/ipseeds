defmodule Ipseeds.UserKeypairTest do
  use Ipseeds.ModelCase

  alias Ipseeds.UserKeypair

  @valid_attrs %{user_id: 42, bip32_hd_public_key: "some content", ecc_public_key_hex: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = UserKeypair.changeset(%UserKeypair{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = UserKeypair.changeset(%UserKeypair{}, @invalid_attrs)
    refute changeset.valid?
  end
end
