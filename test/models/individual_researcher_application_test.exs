defmodule Ipseeds.IndividualResearcherApplicationTest do
  use Ipseeds.ModelCase

  alias Ipseeds.IndividualResearcherApplication

  @valid_attrs valid_application_params("individual")
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = IndividualResearcherApplication.changeset(%IndividualResearcherApplication{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = IndividualResearcherApplication.changeset(%IndividualResearcherApplication{}, @invalid_attrs)
    refute changeset.valid?
  end
end
