defmodule Ipseeds.IndustryApplicationTest do
  use Ipseeds.ModelCase

  alias Ipseeds.IndustryApplication

  @valid_attrs valid_application_params("industry")
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = create_changeset(@valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = create_changeset(@invalid_attrs)
    refute changeset.valid?
  end
  
  test "company_name_zh_tw should be required if nationality is TW" do
    params = %{Map.drop(@valid_attrs, [:company_name_zh_tw]) | company_nationality: "TW"}
    changeset = create_changeset(params)
    refute changeset.valid?
  end

  test "company_name_zh_tw should not be required if nationality is not TW" do
    params = %{Map.drop(@valid_attrs, [:company_name_zh_tw]) | company_nationality: "US"}
    changeset = create_changeset(params)
    assert changeset.valid?
  end

  test "company_tax_id should be required if nationality is TW" do
    params = %{Map.drop(@valid_attrs, [:company_tax_id]) | company_nationality: "TW"}
    changeset = create_changeset(params)
    refute changeset.valid?
  end

  test "company_tax_id should not be required if nationality is not TW" do
    params = %{Map.drop(@valid_attrs, [:company_tax_id]) | company_nationality: "US"}
    changeset = create_changeset(params)
    assert changeset.valid?
  end

  defp create_changeset(params) do
    IndustryApplication.changeset(%IndustryApplication{}, params)
  end
end
