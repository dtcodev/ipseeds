defmodule Ipseeds.ContractTest do
  use Ipseeds.ModelCase

  alias Ipseeds.Contract

  @valid_attrs %{api_transaction_id: "some content", issuer_user_id: 42, recipient_user_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Contract.changeset(%Contract{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Contract.changeset(%Contract{}, @invalid_attrs)
    refute changeset.valid?
  end
end
