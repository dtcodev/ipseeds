defmodule Ipseeds.IpNotaryTest do
  use Ipseeds.ModelCase

  alias Ipseeds.IpNotary

  @valid_create_attrs %{name_short: "abc", valid_time: DateTime.to_unix(DateTime.utc_now), name: "abc123",
    description: "description", hash_attachment: "attachmenthash"}
  @invalid_create_attrs %{name_short: ""}

  @valid_api_link_attrs %{user_id: 42, api_asset_id: "abc123"}

  test "create changeset with valid attributes" do
    changeset = IpNotary.create_changeset(%IpNotary{}, @valid_create_attrs)
    assert changeset.valid?
  end

  test "create changeset with invalid attributes" do
    changeset = IpNotary.create_changeset(%IpNotary{}, @invalid_create_attrs)
    refute changeset.valid?
  end

  test "automatically assigns asset type based on env and does not allow user entry" do
    user_type = "user_type"
    create_attrs = Map.merge(@valid_create_attrs, %{type: user_type})
    changeset = IpNotary.create_changeset(%IpNotary{}, create_attrs)
    assert changeset.valid?
    assert changeset.changes[:type] != user_type
    assert changeset.changes[:type] == "ip_notary_test"
  end

  test "api link changeset with valid attributes" do
    changeset = IpNotary.api_link_changeset(%IpNotary{}, @valid_api_link_attrs)
    assert changeset.valid?
  end

  test "api link changeset with invalid attributes" do
    invalid_attrs = Map.put(@valid_api_link_attrs, :api_asset_id, "")
    changeset = IpNotary.api_link_changeset(%IpNotary{}, invalid_attrs)
    refute changeset.valid?
  end
end
