defmodule Ipseeds.EmailTest do
  use ExUnit.Case
  use Bamboo.Test
  alias Ipseeds.Mailer
  alias Ipseeds.Email

  test "sending an user registration email" do
    review_link = "http://some_review_link_here"

    email = Email.new_user_registration_email("test@example.com", review_link)
            |> Mailer.deliver_now

    assert_delivered_email email
  end

  test "sending an admin registration email" do
    reset_url = "http://www.ipseed.com/admin/user_registrations/SecretTokenHereBlah1083"

    email = Email.new_admin_registration_email("test@example.com", reset_url)
            |> Mailer.deliver_now

    assert_delivered_email email
  end

  test "send_admin_password_reset_email" do
    reset_url = "http://www.ipseed.com/password_reset/SecretTokenHereBlah1083/"

    email = Email.admin_password_reset_email("test@example.com", reset_url)
            |> Mailer.deliver_now

    assert_delivered_email email
  end

  test "send_admin_invitation_email" do
    username = "new admin"
    email = "test@example.com"
    reset_url = "http://www.ipseed.com/password_reset/SecretTokenHereBlah1083?email=#{email}&type=invitation"

    email = Email.admin_invitation_email(email, reset_url, username)
            |> Mailer.deliver_now

    assert_delivered_email email
  end
  
end
