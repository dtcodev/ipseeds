defmodule Ipseeds.ContractAttachmentTest do
  use Ipseeds.ModelCase

  alias Ipseeds.ContractAttachment

  @valid_attrs %{contract_id: 42, mime_type: "some content", original_filename: "some content", s3_filename: %Plug.Upload{filename: "test_image.jpg", content_type: "image/jpeg", path: "test/support/test_image.jpg"}}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = ContractAttachment.changeset(%ContractAttachment{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ContractAttachment.changeset(%ContractAttachment{}, @invalid_attrs)
    refute changeset.valid?
  end
end
