defmodule Ipseeds.AgreeableContractContractTest do
  use Ipseeds.ModelCase

  alias Ipseeds.AgreeableContract



  @valid_issuing_nda_attrs %{"nda_contract_type" => "unilateral", 
    "recipient_user_id" => 1, 
    "sender_signature_url" => %Plug.Upload{filename: "test_image.jpg", content_type: "image/jpeg", 
      path: "test/support/test_image.jpg"}, 
    "sender_signer_name" => "someone's name", 
    "sender_signer_title" => "a title name", 
    "unilateral_input_pnddo" => "some content",
    "unilateral_input_ndd" => "some_content", 
    "unilateral_input_yr_01" => "5", 
    "unilateral_input_yr_02" => "5", 
    "unilateral_input_days" => "30", 
    "unilateral_nationality" => "TW"}

  @invalid_issuing_nda_attrs %{"nda_contract_type" => "unilateral"}

  @valid_issued_nda_attrs %{sender_user_id: 1, recipient_user_id: 2, draft_asset_id: "some texts here"}
  @invalid_issued_nda_attrs %{}

  test "Valid data of issuing NDA: changeset with valid attributes" do
    recipient_user = create_user!()
    changeset = AgreeableContract.issue_changeset(%AgreeableContract{}, create_user!(), 
      Map.merge(@valid_issuing_nda_attrs, %{"recipient_user_id" => recipient_user.id}))
    assert changeset.valid?
  end

  test "Valid data of issuing NDA: changeset with invalid attributes" do
    changeset = AgreeableContract.issue_changeset(%AgreeableContract{}, create_user!(), @invalid_issuing_nda_attrs)
    refute changeset.valid?
  end


  test "Save data of issued NDA: changeset with valid attributes" do
    changeset = AgreeableContract.changeset(%AgreeableContract{}, @valid_issued_nda_attrs)
    assert changeset.valid?
  end

end