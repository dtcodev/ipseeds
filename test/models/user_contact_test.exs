defmodule Ipseeds.UserContactTest do
  use Ipseeds.ModelCase

  alias Ipseeds.{Repo, UserContact}

  @valid_attrs %{user_id: 42, contact_user_id: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = UserContact.changeset(%UserContact{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = UserContact.changeset(%UserContact{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "does not allow duplicate contacts" do
    current_user = create_user!()
    user = create_user!()

    changeset = UserContact.changeset(%UserContact{},
      %{user_id: current_user.id, contact_user_id: user.id})
    Repo.insert! changeset

    assert_raise Ecto.InvalidChangesetError, fn -> Repo.insert!(changeset) end
  end

  test "is contact?" do
    current_user = create_user!()
    user = create_user!()

    refute UserContact.is_contact? current_user, user
    refute UserContact.is_contact? user, current_user

    # adds "user" as a contact of "current user"
    changeset = UserContact.changeset(%UserContact{},
      %{user_id: current_user.id, contact_user_id: user.id})
    Repo.insert! changeset

    assert UserContact.is_contact? current_user, user
    refute UserContact.is_contact? user, current_user
  end
end
