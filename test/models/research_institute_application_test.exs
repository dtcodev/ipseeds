defmodule Ipseeds.ResearchInstituteApplicationTest do
  use Ipseeds.ModelCase

  alias Ipseeds.ResearchInstituteApplication

  @valid_attrs valid_application_params("institute")
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = create_changeset(@valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = create_changeset(@invalid_attrs)
    refute changeset.valid?
  end

  test "institute_name_zh_tw should be required if nationality is TW" do
    params = %{Map.drop(@valid_attrs, [:institute_name_zh_tw]) | institute_nationality: "TW"}
    changeset = create_changeset(params)
    refute changeset.valid?
  end

  test "institute_name_zh_tw should not be required if nationality is not TW" do
    params = %{Map.drop(@valid_attrs, [:institute_name_zh_tw]) | institute_nationality: "US"}
    changeset = create_changeset(params)
    assert changeset.valid?
  end

  test "institute_tax_id should be required if nationality is TW" do
    params = %{Map.drop(@valid_attrs, [:institute_tax_id]) | institute_nationality: "TW"}
    changeset = create_changeset(params)
    refute changeset.valid?
  end

  test "institute_tax_id should not be required if nationality is not TW" do
    params = %{Map.drop(@valid_attrs, [:institute_tax_id]) | institute_nationality: "US"}
    changeset = create_changeset(params)
    assert changeset.valid?
  end

  defp create_changeset(params) do
    ResearchInstituteApplication.changeset(%ResearchInstituteApplication{}, params)
  end
end
