defmodule Ipseeds.ContactUsInfoTest do
  use Ipseeds.ModelCase

  alias Ipseeds.ContactUsInfo

  @valid_attrs %{first_name: "Im", last_name: "Testing", email: "testing@example.com", 
  message: "Some message from user. \nAnother line."}

  test "changeset with valid attributes" do
    changeset = ContactUsInfo.changeset(%ContactUsInfo{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with empty first_name" do
    invalid_fields = Map.put(@valid_attrs, :first_name, "")
    changeset = ContactUsInfo.changeset(%ContactUsInfo{}, invalid_fields)
    refute changeset.valid?
  end

  test "changeset with empty last_name" do
    invalid_fields = Map.put(@valid_attrs, :last_name, "")
    changeset = ContactUsInfo.changeset(%ContactUsInfo{}, invalid_fields)
    refute changeset.valid?
  end

  test "changeset with empty email" do
    invalid_fields = Map.put(@valid_attrs, :email, "")
    changeset = ContactUsInfo.changeset(%ContactUsInfo{}, invalid_fields)
    refute changeset.valid?
  end

  test "changeset with empty message" do
    invalid_fields = Map.put(@valid_attrs, :message, "")
    changeset = ContactUsInfo.changeset(%ContactUsInfo{}, invalid_fields)
    refute changeset.valid?
  end

  test "changeset with invalid email format" do
    invalid_fields = Map.put(@valid_attrs, :email, "testingexample.com")
    changeset = ContactUsInfo.changeset(%ContactUsInfo{}, invalid_fields)
    refute changeset.valid?
  end
end
