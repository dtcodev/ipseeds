defmodule Ipseeds.AdminUserTest do
  use Ipseeds.ModelCase, async: false

  alias Ipseeds.{AdminUser, Repo}

  @valid_attrs %{deleted_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010}, deleted_by_admin_id: 42,
    disabled_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010}, disabled_by_admin_id: 42,
    email: "some content", invitation_accepted_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010},
    invitation_created_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010},
    invitation_created_by_admin_id: 42, invitation_sent_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0,
    year: 2010}, invitation_token_hash: "some content", is_superadmin: true, password_hash: "some content",
    reset_password_created_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010},
    reset_password_sent_at: %{day: 17, hour: 14, min: 0, month: 4, sec: 0, year: 2010},
    reset_password_token_hash: "some content", username: "some content"}

  test "changeset with valid attributes" do
    changeset = AdminUser.changeset(%AdminUser{}, @valid_attrs)
    assert changeset.valid?
  end

  test "not_deleted_query does not return deleted users" do
    deleted_admin_user = create_admin_user!(deleted_at: DateTime.utc_now)
    admin_users = Repo.all(AdminUser.not_deleted_query)
    refute Enum.find(admin_users, fn(admin_user) -> admin_user.id == deleted_admin_user.id end)
  end

  test "not_disabled_query does not return disabled users" do
    disabled_admin_user = create_admin_user!(disabled_at: DateTime.utc_now)
    admin_users = Repo.all(AdminUser.not_disabled_query)
    refute Enum.find(admin_users, fn(admin_user) -> admin_user.id == disabled_admin_user.id end)
  end

  test "is_superadmin? returns true for superadmin and false for all other values" do
    assert AdminUser.is_superadmin?(create_admin_user!(is_superadmin: true))
    refute AdminUser.is_superadmin?(create_admin_user!(is_superadmin: false))
    refute AdminUser.is_superadmin?(nil)
  end

  test "reset_password_changeset is valid" do
    admin_user = create_admin_user!()
    token = AdminUser.gen_token()

    changeset = AdminUser.password_reset_changeset(admin_user, %{"token" => token})
    assert changeset.valid?
  end
end
