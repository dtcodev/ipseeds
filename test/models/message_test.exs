defmodule Ipseeds.App.MessageTest do
  use Ipseeds.ModelCase, async: false

  alias Ipseeds.Message

  @valid_attrs %{sender_user_id: 42, sender_keypair_id: 42,
    recipient_user_id: 43, recipient_keypair_id: 43,
    recipient_encrypted_content: "some content",
    sender_encrypted_content: "some content",
    subject: "some subject"}
  @invalid_attrs %{@valid_attrs | sender_encrypted_content: ""}

  @valid_system_message_attrs %{recipient_user_id: 43, recipient_encrypted_content: "some content",
    recipient_encrypted_content: "some content", subject: "some subject"}
  @invalid_system_message_attrs %{@valid_system_message_attrs | recipient_encrypted_content: ""}

  test "changeset with valid attributes" do
    changeset = Message.changeset(%Message{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Message.changeset(%Message{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "message requires subject" do
    changeset = Message.changeset(%Message{}, %{@valid_attrs | subject: ""})
    refute changeset.valid?
  end

  test "cannot send message to self" do
    sender_user_id = :rand.uniform(100)
    attrs = %{@valid_attrs | sender_user_id: sender_user_id, recipient_user_id: sender_user_id}
    changeset = Message.changeset(%Message{}, attrs)

    refute changeset.valid?
  end

  test "system message changeset with valid attributes" do
    changeset = Message.system_message_changeset(%Message{}, @valid_system_message_attrs)
    assert changeset.valid?
    assert get_change(changeset, :is_system_message) == true
  end

  test "system message changeset with invalid attributes" do
    changeset = Message.system_message_changeset(%Message{}, @invalid_system_message_attrs)
    refute changeset.valid?
    assert get_change(changeset, :is_system_message) == true
  end
end
