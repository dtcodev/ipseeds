defmodule Ipseeds.IpSummaryTest do
  use Ipseeds.ModelCase

  alias Ipseeds.IpSummary

  @valid_create_attrs %{"icon_url" => image_upload("icon.jpg"),
                        "cover_url" => image_upload("cover.jpg"),
                        "name_short" => "abc", "name" => "abc",
                        "valid_time" => DateTime.to_unix(DateTime.utc_now),
                        "category" => "#category", "tag" => "{\"items\":[{\"asset_id\":\"xyz\"}]}"}
  @invalid_create_attrs %{}

  @valid_api_link_attrs %{user_id: 42, api_asset_id: "abc123"}

  test "create changeset with valid attributes" do
    changeset = IpSummary.create_changeset(%IpSummary{}, @valid_create_attrs)
    assert changeset.valid?
  end

  test "create changeset with invalid attributes" do
    changeset = IpSummary.create_changeset(%IpSummary{}, @invalid_create_attrs)
    refute changeset.valid?
  end

  test "automatically assigns asset type based on env and does not allow user entry" do
    user_type = "user_type"
    create_attrs = Map.merge(@valid_create_attrs, %{"type" => user_type})
    changeset = IpSummary.create_changeset(%IpSummary{}, create_attrs)
    assert changeset.valid?
    assert changeset.changes[:type] != user_type
    assert changeset.changes[:type] == "ip_summary_test"
  end

  test "api link changeset with valid attributes" do
    changeset = IpSummary.api_link_changeset(%IpSummary{}, @valid_api_link_attrs)
    assert changeset.valid?
  end

  test "api link changeset with invalid attributes" do
    invalid_attrs = Map.put(@valid_api_link_attrs, :api_asset_id, "")
    changeset = IpSummary.api_link_changeset(%IpSummary{}, invalid_attrs)
    refute changeset.valid?
  end
end
