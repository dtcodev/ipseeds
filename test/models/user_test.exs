defmodule Ipseeds.UserTest do
  use Ipseeds.ModelCase

  alias Ipseeds.User

  @valid_attrs %{active_keypair_id: 42, biography: "some content", email: "some content",
   is_paid: true, message_notify_by_email: true, profile_show_email: true,
   receiver_asset_addr: "some content", user_registration_id: 42, username: "some content"}

  @valid_registration_attrs %{username: "exampleuser", email: "user@example.com",
    email_confirmation: "user@example.com", password: "Password_123",
    password_confirmation: "Password_123"}
  @invalid_registration_attrs %{email: "", username: "", password: "",
    password_confirmation: ""}

  @valid_forgot_password_attrs %{email: "user@example.com"}

  @valid_reset_password_attrs %{reset_password_token: "abc123", password: "Password_123",
    password_confirmation: "Password_123"}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "registration changeset with valid attributes" do
    changeset = User.registration_changeset(%User{}, @valid_registration_attrs)
    assert changeset.valid?
  end

  test "registration changeset with invalid attributes" do
    changeset = User.registration_changeset(%User{}, @invalid_registration_attrs)
    refute changeset.valid?
  end

  test "registration changeset is invalid if password and password confirmation do not match" do
    invalid_fields = Map.put(@valid_registration_attrs, :password_confirmation, "")
    changeset = User.registration_changeset(%User{}, invalid_fields)
    refute changeset.valid?
  end

  test "registration changeset is invalid if email and email confirmation do not match" do
    invalid_fields = Map.put(@valid_registration_attrs, :email_confirmation, "")
    changeset = User.registration_changeset(%User{}, invalid_fields)
    refute changeset.valid?
  end

  test "forgot password changeset with valid attributes" do
    changeset = User.forgot_password_changeset(%User{}, @valid_forgot_password_attrs)
    assert changeset.valid?
  end

  test "forgot password changeset without email address" do
    changeset = User.forgot_password_changeset(%User{},
      Map.drop(@valid_forgot_password_attrs, [:email]))
    refute changeset.valid?
  end

  test "forgot password changeset with invalid email address" do
    changeset = User.forgot_password_changeset(%User{},
      Map.put(@valid_forgot_password_attrs, :email, "invalid@email"))
    refute changeset.valid?
  end

  test "reset password changeset with valid attributes" do
    changeset = User.reset_password_changeset(%User{}, @valid_reset_password_attrs)
    assert changeset.valid?
  end

  test "reset password changeset without token" do
    changeset = User.reset_password_changeset(%User{},
      Map.drop(@valid_reset_password_attrs, [:reset_password_token]))
    refute changeset.valid?
  end

  test "reset password changeset where passwords don't match" do
    changeset = User.reset_password_changeset(%User{},
      Map.put(@valid_reset_password_attrs, :password_confirmation, "asdasda"))
    refute changeset.valid?
  end
end
