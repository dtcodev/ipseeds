defmodule Ipseeds.ScrivenerHelpersTest do
  use ExUnit.Case, async: true

  alias Ipseeds.ScrivenerHelpers

  test "gets query paramaters as a keyword list from conn when they exist" do
    conn = %{query_params: %{"param1" => "value1", "param2" => "value2"}}
    assert ScrivenerHelpers.query_params(conn) == [param1: "value1", param2: "value2"]
  end

  test "excludes the page key if it exists" do
    conn = %{query_params: %{"param1" => "value1", "page" => "3"}}
    assert ScrivenerHelpers.query_params(conn) == [param1: "value1"]
  end

  test "returns an empty list in all other cases" do
    conn = %{}
    assert ScrivenerHelpers.query_params(conn) == []
  end
end