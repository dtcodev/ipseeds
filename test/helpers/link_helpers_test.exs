defmodule Ipseeds.LinkHelpersTest do
  use Ipseeds.ConnCase

  import Phoenix.HTML, only: [safe_to_string: 1]

  alias Ipseeds.{LinkHelpers, PageController, UserController, SessionController}

  @default_class "active"
  @test_class "on"

  setup %{conn: conn} do
    active_path = page_path(conn, :index, "en")
    conn = get conn, active_path

    {:ok, %{conn: conn, active_path: active_path}}
  end

  test "active_class_path returns active for active path", %{conn: conn, active_path: active_path} do
    assert LinkHelpers.active_class_path(conn, active_path) == @default_class
    assert LinkHelpers.active_class_path(conn, active_path, @test_class) == @test_class
  end

  test "active_class_path returns empty string for non-active path", %{conn: conn} do
    assert LinkHelpers.active_class_path(conn, "/abc") == ""
    assert LinkHelpers.active_class_path(conn, "/abc", @test_class) == ""
  end

  test "active_class_path returns active if active_path is inside array of paths",
    %{conn: conn, active_path: active_path} do

    assert LinkHelpers.active_class_path(conn, [active_path, "/abc"]) == @default_class
    assert LinkHelpers.active_class_path(conn, [active_path, "/abc"], @test_class) == @test_class
  end

  test "active_class_path returns empty string if active_path is not inside array of paths", %{conn: conn} do
    assert LinkHelpers.active_class_path(conn, ["/abc", "/123"]) == ""
    assert LinkHelpers.active_class_path(conn, ["/abc", "/123"], @test_class) == ""
  end

  test "active_class_controller returns active for active controller", %{conn: conn} do
    assert LinkHelpers.active_class_controller(conn, PageController) == @default_class
    assert LinkHelpers.active_class_controller(conn, PageController, @test_class) == @test_class
  end

  test "active_class_controller returns empty string for non-active controller", %{conn: conn} do
    assert LinkHelpers.active_class_controller(conn, SessionController) == ""
    assert LinkHelpers.active_class_controller(conn, SessionController, @test_class) == ""
  end

  test "active_class_controller returns active if active_path is inside array of controllers", %{conn: conn} do
    assert LinkHelpers.active_class_controller(conn, [SessionController, PageController]) == @default_class
    assert LinkHelpers.active_class_controller(conn, [SessionController, PageController],
      @test_class) == @test_class
  end

  test "active_class_controller returns empty string if active_path is not inside array of controllers",
    %{conn: conn} do

    assert LinkHelpers.active_class_controller(conn, [SessionController, UserController]) == ""
    assert LinkHelpers.active_class_controller(conn, [SessionController, UserController], @test_class) == ""
  end

  test "link_unless_current returns correct link with empty class for non-active path", %{conn: conn} do
    assert safe_to_string(LinkHelpers.link_unless_current(conn, "Hello", to: "/abc")) == 
      ~s[<a class="" href="/abc">Hello</a>]
  end

  test "link_unless_current returns empty link with active class for active path", %{conn: conn, active_path: active_path} do
    assert safe_to_string(LinkHelpers.link_unless_current(conn, "Hello", to: active_path)) == 
      ~s[<a class="#{@default_class}" href="#">Hello</a>]

    assert safe_to_string(LinkHelpers.link_unless_current(conn, "Hello", to: active_path, active_class: @test_class)) == 
      ~s[<a class="#{@test_class}" href="#">Hello</a>]
  end
end
