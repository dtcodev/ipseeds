defmodule Ipseeds.NameHelpersTest do
  use ExUnit.Case, async: true

  alias Ipseeds.NameHelpers

  test "formats chinese name correctly" do
    assert NameHelpers.full_name("木森", "林") == "林木森"
  end

  test "formats non-chinese name correctly" do
    assert NameHelpers.full_name("John Middlename", "Doe") == "John Middlename Doe"
  end
end