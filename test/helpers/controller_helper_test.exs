defmodule Ipseeds.ControllerHelpersTest do
  use Ipseeds.ConnCase

  alias Ipseeds.ControllerHelpers

  setup %{conn: conn} do
    conn = conn
      |> bypass_through(Ipseeds.Router, [:browser])
      |> get("/", locale: "en")

    {:ok, %{conn: conn}}
  end

  test "puts alert message and type into conn", %{conn: conn} do
    alert_message = "Hello, world!"
    alert_type = "success"
    conn = ControllerHelpers.put_flash_alert(conn, alert_message, alert_type)
    assert get_flash(conn, :alert_message) == alert_message
    assert get_flash(conn, :alert_type) == alert_type
  end

  test "sets alert type to info by default", %{conn: conn} do
    alert_message = "Hello, world!"
    conn = ControllerHelpers.put_flash_alert(conn, alert_message)
    assert get_flash(conn, :alert_message) == alert_message
    assert get_flash(conn, :alert_type) == "info"
  end
end