defmodule Ipseeds.EnumHelpersTest do
  use ExUnit.Case, async: true

  alias Ipseeds.EnumHelpers

  test "atomize atomizes binary keys" do
    assert EnumHelpers.atomize_keys(%{"key1" => "value1", "key2" => "value2"})
      == %{key1: "value1", key2: "value2"}
  end

  test "atomize leaves atom keys as they are" do
    assert EnumHelpers.atomize_keys(%{key1: "value1", key2: "value2"})
      == %{key1: "value1", key2: "value2"}
  end

  test "atomize converts non-atom, non-binary keys to atomized version" do
    assert EnumHelpers.atomize_keys(%{1 => "value1", {:tuple, :key} => "value2"})
      == %{"1": "value1", "{:tuple, :key}": "value2"}
  end

  test "mapify_map_list converts list of maps into a map" do
    list = [%{key1: "abc", key2: "123"}, %{key1: "def", key2: "456"}]
    assert EnumHelpers.mapify_map_list(list, :key1) ==
      %{"abc" => Enum.at(list, 0), "def" => Enum.at(list, 1)}
  end

  test "merge map fields" do
    map1 = %{key1: "abc", key2: "123"}
    map2 = %{key3: "def", key4: "456", key5: "1b3"}
    assert EnumHelpers.merge_map_fields(map1, map2, :key4) == %{key1: "abc", key2: "123", key4: "456"}
    assert EnumHelpers.merge_map_fields(map1, map2, [:key3, :key4]) ==
      %{key1: "abc", key2: "123", key3: "def", key4: "456"}
  end
end