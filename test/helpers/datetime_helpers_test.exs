defmodule Ipseeds.DateTimeHelpersTest do
  use ExUnit.Case, async: true

  @test_timestamp 1473946424
  @taipei_datetime_string ~s[2016-09-15 21:33:44]

  alias Ipseeds.DateTimeHelpers

  setup do
    datetime = DateTime.from_unix!(@test_timestamp)

    ecto_datetime = 
      datetime 
      |> DateTime.to_naive
      |> NaiveDateTime.to_erl
      |> Ecto.DateTime.from_erl

    {:ok, %{timestamp: @test_timestamp, ecto_datetime: ecto_datetime, datetime: datetime}}
  end

  test "api_timestamp_to_datetimestring returns string representation of an Ecto.DateTime struct",
    %{ecto_datetime: ecto_datetime} do
    assert DateTimeHelpers.api_timestamp_to_datetimestring(ecto_datetime) == @taipei_datetime_string
  end

  test "api_timestamp_to_datetimestring returns string representation of a timestamp",
    %{timestamp: timestamp} do
    assert DateTimeHelpers.api_timestamp_to_datetimestring(timestamp) == @taipei_datetime_string
  end

  test "api_timestamp_to_datetimestring returns string representation of a DateTime struct",
    %{datetime: datetime} do
    assert DateTimeHelpers.api_timestamp_to_datetimestring(datetime) == @taipei_datetime_string
  end

end
