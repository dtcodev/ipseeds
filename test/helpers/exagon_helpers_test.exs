defmodule Ipseeds.ExagonHelpersTest do
  use Ipseeds.ConnCase

  alias Ipseeds.{ExagonHelpers}

  setup %{conn: conn} do
    {:ok, %{conn: conn}}
  end

  test "put puts values in keys", %{conn: conn} do
    conn = conn
      |> ExagonHelpers.put(:somekey, "some value")
      |> ExagonHelpers.put(:somekey2, "some value 2")

    assert %{somekey: "some value", somekey2: "some value 2"} = ExagonHelpers.get_exagon(conn)
  end

  test "get gets value in key", %{conn: conn} do
    conn = ExagonHelpers.put(conn, :somekey, "some value")

    assert ExagonHelpers.get(conn, :somekey) == "some value"
  end

  test "put/2 with a map acts like a map merge", %{conn: conn} do
    conn = conn
      |> ExagonHelpers.put(:somekey, "some value")
      |> ExagonHelpers.put(%{somekey: "some value 2", otherkey: "other value"})

    assert ExagonHelpers.get(conn, :somekey) == "some value 2"
    assert ExagonHelpers.get(conn, :otherkey) == "other value"
  end

  test "put/2 with kw list acts like a map merge", %{conn: conn} do
    conn = conn
      |> ExagonHelpers.put(:somekey, "some value")
      |> ExagonHelpers.put(somekey: "some value 2", otherkey: "other value")
      |> ExagonHelpers.put(otherotherkey: "other other value")

    assert ExagonHelpers.get(conn, :somekey) == "some value 2"
    assert ExagonHelpers.get(conn, :otherkey) == "other value"
    assert ExagonHelpers.get(conn, :otherotherkey) == "other other value"
  end
end