defmodule Ipseeds.PageControllerTest do
  use Ipseeds.ConnCase

  test "GET / (no locale)", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 302)
  end

  test "GET /en", %{conn: conn} do
    conn = get conn, "/en"
    assert conn.assigns[:locale] == "en"
    assert html_response(conn, 200)
  end

  test "GET /en/terms", %{conn: conn} do
    conn = get conn, "/en/terms"
    assert html_response(conn, 200)
  end

  test "GET /en/faq", %{conn: conn} do
    conn = get conn, "/en/faq"
    assert html_response(conn, 200)
  end

  test "GET /en/about", %{conn: conn} do
    conn = get conn, "/en/about"
    assert html_response(conn, 200)
  end

  test "GET /en/how_it_works", %{conn: conn} do
    conn = get conn, "/en/how_it_works"
    assert html_response(conn, 200)
  end

  test "GET /en/pricing", %{conn: conn} do
    conn = get conn, "/en/pricing"
    assert html_response(conn, 200)
  end
end
