defmodule Ipseeds.Admin.PageControllerTest do
  use Ipseeds.ConnCase

  setup context do
    controller_test_setup_admin(context, "en", username: "Admin Tester", is_superadmin: false)
  end

  @tag logged_in?: true
  test "shows admin dashboard", %{conn: conn} do
    conn = get conn, admin_page_path(conn, :dashboard, conn.assigns.locale)
    assert html_response(conn, 200)
  end
end