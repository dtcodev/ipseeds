defmodule Ipseeds.Admin.UserRegistrationControllerTest do
  use Ipseeds.ConnCase, async: false

  alias Ipseeds.{UserRegistration}

  @application_types ~w(individual industry institute)

  setup context do
    controller_test_setup_admin(context, "en", username: "Admin Tester", is_superadmin: false)
  end

  @tag logged_in?: true
  test "shows index", %{conn: conn} do
    conn = get conn, admin_user_registration_path(conn, :index, conn.assigns.locale)

    assert html_response(conn, 200)
  end

  @tag logged_in?: true
  test "shows show for existent user registrations", %{conn: conn} do
    for application_type <- @application_types do
      user = create_user!(false, application_type: application_type) |> Repo.preload(:user_registration)
      conn = get conn, admin_user_registration_path(conn, :show, conn.assigns.locale,
        user.user_registration.id)

      assert html_response(conn, 200)
    end
  end

  @tag logged_in?: true
  test "shows edit for existent user registrations", %{conn: conn} do
    for application_type <- @application_types do
      user = create_user!(false, application_type: application_type) |> Repo.preload(:user_registration)
      conn = get conn, admin_user_registration_path(conn, :edit, conn.assigns.locale,
        user.user_registration.id)

      assert html_response(conn, 200)
    end
  end

  @tag logged_in?: true
  test "approves user registration if not yet approved or disapproved", %{conn: conn} do
    for application_type <- @application_types do
      user = create_user!(false, application_type: application_type) |> Repo.preload(:user_registration)
      approve(conn, user)
      user_registration_updated = Repo.get!(UserRegistration, user.user_registration.id)
      assert UserRegistration.is_approved?(user_registration_updated)
    end
  end

  @tag logged_in?: true
  test "disapproves user registration if not yet approved or disapproved", %{conn: conn} do
    for application_type <- @application_types do
      user = create_user!(false, application_type: application_type) |> Repo.preload(:user_registration)
      disapprove(conn, user)
      user_registration_updated = Repo.get!(UserRegistration, user.user_registration.id)
      assert UserRegistration.is_disapproved?(user_registration_updated)
    end
  end

  @tag logged_in?: true
  @tag pending: true
  test "not allowed to disapprove a registration that already been approved", %{conn: conn} do
    for application_type <- @application_types do
      user = create_user!(true, application_type: application_type) |> Repo.preload(:user_registration)

      user_registration_updated = Repo.get!(UserRegistration, user.user_registration.id)
      assert UserRegistration.is_approved?(user_registration_updated)

      disapprove(conn, user)
      user_registration_updated = Repo.get!(UserRegistration, user.user_registration.id)
      refute UserRegistration.is_disapproved?(user_registration_updated)
    end
  end

  @tag logged_in?: true
  @tag pending: true
  test "not allowed to approve a registration that has already been disapproved", %{conn: conn} do
    for application_type <- @application_types do
      user = create_user!(false, application_type: application_type) |> Repo.preload(:user_registration)

      disapprove(conn, user)
      user_registration_updated = Repo.get!(UserRegistration, user.user_registration.id)
      assert UserRegistration.is_disapproved?(user_registration_updated)

      approve(conn, user)
      user_registration_updated = Repo.get!(UserRegistration, user.user_registration.id)
      refute UserRegistration.is_approved?(user_registration_updated)
    end
  end

  defp disapprove(conn, user) do
    post conn, admin_user_registration_path(conn, :disapprove, conn.assigns.locale,
      user.user_registration.id, disapproved_reason: "Not good")
  end

  defp approve(conn, user) do
    post conn, admin_user_registration_path(conn, :approve, conn.assigns.locale,
      user.user_registration.id)
  end
end