defmodule Ipseeds.Admin.PasswordResetControllerTest do
  alias Ipseeds.AdminUser
  use Ipseeds.ConnCase

  alias Ipseeds.AdminUser

  setup context do
    controller_test_setup_admin(context, "en")
  end

  test "shows forgot password form", %{conn: conn} do
    conn = get conn, public_admin_password_reset_path(conn, :new, conn.assigns.locale)

    assert html_response(conn, 200) =~ "Forgot password"
  end

  test "forgot password request for valid admin user", %{conn: conn} do
    _admin_user = create_admin_user!(email: "reset_password@admin.com")
    valid_attrs = %{"email" => "reset_password@admin.com"}

    conn = post conn, public_admin_password_reset_path(conn, :create, conn.assigns.locale),
      admin_user: valid_attrs
    assert redirected_to(conn) == public_admin_session_path(conn, :new, conn.assigns.locale)
  end

  test "forgot password request for non existing admin user", %{conn: conn} do
    invalid_attrs = %{"email" => "not_exist@admin.com"}

    conn = post conn, public_admin_password_reset_path(conn, :create, conn.assigns.locale), admin_user: invalid_attrs
    assert html_response(conn, 200) =~ "Forgot password"
  end

  test "render password reset form", %{conn: conn} do
    token = AdminUser.gen_token
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    now = DateTime.utc_now

    admin_user = create_admin_user!([reset_password_token_hash: hash,
                                     reset_password_created_at: now,
                                     reset_password_sent_at: now])

    conn = get conn, public_admin_password_reset_path(conn, :edit, conn.assigns.locale, token), email: admin_user.email
    assert html_response(conn, 200) =~ "Reset password"
  end

  test "expired password reset", %{conn: conn} do
    token = AdminUser.gen_token
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    three_hours_ago = Timex.now |> Timex.shift(hours: -3)

    admin_user = create_admin_user!([reset_password_token_hash: hash,
                                 reset_password_created_at: three_hours_ago,
                                 reset_password_sent_at: three_hours_ago])

    conn = get conn, public_admin_password_reset_path(conn, :edit, :en, token), email: admin_user.email
    assert html_response(conn, 200) =~ "Forgot password"
  end

  test "password reset with invalid token", %{conn: conn} do
    token = AdminUser.gen_token
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    now = Timex.now

    admin_user = create_admin_user!([reset_password_token_hash: hash,
                                     reset_password_created_at: now,
                                     reset_password_sent_at: now])

    conn = get conn, public_admin_password_reset_path(conn, :edit, :en, "THIS IS A INVALID TOKEN"), email: admin_user.email
    assert html_response(conn, 200) =~ "Forgot password"
  end

  test "password reset commit", %{conn: conn} do
    token = AdminUser.gen_token
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    now = Timex.now

    admin_user = create_admin_user!([reset_password_token_hash: hash,
                                     reset_password_created_at: now,
                                     reset_password_sent_at: now])
    params = %{
      "email" => admin_user.email,
      "password" => "good!password!",
      "password_confirmation" => "good!password!",
    }

    conn = put conn, public_admin_password_reset_path(conn, :update, :en, token), %{"admin_user" => params}
    assert html_response(conn, 302)
  end

  test "password reset token can only access once", %{conn: conn} do
    token = AdminUser.gen_token
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    a_day_ago = Timex.now |> Timex.shift(days: -1)

    admin_user = create_admin_user!([reset_password_token_hash: hash,
                                     reset_password_created_at: a_day_ago,
                                     reset_password_sent_at: a_day_ago,
                                     password_reseted_at: Timex.now])
    params = %{
      "email" => admin_user.email,
      "password" => "password!",
      "password_confirmation" => "password!",
    }

    conn = put conn, public_admin_password_reset_path(conn, :update, :en, token),
             %{"admin_user" => params}

    %{private: %{phoenix_flash: %{"alert_message" => msg}}} = conn
    assert msg =~ "Password reset token is expired or invalid"
    assert html_response(conn, 200) =~ "Forgot password"
  end

  test "password reset only access once (full path)", %{conn: conn} do
    token = AdminUser.gen_token
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    now = Timex.now

    admin_user = create_admin_user!([reset_password_token_hash: hash,
                                     reset_password_created_at: now,
                                     reset_password_sent_at: now])
    params = %{
      "email" => admin_user.email,
      "password" => "password!",
      "password_confirmation" => "password!",
    }

    conn1 = put conn, public_admin_password_reset_path(conn, :update, :en, token), %{"admin_user" => params}
    %{private: %{phoenix_flash: %{"alert_message" => msg1}}} = conn1
    assert html_response(conn1, 302)
    assert msg1 =~ "Password successfully reset"

    conn2 = put build_conn(), public_admin_password_reset_path(conn, :update, :en, token), %{"admin_user" => params}

    %{private: %{phoenix_flash: %{"alert_message" => msg}}} = conn2
    assert msg =~ "Password reset token is expired or invalid"
    assert html_response(conn2, 200) =~ "Forgot password"
  end

  test "expired invitation token", %{conn: conn} do
    token = AdminUser.gen_token
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    three_days_ago = Timex.now |> Timex.shift(days: -3)

    admin_user = create_admin_user!([invitation_token_hash: hash,
                                     invitation_created_at: three_days_ago,
                                     invitation_sent_at: three_days_ago])

    conn = get conn, public_admin_password_reset_path(conn, :edit, conn.assigns.locale, token),
             email: admin_user.email, type: "invitation"
    assert html_response(conn, 302)
  end

  test "render invitation password setup form", %{conn: conn} do
    token = AdminUser.gen_token
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    now = Timex.now

    admin_user = create_admin_user!([invitation_token_hash: hash,
                                     invitation_created_at: now,
                                     invitation_sent_at: now])

    conn = get conn, public_admin_password_reset_path(conn, :edit, conn.assigns.locale, token),
             email: admin_user.email, type: "invitation"
    assert html_response(conn, 200) =~ "Setup password"
  end

  test "invitation password setup commit", %{conn: conn} do
    token = AdminUser.gen_token
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    a_day_ago = Timex.now |> Timex.shift(days: -1)

    admin_user = create_admin_user!([invitation_token_hash: hash,
                                 invitation_created_at: a_day_ago,
                                 invitation_sent_at: a_day_ago])
    params = %{
      "email" => admin_user.email,
      "password" => "good!password!",
      "password_confirmation" => "good!password!",
    }

    conn = put conn, public_admin_password_reset_path(conn, :update, :en, token, type: "invitation"),
             %{"admin_user" => params}

    assert redirected_to(conn) == public_admin_session_path(conn, :new, conn.assigns.locale)
  end

  test "invitation token can only access once", %{conn: conn} do
    token = AdminUser.gen_token
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    a_day_ago = Timex.now |> Timex.shift(days: -1)

    admin_user = create_admin_user!([invitation_token_hash: hash,
                                     invitation_created_at: a_day_ago,
                                     invitation_sent_at: a_day_ago,
                                     invitation_accepted_at: Timex.now])
    params = %{
      "email" => admin_user.email,
      "password" => "password!",
      "password_confirmation" => "password!",
    }

    conn = put conn, public_admin_password_reset_path(conn, :update, :en, token, type: "invitation"),
             %{"admin_user" => params}

    %{private: %{phoenix_flash: %{"alert_message" => msg}}} = conn
    assert msg =~ "Invitation token is expired or invalid"
    assert redirected_to(conn) == public_admin_session_path(conn, :new, conn.assigns.locale)
  end

  test "invitation token only access once (full path)", %{conn: conn} do
    token = AdminUser.gen_token
    hash = Comeonin.Bcrypt.hashpwsalt(token)
    now = Timex.now

    admin_user = create_admin_user!([invitation_token_hash: hash,
                                     invitation_created_at: now,
                                     invitation_sent_at: now,])

    params = %{
      "email" => admin_user.email,
      "password" => "password!",
      "password_confirmation" => "password!",
    }

    conn1 = put conn,
                public_admin_password_reset_path(conn, :update, :en, token, type: "invitation"),
                %{"admin_user" => params}

    %{private: %{phoenix_flash: %{"alert_message" => msg1}}} = conn1
    assert html_response(conn1, 302)
    assert msg1 =~ "Password successfully reset"


    conn2 = put build_conn(),
                public_admin_password_reset_path(conn, :update, :en, token, type: "invitation"),
               %{"admin_user" => params}

    %{private: %{phoenix_flash: %{"alert_message" => msg2}}} = conn2
    assert html_response(conn2, 302)
    assert msg2 =~ "Invitation token is expired or invalid"
  end
end
