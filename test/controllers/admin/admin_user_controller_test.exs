defmodule Ipseeds.Admin.AdminUserControllerTest do
  use Ipseeds.ConnCase, async: false

  alias Ipseeds.AdminUser
  @valid_attrs %{
    username: "Yet Another Admin",
    email: "yaadmin#{:rand.uniform(10)}@admin.com",
  }

  setup context do
    controller_test_setup_admin(context, "en", username: "Admin Tester", is_superadmin: true)
  end

  @tag logged_in?: false
  test "redirect if not login", %{conn: conn} do
    conn = get conn, admin_admin_user_path(conn, :index, conn.assigns.locale)
    assert html_response(conn, 302)
  end

  @tag logged_in?: true
  test "show index page for logged in admin user", %{conn: conn} do
    conn = get conn, admin_admin_user_path(conn, :index, conn.assigns.locale)
    assert html_response(conn, 200)
  end

  @tag logged_in?: true
  test "renders form for create admin user", %{conn: conn} do
    conn = get conn, admin_admin_user_path(conn, :new, conn.assigns.locale)

    assert html_response(conn, 200)
  end

  @tag logged_in?: true
  test "redirect when create admin user with invalid attributes", %{conn: conn} do
    invalid_attrs = %{username: "Updated Admin"}
    conn = post conn, admin_admin_user_path(conn, :create, conn.assigns.locale), admin_user: invalid_attrs

    assert html_response(conn, 200) =~ "New Admin User"
  end

  @tag logged_in?: true
  test "create new admin user", %{conn: conn} do
    new_attrs = %{@valid_attrs | username: "Updated Admin"}
    conn = post conn, admin_admin_user_path(conn, :create, conn.assigns.locale), admin_user: new_attrs

    assert redirected_to(conn) == admin_admin_user_path(conn, :index, conn.assigns.locale)
  end

  @tag logged_in?: true
  test "renders form for editing chosen resource", %{conn: conn} do
    admin_user = create_admin_user!()
    conn = get conn, admin_admin_user_path(conn, :edit, conn.assigns.locale, admin_user)
    assert html_response(conn, 200)
  end

  @tag logged_in?: true
  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    admin_user = create_admin_user!()
    new_attrs = %{@valid_attrs | username: "Updated Admin"}
    conn = put conn, admin_admin_user_path(conn, :update, conn.assigns.locale, admin_user),
      admin_user: new_attrs
    assert redirected_to(conn) == admin_admin_user_path(conn, :index, conn.assigns.locale)
    assert Repo.get_by(AdminUser, %{username: "Updated Admin"})
  end

  @tag logged_in?: true
  test "update does not update password", %{conn: conn} do
    admin_user = create_admin_user!()
    new_attrs = Map.put(@valid_attrs, :password, "pPassword_123")
    conn = put conn, admin_admin_user_path(conn, :update, conn.assigns.locale, admin_user),
      admin_user: new_attrs
    assert redirected_to(conn) == admin_admin_user_path(conn, :index, conn.assigns.locale)
    updated_admin_user = Repo.get(AdminUser, admin_user.id)
    assert updated_admin_user.password_hash == admin_user.password_hash
  end

  @tag logged_in?: true
  test "deletes chosen resource", %{conn: conn} do
    admin_user = create_admin_user!()
    conn = delete conn, admin_admin_user_path(conn, :delete, conn.assigns.locale, admin_user)
    assert redirected_to(conn) == admin_admin_user_path(conn, :index, conn.assigns.locale)
    refute Repo.get(AdminUser.not_deleted_query, admin_user.id)
  end
end
