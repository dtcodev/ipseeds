defmodule Ipseeds.Admin.SessionControllerTest do
  use Ipseeds.ConnCase

  setup context do
    controller_test_setup_admin(context, "en", username: "Admin Tester", is_superadmin: false)
  end

  test "show login page when not logged in", %{conn: conn} do
    conn = get conn, public_admin_session_path(conn, :new, conn.assigns.locale)
    assert html_response(conn, 200)
  end

  test "redirect user to dashboard from login page if logged in", %{conn: conn} do
    admin_user = create_admin_user!()
    conn = conn
      |> login_as_admin_user(admin_user)
      |> get(public_admin_session_path(conn, :new, conn.assigns.locale))

    assert redirected_to(conn) == admin_page_path(conn, :dashboard, conn.assigns.locale)
  end

  test "correct login takes admin user to dashboard page", %{conn: conn} do
    _admin_user = create_admin_user!(username: "exampleuser", password: "Password_123")

    conn = post conn, public_admin_session_path(conn, :create, conn.assigns.locale),
      session: %{username: "exampleuser", password: "Password_123"}

    assert redirected_to(conn) == admin_page_path(conn, :dashboard, conn.assigns.locale)
  end

  test "incorrect login loads the login page", %{conn: conn} do
    _admin_user = create_admin_user!(username: "exampleuser", password: "Password_123")

    conn = post conn, public_admin_session_path(conn, :create, conn.assigns.locale),
      session: %{username: "exampleuser", password: "wrongpass"}

    assert html_response(conn, 200)
  end

  test "logout redirects user to login page after logging out", %{conn: conn} do
    admin_user = create_admin_user!()
    conn = conn
      |> login_as_admin_user(admin_user)
      |> delete(public_admin_session_path(conn, :delete, conn.assigns.locale, admin_user))

    assert redirected_to(conn) == public_admin_session_path(conn, :new, conn.assigns.locale)
  end
end
