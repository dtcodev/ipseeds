defmodule Ipseeds.SessionControllerTest do
  use Ipseeds.ConnCase

  import Mock

  setup %{conn: conn} do
    conn = assign(conn, :locale, "en")
    user = create_user!()
    login_response = create_login_response()

    {:ok, conn: conn, user: user, login_response: login_response}
  end

  test "redirect to IP Discovery page after login succeeded with no redirect_url",
    %{conn: conn, user: user, login_response: login_response} do

    with_mock Ipseeds.AuthHelpers,
      [log_user_in: fn (_username, _password) ->
        {:ok, user, login_response} end] do
      conn = post conn, session_path(conn, :create, conn.assigns.locale), create_login_params(user)

      assert redirected_to(conn) == app_ip_discovery_path(conn, :index, conn.assigns.locale)
    end
  end

  test "redirect to redirect_url and clear redirect_url after login succeeded with redirect_url",
    %{conn: conn, user: user, login_response: login_response} do

    redirect_url = app_message_path(conn, :index, conn.assigns.locale)
    conn = get conn, redirect_url
    with_mock Ipseeds.AuthHelpers,
      [log_user_in: fn (_username, _password) ->
        {:ok, user, login_response} end] do
      conn_redirected = post recycle(conn),
        session_path(conn, :create, conn.assigns.locale),
        create_login_params(user)

      assert redirected_to(conn_redirected) == redirect_url
      assert get_session(conn_redirected, :redirect_url) == nil
    end
  end

  defp create_login_params(user) do
    %{
      "user" => %{
        "username" => user.username,
        "password" => "123456789"
      }
    }
  end

  defp create_login_response() do
    current_time = DateTime.utc_now |> DateTime.to_unix
    %{
      "account_auth" => %{
        "auth_code" => "dummy_auth_code",
        "expire_time_utc" => current_time
      },
      "addrlist" => [
        %{
          "atag" => "Issuer",
          "address" => "dummy_issuer_address"
        },
        %{
          "atag" => "Receiver",
          "address" => "dummy_receiver_address"
        }
      ]
    }   
  end
end
