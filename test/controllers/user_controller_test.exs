defmodule Ipseeds.UserControllerTest do
  use Ipseeds.ConnCase
  use Bamboo.Test

  alias Ipseeds.{User}

  setup %{conn: conn} = _context do
    conn = assign(conn, :locale, "en")
    user = create_user!()
    {:ok, conn: conn, user: user}
  end

  test "forgot password returns with error message if email is empty", %{conn: conn} do
    conn = post conn, user_path(conn, :request_reset_password, conn.assigns.locale), user: %{email: ""}
    assert html_response(conn, 200) =~ "<div class=\"alert alert-danger\">"
  end

  test "forgot password returns with success message but does nothing when email does not exist",
    %{conn: conn, user: user} do

    email = "does@notexist.com"
    assert user.email != email
    conn = post conn, user_path(conn, :request_reset_password, conn.assigns.locale), user: %{email: email}
    assert html_response(conn, 200)
    refute html_response(conn, 200) =~ "<div class=\"alert alert-danger\">"
  end

  test "forgot password returns with success message and contacts API if email exists",
    %{conn: conn, user: user} do

    conn = post conn, user_path(conn, :request_reset_password, conn.assigns.locale),
      user: %{email: user.email}
    assert html_response(conn, 200)
    refute html_response(conn, 200) =~ "<div class=\"alert alert-danger\">"
  end

  test "api reset password hook sends email to the user if they exist",
    %{conn: conn, user: user} do

    token = "abc123"
    conn = post conn, user_path(conn, :api_password_reset_hook, conn.assigns.locale),
      Email: user.email, Token: token
    assert json_response(conn, 200)
    assert_delivered_email Ipseeds.Email.reset_password_email(user.email, User.get_name(user),
      reset_password_link(conn, token))
  end

  test "api reset password hook does not send email if the user does not exist",
    %{conn: conn, user: _user} do

    token = "abc123"
    random_email = "jkasdla@nonexistent.com"
    assert Repo.get_by(User, %{email: random_email}) |> is_nil()
    conn = post conn, user_path(conn, :api_password_reset_hook, conn.assigns.locale),
      Email: random_email, Token: token
    assert json_response(conn, 200)
    refute_delivered_email Ipseeds.Email.reset_password_email(random_email, "name",
      reset_password_link(conn, token))
  end

  defp reset_password_link(conn, token) do
    user_url(conn, :reset_password, conn.assigns.locale, info: token)
  end
end
