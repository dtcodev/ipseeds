defmodule Ipseeds.App.AgreeableContractControllerTest do
  use Ipseeds.ConnCase

  setup %{conn: conn} = context do
    conn = assign(conn, :locale, "en")
    if context[:logged_in?] do
      user = create_user!()
      conn = login_as_user(conn, user)
      {:ok, conn: conn, user: user}
    else
      {:ok, conn: conn}
    end
  end

  @tag logged_in?: true
  test "GET /:locale/secure/agreeable_contract/new", %{conn: conn} do
    conn = get conn, app_agreeable_contract_path(conn, :new, conn.assigns.locale)
    assert html_response(conn, 200)
  end
end
