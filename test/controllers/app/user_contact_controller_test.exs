defmodule Ipseeds.App.UserContactControllerTest do
  use Ipseeds.ConnCase, async: false

  alias Ipseeds.UserContact
  @invalid_attrs %{}

  setup %{conn: conn} = context do
    conn = assign(conn, :locale, "en")
    if context[:logged_in?] do
      user = create_user!()
      contact_user = create_user!()
      conn = login_as_user(conn, user)
      {:ok, conn: conn, user: user, contact_user: contact_user}
    else
      {:ok, conn: conn}
    end
  end

  @tag logged_in?: true
  test "lists all entries on index", %{conn: conn} do
    conn = get conn, app_user_contact_path(conn, :index, conn.assigns.locale)
    assert html_response(conn, :ok)
  end

  @tag logged_in?: true
  test "creates resource and redirects when data is valid",
    %{conn: conn, user: user, contact_user: contact_user} do

    user_contact_params = %{user_id: user.id, contact_user_id: contact_user.id}
    conn = post conn, app_user_contact_path(conn, :create, conn.assigns.locale),
      user_contact: user_contact_params
    assert json_response(conn, :created)
    assert Repo.get_by(UserContact, user_contact_params)
  end

  @tag logged_in?: true
  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, app_user_contact_path(conn, :create, conn.assigns.locale),
      user_contact: @invalid_attrs
    assert json_response(conn, :unprocessable_entity)
  end

  @tag logged_in?: true
  test "deletes chosen resource", %{conn: conn} do
    user_contact = Repo.insert! %UserContact{}
    conn = delete conn, app_user_contact_path(conn, :delete, conn.assigns.locale, user_contact)
    assert response(conn, :no_content)
    refute Repo.get(UserContact, user_contact.id)
  end
end
