defmodule Ipseeds.App.UserControllerTest do
  use Ipseeds.ConnCase

  setup %{conn: conn} = context do
    conn = assign(conn, :locale, "en")
    if context[:logged_in?] do
      user = create_user!()
      conn = login_as_user(conn, user)
      {:ok, conn: conn, user: user}
    else
      {:ok, conn: conn}
    end
  end

  @tag logged_in?: true
  test "views profile of an unapproved user", %{conn: conn} do
    unapproved_user = create_user!(false)

    response = assert_error_sent 404, fn ->
      get conn, app_user_path(conn, :show, conn.assigns.locale, unapproved_user.username)
    end

    assert {404, _header, _body} = response
  end

  @tag logged_in?: true
  test "current user can only edit their own profile", %{conn: conn} do
    approved_user = create_user!()
    conn = get conn, app_user_path(conn, :edit, conn.assigns.locale, approved_user.username)
    assert redirected_to(conn) =~ app_user_path(conn, :show, conn.assigns.locale, approved_user.username)
  end
end
