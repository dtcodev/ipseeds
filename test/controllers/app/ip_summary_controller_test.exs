defmodule Ipseeds.App.IpSummaryControllerTest do
  use Ipseeds.ConnCase

  setup %{conn: conn} = context do
    conn = assign(conn, :locale, "en")
    if context[:logged_in?] do
      current_user = create_user!()
      current_user_ip_summary = create_ip_summary!(user_id: current_user.id)
      other_user = create_user!()
      other_user_ip_summary = create_ip_summary!(user_id: other_user.id)
      conn = login_as_user(conn, current_user)
      {:ok, conn: conn, current_user: current_user, other_user: other_user,
        current_user_ip_summary: current_user_ip_summary,
        other_user_ip_summary: other_user_ip_summary}
    else
      {:ok, conn: conn}
    end
  end
end
