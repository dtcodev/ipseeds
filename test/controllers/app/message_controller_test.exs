defmodule Ipseeds.App.MessageControllerTest do
  use Ipseeds.ConnCase, async: false
  use Bamboo.Test

  alias Ipseeds.{Message, User}
  alias Ipseeds.App.MessageController

  setup %{conn: conn} = context do
    conn = assign(conn, :locale, "en")
    if context[:logged_in?] do
      current_user = create_user!(true, message_notify_by_email: true)
      other_user = create_user!(true, message_notify_by_email: true)
      _user_contact = create_user_contact!(current_user, other_user)
      conn = login_as_user(conn, current_user)
      {:ok, conn: conn, current_user: current_user, other_user: other_user}
    else
      {:ok, conn: conn}
    end
  end

  @tag logged_in?: true
  test "lists all conversations on index", %{conn: conn} do
    conn = get conn, app_message_path(conn, :index, conn.assigns.locale)
    assert html_response(conn, 200)
  end

  @tag logged_in?: true
  test "renders new", %{conn: conn} do
    conn = get conn, app_message_path(conn, :new, conn.assigns.locale)
    assert html_response(conn, 200)
  end

  @tag logged_in?: true
  test "creates resource and redirects when data is valid",
    %{conn: conn, current_user: current_user, other_user: other_user} do

    message_params = valid_create_message_params(other_user)
    conn = post conn, app_message_path(conn, :create, conn.assigns.locale),
      message: message_params
    assert json_response(conn, :created)
    assert Repo.get_by(Message, message_params)
    assert_delivered_email Ipseeds.Email.new_message_email(current_user, other_user)
  end

  @tag logged_in?: true
  test "does not create resource and returns error status with invalid content",
    %{conn: conn, current_user: _current_user, other_user: other_user} do

    message_params = %{valid_create_message_params(other_user) | sender_encrypted_content: ""}
    conn = post conn, app_message_path(conn, :create, conn.assigns.locale), message: message_params
    assert json_response(conn, :unprocessable_entity)
    refute Repo.get_by(Message, message_params)
  end

  @tag logged_in?: true
  test "renders show if messages exist between users",
    %{conn: conn, current_user: current_user, other_user: other_user} do

    subject = "Hello"
    create_message!(current_user, other_user, subject: subject)
    conn = get conn, app_message_path(conn, :show, conn.assigns.locale, other_user.username, subject)
    assert html_response(conn, 200)
  end

  @tag logged_in?: true
  test "redirects to new if messages do not exist between users",
    %{conn: conn, other_user: other_user} do

    conn = get conn, app_message_path(conn, :show, conn.assigns.locale, other_user.username,
      "nonexistent subject")
    assert redirected_to(conn) == app_message_path(conn, :new, conn.assigns.locale)
  end

  @tag logged_in?: true
  test "updates chosen resource and redirects when data is valid",
    %{conn: conn, current_user: current_user, other_user: other_user} do

    subject = "Hello"
    message = create_message!(current_user, other_user, subject: subject)
    message_params = %{sender_encrypted_content: "Bye", recipient_encrypted_content: "eyB"}
    conn = put conn, app_message_path(conn, :update, conn.assigns.locale, message),
      message: message_params
    assert redirected_to(conn) == app_message_path(conn, :show, conn.assigns.locale, other_user.username,
      subject)
    assert Repo.get_by(Message, message_params)
  end

  @tag logged_in?: true
  test "deletes chosen resource", %{conn: conn} do
    message = Repo.insert! %Message{}
    conn = delete conn, app_message_path(conn, :delete, conn.assigns.locale, message)
    assert redirected_to(conn) == app_message_path(conn, :index, conn.assigns.locale)
    refute Repo.get(Message, message.id)
  end

  @tag logged_in?: true
  test "strips subject tags for create in case front end does not", %{conn: conn, other_user: other_user} do
    message_params = valid_create_message_params(other_user)
     |> Map.put(:subject, "<script>alert(\"xss\")</script> Subject");
    _conn = post conn, app_message_path(conn, :create, conn.assigns.locale),
      message: message_params
    refute Repo.get_by(Message, message_params)
    assert Repo.get_by(Message, %{message_params | subject: "alert(\"xss\") Subject"})
  end

  @tag logged_in?: true
  test "marks multiple messages as read",
    %{conn: conn, current_user: current_user, other_user: other_user} do

    message_count = 5
    subject = "Hello"
    _messages = for _ <- 1..(message_count) do
      create_message!(other_user, current_user, subject: subject)
    end
    current_user_keypair = User.active_keypair(current_user)
    index_query = MessageController.index_query(current_user, current_user_keypair)
    conversations = Repo.all(index_query)
    unread_count = conversations |> List.first |> Map.get(:unread_count)

    assert unread_count == message_count

    conn = patch conn, app_message_path(conn, :mark_as_read, conn.assigns.locale, other_user.id, subject)
    assert json_response(conn, :ok)

    updated_conversations = Repo.all(from c in index_query, where: c.subject == ^subject)
    updated_unread_count = updated_conversations |> List.first |> Map.get(:unread_count)
    assert updated_unread_count == 0
  end

  @tag logged_in?: true
  test "mark as read only marks messages of current user as read",
    %{conn: conn, current_user: current_user, other_user: other_user} do

    to_current_user_message_count = 2
    from_current_user_message_count = 3
    subject = "Hello"
    _messages_to_current_user = for _ <- 1..(to_current_user_message_count) do
      create_message!(other_user, current_user, subject: subject)
    end
    _messages_from_current_user = for _ <- 1..(from_current_user_message_count) do
      create_message!(current_user, other_user, subject: subject)
    end

    current_user_keypair = User.active_keypair(current_user)
    current_user_index_query = MessageController.index_query(current_user, current_user_keypair)
    current_user_conversations = Repo.all(current_user_index_query)
    current_user_unread_count = current_user_conversations |> List.first |> Map.get(:unread_count)
    assert current_user_unread_count == to_current_user_message_count

    other_user_keypair = User.active_keypair(other_user)
    other_user_index_query = MessageController.index_query(other_user, other_user_keypair)
    other_user_conversations = Repo.all(other_user_index_query)
    other_user_unread_count = other_user_conversations |> List.first |> Map.get(:unread_count)
    assert other_user_unread_count == from_current_user_message_count

    mark_current_user_messages_as_read_conn =
      patch conn, app_message_path(conn, :mark_as_read, conn.assigns.locale, other_user.id, subject)
    assert json_response(mark_current_user_messages_as_read_conn, :ok)

    updated_current_user_conversations =
      Repo.all(from c in current_user_index_query, where: c.subject == ^subject)
    updated_current_user_unread_count =
      updated_current_user_conversations |> List.first |> Map.get(:unread_count)
    assert updated_current_user_unread_count == 0

    updated_other_user_conversations =
      Repo.all(from c in other_user_index_query, where: c.subject == ^subject)
    updated_other_user_unread_count =
      updated_other_user_conversations |> List.first |> Map.get(:unread_count)
    assert updated_other_user_unread_count == from_current_user_message_count
  end

  @tag logged_in?: true
  test "sends a system message to the current user", %{current_user: current_user} do
    subject = "Subject"
    message = "Message"
    MessageController.send_system_message(current_user, subject, message)
    assert Repo.get_by!(Message, %{recipient_user_id: current_user.id, is_system_message: true,
      subject: subject, recipient_encrypted_content: message})
  end

  @tag logged_in?: true
  test "renders show for system messages and marks as read", %{conn: conn, current_user: current_user} do
    subject = "Subject"
    message = "Message"
    MessageController.send_system_message(current_user, subject, message)
    message_params = %{recipient_user_id: current_user.id, is_system_message: true,
      subject: subject, recipient_encrypted_content: message}

    system_message = Repo.get_by!(Message, message_params)
    assert is_nil(system_message.read_at)

    conn = get conn, app_message_path(conn, :show, conn.assigns.locale, User.system_username, subject)
    assert html_response(conn, 200)

    system_message_updated = Repo.get_by!(Message, message_params)
    refute is_nil(system_message_updated.read_at)
  end

  @tag logged_in?: true
  test "does not render nonexistent system message", %{conn: conn} do
    subject = "Nonexistent"

    conn = get conn, app_message_path(conn, :show, conn.assigns.locale, User.system_username, subject)
    assert redirected_to(conn) == app_message_path(conn, :index, conn.assigns.locale)
  end
end
