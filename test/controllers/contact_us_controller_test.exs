defmodule Ipseeds.ContactUsControllerTest do
  use Ipseeds.ConnCase
  use Bamboo.Test

  alias Ipseeds.Email

  @valid_contact_us_info %{first_name: "Im", last_name: "fromtesting", email: "send@from.test",
    message: "This mail is sent by testing."}

  setup %{conn: conn} = _context do
    conn = assign(conn, :locale, "en")
    {:ok, conn: conn}
  end

  test "renders new", %{conn: conn} do
    conn = get conn, contact_us_path(conn, :new, conn.assigns.locale)
    assert html_response(conn, 200)
  end

  test "sends email with valid information", %{conn: conn} do
    %{first_name: first_name, last_name: last_name, email: email, message: message} = @valid_contact_us_info
    conn = post conn, contact_us_path(conn, :create, conn.assigns.locale),
      %{contact_us_info: @valid_contact_us_info, "g-recaptcha-response": "XXXX"}
    assert html_response(conn, 200)
    assert_delivered_email Email.new_contact_us_email(first_name, last_name, email, message)
  end

  test "does not send email with invalid information", %{conn: conn} do
    %{first_name: first_name, last_name: last_name, email: email} = @valid_contact_us_info
    conn = post conn, contact_us_path(conn, :create, conn.assigns.locale),
      %{contact_us_info: Map.take(@valid_contact_us_info, [:first_name, :last_name, :email]), 
        "g-recaptcha-response": "XXXX"}
    assert html_response(conn, 200)
    refute_delivered_email Email.new_contact_us_email(first_name, last_name, email, "")
  end
end