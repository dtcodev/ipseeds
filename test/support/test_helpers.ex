defmodule Ipseeds.TestHelpers do
  alias Ipseeds.{Message, Repo, User, UserRegistration, UserRegistrationController}

  @default_application_type "individual"

  def login_as_user(conn, user) do
    Plug.Conn.assign(conn, :current_user, user)
  end

  def create_user!(marked_as_approved \\ true, custom_user_params \\ []) do
    user_params = Map.merge(valid_user_params(), Enum.into(custom_user_params, %{}))
    application_type = Keyword.get(custom_user_params, :application_type, @default_application_type)
    action_batch = UserRegistrationController.submit_application(
      application_type_module(application_type)[:module],
      valid_application_params(application_type),
      user_params, valid_user_registration_params(application_type))

    # insert user, registration and application
    user = case Repo.transaction(action_batch) do
      {:ok, changeset} -> changeset[:user]
      {:error, _failed_operations, failed_changeset, _changes_so_far} ->
        IO.inspect(failed_changeset)
        raise "insert user failed"
    end

    # insert user keypair
    %Ipseeds.UserKeypair{}
    |> Ipseeds.UserKeypair.changeset(
      %{bip32_hd_public_key: "abc123", ecc_public_key_hex: "123abc", user_id: user.id})
    |> Repo.insert!

    case marked_as_approved do
      true -> mark_user_as_approved(user)
      _ -> user
    end
  end


  defp mark_user_as_approved(user) do
    # Need an admin to approve a registration
    admin_user = create_admin_user!()
    _user_registration = Ecto.assoc(user, :user_registration)
      |> Repo.one!
      |> UserRegistration.changeset(%{approved_at: DateTime.utc_now(), approved_by_admin_id: admin_user.id })
      |> Repo.update!
    user
  end

  def login_as_admin_user(conn, admin_user) do
    Plug.Conn.assign(conn, :current_admin_user, admin_user)
  end

  def create_admin_user!(attrs \\ []) do
    changes = Map.merge(%{
      username: "An Admin #{get_random_string()}",
      email: "admin#{get_random_string()}@admin.com",
      password: "supersecret",
      is_superadmin: false
    }, Enum.into(attrs, Map.new))

    %Ipseeds.AdminUser{}
    |> Ipseeds.AdminUser.registration_changeset(changes)
    |> Repo.insert!()
  end

  def controller_test_setup_admin(%{conn: conn} = context, locale, admin_user_params \\ []) do
    conn = Plug.Conn.assign(conn, :locale, locale)

    if context[:logged_in?] do
      admin_user = create_admin_user!(admin_user_params)
      conn = login_as_admin_user(conn, admin_user)
      {:ok, conn: conn, current_user: admin_user}
    else
      {:ok, conn: conn}
    end
  end

  def create_user_contact!(current_user, contact_user) do
    %Ipseeds.UserContact{}
    |> Ipseeds.UserContact.changeset(%{user_id: current_user.id, contact_user_id: contact_user.id})
    |> Repo.insert!()
  end

  def create_ip_summary!(ip_summary_params \\ []) do
    changes = Map.merge(%{
      user_id: 1,
      api_asset_id: get_random_string()
    }, Enum.into(ip_summary_params, %{}))

    Ecto.Changeset.change(%Ipseeds.IpSummary{}, changes)
    |> Repo.insert!()
  end

  def image_upload(filename \\ "test.jpg") do
    %Plug.Upload{content_type: "image/jpg", filename: filename,
      path: Path.absname("test/support/test_image.jpg", System.cwd)}
  end

  def create_message!(sender, recipient, message_params \\ []) do
    changes = Map.merge(valid_message_params(sender, recipient), Enum.into(message_params, %{}))
    Ecto.Changeset.change(%Message{}, changes) |> Repo.insert!
  end

  def valid_message_params(sender, recipient, subject \\ "Hello", content \\ "Hello") do
    sender_keypair = User.active_keypair sender
    recipient_keypair = User.active_keypair recipient
    Map.merge(valid_create_message_params(recipient, subject, content), %{
      sender_user_id: sender.id, sender_keypair_id: sender_keypair.id,
      recipient_keypair_id: recipient_keypair.id})
  end

  def valid_create_message_params(recipient, subject \\ "Hello", content \\ "Hello") do
    %{recipient_user_id: recipient.id, sender_encrypted_content: content,
      recipient_encrypted_content: String.reverse(content), subject: subject}
  end

  defp valid_user_params do
    username = "user_#{get_random_string()}"
    email = "#{username}@example.com"
    %{username: username, email: email, email_confirmation: email,
      password: "Password_123", password_confirmation: "Password_123"}
  end

  def valid_application_params("individual") do
    %{given_names: "John", last_name: "Doe", id_number: "abc123", id_type: "id_card", nationality: "TW",
      date_of_birth: "1970-01-01", gender: "m", organization: "Taiwan University",
      department: "Biotechnology Dept", title: "mr", telephone: "0800-555-0199",
      mobile: "0800-555-0199", address: "123 Fake St, Faketon"}
  end
  def valid_application_params("industry") do
    %{company_name_en: "some content",
      company_name_zh_tw: "some content", company_tax_id: "some content", company_nationality: "TW", 
      company_website: "some content", contact_address: "some content",
      contact_department: "some content", contact_given_names: "Jane Middlename", contact_last_name: "Doe",
      contact_telephone: "some content", contact_title: "some content", contact_email: "some_email@example.com",
      user_address: "some content", user_department: "some content", user_title: "some content",
      user_given_names: "John Middlename", user_last_name: "Doe", user_telephone: "some content"}
  end
  def valid_application_params("institute") do
    %{contact_address: "some content", contact_department: "some content",
      contact_last_name: "Doe", contact_given_names: "Jane Middlename", contact_telephone: "some content",
      contact_title: "some content", contact_email: "some_email@example.com",
      institute_name_en: "some content", institute_name_zh_tw: "some content",
      institute_tax_id: "some content", institute_website: "some content", institute_nationality: "TW",
      user_address: "some content", user_department: "some content", user_given_names: "John Middlename",
      user_last_name: "Doe", user_telephone: "some content", user_title: "some content"}
  end

  defp valid_user_registration_params(application_type) do
    %{terms_of_service: true, application_type: application_type}
  end

  defp application_type_module(application_type) do
    UserRegistration.type_map[application_type]
  end

  defp get_random_string(len \\ 16) do
    len |> div(2) |> :crypto.strong_rand_bytes() |> Base.encode16()
  end
end
