defmodule Ipseeds.App.IpDiscoveryViewTest do
  use Ipseeds.ConnCase, async: true

  alias Ipseeds.App.IpDiscoveryView

  test "entry_range" do
    assert IpDiscoveryView.entry_range([], 1, 1) == "0-0"
    assert IpDiscoveryView.entry_range(asset_list(1), 1, 1) == "1-1"
    assert IpDiscoveryView.entry_range(asset_list(3), 1, 10) == "1-3"
    assert IpDiscoveryView.entry_range(asset_list(3), 2, 10) == "11-13"
    assert IpDiscoveryView.entry_range(asset_list(3), 3, 10) == "21-23"
  end

  test "total_entries" do
    assert IpDiscoveryView.total_entries([], 1, 1, 1) == 0
    assert IpDiscoveryView.total_entries([], 1, 10, 10) == 0
    assert IpDiscoveryView.total_entries(asset_list(3), 1, 10, 1) == 3
    assert IpDiscoveryView.total_entries(asset_list(3), 1, 10, 10) == 100
    assert IpDiscoveryView.total_entries(asset_list(3), 2, 10, 10) == 100
    assert IpDiscoveryView.total_entries(asset_list(3), 10, 10, 10) == 93
  end

  defp asset_list(0 = _length), do: []
  defp asset_list(length) when is_integer(length) do
    for n <- 0..(length - 1), do: %{id: n}
  end
  defp asset_list(_), do: []
end
