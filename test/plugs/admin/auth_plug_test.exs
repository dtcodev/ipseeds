defmodule Ipseeds.Admin.AuthPlugTest do
  use Ipseeds.ConnCase
  alias Ipseeds.{AdminUser, Repo}
  alias Ipseeds.Admin.AuthPlug

  setup %{conn: conn} do
    conn = conn
      |> bypass_through(Ipseeds.Router, [:browser])
      |> get("/", locale: "en")

    {:ok, %{conn: conn}}
  end

  test "authenticate_login halts when no current_admin_user exists", %{conn: conn} do
    conn = AuthPlug.authenticate_login(conn)
    assert conn.halted
  end

  test "authenticate_login continues when current_admin_user is logged in", %{conn: conn} do
    conn = conn
      |> login_as_admin_user(%AdminUser{id: 123})
      |> AuthPlug.authenticate_login()

    refute conn.halted
  end

  test "authenticate_superadmin halts when current_admin_user is not superadmin", %{conn: conn} do
    not_superadmin = create_admin_user!(is_superadmin: false)
    conn = conn
      |> login_as_admin_user(not_superadmin)
      |> AuthPlug.authenticate_superadmin()

    assert conn.halted
    assert redirected_to(conn) == admin_page_path(conn, :dashboard, conn.assigns.locale)
  end

  test "authenticate_superadmin continues when current_admin_user is superadmin", %{conn: conn} do
    superadmin = create_admin_user!(is_superadmin: true)
    conn = conn
      |> login_as_admin_user(superadmin)
      |> AuthPlug.authenticate_superadmin()

    refute conn.halted
  end

  test "login puts the user in the session", %{conn: conn} do
    logged_in_conn = conn
      |> AuthPlug.login(%AdminUser{id: 123})
      |> send_resp(:ok, " ")

    assert get_session(logged_in_conn, :admin_user_id) == 123
    next_conn = get(logged_in_conn, "/")
    assert get_session(next_conn, :admin_user_id) == 123
  end

  test "logout drops the session", %{conn: conn} do
    logged_out_conn =
      conn
      |> put_session(:admin_user_id, 123)
      |> AuthPlug.logout
      |> send_resp(:ok, " ")

    next_conn = get(logged_out_conn, "/")
    refute get_session(next_conn, :admin_user_id)
  end

  test "call places current user in session from user id", %{conn: conn} do
    %AdminUser{id: admin_user_id} = create_admin_user!()
    conn = conn
      |> put_session(:admin_user_id, admin_user_id)
      |> AuthPlug.call(Repo)

    assert conn.assigns.current_admin_user.id == admin_user_id
  end
  
  test "call places user from session into assigns", %{conn: conn} do
    admin_user = create_admin_user!()
    conn = conn
      |> put_session(:admin_user_id, admin_user.id)
      |> AuthPlug.call(Repo)

    assert conn.assigns.current_admin_user.id == admin_user.id
  end
  
  test "login with a valid email and pass", %{conn: conn} do
    admin_user = create_admin_user!(username: "exampleuser", password: "password")
    {:ok, conn} = AuthPlug.login_by_username_and_pass(
      conn, "exampleuser", "password", repo: Repo)

    assert conn.assigns.current_admin_user.id == admin_user.id
  end

  test "login with a not found solution", %{conn: conn} do
    assert {:error, :not_found, _conn} = AuthPlug.login_by_username_and_pass(
      conn, "exampleuser", "password", repo: Repo)
  end

  test "login with incorrect password", %{conn: conn} do
    _admin_user = create_admin_user!(username: "exampleuser", password: "password")
    assert {:error, :unauthorized, _conn} =
      AuthPlug.login_by_username_and_pass(conn, "exampleuser", "secret123", repo: Repo)
  end

  test "login with deleted user", %{conn: conn} do
    _admin_user = create_admin_user!(username: "exampleuser", password: "password",
      deleted_at: DateTime.utc_now)

    assert {:error, :not_found, _conn} =
          AuthPlug.login_by_username_and_pass(conn, "exampleuser", "password", repo: Repo)
  end

  test "login with disabled user", %{conn: conn} do
    _admin_user = create_admin_user!(username: "exampleuser", password: "password",
      disabled_at: DateTime.utc_now)

    assert {:error, :not_found, _conn} =
          AuthPlug.login_by_username_and_pass(conn, "exampleuser", "password", repo: Repo)
  end
end
