defmodule Ipseeds.ParamPlugTest do
  use Ipseeds.ConnCase

  alias Ipseeds.ParamPlug

  @test_binary_param "<h1 onload='alert(\"xss\")'>XSS</h1><p>More content</p>"
  @test_binary_param_stripped "XSSMore content"

  test "strip_tags strips tags when required param is a binary" do
    conn = build_conn(:get, "/en", %{"test_param" => @test_binary_param})
    conn = ParamPlug.strip_tags(conn, "test_param")
    assert Map.get(conn.params, "test_param") == @test_binary_param_stripped
  end

  test "strip_tags strips tags of binaries inside a list" do
    conn = build_conn(:get, "/en", %{"test_param" => [@test_binary_param, @test_binary_param]})
    conn = ParamPlug.strip_tags(conn, "test_param")
    assert Map.get(conn.params, "test_param") ==
      [@test_binary_param_stripped, @test_binary_param_stripped]
  end

  test "strip_tags strips tags of nested map" do
    conn = build_conn(:get, "/en", %{"test_param" => %{
      "nested_param" => @test_binary_param
    }})
    conn = ParamPlug.strip_tags(conn, "test_param")
    assert Map.get(conn.params, "test_param") == %{
      "nested_param" => @test_binary_param_stripped
    }
  end

  test "strip_tags handles uploads" do
    upload = image_upload()
    conn = build_conn(:get, "/en", %{"test_param" => %{
      "nested_param" => @test_binary_param,
      "upload" => upload
    }})
    conn = ParamPlug.strip_tags(conn, "test_param")
    assert Map.get(conn.params, "test_param") == %{
      "nested_param" => @test_binary_param_stripped,
      "upload" => upload
    }
  end
end