defmodule Ipseeds.App.AuthPlugTest do
  use Ipseeds.ConnCase

  setup %{conn: conn} do
    conn = conn
      |> bypass_through(Ipseeds.Router, [:browser])
      |> get("/", locale: "en")

    {:ok, %{conn: conn}}
  end

  test "denies access when user is not logged in and stores redirect_path", %{conn: conn} do
    req_path = app_message_path(conn, :index, conn.assigns.locale)
    conn_halted = get conn, req_path
    assert conn_halted.halted
    assert get_session(conn_halted, :redirect_url) == req_path
  end
end
