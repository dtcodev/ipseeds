defmodule Ipseeds.UnreadMessagesPlugTest do
  use Ipseeds.ConnCase

  alias Ipseeds.App.MessageController

  setup %{conn: conn} = context do
    conn = assign(conn, :locale, "en")
    if context[:logged_in?] do
      current_user = create_user!()
      other_user = create_user!()
      _user_contact = create_user_contact!(current_user, other_user)
      conn = login_as_user(conn, current_user)
      {:ok, conn: conn, current_user: current_user, other_user: other_user}
    else
      {:ok, conn: conn}
    end
  end

  @tag logged_in?: true
  test "unread message count should be 0 if there are no messages to the current user", %{conn: conn} do

    conn = get conn, app_message_path(conn, :index, conn.assigns.locale)
    assert conn.assigns.unread_message_count == 0
  end

  @tag logged_in?: true
  test "messages sent by current user are not counted as unread with respect to current user",
    %{conn: conn, current_user: current_user, other_user: other_user} do

    message_count = 5
    _message = for _ <- 1..(message_count), do: create_message!(current_user, other_user)

    conn = get conn, app_message_path(conn, :index, conn.assigns.locale)
    assert conn.assigns.unread_message_count == 0
  end

  @tag logged_in?: true
  test "messages sent by other user are counted as unread with respect to current user",
    %{conn: conn, current_user: current_user, other_user: other_user} do

    message_count = 5
    _message = for _ <- 1..(message_count), do: create_message!(other_user, current_user)

    conn = get conn, app_message_path(conn, :index, conn.assigns.locale)
    assert conn.assigns.unread_message_count == message_count
  end

  @tag logged_in?: true
  test "counts system messages in unread message count",
    %{conn: conn, current_user: current_user, other_user: other_user} do

    message_count = 5
    _message = for _ <- 1..(message_count), do: create_message!(other_user, current_user)

    system_message_count = 3
    subject = "Subject"
    message = "Message"
    _sm = for _ <- 1..(system_message_count) do
      MessageController.send_system_message(current_user, subject, message)
    end

    conn = get conn, app_message_path(conn, :index, conn.assigns.locale)
    assert conn.assigns.unread_message_count == message_count + system_message_count
  end
end