defmodule Ipseeds.Mixfile do
  use Mix.Project

  def project do
    [app: :ipseeds,
     version: "0.0.1",
     elixir: "~> 1.3",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix, :gettext] ++ Mix.compilers,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     aliases: aliases(),
     deps: deps()]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [mod: {Ipseeds, []},
     applications: [:phoenix, :phoenix_pubsub, :phoenix_html, :cowboy, :logger, :gettext,
                    :phoenix_ecto, :postgrex, :yaml_elixir, :httpoison, :bamboo,
                    :bamboo_smtp, :comeonin, :ex_aws, :timex, :recaptcha]]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
  defp elixirc_paths(_),     do: ["lib", "web"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.2"},
      {:phoenix_pubsub, "~> 1.0"},
      {:phoenix_ecto, "~> 3.0"},
      {:postgrex, "~> 0.12"},
      {:phoenix_html, "~> 2.6"},
      {:phoenix_live_reload, "~> 1.0", only: :dev},
      {:gettext, "~> 0.12"},
      {:cowboy, "~> 1.0"},
      {:yamerl, "~> 0.4"},
      {:yaml_elixir, "~> 1.0"},
      {:httpoison, "~> 0.9"},
      {:logger_file_backend, "~> 0.0"},
      {:bamboo, "~> 0.7"},
      {:bamboo_smtp, "~> 1.1"},
      {:comeonin, "~> 3.0"},
      {:comeonin_ecto_password, "~> 2.1"},
      {:arc, "~> 0.5"},
      {:ex_aws, "~> 0.5"},
      {:poison, "~> 2.0"},
      {:arc_ecto, "~> 0.4"},
      {:uuid, "~> 1.1"},
      {:scrivener, "~> 2.0"},
      {:scrivener_ecto, "~> 1.0"},
      {:scrivener_list, "~> 1.0"},
      {:scrivener_html, "~> 1.7"},
      {:timex, "~> 3.0"},
      {:html_sanitize_ex, "~> 1.0"},
      {:mock, "~> 0.2", only: :test},
      {:curtail, "~> 0.1"},
      {:recaptcha, "~> 2.0"},
      {:browser, "~> 0.1"},
      {:sizeable, "~> 0.1.5"},
      {:hackney, "1.6.5"} # locked to 1.6.5 in order to solve a problem in 1.6.6
     ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    ["ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
     "ecto.reset": ["ecto.drop", "ecto.setup"],
     "test": ["ipseeds.create --quiet", "ipseeds.migrate", "test"]]
  end
end
