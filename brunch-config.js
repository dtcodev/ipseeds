exports.config = {
  files: {
    javascripts: {
      joinTo: {
        "js/public.js": /^web\/static\/js\/(public|shared)/,
        "js/app.js": /^web\/static\/js\/(app|shared)/,
        "js/admin.js": /^web\/static\/js\/(admin|shared)/,
        "js/vendor.js": /^(web\/static\/vendor)|(deps)|(node_modules)/
      },
      order: {
        before: [
          "web/static/vendor/js/jquery-1.11.2.min.js",
          "web/static/vendor/js/jquery-migrate-1.2.1.min.js",
          "web/static/vendor/js/jquery.ui.widget.js",
          "web/static/vendor/js/jquery.iframe-transport.js",
          "web/static/vendor/js/promiz-1.0.6.js",
          "web/static/vendor/js/webcrypto-shim-0.1.1.js"
        ]
      }
    },
    stylesheets: {
      joinTo: {
        "css/public.css": "web/static/scss/public.scss",
        "css/app.css": "web/static/scss/app.scss",
        "css/admin.css": "web/static/scss/admin.scss",
        "css/vendor.css": /^(web\/static\/vendor)|(deps)/,
        "css/reset.css": "web/static/scss/reset.scss"
      }
    },
    templates: {
      joinTo: "js/app.js"
    }
  },

  conventions: {
    // This option sets where we should place non-css and non-js assets in.
    // By default, we set this to "/web/static/assets". Files in this directory
    // will be copied to `paths.public`, which is "priv/static" by default.
    assets: /^(web\/static\/assets)/
  },

  // Phoenix paths configuration
  paths: {
    // Dependencies and current project directories to watch
    watched: [
      "web/static",
      "test/static"
    ],

    // Where to compile files to
    public: "priv/static"
  },

  // Configure your plugins
  plugins: {
    babel: {
      // Do not use ES6 compiler in vendor code
      ignore: [/web\/static\/vendor/]
    },
    postcss: {
      processors: [
        require("autoprefixer")(["last 8 versions"])
      ]
    }
  },

  npm: {
    enabled: true
  }
};
